package com.spt.app.service.impl;

import com.google.gson.*;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.DocumentAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Set;

@Service("DocumentAccessService")
public class DocumentAccessServiceImpl extends AbstractEngineService implements DocumentAccessService {

//    @Autowired
//    FlowManageService flowManageService;


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
    
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

	@Override
	public Set<String> getDocumentAccessPerson(String docId) {
		// TODO Auto-generated method stub
		String result = getResultString("/documentCustom/getDocumentAccessPerson?doc="+docId);
		return gson.fromJson(result, Set.class);
	}

    

}
