package com.spt.app.spring.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.service.DocumentAccessService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Configurable
public class DocumentAuthorizationInteceptor extends HandlerInterceptorAdapter {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentAuthorizationInteceptor.class);

    @Autowired
    DocumentAccessService documentAccessService;
    
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean flagAccessDenied = false;
        String requestUrl = "";
        try {
        	  String userName = (String) request.getSession(true).getAttribute("userName");
        	LOGGER.info("in DocumentAuthorizationInteceptor documentAccessService={}",documentAccessService);
        	String docId = request.getParameter("doc");
        	if(docId!=null && docId.trim().length() > 0){
        		Set<String> setOfUser = documentAccessService.getDocumentAccessPerson(docId);
        		if(!setOfUser.contains(userName) && !"v".equals(request.getParameter("type"))){
        			response.sendRedirect(request.getContextPath() + "/Doc_Access_Denied");
        			flagAccessDenied = true;
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!flagAccessDenied) {
            //Re render menu by lang
            String langSession = RequestContextUtils.getLocaleResolver(request).resolveLocale(request).toString();
            String menuLang = String.valueOf(request.getSession(true).getAttribute("MENU_LANG"));
            if ("menus".equals(requestUrl)) {
                String shortLang = String.valueOf(langSession.split("_")[0]);
                request.getSession(true).setAttribute("menu", String.valueOf(request.getSession(true).getAttribute("MENU_EDIT_" + shortLang)));
                request.getSession(true).setAttribute("MENU_LANG", langSession);
            } else {
                //if(!menuLang.equals(langSession)){ }
                String shortLang = String.valueOf(langSession.split("_")[0]);
                request.getSession(true).setAttribute("menu", String.valueOf(request.getSession(true).getAttribute("MENU_" + shortLang)));
                request.getSession(true).setAttribute("MENU_LANG", langSession);

            }
        }

        return true;
    }

}
