package com.spt.app.spring.security;
import com.google.common.base.Splitter;
import com.google.gson.*;
import com.spt.app.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@Configuration
public class FixAuthenticationProvider implements AuthenticationProvider {

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


    static final Logger LOGGER = LoggerFactory.getLogger(FixAuthenticationProvider.class);

    RestTemplate restTemplate = new RestTemplate();

    @Value("${OrgSysServer}")
    protected String orgSysServer =""; //http://10.25.68.183:8066/OSX
    
    @Value("${DefaultPassword}")
    protected String defaultPassword=""; 

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("========================== DBAuthenticationProvider ");
        String name = authentication.getName().toLowerCase();;
        String password = authentication.getCredentials().toString();

        if (checkAuthentication(name, password)) {
            LOGGER.info("Fix Credential Success");
            // use the credentials
            // and authenticate against the third-party system
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            LOGGER.error("Fix Credential not found !!");
            return null;
        }
    }


    private boolean checkAuthentication(String userName, String rawPassword) {
        boolean isUserOsx = false;
        if (defaultPassword.equals(rawPassword)) {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Content-Type", "application/json; charset=utf-8");
            headers.add("userName", userName);


	        /* Find User */
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String url = "/profile/" + userName;
            String json = restTemplate.exchange(orgSysServer + url, HttpMethod.GET, entity, String.class).getBody();
            Map userMap = gson.fromJson(json, Map.class);
            String dataProfile = String.valueOf(userMap.get("profile")).replaceAll("Corp.,", "Corp").replaceAll("Co.,", "Co ")
                    .replaceAll("Safety,H","Safety H").replaceAll("Safety , H","Safety H");
            dataProfile = StringUtil.trimStringByString(dataProfile, "{", "}");
            Map<String, String> userProfileMap = Splitter.on(", ").withKeyValueSeparator("=").split(dataProfile);
            String userNameOSX = userProfileMap.get("User_name");

            if (userNameOSX != null && !"NONE".equals(userNameOSX)) {
                isUserOsx = true;
            }

        }

        return isUserOsx;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
