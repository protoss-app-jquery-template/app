package com.spt.app.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("customUserDetailsService")
    UserDetailsService userDetailsService;

    @Autowired
    FixAuthenticationProvider fixAuthenticationProvider;

    @Autowired
    LDAPAuthenticationProvider ldapAuthenticationProvider;

    @Autowired
    DBAuthenticationProvider dbAuthenticationProvider;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        //1.Default Memory Authentication Provider
        auth.inMemoryAuthentication().withUser("system").password("manager").roles("SUPER_ADMIN");

        //Set Sequence of Provider
        //2.Custom Authentication Provider
		auth.authenticationProvider(ldapAuthenticationProvider)
			.authenticationProvider(dbAuthenticationProvider)
			.authenticationProvider(fixAuthenticationProvider);


    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/login/**",
                        "/resources/images/**",
                        "/resources/fonts/**",
                        "/resources/logs/**",
                        "/resources/images/arrow_right.png",
                        "/resources/images/access_denied_2.png",
                        "/qrcodescandata/**",
                        "/qrcodeActionData/**",
                        "/resources/scripts/**",
                        "/resources/styles/**",
                        "/resources/**/scripts/app/qrcode/viewAs.js",
                        "/resources/**/scripts/app/checkAuthenLDAP/checkAuthenEmail.js",
                        "/resources/**/scripts/app/preViewFile/preViewFile.js",
                        "/resources/WEB-INF/views/app/qrcode/**",
                        "/resources/WEB-INF/views/app/previewFile/**",
                        "/resources/WEB-INF/views/app/responseStatus/checkAuthen/**",
                        "/requests/approveRequestRedirect/**",
                        "/requests/sendApproveRequest/**",
                        "/requests/rejectRequestRedirect/**",
                        "/requests/sendRejectRequest/**",
                        "/advance/findByDocNumber/**",
                        "/masterDatas/findUserMember**",
                        "/intermediaries/findAutoCompleteEmployeeByUserName**",
                        "/downloadUserManual/**",
                        "/requests/**",
                        "/resources/styles/font-awesome.min.css",
                        "/lbAccessStatus/**").permitAll()
                .antMatchers("/resources/**").authenticated()
                .antMatchers("/**").access("isAuthenticated()")
                .and().logout().logoutUrl("/logout")
                .and().formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check").usernameParameter("j_username").passwordParameter("j_password").successHandler(getAuthenticationSuccessHandler())
                .and().exceptionHandling().authenticationEntryPoint(getAuthenticationEntryPoint())
                .accessDeniedPage("/Access_Denied")
                .and().sessionManagement()
                .maximumSessions(1000)
                .expiredUrl("/login")
                .maxSessionsPreventsLogin(true)
                .sessionRegistry(sessionRegistry());
        ;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        SessionRegistry sessionRegistry = new SessionRegistryImpl();
        return sessionRegistry;
    }

    @Bean
    public CustomAuthenticationEntryPoint getAuthenticationEntryPoint() {
        CustomAuthenticationEntryPoint authenticationEntryPoint = new CustomAuthenticationEntryPoint();
        authenticationEntryPoint.setLoginPageUrl("/login");
        authenticationEntryPoint.setReturnParameterEnabled(true);
        authenticationEntryPoint.setReturnParameterName("r");

        return authenticationEntryPoint;
    }

    @Bean
    public CustomAuthenticationSuccessHandler getAuthenticationSuccessHandler() {
        CustomAuthenticationSuccessHandler authenticationHandler = new CustomAuthenticationSuccessHandler();
        authenticationHandler.setDefaultTargetUrl("/");
        authenticationHandler.setTargetUrlParameter("spring-security-redirect");

        return authenticationHandler;
    }


}
