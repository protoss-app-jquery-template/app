package com.spt.app.spring.websocket.qrcode;

import com.spt.app.aop.annotation.Loggable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ReadQrcodeController {

	static final Logger LOGGER = LoggerFactory.getLogger(ReadQrcodeController.class);
	

	@Autowired
	private SimpMessagingTemplate messageSender;
	
	@MessageMapping("/qrcodescan")
    @SendTo("/topic/qrcodescan/messages")
    public OutputMessage send(final Message message) throws Exception {

        final String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessage(message.getActionUser(), message.getQrcode(), time);
    }
	
	@GetMapping("/qrcodescandata/{actionUser}/{qrcode}")
	@Loggable
//	@ResponseBody
	public String barcodescan(ModelMap uiModel,@PathVariable String actionUser,@PathVariable String qrcode) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        Message message = new Message();
        message.setActionUser(actionUser);
        message.setQrcode(qrcode);
        messageSender.convertAndSend("/topic/qrcodescan/messages", message);
        uiModel.addAttribute("userName",actionUser);
        uiModel.addAttribute("qrcode",qrcode);
		return "viewAs";
//		return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
	}

	@MessageMapping("/qrcodeAction")
    @SendTo("/topic/qrcodeAction/messages")
    public OutputMessage action(final Message message) throws Exception{
	    final String time = new SimpleDateFormat("HH:mm").format(new Date());
	    return new OutputMessage(message.getActionUser(),message.getQrcode(),time);
    }

    @GetMapping("/qrcodeActionData/{actionUser}/{qrcode}")
    @Loggable
    public ResponseEntity<String> actionQrcode(@PathVariable String actionUser,@PathVariable String qrcode){
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Content-Type","application/json; charset=utf-8");
	    Message message = new Message();
	    message.setActionUser(actionUser);
	    message.setQrcode(qrcode);
	    messageSender.convertAndSend("/topic/qrcodeAction/messages",message);
//	    return "viewAs";
		return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
    }
}
