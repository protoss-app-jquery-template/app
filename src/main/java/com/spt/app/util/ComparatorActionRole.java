package com.spt.app.util;

import java.util.Comparator;
import java.util.Map;

public class ComparatorActionRole implements Comparator<Map>{

	@Override
	public int compare(Map o2, Map o1) {

		Integer i1 = Integer.parseInt(String.valueOf(o1.get("actionRole")));
		Integer i2 = Integer.parseInt(String.valueOf(o2.get("actionRole")));
		
		if (i1 > i2)
            return -1; // highest value first
        if (i1 == i2)
            return 0;
        return 1;
        
	}

}
