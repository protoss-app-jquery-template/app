
var $DATA_EMPLOYEE;
var DATA_PARAMETER_DUEDATE_ADVANCE;
var checkCostCenterBoolean = true;

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-50) + 'px';
    }
}

function saveAdvance() {
    $('.dv-background').show();
    var jsonData = {};
    var jsonDocumentAdvance = {};
    var advancePurpose = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    var dateCheck = $("#startDateEndDate").val();
    var amount = ""==$("#amount").autoNumeric('get')?0:$("#amount").autoNumeric('get');
    var costCenter = $("#costCenter").val();

    var startDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[0]);
    var endDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]);
    var dueDate = new Date(endDate).setDate(new Date(endDate).getDate() + DATA_PARAMETER_DUEDATE_ADVANCE);

    jsonDocumentAdvance['parentId']     	= 1;
    jsonDocumentAdvance[csrfParameter]  	= csrfToken;
    jsonDocumentAdvance['amount']      = amount;
    jsonDocumentAdvance['bankNumber']  = $("#bankNumber").val();
    jsonDocumentAdvance['advancePurpose']  = advancePurpose;
    jsonDocumentAdvance['requiredReference']  = $("#requiredReference").val();
    jsonDocumentAdvance['startDate']  = new Date(startDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['endDate']  = new Date(endDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['dueDate']  = new Date(dueDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['remark']  = $("#remark").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

    jsonData['parentId']     	    = 1;
    jsonData[csrfParameter]  	    = csrfToken;
    jsonData['documentType']        = MASTER_DATA.DOC_TYPE;
    jsonData['documentStatus']      = MASTER_DATA.DOC_STATUS;
    jsonData['requester']           = $("#empNameInputEmployee").attr('data-userName');
    jsonData['companyCode']         = $("#company").attr('pa');
    jsonData['departmentCode']      = $("#empNameInputEmployee").attr('data-deptCode');
    jsonData['personalId']          = $("#empNameInputEmployee").attr('data-personalId');
    jsonData['psa']                 = $("#department").attr('psa');
    jsonData['costCenterCode']      = costCenter;
    jsonData['totalAmount']         = amount;
    jsonData['titleDescription']    = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    jsonData['documentAdvance']     = JSON.stringify(jsonDocumentAdvance);

    setTimeout(function () {
        if($("#outstanding").autoNumeric('get') == 0) {
            if(checkCostCenterBoolean) {
                if ("" != $("#empNameInputEmployee").attr('data-userName') && "" != advancePurpose && "" != dateCheck && 0 != amount && "" != $("#bankNumber").val() && costCenter != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/advance/saveDocument',
                        data: jsonData,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                var documentReference = $("#docRefInputDocRefAppForExpense").val() == "" ? "" : $("#docRefInputDocRefAppForExpense").attr('data-doc');
                                if (documentReference != "") {
                                    var jsonDocumentReference = {};
                                    jsonDocumentReference['parentId'] = 1;
                                    jsonDocumentReference[csrfParameter] = csrfToken;
                                    jsonDocumentReference['docReferenceNumber'] = documentReference;
                                    jsonDocumentReference['documentTypeCode'] = xhr.responseJSON.documentType;
                                    jsonDocumentReference['approveTypeCode'] = "ADV";
                                    jsonDocumentReference['document'] = xhr.responseJSON.id;

                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            Accept: 'application/json'
                                        },
                                        url: session['context'] + '/advance/saveDocumentReference',
                                        data: jsonDocumentReference,
                                        async: false,
                                        complete: function (xhr2) {
                                            if (xhr2.readyState == 4) {
                                                window.location.href = session.context + '/advance/createDoc?doc=' + xhr.responseJSON.id
                                                $('.dv-background').hide();
                                            }
                                        }
                                    });
                                } else {
                                    window.location.href = session.context + '/advance/createDoc?doc=' + xhr.responseJSON.id
                                    $('.dv-background').hide();
                                }
                            }
                        }
                    });
                } else {
                    var messageRequired = "";
                    if ("" == $("#empNameInputEmployee").attr('data-userName')) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_EMPLOYEE_NAME + "<br/>";
                    }
                    if ("" == advancePurpose) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_ADVANCE_PURPOSE + "<br/>";
                    }
                    if ("" == dateCheck) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_START_DATE + "-" + LB_ADVANCE.LABEL_END_DATE + "<br/>"
                    }
                    if (0 == amount) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_AMOUNT_BATH + "<br/>"
                    }
                    if ("" == $("#bankNumber").val()) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_BANK_NUMBER + "<br/>"
                    }
                    if ("" == costCenter) {
                        messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_COST_CENTER + "<br/>"
                    }


                    $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_REQUIRE_FIELD + "<br/>" + messageRequired);
                    $('#warningModal').modal('show');
                    $('.dv-background').hide();
                }
            }else{
                $('#warningCostCenterModal .modal-body').html(MSG_ADVANCE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
                $("#warningCostCenterModal").modal('show');
                $('.dv-background').hide();
            }
        }else{
            $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_HAVE_ADVANCE_OUTSTANDING);
            $('#warningModal').modal('show');
            $('.dv-background').hide();
        }
    },1000);

}

function checkCostCenter() {
    $('.dv-background').show();
    var costCenter = $("#costCenter").val();

    setTimeout(function () {
        if(costCenter != "" && costCenter != $("#empNameInputEmployee").attr('data-costCenter')){

            var data = $.ajax({
                url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(data){

                var costCenterSplit = data.profile.split('#');
                if(costCenterSplit[2] != "0"){
                    checkCostCenterBoolean = true;
                    var errorMessage = MSG_ADVANCE.MESSAGE_COST_CENTER_NOT_AFFILIATED+"<br/>";
                    $('#warningCostCenterModal .modal-body').html(errorMessage);
                    $("#warningCostCenterModal").modal('show');

                    var paPsaSplit = costCenterSplit[2].split('-');
                    var index1 = paPsaSplit.length-7!=0?paPsaSplit.length-7:0;
                    var index2 = paPsaSplit.length-6!=0?paPsaSplit.length-6:0;
                    var index3 = paPsaSplit.length-5!=0?paPsaSplit.length-5:0;
                    var index4 = paPsaSplit.length-4!=0?paPsaSplit.length-4:0;
                    var index5 = paPsaSplit.length-3!=0?paPsaSplit.length-3:0;

                    var pa1 = index1 > 0?paPsaSplit[index1]:"";
                    var pa2 = index2 > 0?paPsaSplit[index2]:"";
                    var pa3 = index3 > 0?paPsaSplit[index3]:"";
                    var pa4 = index4 > 0?paPsaSplit[index4]:"";
                    var pa5 = index5 > 0?paPsaSplit[index5]:"";

                    var text= pa1+pa2+pa3+pa4+pa5;

                    $("#company").text(paPsaSplit[0] + " : " + text);
                    $("#company").attr('pa', paPsaSplit[0]);
                    // $("#department").text(paPsaSplit[paPsaSplit.length -2] + " : " + paPsaSplit[paPsaSplit.length -1]);
                    // $("#department").attr('psa', paPsaSplit[paPsaSplit.length -2]);

                    if($DATA_ADVANCE){
                        getAuthorizeForLineApprove($DATA_ADVANCE.requester,$DATA_ADVANCE.documentAdvance.amount,function () {
                            $("#amount").blur();
                            $('.dv-background').hide();
                        });
                    }else{
                        getAuthorizeForLineApprove(session.userName,100,function () {
                            $('.dv-background').hide();
                        });
                    }
                }else{
                    $("#company").text("-");
                    $("#company").attr('pa',"");
                    // $("#department").text("-");
                    // $("#department").attr('psa',"");

                    $('#warningCostCenterModal .modal-body').html(MSG_ADVANCE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
                    $("#warningCostCenterModal").modal('show');
                    checkCostCenterBoolean = false;
                    $('.dv-background').hide();
                }

            }

        }else{
            if(costCenter == ""){
                $("#company").text("-");
                $("#company").attr('pa',"");
                // $("#department").text("-");
                // $("#department").attr('psa',"");
                checkCostCenterBoolean = false;
                $('.dv-background').hide();
            }

            if(costCenter == $("#empNameInputEmployee").attr('data-costCenter')){
                checkCostCenterBoolean = true;

                var data = $.ajax({
                    url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
                    headers: {
                        Accept : "application/json"
                    },
                    type: "GET",
                    async: false
                }).responseJSON;

                if(data){

                    var costCenterSplit = data.profile.split('#');
                    if(costCenterSplit[2] != "0"){
                        var paPsaSplit = costCenterSplit[2].split('-');
                        var index1 = paPsaSplit.length-7!=0?paPsaSplit.length-7:0;
                        var index2 = paPsaSplit.length-6!=0?paPsaSplit.length-6:0;
                        var index3 = paPsaSplit.length-5!=0?paPsaSplit.length-5:0;
                        var index4 = paPsaSplit.length-4!=0?paPsaSplit.length-4:0;
                        var index5 = paPsaSplit.length-3!=0?paPsaSplit.length-3:0;

                        var pa1 = index1 > 0?paPsaSplit[index1]:"";
                        var pa2 = index2 > 0?paPsaSplit[index2]:"";
                        var pa3 = index3 > 0?paPsaSplit[index3]:"";
                        var pa4 = index4 > 0?paPsaSplit[index4]:"";
                        var pa5 = index5 > 0?paPsaSplit[index5]:"";

                        var text= pa1+pa2+pa3+pa4+pa5;

                        $("#company").text(paPsaSplit[0] + " : " + text);
                        $("#company").attr('pa', paPsaSplit[0]);
                        // $("#department").text(paPsaSplit[paPsaSplit.length -2] + " : " + paPsaSplit[paPsaSplit.length -1]);
                        // $("#department").attr('psa', paPsaSplit[paPsaSplit.length -2]);

                        if($DATA_ADVANCE){
                            getAuthorizeForLineApprove($DATA_ADVANCE.requester,$DATA_ADVANCE.documentAdvance.amount,function () {
                                $("#amount").blur();
                                $('.dv-background').hide();
                            });
                        }else{
                            getAuthorizeForLineApprove(session.userName,100,function () {
                                $('.dv-background').hide();
                            });
                        }
                    }else{
                        $("#company").text("-");
                        $("#company").attr('pa',"");
                        // $("#department").text("-");
                        // $("#department").attr('psa',"");

                        $('#warningCostCenterModal .modal-body').html(MSG_ADVANCE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
                        $("#warningCostCenterModal").modal('show');
                        checkCostCenterBoolean = false;
                        $('.dv-background').hide();
                    }

                }

                if($("#company").attr('pa') != "" && $("#department").attr('psa') != ""){
                    var papsa = $("#company").attr('pa')+"-"+$("#department").attr('psa');
                    var personalId = $("#empCode").text();

                    var data = $.ajax({
                        url: session.context + "/intermediaries/findAccountByPapsa/"+papsa+"/"+personalId,
                        headers: {
                            Accept : "application/json"
                        },
                        type: "GET",
                        async: false
                    }).responseJSON;

                    if(data) {
                        var dataAccountSplit = data.profile.split(':');
                        if (dataAccountSplit[1] != "0") {
                            var accountName = dataAccountSplit[1].split("-");

                            var data = $.ajax({
                                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + accountName[0],
                                headers: {
                                    Accept: "application/json"
                                },
                                type: "GET",
                                async: false
                            }).responseJSON;

                            if (data) {

                                if ($("#accountName").text() == "") {
                                    $("#accountName").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);
                                }
                            }
                        }
                    }
                }

            }
        }
    },1000);

}

function initAdvance() {
    if($DATA_ADVANCE){
        findEmployeeProfileByUserName($DATA_ADVANCE.requester);
        findCreatorProfileByUserName($DATA_ADVANCE.createdBy);
        $("#empNameInputEmployee").val($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        getParameterDueDateFromPACode($DATA_ADVANCE.companyCode);
        
        setTimeout(function () {
            validateDocStatus();
            AutocompleteDocRefAppForExpense.init("docRefInputDocRefAppForExpense",$DATA_ADVANCE.requester);
            AutocompleteEmployee.setId('empNameInputEmployee');
            AutocompleteEmployee.search("empNameInputEmployee",$DATA_ADVANCE.requester);
            AutocompleteEmployee.renderValue($DATA_EMPLOYEE.User_name.toLocaleLowerCase());
            $("#empNameInputEmployee").blur();
            $("#department").text($("#empNameInputEmployee").attr("data-psa") + " : " + $("#empNameInputEmployee").attr("data-psaname"));
            $("#department").attr('psa', $("#empNameInputEmployee").attr("data-psa"));
        },1000);

        if($DATA_ADVANCE.documentReference.length != 0){
            setTimeout(function () {
                AutocompleteDocRefAppForExpense.renderValue($DATA_ADVANCE.documentReference[0].docReferenceNumber,"docRefInputDocRefAppForExpense");
            },1000);
        }
        $("#divDocNumber").removeClass('hide');
        $("#documentNo").text($DATA_ADVANCE.docNumber);

        if($DATA_ADVANCE.documentAdvance != null){
            $("#advancePurpose").val($DATA_ADVANCE.documentAdvance.advancePurpose);
            if( $("#advancePurpose").val() == null){
                $("#advancePurpose").val('other')
                $("#otherDiv").removeClass('hide');
                $("#advancePurposeOther").val($DATA_ADVANCE.documentAdvance.advancePurpose)
            }
            $("#amount").autoNumeric('set',$DATA_ADVANCE.documentAdvance.amount);
            $("#startDateEndDate").val(new Date($DATA_ADVANCE.documentAdvance.startDate).format('dd/mm/yyyy') +" to "+ new Date($DATA_ADVANCE.documentAdvance.endDate).format('dd/mm/yyyy'));
            $("#dueDate").text(new Date($DATA_ADVANCE.documentAdvance.dueDate).format('dd/mm/yyyy'));
            $("#bankNumber").val($DATA_ADVANCE.documentAdvance.bankNumber);
            $("#remark").val($DATA_ADVANCE.documentAdvance.remark);

        }

        $("[name=btnAction2]").removeClass('hide');
        $("[name=btnAction1]").addClass('hide');

    }else{
        findEmployeeProfileByUserName(session.userName);
        $("#empNameInputEmployee").val($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        setTimeout(function () {
            AutocompleteDocRefAppForExpense.init("docRefInputDocRefAppForExpense",session.userName);
            AutocompleteEmployee.setId('empNameInputEmployee');
            AutocompleteEmployee.search("empNameInputEmployee",$USERNAME);
            AutocompleteEmployee.renderValue($DATA_EMPLOYEE.User_name.toLocaleLowerCase());
            $("#empNameInputEmployee").blur();
            getParameterDueDateFromPACode($("#empNameInputEmployee").attr('data-compCode'))
            defaultBankNumber($("#empNameInputEmployee").attr("data-username"));
            $("#department").text($("#empNameInputEmployee").attr("data-psa") + " : " + $("#empNameInputEmployee").attr("data-psaname"));
            $("#department").attr('psa', $("#empNameInputEmployee").attr("data-psa"));
        },1000);
        findCreatorProfileByUserName(session.userName);

    }

    $("#docRefInputDocRefAppForExpense").on('blur',function (){
        $("#docRefBtn").attr('data-attn',$("#docRefInputDocRefAppForExpense").attr('data-doc'));
    });
}

function getOutStandingBalance(callback) {
    var dataAdvanceOutstandings = $.ajax({
        url: session.context + "/advanceOutstandings/findAdvanceOutstanding?venderNo="+$("#empNameInputEmployee").attr('data-personalid')+"&userName="+$("#empNameInputEmployee").attr('data-username'),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataAdvanceOutstandings){
        if(dataAdvanceOutstandings.amount > 0){
            $("#outstanding").autoNumeric('set',dataAdvanceOutstandings.amount);
            $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_HAVE_ADVANCE_OUTSTANDING);
            $('#warningModal').modal('show');
        }
        callback();
    }else{
        $("#outstanding").autoNumeric('set',0);
        callback();
    }
}

function defaultBankNumber(userName) {
    var dataEmployee = $.ajax({
        url: session.context + "/intermediaries/getBankAccount/"+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataEmployee && (dataEmployee.profile != null || dataEmployee.profile != "")){
        $("#bankNumber").val(dataEmployee.profile);
    }
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;
}

function findCreatorProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#createBy").text(data.FOA+data.FNameTH+" "+data.LNameTH);
}

function documentAttachment(){
    if($DATA_ADVANCE != ""){
        getDataAttachmentType();
        findDocumentAttachmentByDocumentId($DATA_ADVANCE.id);
        $("#modalDocumentAttachment").modal('show');
        $("#attachmentType").val("");
        $("#textFileName").val("");
    }else{
        $('#warningModal .modal-body').html("Document is not save");
        $('#warningModal').modal('show');
    }

}

function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/advance/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            var $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItem(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button>' +
                '</td>' +
                '</tr>'
            );
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}

function saveDocumentAttachment() {
    var jsonData = {}
    jsonData['parentId'] = 1;
    jsonData[csrfParameter] = csrfToken;
    jsonData['attachmentType'] = $("#attachmentType").val();
    jsonData['document'] = $DATA_ADVANCE.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename", $fileUpload.name);
    formData.append("attachmentType", $("#attachmentType").val());
    formData.append("document", $DATA_ADVANCE.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/advance/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    $("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    $("#textFileName").val("");
                    findDocumentAttachmentByDocumentId($DATA_ADVANCE.id);
                    renderDocumentAttachment(dataDocumentAttachment);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function deleteDocumentAttachment(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/advance/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findDocumentAttachmentByDocumentId($DATA_ADVANCE.id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

function downloadDocumentFile(btn){
    var id = btn.attr('id');
    var fileName = btn.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}

function validateFileExtensions(){
    var extensionFile = $('#attachmentType option:selected').attr('extension');

    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG_ADVANCE.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

function validateDocStatus(){
    if($DATA_ADVANCE.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if($DATA_ADVANCE.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
        $("#btnCancel").addClass('hide');
        $("#btnFile").css('margin-top','80px');
        $("#btnEmail").css('margin-top','160px');
        $("#btnCopy").css('margin-top','240px');
    }if($DATA_ADVANCE.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
        $("#btnSendRequest").addClass('hide')
    }if($DATA_ADVANCE.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if($DATA_ADVANCE.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }
}

function getAuthorizeForLineApprove(userName,amount,callback){


    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = amount;
    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
    jsonDetails['costCenter']  = $("#costCenter").val();

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['requester']   = userName;
    jsonData['approveType'] = "";
    jsonData['papsa'] = $("#company").attr('pa')+"-"+$("#department").attr('psa');
    jsonData['details'] = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/advance/getAuthorizeForLineApprove',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                renderLineApprove(xhr.responseJSON);
                callback();
            }
        }
    });
}

function getAuthorizeForLineApproveForInit(userName){

    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = 100;
    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
    jsonDetails['costCenter']  = $("#costCenter").val();

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['requester'] = userName;
    jsonData['approveType'] = "";
    jsonData['papsa'] = "";
    jsonData['details'] = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/advance/getAuthorizeForLineApprove',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                renderLineApprove(xhr.responseJSON);
            }
        }
    });
}

function renderLineApprove(requestApprover){
    $("#lineApproverDetail").empty();
    $("#lineApproveMobile").empty();
    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;

    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB_ADVANCE.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+LB_ADVANCE.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
            '<span class="step"><span><img src='+validateIMG(requestApprover[i].actionRoleCode)+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionRoleName+'</div>'+
            '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
            '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src='+validateIMG(requestApprover[i].actionRoleCode)+' width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionRoleName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].name+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].position+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    }

}

function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
}

function copyDocument(){
    $('.dv-background').show();
    var jsonDocument = {};
    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']   			= $DATA_ADVANCE.id;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/advance/copyDocument',
            data:jsonDocument,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    window.location.href = session.context+'/advance/createDoc?doc='+xhr.responseJSON.id;
                }
                $('.dv-background').hide();
            }
        });
    },1000);
}

function updateAdvance() {
    $('.dv-background').show();
    var jsonData = {};
    var jsonDocumentAdvance = {};
    var advancePurpose = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    var dateCheck = $("#startDateEndDate").val();
    var amount = ""==$("#amount").autoNumeric('get')?0:$("#amount").autoNumeric('get');

    var startDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[0]);
    var endDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]);
    var dueDate = new Date(endDate).setDate(new Date(endDate).getDate() + 15);

    jsonDocumentAdvance['parentId']     	= 1;
    jsonDocumentAdvance[csrfParameter]  	= csrfToken;
    jsonDocumentAdvance['amount']      = amount
    jsonDocumentAdvance['bankNumber']  = $("#bankNumber").val();
    jsonDocumentAdvance['advancePurpose']  = advancePurpose;
    jsonDocumentAdvance['requiredReference']  = $("#requiredReference").val();
    jsonDocumentAdvance['startDate']  = new Date(startDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['endDate']  = new Date(endDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['dueDate']  = new Date(dueDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['remark']  = $("#remark").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']  	= $DATA_ADVANCE.id;
    jsonData['documentType']    = MASTER_DATA.DOC_TYPE;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS;
    jsonData['requester']       = $("#empNameInputEmployee").attr('data-userName');
    jsonData['companyCode']         = $("#company").attr('pa');
    jsonData['departmentCode']      = $("#empNameInputEmployee").attr('data-deptCode');
    jsonData['personalId']          = $("#empNameInputEmployee").attr('data-personalId');
    jsonData['psa']                 = $("#department").attr('psa');
    jsonData['costCenterCode']      = $("#costCenter").val();
    jsonData['titleDescription']      = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    jsonData['documentAdvance'] = JSON.stringify(jsonDocumentAdvance);

    setTimeout(function () {
        if(checkCostCenterBoolean) {
            if ("" != $("#empNameInputEmployee").attr('data-userName') && "" != advancePurpose && "" != dateCheck && 0 != amount && "" != $("#bankNumber").val()) {
                $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/advance/updateDocumentAdvance',
                    data: jsonData,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            var documentReference = $("#docRefInputDocRefAppForExpense").val() == "" ? "" : $("#docRefInputDocRefAppForExpense").attr('data-doc');
                            if (documentReference != "") {
                                var jsonDocumentReference = {};
                                jsonDocumentReference['parentId'] = 1;
                                jsonDocumentReference[csrfParameter] = csrfToken;
                                jsonDocumentReference['docReferenceNumber'] = documentReference;
                                jsonDocumentReference['documentTypeCode'] = xhr.responseJSON.documentType;
                                jsonDocumentReference['approveTypeCode'] = "ADV";
                                jsonDocumentReference['document'] = xhr.responseJSON.id;

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/advance/updateDocumentReference',
                                    data: jsonDocumentReference,
                                    async: false,
                                    complete: function (xhr2) {
                                        if (xhr2.readyState == 4) {
                                            $("#modalbodySuccess").text('Update Success');
                                            $("#successModal").modal('show');
                                            $('.dv-background').hide();
                                        }
                                    }
                                });
                            } else {
                                $("#modalbodySuccess").text('Update Success');
                                $("#successModal").modal('show');
                                $('.dv-background').hide();
                            }

                            setTimeout(function () {
                                $("#successModal").modal('hide');
                                $("#modalAddDetail").modal('hide')
                            }, 2000)
                        }
                    }
                });
            } else {
                var messageRequired = "";
                if ("" == $("#empNameInputEmployee").attr('data-userName')) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_EMPLOYEE_NAME + "<br/>";
                }
                if ("" == advancePurpose) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_ADVANCE_PURPOSE + "<br/>";
                }
                if ("" == dateCheck) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_START_DATE + "-" + LB_ADVANCE.LABEL_END_DATE + "<br/>"
                }
                if (0 == amount) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_AMOUNT_BATH + "<br/>"
                }
                if ("" == $("#bankNumber").val()) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_BANK_NUMBER + "<br/>"
                }

                $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_REQUIRE_FIELD + "<br/>" + messageRequired);
                $('#warningModal').modal('show');
                $('.dv-background').hide();
            }
        }else{
            $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
            $('#warningModal').modal('show');
            $('.dv-background').hide();
        }
    },1000);

}

function sendRequest() {
    $('.dv-background').show();
    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = $("#amount").autoNumeric('get');
    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
    jsonDetails['costCenter'] = $("#costCenter").val();
    listJsonDetails.push(jsonDetails);

    var jsonSendRequestData = {};
    jsonSendRequestData['id'] = $DATA_ADVANCE.id;
    jsonSendRequestData['requester'] = $DATA_ADVANCE.requester;
    jsonSendRequestData['approveType'] = $DATA_ADVANCE.approveType;
    jsonSendRequestData['company'] = 1;
    jsonSendRequestData['documentType'] = $DATA_ADVANCE.documentType;
    jsonSendRequestData['tmpDocNumber'] = $DATA_ADVANCE.tmpDocNumber;
    jsonSendRequestData['details'] = JSON.stringify(listJsonDetails);
    jsonSendRequestData['pa'] = $("#company").attr('pa');
    jsonSendRequestData['psa'] = $("#department").attr('psa');

    var jsonData = {};
    var jsonDocumentAdvance = {};
    var advancePurpose = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    var dateCheck = $("#startDateEndDate").val();
    var amount = ""==$("#amount").autoNumeric('get')?0:$("#amount").autoNumeric('get');

    var startDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[0]);
    var endDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]);
    var dueDate = new Date(endDate).setDate(new Date(endDate).getDate() + 15);

    jsonDocumentAdvance['parentId']     	= 1;
    jsonDocumentAdvance[csrfParameter]  	= csrfToken;
    jsonDocumentAdvance['amount']      = amount
    jsonDocumentAdvance['bankNumber']  = $("#bankNumber").val();
    jsonDocumentAdvance['advancePurpose']  = advancePurpose;
    jsonDocumentAdvance['requiredReference']  = $("#requiredReference").val();
    jsonDocumentAdvance['startDate']  = new Date(startDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['endDate']  = new Date(endDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['dueDate']  = new Date(dueDate).format('yyyy-mm-dd HH:MM:ss');
    jsonDocumentAdvance['remark']  = $("#remark").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']  	= $DATA_ADVANCE.id;
    jsonData['documentType']    = MASTER_DATA.DOC_TYPE;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS;
    jsonData['requester']       = $("#empNameInputEmployee").attr('data-userName');
    jsonData['companyCode']         = $("#company").attr('pa');
    jsonData['departmentCode']      = $("#empNameInputEmployee").attr('data-deptCode');
    jsonData['personalId']          = $("#empNameInputEmployee").attr('data-personalId');
    jsonData['psa']                 = $("#department").attr('psa');
    jsonData['costCenterCode']      = $("#costCenter").val();
    jsonData['titleDescription']      = $("#advancePurpose")[0].selectedOptions[0].value=="other"?$("#advancePurposeOther").val():$("#advancePurpose")[0].selectedOptions[0].value;
    jsonData['documentAdvance'] = JSON.stringify(jsonDocumentAdvance);

    setTimeout(function () {
        if(checkCostCenterBoolean) {
            if ("" != $("#empNameInputEmployee").attr('data-userName') && "" != advancePurpose && "" != dateCheck && 0 != amount && "" != $("#bankNumber").val()) {
                $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/advance/updateDocumentAdvance',
                    data: jsonData,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            var documentReference = $("#docRefInputDocRefAppForExpense").val() == "" ? "" : $("#docRefInputDocRefAppForExpense").attr('data-doc');
                            if (documentReference != "") {
                                var jsonDocumentReference = {};
                                jsonDocumentReference['parentId'] = 1;
                                jsonDocumentReference[csrfParameter] = csrfToken;
                                jsonDocumentReference['docReferenceNumber'] = documentReference;
                                jsonDocumentReference['documentTypeCode'] = xhr.responseJSON.documentType;
                                jsonDocumentReference['approveTypeCode'] = "ADV";
                                jsonDocumentReference['document'] = xhr.responseJSON.id;

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/advance/updateDocumentReference',
                                    data: jsonDocumentReference,
                                    async: false,
                                    complete: function (xhr2) {
                                        if (xhr2.readyState == 4) {
                                            $.ajax({
                                                type: "POST",
                                                headers: {
                                                    Accept: 'application/json'
                                                },
                                                url: session['context'] + '/requests/createRequest',
                                                data: jsonSendRequestData,
                                                async: false,
                                                complete: function (xhr) {
                                                    if (xhr.readyState == 4) {
                                                        window.location.href = session.context + "/expense/expenseMainMenu";
                                                        $('.dv-background').hide();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/requests/createRequest',
                                    data: jsonSendRequestData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            window.location.href = session.context + "/expense/expenseMainMenu";
                                            $('.dv-background').hide();
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            } else {
                var messageRequired = "";
                if ("" == $("#empNameInputEmployee").attr('data-userName')) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_EMPLOYEE_NAME + "<br/>";
                }
                if ("" == advancePurpose) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_ADVANCE_PURPOSE + "<br/>";
                }
                if ("" == dateCheck) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_START_DATE + "-" + LB_ADVANCE.LABEL_END_DATE + "<br/>"
                }
                if (0 == amount) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_AMOUNT_BATH + "<br/>"
                }
                if ("" == $("#bankNumber").val()) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_ADVANCE.LABEL_BANK_NUMBER + "<br/>"
                }

                $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_REQUIRE_FIELD + "<br/>" + messageRequired);
                $('#warningModal').modal('show');
                $('.dv-background').hide();
            }
        }else{
            $('#warningModal .modal-body').html(MSG_ADVANCE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
            $('#warningModal').modal('show');
            $('.dv-background').hide();
        }
    },1000);

}

function cancelAdvance() {
    var warningMessage = MSG_ADVANCE.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_ADVANCE.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelDocument(){
    $('.dv-background').show();
    if($DATA_ADVANCE != ""){
        var jsonData = {};
        jsonData['parentId']     	= 1;
        jsonData[csrfParameter]  	= csrfToken;
        jsonData['id']    			= $DATA_ADVANCE.id;
        jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

        setTimeout(function () {
            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/advance/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('#confirmModal').modal('hide');
                        $("#completeModal").modal('show');
                        $('.dv-background').hide();
                        backToMenu();
                        // window.location.href = session.context+"/advance/createDoc?doc="+$DATA_ADVANCE.id;
                        // validateDocStatus(xhr.responseJSON.documentStatus);
                    }
                }
            });
        },1000);

    }else{
        $('#warningModal .modal-body').html("Document is not save");
        $('#warningModal').modal('show');
        $('.dv-background').hide();
    }
}

$(document).ready(function () {
    $('.dv-background').show();
    moveFixedActionButton();
    readDataZip(function () {
        readMasterDataDocType();
        readMasterDataDocStatus();
        readMasterDataFlowTypeAdvance();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataDocStatusLost();
        readMasterDataAttachmentType();
        $("#outstanding").autoNumeric('init',{vMin:0});
        $("#amount").autoNumeric('init',{vMin:0});
        initAdvance();
    });

    $("#startDateEndDate").on('change',function () {
        var value = $("#startDateEndDate").val();
        if(value.indexOf("to") !== -1){
            var endDate = new Date(DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]));
            var dueDate = new Date(endDate.setDate(endDate.getDate() + DATA_PARAMETER_DUEDATE_ADVANCE));
            $("#dueDate").text(dueDate.format('dd/mm/yyyy'))
        }else{
            $("#dueDate").text("-");
        }

    });

    $("#advancePurpose").on('change',function () {
        if($("#advancePurpose option:selected").val() == "other"){
            $("#otherDiv").removeClass('hide');
        }else{
            $("#otherDiv").addClass('hide');
        }
    });

    $("#empNameInputEmployee").on('keyup',function(){
        if($("#empNameInputEmployee").val() != ""){
            AutocompleteEmployee.setId('empNameInputEmployee');
            AutocompleteEmployee.search("empNameInputEmployee",$("#empNameInputEmployee").val());
        }
    });

    $("#empNameInputEmployee").on('blur',function (){
        $("#empCode").text($("#empNameInputEmployee").attr('data-personalid'));
        $("#position").text($("#empNameInputEmployee").attr('data-position'));
        // $("#company").text($("#empNameInputEmployee").attr('data-compCode')+" : "+$("#empNameInputEmployee").attr('data-compName'));
        // $("#department").text($("#empNameInputEmployee").attr('data-deptName'));
        if($DATA_ADVANCE){
            $("#costCenter").val($DATA_ADVANCE.costCenterCode);
        }else{
            $("#costCenter").val($("#empNameInputEmployee").attr('data-costCenter'));
        }
        getOutStandingBalance(function () {
            checkCostCenter();
        });

    });

    // init
    $("#startDateEndDate").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#imageCalendar").on('click',function () {
        $("#startDateEndDate").focus()
    });

    $("#browseFile").on('click',function(){
        if($("#attachmentType").val() == ""){
            var errorMessage = MSG_ADVANCE.MESSAGE_REQUIRE_FIELD +" "+LB_ADVANCE.LABEL_ATTACHMENT_TYPE+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }else{
            var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
            $("#uploadFileDocument").attr('accept',extension);
            $("#uploadFileDocument").click();
        }
    });

    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
        // validateFileExtensions();
    });

    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });

    $("#confirmDelete").on('click',function (){
        deleteDocumentAttachment($("#idItemDelete").val());
    });


    $("#amount").on('blur',function () {
        $('.dv-background').show();
        var amount = ""==$("#amount").autoNumeric('get')?1:$("#amount").autoNumeric('get');
        var userName = ""==$("#empNameInputEmployee").attr('data-userName')?session.userName:$("#empNameInputEmployee").attr('data-userName');
        // $("#outstanding").autoNumeric('set',""==$("#amount").autoNumeric('get')?0:$("#amount").autoNumeric('get'));
        setTimeout(function () {
            getAuthorizeForLineApprove(userName,amount,function () {
                $('.dv-background').hide();
            })
        },1000);

    });

    $("#confirmCancel").on('click',function(){
        cancelDocument();
    });

    $("#costCenter").on('blur',function () {
        checkCostCenter();
    })
});

function getParameterDueDateFromPACode(paCode){
    var parameter = $.ajax({
        url: session.context + "/intermediaries/findParameterDueDateAdvance",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterDetailDueDateAdvance/"+parameter.id,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    for(var i=0;i<data.content.length;i++){
       if(data.content[i].code == paCode){
            DATA_PARAMETER_DUEDATE_ADVANCE =  parseInt(data.content[i].variable1);
            break;
        }
    }

}


function backToMenu(){

    window.location.href = session.context+'/expense/expenseMainMenu';

}

function preViewAttachmentFileExpenseItem(id,fileName) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(fileType == 'pdf' || fileType == 'PDF' || fileType =='txt' || fileType == 'TXT'){
        var url = session['context']+'/approve/preViewPDFDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 20;
        var positionY = 20;
        var width = 1000;
        var height = 800;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        window.open(url, '', properties);
    }

    if(fileType == 'png' || fileType == 'PNG' || fileType =='jpg' || fileType == 'JPG' || fileType == 'jpeg'  || fileType == 'JPEG'){


        var urlIMG = session['context']+'/approve/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 20;
        var positionY = 20;
        var width = 1000;
        var height = 800;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        window.open(urlIMG, '', properties);
    }



}