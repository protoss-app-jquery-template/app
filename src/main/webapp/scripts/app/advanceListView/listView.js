var $DATA_REQUEST;
var $DATA_REQUEST_APPROVER;
var $DATA_EMPLOYEE_FOR_LINE_APPROVE;
var checkStatus=0
var checkStatusReject = 0;
$(document).ready(function () {
    moveFixedActionButton();
    $('.dv-background').show();
    readDataZip(function () {
        readMasterDataDocType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataFlowTypeAdvance();
        readMasterDataAttachmentType();
        readMasterDataActionReason();
        readMasterDataRequestStatus();

        $("#amount").autoNumeric('init');
        $("#amountMobile").autoNumeric('init');
        $("#outstanding").autoNumeric('init');

        findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
        findCreatorProfileByUserName($DATA_DOCUMENT.createdBy);
        renderDetail();

        if($DATA_DOCUMENT.requests){
            findRequestByDocument($DATA_DOCUMENT.id);
        }else{
            getAuthorizeForLineApprove($DATA_DOCUMENT.requester,$DATA_DOCUMENT.totalAmount);
        }

        if($DATA_DOCUMENT.costCenterCode){
            $("#costCenter").text($DATA_DOCUMENT.costCenterCode);
            $("#costCenterMobile").text($DATA_DOCUMENT.costCenterCode);
        }

        if($DATA_DOCUMENT.companyCode){
            findByPaCode($DATA_DOCUMENT.companyCode);
        }

        if($DATA_DOCUMENT.psa){
            findByPsaCode($DATA_DOCUMENT.psa);
        }

        if($USERNAME!= $DATA_DOCUMENT.createdBy){
            if(session.roleName.indexOf("ROLE_ACCOUNT") != -1){
                if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
                    console.log("account complete")
                    $("[name=accUnlock]").removeClass('hide');
                    $("[name=acc]").addClass('hide');
                    $("[name=in]").addClass('hide');
                    $("[name=out]").addClass('hide');
                }else{
                    console.log("account not complete");
                    $("[name=acc]").addClass('hide');
                    $("[name=in]").addClass('hide');
                    $("[name=out]").removeClass('hide');
                }
            }else{
                $("[name=in]").removeClass('hide');
                $("[name=out]").addClass('hide');
                $("[name=acc]").addClass('hide');
            }

        }else{
            $("[name=in]").addClass('hide');
            $("[name=out]").removeClass('hide');
            $("[name=acc]").addClass('hide');
        }

        validateDocStatus();

        setTimeout(function (){
            $('.dv-background').hide();
        },1000);
    });



    $("#confirmCancel").on('click',function(){
        cancelDocument();
    });

    $("#confirmReject").on('click',function(){
        rejectRequest();
    });

    $("#browseFile").on('click',function(){
        if($("#attachmentType").val() == ""){
            var errorMessage = MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ATTACHMENT_TYPE+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }else{
            var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
            $("#uploadFileDocument").attr('accept',extension);
            $("#uploadFileDocument").click();
        }
    });

    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
        // validateFileExtensions();
    });

    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });

    $("#confirmDelete").on('click',function (){
        deleteDocumentAttachment($("#idItemDelete").val());
    });

    $("#btnQR1,#btnQR2,#btnQR3,#btnQRMobile1,#btnQRMobile2,#btnQR4,#btnQRMobile3").on('click',function () {
        let user_account;

        $DATA_REQUEST_APPROVER.forEach(function (item) {
            if(item.actionState.indexOf("ACC") >= 0){
                user_account = item.userNameApprover;
            }
        })
        window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
    });
});

function getAuthorizeForLineApprove(userName,amount){


    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = amount;
    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
    jsonDetails['costCenter']  = $DATA_DOCUMENT.costCenterCode;

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['requester']   = userName;
    jsonData['approveType'] = "";
    jsonData['papsa'] = $DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa;
    jsonData['details'] = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/advance/getAuthorizeForLineApprove',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                renderLineApproveAcc(xhr.responseJSON);
            }
        }
    });
}

function unlockAdvance() {
    $('.dv-background').show();
    var jsonDocument = {};
    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']    =  $DATA_DOCUMENT.id;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/advance/unlockAdvance',
            data:jsonDocument,
            async: false,
            complete: function (xhr2) {
                if (xhr2.readyState == 4) {
                    if(xhr2.responseJSON == "Success"){
                        window.location.href = session.context;
                    }else{
                        $('.dv-background').hide();
                        $('#warningModal .modal-body').html(xhr2.responseJSON);
                        $("#warningModal").modal('show');
                    }
                }
            }
        });
    },1000);
}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#company").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#companyMobile").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#company").attr('pa',paData.PaCode);
    }else{
        $("#company").text("-");
        $("#companyMobile").text("-");
        $("#company").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#departmentMobile").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#department").attr('psa',psaData.PsaCode);
    }else{
        $("#department").text("-");
        $("#departmentMobile").text("-");
        $("#department").attr('psa',"");
    }

    if($("#company").attr('pa') != "" && $("#department").attr('psa') != ""){
        var papsa = $("#company").attr('pa')+'-'+$("#department").attr('psa');

        var data = $.ajax({
            url: session.context + "/intermediaries/findAccountByPapsa/"+papsa+"/"+$DATA_DOCUMENT.personalId,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        console.info("><><>< DATA ACCOUNT BY PAPSA");
        console.info(data);
        if(data) {
            var dataAccountSplit = data.profile.split(':');
            if (dataAccountSplit[1] != "0") {
                var accountName = dataAccountSplit[1].split("-");

                var data = $.ajax({
                    url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + accountName[0],
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false
                }).responseJSON;

                if (data) {

                    $("#accountName").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);
                    $("#accountNameMobile").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);


                }
            }
        }
    }
}

function deleteDocumentAttachment(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/advance/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

function validateFileExtensions(){
    var extensionFile = $('#attachmentType option:selected').attr('extension');

    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

function saveDocumentAttachment() {
    var jsonData = {}
    jsonData['parentId'] = 1;
    jsonData[csrfParameter] = csrfToken;
    jsonData['attachmentType'] = $("#attachmentType").val();
    jsonData['document'] = $DATA_DOCUMENT.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename", $fileUpload.name);
    formData.append("attachmentType", $("#attachmentType").val());
    formData.append("document", $DATA_DOCUMENT.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/advance/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    $("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    $("#textFileName").val("");
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                    renderDocumentAttachment(dataDocumentAttachment);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/advance/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            var $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItem(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '</tr>'
            );
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}

function downloadDocumentFile(btn){
    var id = btn.attr('id');
    var fileName = btn.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}

function documentAttachment(){
    getDataAttachmentType();
    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}

function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function cancelAdvance() {
    var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelDocument(){
    var jsonData = {}
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']    			= $DATA_DOCUMENT.id;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/advance/updateDocumentStatus',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $('#confirmModal').modal('hide');
                $("#completeModal").modal('show');
                window.location.href = session.context+"/advance/advanceDetail?doc="+$DATA_DOCUMENT.id;
            }
        }
    });
}

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-50) + 'px';
    }
}

function renderDetail() {
    $("#docNumber").text($DATA_DOCUMENT.docNumber!=null?$DATA_DOCUMENT.docNumber:"-");
    $("#docDate").text($DATA_DOCUMENT.createdDate!=null?new Date($DATA_DOCUMENT.createdDate).format('dd/mm/yyyy HH:MM'):"-");
    $("#docNumberMobile").text($DATA_DOCUMENT.docNumber!=null?$DATA_DOCUMENT.docNumber:"-");
    $("#docDateMobile").text($DATA_DOCUMENT.createdDate!=null?new Date($DATA_DOCUMENT.createdDate).format('dd/mm/yyyy HH:MM'):"-");

    $("#advancePurpose").text($DATA_DOCUMENT.documentAdvance.advancePurpose);
    $("#requiredReference").text($DATA_DOCUMENT.documentReference.length!=0?$DATA_DOCUMENT.documentReference[0].docReferenceNumber:"-");
    $("#amount").autoNumeric('set',$DATA_DOCUMENT.documentAdvance.amount);
    $("#startDateEndDate").text(new Date($DATA_DOCUMENT.documentAdvance.startDate).format('dd/mm/yyyy') +" to "+ new Date($DATA_DOCUMENT.documentAdvance.endDate).format('dd/mm/yyyy'));
    $("#dueDate").text(new Date($DATA_DOCUMENT.documentAdvance.dueDate).format('dd/mm/yyyy'));
    $("#bankNumber").text($DATA_DOCUMENT.documentAdvance.bankNumber);
    $("#remark").text($DATA_DOCUMENT.documentAdvance.remark==""?"-":$DATA_DOCUMENT.documentAdvance.remark);


    $("#advancePurposeMobile").text($DATA_DOCUMENT.documentAdvance.advancePurpose);
    $("#requiredReferenceMobile").text($DATA_DOCUMENT.documentReference.length!=0?$DATA_DOCUMENT.documentReference[0].docReferenceNumber:"-");
    $("#amountMobile").autoNumeric('set',$DATA_DOCUMENT.documentAdvance.amount);
    $("#startDateEndDateMobile").text(new Date($DATA_DOCUMENT.documentAdvance.startDate).format('dd/mm/yyyy') +" to "+ new Date($DATA_DOCUMENT.documentAdvance.endDate).format('dd/mm/yyyy'));
    $("#dueDateMobile").text(new Date($DATA_DOCUMENT.documentAdvance.dueDate).format('dd/mm/yyyy'));
    $("#bankNumberMobile").text($DATA_DOCUMENT.documentAdvance.bankNumber);
    $("#remarkMobile").text($DATA_DOCUMENT.documentAdvance.remark==""?"-":$DATA_DOCUMENT.documentAdvance.remark);
    // $("#outstanding").autoNumeric('set',$DATA_DOCUMENT.documentAdvance.amount);
    if($DATA_DOCUMENT.documentAdvance.flagUnlock == "Y"){
        $("#imgOpenUnlock").removeClass('hide');
    }else{
        $("#imgOpenUnlock").addClass('hide');
    }

    var externalNumber = $DATA_DOCUMENT.documentAdvance.externalDocNumber?$DATA_DOCUMENT.documentAdvance.externalDocNumber:"";
    var externalPaymentNumber = $DATA_DOCUMENT.documentAdvance.externalPaymentDocNumber?$DATA_DOCUMENT.documentAdvance.externalPaymentDocNumber:"";
    var externalClearingNumber = $DATA_DOCUMENT.documentAdvance.externalClearingDocNumber?$DATA_DOCUMENT.documentAdvance.externalClearingDocNumber:"";
    var textFI = "";
    if(externalNumber){
        if(textFI){
            textFI = textFI+" , "+externalNumber;
        }else{
            textFI = externalNumber;
        }
    }
    if(externalPaymentNumber){
        if(textFI){
            textFI = textFI+" , "+externalPaymentNumber;
        }else{
            textFI = externalPaymentNumber;
        }
    }
    if(externalClearingNumber){
        if(textFI){
            textFI = textFI+" , "+externalClearingNumber;
        }else{
            textFI = externalClearingNumber;
        }
    }

    var fullTextFi = textFI?"("+textFI+")":"";

    $("#fiDoc").text(fullTextFi);

    getOutStandingBalance();
}

function getOutStandingBalance() {
    var dataAdvanceOutstandings = $.ajax({
        url: session.context + "/advanceOutstandings/findAdvanceOutstanding?venderNo="+$DATA_EMPLOYEE.Personal_ID+"&userName="+$DATA_EMPLOYEE.User_name.toLowerCase(),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataAdvanceOutstandings){
        $("#outstanding").autoNumeric('set',dataAdvanceOutstandings.amount);
    }else{
        $("#outstanding").autoNumeric('set',0);
    }
}

function validateDocStatus(){
    /* update by siriradC.  2017.08.04 */
    if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
        $("#btnCancel1").addClass('hide');
        $("#btnFile1").css('margin-top','0');
        $("#btnQR1").css('margin-top','80px');

        $("#btnTick").addClass('hide');
        $("#btnCancel2").addClass('hide');
        $("#btnFile2").css('margin-top','0');
        $("#btnQR2").css('margin-top','80px');

        $("[name=in]").addClass('hide');
        $("[name=out]").removeClass('hide');
        $("[name=acc]").addClass('hide');
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");

        var checkState = false;
        var checkRoleFinance = false;
        if($DATA_REQUEST_APPROVER){
            $.each($DATA_REQUEST_APPROVER,function (index,item) {
                if(item.requestStatusCode == null && session.userName == item.userNameApprover){
                    checkState = true;
                }
                if(session.userName == item.userNameApprover && item.actionState == 'FNC'){
                    checkRoleFinance= true;
                }
            });
        }

        if(checkState){
            if(checkRoleFinance){
                $("#btnCancel1").addClass('hide');
                $("#btnFile1").css('margin-top','0');
                $("#btnQR1").css('margin-top','80px');

                $("#btnTick").addClass('hide');
                $("#btnCancel2").addClass('hide');
                $("#btnFile2").css('margin-top','0');
                $("#btnQR2").css('margin-top','80px');

                $("[name=in]").addClass('hide');
                $("[name=out]").removeClass('hide');
                $("[name=acc]").addClass('hide');
            }else{
                $("#actionReasonDiv").removeClass('hide');
            }
        }else{
            $("#btnCancel1").addClass('hide');
            $("#btnFile1").css('margin-top','0');
            $("#btnQR1").css('margin-top','80px');

            $("#btnTick").addClass('hide');
            $("#btnCancel2").addClass('hide');
            $("#btnFile2").css('margin-top','0');
            $("#btnQR2").css('margin-top','80px');

            $("[name=in]").addClass('hide');
            $("[name=out]").removeClass('hide');
            $("[name=acc]").addClass('hide');
        }

    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
        $("#btnCancel1").addClass('hide');
        $("#btnFile1").css('margin-top','0');
        $("#btnQR1").css('margin-top','80px');

        $("#btnTick").addClass('hide');
        $("#btnCancel2").addClass('hide');
        $("#btnFile2").css('margin-top','0');
        $("#btnQR2").css('margin-top','80px');

        $("[name=in]").addClass('hide');
        $("[name=out]").removeClass('hide');
        $("[name=acc]").addClass('hide');
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
        $("#btnCancel1").addClass('hide');
        $("#btnFile1").css('margin-top','0');
        $("#btnQR1").css('margin-top','80px');

        $("#btnTick").addClass('hide');
        $("#btnCancel2").addClass('hide');
        $("#btnFile2").css('margin-top','0');
        $("#btnQR2").css('margin-top','80px');

        $("[name=in]").addClass('hide');
        $("[name=out]").removeClass('hide');
        $("[name=acc]").addClass('hide');
    }
}

function renderLineApproveAcc(requestApprover){
    console.log(requestApprover.length + 1);
    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;
    $("#lineApproverDetail").empty();
    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );
    for(var i=0;i<requestApprover.length;i++){
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
            '<span class="step"><span>'+ validateStatusReject(requestApprover[i].actionRoleCode,requestApprover[i].actionReasonDetail)+'</span></span>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionRoleName+'</div>'+
            '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
            '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
            '</span>'+
            '</li>'
        );
    }
}

function validateStatusReject(actionState,detail){
    if(checkStatus === 1 && checkStatusReject === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" class="animation-border two"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }else if(checkStatusReject === 1 && checkStatus === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" class="animation-border three"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }else{
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }
}

function renderLineApprove(requestApprover){
    $("#lineApproverDetail").empty();
    $("#lineApproveMobile").empty();

    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;
    
    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle-complete" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        console.log(requestApprover[i].requestStatusCode)
        findEmployeeProfileByUserNameLineApprove(requestApprover[i].userNameApprover);
        $("#lineApproverDetail").append(
            '<li class='+validateStatus(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
            '<span class="step"><a><span>'+ ((checkStatus===1) ? '<img onclick="remarkRequestDetail(\''+requestApprover[i].actionReasonDetail+'\')" title="View Remark" class="animation-border two"   src='+validateIMG(requestApprover[i].actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>' : '<img onclick="remarkRequestDetail(\''+requestApprover[i].actionReasonDetail+'\')" title="View Remark"  src='+validateIMG(requestApprover[i].actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>') +'</span></a>>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionStateName+'</div>'+
            '<div style="color: blue;">'+nvl(requestApprover[i].approver)+'</div>'+
            '<div style="color: lightseagreen;">'+nvl($DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH)+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading '+validateStatusMobile(requestApprover[i].requestStatusCode)+'" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionStateName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl(requestApprover[i].approver)+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl($DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH)+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    }
}

function validateStatusMobile(status) {
    if(status != null){
        return "collapseLightGreen";
    }else{
        return "collapseLightBlue";
    }
}

function validateStatus(status){
    if(status != null && status != "REJ"){
        return "idle-complete";
    }else if(status != null && status == "REJ"){
        checkStatusReject++;
        return "idle";
    }else{
        checkStatus++;
        return "idle";
    }
}


function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;

    $("#empName").text(data.FOA+data.FNameTH+" "+data.LNameTH);
    $("#empCode").text(data.Personal_ID);
    // $("#company").text(data.Personal_PA_ID+" "+data.Personal_PA_Name);
    // $("#department").text(data.Org_Name_TH_800);
    $("#position").text(data.PositionTH);
    // $("#costCenter").text(data.Position_Cost_Center);

    $("#empNameMobile").text(data.FOA+data.FNameTH+" "+data.LNameTH);
    $("#empCodeMobile").text(data.Personal_ID);
    // $("#companyMobile").text(data.Personal_PA_ID+" "+data.Personal_PA_Name);
    // $("#departmentMobile").text(data.Org_Name_TH_800);
    $("#positionMobile").text(data.PositionTH);
    // $("#costCenterMobile").text(data.Position_Cost_Center);
}

function findCreatorProfileByUserName(userName){
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#createBy").text(data.FOA+data.FNameTH+" "+data.LNameTH);
    $("#createByMobile").text(data.FOA+data.FNameTH+" "+data.LNameTH);
}

function findEmployeeProfileByUserNameLineApprove(userName){
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE_FOR_LINE_APPROVE = data;
}

function findRequestByDocument(id){
    var dataRequest = $.ajax({
        url: session.context + "/approve/findRequestByDocument/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;
    
    $DATA_REQUEST = dataRequest;

    var dataRequestApprover = $.ajax({
        url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST_APPROVER = dataRequestApprover;
    renderLineApprove($DATA_REQUEST_APPROVER)
}

function approveDocument(){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = $USERNAME;
    jsonData['actionStatus'] = validateActionState($USERNAME);
    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/approveRequest',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    $DATA_APPROVE = JSON.parse(xhr.responseText);

                    if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                        window.location.href = session.context;
                    }
                }
            }
        });
    },1000);
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function sendToSAPAdvance() {
    $('.dv-background').show();
    var jsonData = {}
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']  	= $DATA_DOCUMENT.id;

    var jsonApprove = {};
    jsonApprove['userName'] = $USERNAME;
    jsonApprove['actionStatus'] = validateActionState($USERNAME);
    jsonApprove['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonApprove['docType'] = $DATA_DOCUMENT.documentType;
    jsonApprove['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonApprove['processId'] = $DATA_DOCUMENT.processId;
    jsonApprove['documentId'] = $DATA_DOCUMENT.id;
    jsonApprove['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonApprove['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/advance/advanceTransfer',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    console.log(xhr.responseText);
                    if(xhr.responseText == ""){
                        $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/requests/approveRequest',
                            data: jsonApprove,
                            async: false,
                            complete: function (xhr2) {
                                $('.dv-background').hide();
                                window.location.href = session.context;
                            }
                        });
                    }else{
                        $('.dv-background').hide();
                        $('#warningModal .modal-body').html(xhr.responseText);
                        $("#warningModal").modal('show');
                    }
                }
            }
        });
    },1000);
}

function beforeRejectRequest() {

    var actionReasonCode = $("#actionReason")[0].selectedOptions[0].value;
    var actionReasonDetail = $("#actionReasonDetail").val();

    if(actionReasonCode == "" || actionReasonDetail == ""){
        $('#warningModal .modal-body').html(MSG.MESSAGE_INPUT_ACTION_REASON);
        $("#warningModal").modal('show');
    }else{
        var warningMessage = MSG.MESSAGE_CONFIRM_REJECT_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

        $('#confirmRejectModal .modal-body').html(warningMessage);
        $('#confirmRejectModal').modal('show');
    }

}

function rejectRequest(){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = $USERNAME;
    jsonData['actionStatus'] = validateActionState($USERNAME);
    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+$DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        } else {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/rejectRequest',
                                data: jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $('.dv-background').hide();
                                        var $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },1000);
}

function cancelRequest(){

    $('.dv-background').show();
    console.info("cancel Request");

    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = $("#amount").autoNumeric('get');
    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['id'] = $DATA_DOCUMENT.id;
    jsonData['requester'] = $DATA_DOCUMENT.requester;
    jsonData['docNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['details'] = JSON.stringify(listJsonDetails);

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/cancelRequest',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_REQUEST = JSON.parse(xhr.responseText);
                    if($DATA_REQUEST != null){
                        cancelDocument($DATA_REQUEST.document.id);
                        $('.dv-background').hide();
                    }

                }
            }
        });
    },1000);
}

function nvl(e){
    if(e == null || e == undefined){
        return "";
    }else{
        return e;
    }
}

function preViewAttachmentFileExpenseItem(id,fileName) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(fileType == 'pdf' || fileType == 'PDF' || fileType =='txt' || fileType == 'TXT'){
        var url = session['context']+'/approve/preViewPDFDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(url, '', properties);
    }

    if(fileType == 'png' || fileType == 'PNG' || fileType =='jpg' || fileType == 'JPG' || fileType == 'jpeg'  || fileType == 'JPEG'){


        var urlIMG = session['context']+'/approve/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(url, '', properties);
    }



}



function remarkRequestDetail(txt){

                console.log('>>Message Reason>>>'+txt)

            if(txt != null && txt != '' && txt!= undefined && txt != 'null'){

                $("#alertModal").modal('show');
                $("label[id=detailAlert]").html('Remark : '+txt);

            }

}