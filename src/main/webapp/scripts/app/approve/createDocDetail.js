/* GLOBAL VARIABLE BY SIRIRADC. 2017/08/07 */
var type = null;
var $MASTER_DATA_LOCATION   = [];
var $DATA_TRAVEL_DETAIL;
var $DATA_TRAVEL_MEMBER;
var $DATA_EXTERNAL_MEMBER;
var $DATA_CAR_BOOKING;
var $DATA_DOCUMENT_ATTACHMENT;
var $STATUS;
var $TravelDetailId;
var $TravelMemberId;
var $ExternalMemberId;
var $CarTypeCode;
var $fileUpload="";
var $DATA_EMPLOYEE;
var $DATA_REQUEST;
var $DATA_LINE_APPROVE;
var $APP_TYPE;
var NUMBER_MEMBER;
var LIST_TRAVEL_MEMBER;
var STATUS_SAVE_DOCUMENT;
var DOC_REF_SIZE = 0;
var BTN;


function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-80) + 'px';
    }
}

$(document).ready(function () {
    $('.dv-background').show();
    /* call master data */
    readMasterDataDocType();
    readMasterDataDocStatus();
    readMasterDataAprType();
    readMasterDataAttachmentType();
    readMasterDataCarType();
    readMasterDataAirline();
    readMasterDataHotel();
    readMasterDataFlowType();



    $.material.init();
    moveFixedActionButton();
    window.onresize = function (event) {
        moveFixedActionButton()
    };

    $("#222").keypress(function (e) {
        e.preventDefault();
    });


    /* -======================= event date time =========================- */

    $("#dateStart").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#dateStartCar").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#dateHotel").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });


    /* -============== end event date time ===============- */

    /* render value document */
    $APP_TYPE = $DATA_DOCUMENT.approveType;
    findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
    $("#docNumber").text($DATA_DOCUMENT.docNumber);
    $("#sendTime").text($DATA_DOCUMENT.sendDate == null ? "SEND TIME" : $DATA_DOCUMENT.sendDate);
    if($DATA_EMPLOYEE != null) {
        $("#requester").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        $("#empCode").text($DATA_DOCUMENT.personalId == null ? $DATA_EMPLOYEE.Personal_ID : $DATA_DOCUMENT.personalId);
        $("#position").text($DATA_EMPLOYEE.PositionTH);
        $("#costCenter").text($DATA_DOCUMENT.costCenterCode);
    }

    if($DATA_DOCUMENT.companyCode){
        findByPaCode($DATA_DOCUMENT.companyCode);
    }

    if($DATA_DOCUMENT.psa){
        findByPsaCode($DATA_DOCUMENT.psa);
    }

    findEmployeeProfileByUserName($DATA_DOCUMENT.createdBy);
    $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);


    /* validate BTN QR code */
    if($DATA_DOCUMENT.request != null){
        $("#divBtnQRCode").removeClass('hide');
    }

    window.setTimeout(function(){
        validateApproveType();
        validateDocStatus($DATA_DOCUMENT.documentStatus);
        setUpDefaultCarType();

        /* manage doc ref for default */
        AutocompleteDocRefAppCar.find("docRefInputDocRefAppCar",$DATA_DOCUMENT.requester);
        AutocompleteDocRefAppHotel.find("docRefHotelInputDocRefAppHotel",$DATA_DOCUMENT.requester);
        AutocompleteDocRefAppFlight.find("docRefFlightInputDocRefAppFlight",$DATA_DOCUMENT.requester);
        if(listDocRefApproveCar.length == 1){
            verifyDefaultDocReference(listDocRefApproveCar[0].doc);
        }
        if(listDocRefApproveHotel.length == 1){
            verifyDefaultDocReference(listDocRefApproveHotel[0].doc);
        }if(listDocRefApproveFlight.length == 1){
            verifyDefaultDocReference(listDocRefApproveFlight[0].doc);
        }
    },1000);

    $('#timeStart').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeStartCar').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeEnd').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeEndCar').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });


    /* render travel detail */
    window.setTimeout(function(){
        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
        if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id != null){
            findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);

            if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBookings != null){
                renderDataCarBooking($DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBookings);
            }if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking != null){
                renderDataHotelBooking($DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking);
            }if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket != null){
                renderDataFlightTicket($DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket);
            }
        }

        if(DOC_REF_SIZE == 0 && listDocRefApproveCar.length == 1 ) {
            AutocompleteDocRefAppCar.renderValue(listDocRefApproveCar[0].doc, "docRefInputDocRefAppCar");
            $("#docRefInputDocRefAppCar").blur();
        }
        if(DOC_REF_SIZE == 0 && listDocRefApproveHotel.length == 1 ){
            AutocompleteDocRefAppHotel.renderValue(listDocRefApproveHotel[0].doc,"docRefHotelInputDocRefAppHotel");
            $("#docRefHotelInputDocRefAppHotel").blur();
        }
        if(DOC_REF_SIZE == 0 && listDocRefApproveFlight.length == 1){
            AutocompleteDocRefAppFlight.renderValue(listDocRefApproveFlight[0].doc,"docRefFlightInputDocRefAppFlight");
            $("#docRefFlightInputDocRefAppFlight").blur();
        }

        $('.dv-background').hide();
    },3000);

    $("#docRefInputDocRefAppCar").on('blur',function (){
        $("#docRefBtn").attr('data-attn',$("#docRefInputDocRefAppCar").attr('data-doc'));
        validateDefaultCarType($("#docRefInputDocRefAppCar").attr('data-doc'));
    });

    $("#docRefHotelInputDocRefAppHotel").on('blur',function (){
        $("#docRefHotelBtn").attr('data-attn',$("#docRefHotelInputDocRefAppHotel").attr('data-doc'));
        validateDefaultCarType($("#docRefHotelInputDocRefAppHotel").attr('data-doc'));
    });

    $("#docRefFlightInputDocRefAppFlight").on('blur',function (){
        $("#docRefFlightBtn").attr('data-attn',$("#docRefFlightInputDocRefAppFlight").attr('data-doc'));
        validateMemberFlight($("#docRefFlightInputDocRefAppFlight").attr('data-doc'));
    });

    $("#docRefInputDocRefAppCar").on('keyup',function (){
        AutocompleteDocRefAppCar.search("docRefInputDocRefAppCar",$DATA_DOCUMENT.requester,$("#docRefInputDocRefAppCar").val());
    });

    $("#docRefHotelInputDocRefAppHotel").on('keyup',function (){
        AutocompleteDocRefAppHotel.search("docRefHotelInputDocRefAppHotel",$DATA_DOCUMENT.requester,$("#docRefHotelInputDocRefAppHotel").val());
    });

    $("#docRefFlightInputDocRefAppFlight").on('keyup',function (){
        AutocompleteDocRefAppFlight.search("docRefFlightInputDocRefAppFlight",$DATA_DOCUMENT.requester,$("#docRefFlightInputDocRefAppFlight").val());
    });

    /* travel member panel */
    $("#nameMemberInputEmployeeAll").on('keyup',function(){
        AutocompleteEmployeeAll.setId('nameMemberInputEmployeeAll');
        AutocompleteEmployeeAll.search($("#nameMemberInputEmployeeAll").val(),"nameMemberInputEmployeeAll");
    });

    $("#nameMemberInputEmployeeAll").on('blur',function(){
        if($("#nameMemberInputEmployeeAll").val() != ""){
            $("#empCodeMember").text($("#nameMemberInputEmployeeAll").attr("data-personalId"));
            $("#empCodeMember").attr("data-empCode",$("#nameMemberInputEmployeeAll").attr("data-empCode"));
            $("#positionMember").text($("#nameMemberInputEmployeeAll").attr("data-position"));
            $("#departmentMember").text($("#nameMemberInputEmployeeAll").attr("data-deptName"));
        }else{
            $("#empCodeMember").text("");
            $("#positionMember").text("");
            $("#departmentMember").text("");
        }
    });


    /* event confirm delete */
    $("#confirmDelete").on('click',function (){
        if($('#typeDataDelete').val() == "TRAVEL_DETAIL"){
            deleteTravelDetail($("#idItemDelete").val());
        }else if($('#typeDataDelete').val() == "TRAVEL_MEMBER"){
            deleteTravelMember($("#idItemDelete").val());
        }else if($('#typeDataDelete').val() == "DOCUMENT_ATTACHMENT"){
            deleteDocumentAttachment($("#idItemDelete").val());
        }else{
            deleteExternalMember($("#idItemDelete").val());
        }
    });

    $('#completeModal').on('shown.bs.modal', function() {
        window.setTimeout(function(){
            $('#completeModal').modal('hide');
        },1000);
    });


    $("#destinationCarBooking").on('change',function (){
        var destination = $("#destinationCarBooking").val();
        for(var i = 0;i<$DATA_LOCATION_JSON.length; i++){
            if(parseInt(destination) == $DATA_LOCATION_JSON[i].id){
                if($DATA_LOCATION_JSON[i].flagOther == 'Y'){
                    $("#otherDiv").removeClass('hide');
                }else{
                    $("#otherDiv").addClass('hide');
                }
            }
        }
    });

    /* manage hotel booking */
    $("#hotelInputHotel").on('blur',function (){
        if($("#hotelInputHotel").val() != "" && $("#hotelInputHotel").attr('data-hotel-code') != "Other"){
            $("#hotelPriceDiv").removeClass('hide');
            $("#hotelPrice").text($("#hotelInputHotel").attr('data-hotel-rate'));
        }else if($("#hotelInputHotel").val() != "" && $("#hotelInputHotel").attr('data-hotel-code') == "Other"){
            $("#otherHotelDiv").removeClass('hide');
        } else{
            $("#hotelPriceDiv").addClass('hide');
            $("#hotelPrice").text("");
        }
    });


    /* manage cancel document */
    $("#confirmCancel").on('click',function(){
        cancelDocument();
    });

    /* upload document attachment */
    $("#browseFile").on('click',function(){
        if($("#attachmentType").val() == ""){
            var errorMessage = MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ATTACHMENT_TYPE+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }else{
            var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
            $("#uploadFileDocument").attr('accept',extension);
            $("#uploadFileDocument").click();
        }
    });

    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
        // validateFileExtensions();
    });

    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });


    $("#imageCalendar").on('click',function () {
        $("#dateStart").focus()
    });

    $("#imageCalendarCar").on('click',function () {
        $("#dateStartCar").focus()
    });

    $("#imageCalendarHotel").on('click',function () {
        $("#dateHotel").focus()
    });

});

function findCostCenter(costCenter) {
    $('.dv-background').show();
    setTimeout(function () {

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data){
            var costCenterSplit = data.profile.split('#');
            if(costCenterSplit[2] != "0"){
                var paPsaSplit = costCenterSplit[2].split('-');
                $("#company").text(paPsaSplit[0]+" : "+paPsaSplit[1]);
                $("#company").attr('pa',paPsaSplit[0]);
                $("#department").text(paPsaSplit[2]+" : "+paPsaSplit[3]);
                $("#department").attr('psa',paPsaSplit[2]);
                $('.dv-background').hide();
            }else{
                $('.dv-background').hide();
            }
        }

    },1000);

}

function validateDocStatus(documentStatus){
    /* update by siriradC.  2017.08.04 */
    if(documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }
}

function activeCar(btn) {
    if($("[name=iconCar]").hasClass('click') == 'false'){
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").removeClass('hide');
        }
    }else{
        $("[name=iconCar]").removeClass('click');
        setUpDefaultCarType();
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").removeClass('hide');
        }
    }
}

function verifyDefaultDocReference(docNumber){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findDocRefByDocTypeExp/'+docNumber,
        complete: function (xhr) {
            var data  = JSON.parse(xhr.responseText);
            DOC_REF_SIZE = data.length;
        }
    });
}

function setUpDefaultCarType(){
    for(var i=0;i<$("[name=iconCar]").length;i++){
        if('van' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
        }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
        }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
        }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
        }
    }
}

function validateApproveType(){

    if($APP_TYPE == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#divDOMESTIC").removeClass("hide");
        getMasterDataLocation("D");
        $("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason);
    }else if($APP_TYPE == MASTER_DATA.APR_TYPE_FOREIGN){
        $("#divFOREIGN").removeClass("hide");
        getMasterDataLocation("F");
        $("#foreignReason").val($DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason);
    }else if($APP_TYPE == MASTER_DATA.APR_TYPE_CAR){
        $("#divCAR").removeClass("hide");
        getMasterDataLocation("D");
    }else if($APP_TYPE == MASTER_DATA.APR_TYPE_HOTEL){
        $("#divHOTEL").removeClass("hide");
    }else if($APP_TYPE == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        $("#divFLIGHT").removeClass("hide");
        getMasterDataAirline();
    }
}

/* PANEL TRAVEL DETAIL */
function addTravelDetail(btn){

    $('.dv-background').show();
    window.setTimeout(function() {
        $STATUS = btn;
        if (btn == "add") {
            $TravelDetailId = null;
            /* validate default origin */
            if ($DATA_TRAVEL_DETAIL.length == 0) {
                findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
                for (var i = 0; i < $DATA_LOCATION_JSON.length; i++) {
                    if ($DATA_LOCATION_JSON[i].psaCode == $DATA_EMPLOYEE.Personal_PSA_ID) {
                        $("#origin").val($DATA_LOCATION_JSON[i].id);
                    }
                }
            } else {
                $("#origin").val("");
            }

            $("#destination").val("");
            $("#dateStart").val("");
            $("#timeStart").val("");
            $("#timeEnd").val("");
            $("#remark").val("");
            $('.dv-background').hide();
            $("#modalAddDetail").modal('show');
        }
    },500);
}

function insertDataTravel(){

    var originData      = $("#origin")[0].selectedOptions[0].value;
    var destinationData = $("#destination")[0].selectedOptions[0].value;
    var dateStartData	= $("#dateStart").val() == "" ? "" : $("#dateStart").val().split("to")[0].trim();
    var timeStartData	= $("#timeStart").val();
    var dateEndData	    = $("#dateStart").val() == "" ? "" : $("#dateStart").val().split("to")[1].trim();
    var timeEndData	    = $("#timeEnd").val();
    var remarkData	    = $("#remark").val();

    var errorMessage = "";

    if(originData == "" ){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ORIGIN+"<br/>";
    }if (destinationData == "" ){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DESTINATION+"<br/>";
    }if(dateStartData == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
    }if(timeStartData == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_TIME+"<br/>";
    }if( dateEndData == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
    }if(timeEndData == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_TIME+"<br/>";
    }

    if(errorMessage){
        $('#warningModal .modal-body').html(errorMessage);
        $('#warningModal').modal('show');
    }
    else{
        saveTravelDetail(originData,destinationData,dateStartData,timeStartData,dateEndData,timeEndData,remarkData);
    }
}

function getMasterDataLocation(locationType){
    var locationString = $.ajax({
        url: session['context'] + "/locations/findByLocationType",
        type: "GET",
        data:{locationType:locationType},
        async: false
    }).responseText;
    $DATA_LOCATION_JSON = JSON.parse(locationString);

    $("#origin").empty().append('<option value=""></option>');
    $("#destination").empty().append('<option value=""></option>');
    $("#originCarBooking").empty().append('<option value="">'+LB.LABEL_ORIGIN+'</option>');
    $("#destinationCarBooking").empty().append('<option value="">'+LB.LABEL_DESTINATION+'</option>');
    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#origin").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }
    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#destination").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }
    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#originCarBooking").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }
    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#destinationCarBooking").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }

}

function saveTravelDetail(originData,destinationData,dateStartData,timeStartData,dateEndData,timeEndData,remarkData){

    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['id'] = $TravelDetailId;
        jsonData['origin'] = originData;
        jsonData['destination'] = destinationData;
        jsonData['startDate'] = dateStartData;
        jsonData['endDate'] = dateEndData;
        jsonData['startTime'] = timeStartData;
        jsonData['endTime'] = timeEndData;
        jsonData['remark'] = remarkData;
        jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;


        var dataDiffDays = 0;
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: session['context'] + '/approve/calculateDiffDate',
            data: {
                startDate: dateStartData,
                endDate: dateEndData,
                startTime: timeStartData,
                endTime: timeEndData
            },
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDiffDays = JSON.parse(xhr.responseText);
                    jsonData['travelDays'] = dataDiffDays;

                    var dataTravel = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveTravelDetail',
                        data: jsonData,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    $('.dv-background').hide();
                                    findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
                                }
                            }
                        }
                    });
                }
            }
        });
    },500);
}

var remark;
function remarkDetail(obj){
    remark = obj;
    for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
        if($DATA_TRAVEL_DETAIL[i].id == parseInt(obj.id)){
            $("#alertModal").modal('show');
            $("label[id=detailAlert]").text($DATA_TRAVEL_DETAIL[i].remark);
        }
    }
}

function renderDataTravelDetail(dataTravel){
    if($APP_TYPE == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderTravel").empty();
        if(dataTravel.length > 0){
            for(var i=0;i<dataTravel.length;i++){
                var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
                var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);

                $("#collapseHeaderTravel").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)"><img src='+IMG.DELETE+' width="50px" title="DELETE" id='+dataTravel[i].id+'  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataTravel[i].id+'\');$(\'#typeDataDelete\').val(\'TRAVEL_DETAIL\');" /></a></div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:18px;"><b>'+dataTravel[i].origin.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" style="padding-left: 0px;padding-right: 0px;" >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+dataTravel[i].destination.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-2" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" style="padding-left: 0px;padding-right: 0px;">'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;">'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' title="REMARK" width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }else{
        $("#collapseForiegnTravel").empty();
        if(dataTravel.length > 0){
            for(var i=0;i<dataTravel.length;i++){
                var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
                var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);

                $("#collapseForiegnTravel").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)"><img src='+IMG.DELETE+' width="50px" id='+dataTravel[i].id+'  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataTravel[i].id+'\');$(\'#typeDataDelete\').val(\'TRAVEL_DETAIL\');" /></a></div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:18px;"><b>'+dataTravel[i].origin.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravel[i].id+' onclick="editTravelDetail(this)"  >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+dataTravel[i].destination.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-2" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-1">'+
                    '<div class="col-xs-12 col-sm-12"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }


    $("#modalAddDetail").modal('hide');
}

function deleteTravelDetail(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteTravelDetail/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

function editTravelDetail(obj){
    $STATUS = "update";
    $TravelDetailId = obj.id;
    $("#modalAddDetail").modal('show');
    renderDataToModalEditTravelDetail(obj.id);
}

function findTravelDetailsByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
        complete: function (xhr) {
            $DATA_TRAVEL_DETAIL = JSON.parse(xhr.responseText);
            renderDataTravelDetail($DATA_TRAVEL_DETAIL);
        }
    });
}

function renderDataToModalEditTravelDetail(id){
    for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
        if($DATA_TRAVEL_DETAIL[i].id == parseInt(id)){
            $("#origin").val($DATA_TRAVEL_DETAIL[i].origin.id);
            $("#destination").val($DATA_TRAVEL_DETAIL[i].destination.id);
            $("#dateStart").val(DateUtil.coverDateToString($DATA_TRAVEL_DETAIL[i].startDate)+" "+"to"+" "+DateUtil.coverDateToString($DATA_TRAVEL_DETAIL[i].endDate));
            $("#timeStart").val($DATA_TRAVEL_DETAIL[i].startTime);
            $("#timeEnd").val($DATA_TRAVEL_DETAIL[i].endTime);
            $("#remark").val($DATA_TRAVEL_DETAIL[i].remark);
        }
    }
}

/* PANEL MEMBER PERSON */
function addTravelMember(btn){
    $('.dv-background').show();
    window.setTimeout(function() {
        if (btn == "add") {
            $TravelMemberId = null;
            $("#nameMemberInputEmployeeAll").val("");
            $("#empCodeMember").text("");
            $("#positionMember").text("");
            $("#departmentMember").text("");
            $('.dv-background').hide();
            $("#modalAddPerson").modal('show');
        }
    },500);
}

function renderDataTravelMember(dataTravelMember){
    if($APP_TYPE == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderMember").empty();
        $("#personMember").text(dataTravelMember.length);
        if(dataTravelMember.length > 0){
            for(var i=0;i<dataTravelMember.length;i++){
                findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                var memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
                var positionMember = $DATA_EMPLOYEE.PositionTH;
                var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;
                var personalId  = $DATA_EMPLOYEE.Personal_ID;

                if($DATA_DOCUMENT.requester == dataTravelMember[i].memberUser){
                    $("#collapseHeaderMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                        '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                        '<div class="form-group" style="margin-bottom: 0px">'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+'  >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label pull-right" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                }else{
                    $("#collapseHeaderMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                        '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                        '<div class="form-group" style="margin-bottom: 0px">'+
                        '<div class="col-sm-1"><a href="javascript:void(0)"><img src='+IMG.DELETE+' width="50px" id='+dataTravelMember[i].id+'  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataTravelMember[i].id+'\');$(\'#typeDataDelete\').val(\'TRAVEL_MEMBER\');" /></a></div>'+
                        '<div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)"  >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label pull-right" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                }


            }
        }
    }else if($APP_TYPE == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        $("#collapseFlightPerson").empty();
        if(dataTravelMember.length > 0){
            $("#collapseFlightPerson").attr('valueSize',dataTravelMember.length);
            for(var i=0;i<dataTravelMember.length;i++) {

                findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                var memberName = $DATA_EMPLOYEE.FOA + $DATA_EMPLOYEE.FNameTH + " " + $DATA_EMPLOYEE.LNameTH;
                var positionMember = $DATA_EMPLOYEE.PositionTH;
                var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;
                var memberEng = $DATA_EMPLOYEE.NameEN;

                $("#collapseFlightPerson").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id=' + dataTravelMember[i].id + '>' +
                    '<div class="panel-heading collapseGray" id=' + dataTravelMember[i].id + ' role="button" style="padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" >' +
                    '<div class="form-group" style="margin-bottom: 0px">' +
                    '<div class="col-xs-12 col-sm-1" style="padding-left: 0px;padding-top: 15px;">' +
                    '<a href="javascript:void(0)"><img class="pull-left" src=' + IMG.DELETE + ' width="40px" id=' + dataTravelMember[i].id + '  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\'' + dataTravelMember[i].id + '\');$(\'#typeDataDelete\').val(\'TRAVEL_MEMBER\');" /></a>' +
                    '</div>' +
                    '<div class="col-xs-12 col-sm-6" style="padding-left: 0px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' onclick="editTravelMember(this)" >' +
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>' + memberName + '</b></label></div>' +
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;text-align:left;font-size:16px"><b>' + memberEng + '</b></label></div>' +
                    '</div>' +
                    '<div class="col-xs-12 col-sm-5" style="padding-left: 0px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' onclick="editTravelMember(this)"  >' +
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>' + positionMember + '</b></label></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');



            }
        }
    }
    else{
        $("#collapseForeignMember").empty();
        $("#foreignMember").text(dataTravelMember.length);
        if(dataTravelMember.length > 0){
            for(var i=0;i<dataTravelMember.length;i++){

                findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                var memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
                var positionMember = $DATA_EMPLOYEE.PositionTH;
                var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;
                var personalId  = $DATA_EMPLOYEE.Personal_ID;

                if($DATA_DOCUMENT.requester == dataTravelMember[i].memberUser){
                    $("#collapseForeignMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                        '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                        '<div class="form-group" style="margin-bottom: 0px">'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+'  >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                        '</div>'+
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' >'+
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label pull-right" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                }else {
                    $("#collapseForeignMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id=' + dataTravelMember[i].id + '>' +
                        '<div class="panel-heading collapseGray" id=' + dataTravelMember[i].id + ' role="button" style="padding-bottom: 0px" >' +
                        '<div class="form-group" style="margin-bottom: 0px">' +
                        '<div class="col-sm-1"><a href="javascript:void(0)"><img src=' + IMG.DELETE + ' width="50px" id=' + dataTravelMember[i].id + '  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\'' + dataTravelMember[i].id + '\');$(\'#typeDataDelete\').val(\'TRAVEL_MEMBER\');" /></a></div>' +
                        '<div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' onclick="editTravelMember(this)" >' +
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:18px;"><b>' + memberName + '</b></label></div>' +
                        '<div class="col-sm-12"style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white"><b>' + dataTravelMember[i].memberPersonalId + '</b></label></div>' +
                        '</div>' +
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' onclick="editTravelMember(this)"  >' +
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>' + positionMember + '</b></label></div>' +
                        '</div>' +
                        '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' onclick="editTravelMember(this)" >' +
                        '<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label pull-right" style="color: #b2ff59;font-size:18px"><b>' + departmentMember + '</b></label></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }
            }
        }
    }

    $("#modalAddPerson").modal('hide');
}

function insertDataTravelMember(){
    var nameMember = $("#nameMemberInputEmployeeAll").val();

    var errorMessage = "";
    var flagMember = true;

    if(nameMember == ""){

        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_EMPLOYEE_NAME+"<br/>";

        $('#warningModal .modal-body').html(errorMessage);
        $("#warningModal").modal('show');
    }else{

        if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING && LIST_TRAVEL_MEMBER != null){
            for(var i=0;i<LIST_TRAVEL_MEMBER.length;i++){
                if($("#nameMemberInputEmployeeAll").attr("data-userName") != LIST_TRAVEL_MEMBER[i].memberUser){
                    flagMember = false;
                }else {
                    flagMember = true;
                    break;
                }
            }
        }else{
            flagMember = true;
        }

        if(flagMember){
            $('.dv-background').show();
            window.setTimeout(function() {
                var jsonData = {};
                jsonData['parentId'] = 1;
                jsonData[csrfParameter] = csrfToken;
                jsonData['id'] = $TravelMemberId;
                jsonData['memberCode'] = $("#empCodeMember").attr("data-empCode");
                jsonData['memberUser'] = $("#nameMemberInputEmployeeAll").attr("data-userName");
                jsonData['memberPersonalId'] = $("#nameMemberInputEmployeeAll").attr("data-personalId");
                jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                var dataTravel = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/approve/saveTravelMember',
                    data: jsonData,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            $('.dv-background').hide();
                            if (xhr.responseText == "DUPLICATE") {
                                errorMessage += MSG.MESSAGE_DATA_DUPLICATE + "<br/>";
                                $('#warningModal .modal-body').html(errorMessage);
                                $("#warningModal").modal('show');
                            } else {
                                findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
                            }
                        }
                    }
                });
            },500);
        }else{
            errorMessage += MSG.MESSAGE_NOT_MEMBER_TO_TRAVEL+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $("#warningModal").modal('show');
        }

    }
}

function findTravelMembersByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelMembers',
        complete: function (xhr) {
            $DATA_TRAVEL_MEMBER = JSON.parse(xhr.responseText);
            renderDataTravelMember($DATA_TRAVEL_MEMBER);
        }
    });
}

function editTravelMember(obj){
    $STATUS = "update";
    $TravelMemberId = obj.id;
    $("#modalAddPerson").modal('show');
    renderDataToModalEditTravelMember(obj.id);
}

function renderDataToModalEditTravelMember(id){
    for(var i = 0; i<$DATA_TRAVEL_MEMBER.length;i++){
        if($DATA_TRAVEL_MEMBER[i].id == parseInt(id)){
            AutocompleteEmployeeAll.search($DATA_TRAVEL_MEMBER[i].memberUser);
            AutocompleteEmployeeAll.renderValue($DATA_TRAVEL_MEMBER[i].memberUser,"nameMemberInputEmployeeAll");
            $("#empCodeMember").text("");
            $("#positionMember").text("");
            $("#departmentMember").text("");

            $("#empCodeMember").text($("#nameMemberInputEmployeeAll").attr("data-personalId"));
            $("#empCodeMember").attr("data-empCode",$("#nameMemberInputEmployeeAll").attr("data-empCode"));
            $("#positionMember").text($("#nameMemberInputEmployeeAll").attr("data-position"));
            $("#departmentMember").text($("#nameMemberInputEmployeeAll").attr("data-deptName"));
        }
    }
}

function deleteTravelMember(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteTravelMember/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

/* PANEL EXTERNAL MEMBER */
function addExternalMember(btn){
    $('.dv-background').show();
    window.setTimeout(function() {
        if (btn == "add") {
            $ExternalMemberId = null;
            $("#externalName").val("");
            $("#externalCompany").val("");
            $("#externalPhoneNumber").val("");
            $("#externalEmail").val("");
            $("#externalRemark").val("");
            $('.dv-background').hide();
            $("#modalAddOuterPerson").modal('show');
        }
    },500);
}

function renderDataExternalMember(dataExternal){
    if($APP_TYPE == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderExternal").empty();
        $("#personExternal").text(dataExternal.length);
        if(dataExternal.length > 0){
            for(var i=0;i<dataExternal.length;i++){

                $("#collapseHeaderExternal").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="container-fluid">'+
                    '<div class="form-horizontal">'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-sm-1"><a href="javascript:void(0)"><img src='+IMG.DELETE+' width="50px" data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataExternal[i].id+'\');$(\'#typeDataDelete\').val(\'EXTERNAL_MEMBER\');" /></a></div>'+
                    '<div class="col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)" >'+
                    '<div class="col-sm-12"><label class="control-label" style="color: white;font-size:18px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #08f508"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)"  >'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #08f508"><b>'+dataExternal[i].email+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-1">'+
                    '<div class="col-sm-12"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }else{
        $("#collapseForeignExternal").empty();
        $("#foreignExternal").text(dataExternal.length);
        if(dataExternal.length > 0){
            for(var i=0;i<dataExternal.length;i++){

                $("#collapseForeignExternal").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="container-fluid">'+
                    '<div class="form-horizontal">'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-sm-1"><a href="javascript:void(0)"><img src='+IMG.DELETE+' width="50px" data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataExternal[i].id+'\');$(\'#typeDataDelete\').val(\'EXTERNAL_MEMBER\');" /></a></div>'+
                    '<div class="col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)" >'+
                    '<div class="col-sm-12"><label class="control-label" style="color: white;font-size:18px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #08f508"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)"  >'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #08f508"><b>'+dataExternal[i].email+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-1">'+
                    '<div class="col-sm-12"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }


    $("#modalAddOuterPerson").modal('hide');
}

function remarkExternal(obj){
    for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
        if($DATA_EXTERNAL_MEMBER[i].id == parseInt(obj.id)){
            $("#alertModal").modal('show');
            $("label[id=detailAlert]").text($DATA_EXTERNAL_MEMBER[i].remark);
        }
    }
}

function insertDataExternalMember(){
    var externalName      		= $("#externalName").val();
    var externalCompany			= $("#externalCompany").val();
    var externalPhoneNumber	    = $("#externalPhoneNumber").val();
    var externalEmail	    	= $("#externalEmail").val();
    var externalRemark	    	= $("#externalRemark").val();

    var errorMessage = "";
    if(externalName == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_FULL_NAME+"<br/>";
    }

    if(externalCompany == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_COMPANY+"<br/>";
    }

    if(errorMessage != ""){
        $('#warningModal .modal-body').html(errorMessage);
        $("#warningModal").modal('show');
    }
    else{
        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $ExternalMemberId;
            jsonData['memberName'] = externalName;
            jsonData['companyMember'] = externalCompany;
            jsonData['phoneNumber'] = externalPhoneNumber;
            jsonData['email'] = externalEmail;
            jsonData['remark'] = externalRemark;
            jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

            var dataTravel = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/saveExternalMember',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('.dv-background').hide();
                        findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
                    }
                }
            });
        },500);
    }
}

function findExternalMembersByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/externalMembers',
        complete: function (xhr) {
            $DATA_EXTERNAL_MEMBER = JSON.parse(xhr.responseText);
            renderDataExternalMember($DATA_EXTERNAL_MEMBER);
        }
    });
}

function editExternalMemberl(obj){
    $STATUS = "update";
    $ExternalMemberId = obj.id;
    $("#modalAddOuterPerson").modal('show');
    renderDataToModalEditExternalMember(obj.id);
}

function renderDataToModalEditExternalMember(id){
    for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
        if($DATA_EXTERNAL_MEMBER[i].id == parseInt(id)){
            $("#externalName").val($DATA_EXTERNAL_MEMBER[i].memberName);
            $("#externalCompany").val($DATA_EXTERNAL_MEMBER[i].companyMember);
            $("#externalPhoneNumber").val($DATA_EXTERNAL_MEMBER[i].phoneNumber);
            $("#externalEmail").val($DATA_EXTERNAL_MEMBER[i].email);
            $("#externalRemark").val($DATA_EXTERNAL_MEMBER[i].remark);
        }
    }
}

function deleteExternalMember(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteExternalMember/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

/* manage car booking */
function saveCarBooking(){
    var documentReference 	= $("#docRefInputDocRefAppCar").val() == "" ? "" : $("#docRefInputDocRefAppCar").attr('data-doc');
    var origin 				= $("#originCarBooking").val();
    var destination 		= $("#destinationCarBooking").val();
    var startDate 			= $("#dateStartCar").val();
    var endDate   			= $("#dateStartCar").val();
    var startTime			= $("#timeStartCar").val();
    var endTime  			= $("#timeEndCar").val();
    var carType  			= $CarTypeCode;
    var numOfTravel 		= $("#numOfTravel").val();
    var remark      		= $("#remarkCar").val();
    var otherLocation      	= $("#otherLocation").val();
    var otherCarType      	= $("#otherCarType").val();
    var carBookingId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBooking == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBooking.id;


    var errorMessage = "";
    if(documentReference == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
    }if(origin == "" ){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ORIGIN+"<br/>";
    }if (destination == "" ){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DESTINATION+"<br/>";
    }if(startDate == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
    }if(startTime == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_TIME+"<br/>";
    }if( endDate == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
    }if(endTime == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_TIME+"<br/>";
    }if(carType == "" || carType == undefined){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_CAR_TYPE+"<br/>";
    }if(numOfTravel == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_NUMBER_OF_TRAVELERS+"<br/>";
    }

    if(errorMessage){
        $('#warningModal .modal-body').html(errorMessage);
        $('#warningModal').modal('show');
    }else{

        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $DATA_DOCUMENT.id;
            jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocument = JSON.parse(xhr.responseText);
                        if (dataDocument != null) {
                            var jsonDocumentReference = {};
                            jsonDocumentReference['parentId'] = 1;
                            jsonDocumentReference[csrfParameter] = csrfToken;
                            jsonDocumentReference['docReferenceNumber'] = documentReference;
                            jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                            jsonDocumentReference['approveTypeCode'] = $APP_TYPE;
                            jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                            var jsonCarBooking = {};
                            jsonCarBooking['parentId'] = 1;
                            jsonCarBooking[csrfParameter] = csrfToken;
                            jsonCarBooking['id'] = carBookingId;
                            jsonCarBooking['startDate'] = $("#dateStartCar").val().split('to')[0].trim();
                            jsonCarBooking['endDate'] = $("#dateStartCar").val().split('to')[1].trim();
                            jsonCarBooking['carTypeCode'] = carType;
                            jsonCarBooking['startTime'] = startTime;
                            jsonCarBooking['endTime'] = endTime;
                            jsonCarBooking['numMember'] = numOfTravel;
                            jsonCarBooking['remark'] = remark;
                            jsonCarBooking['origin'] = origin;
                            jsonCarBooking['destination'] = destination;
                            jsonCarBooking['otherLocation'] = otherLocation;
                            jsonCarBooking['otherCarType'] = otherCarType;
                            jsonCarBooking['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                            var dataCarBooking = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/approve/saveCarBooking',
                                data: jsonCarBooking,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        dataCarBooking = JSON.parse(xhr.responseText);

                                        var dataDocumentReference = $.ajax({
                                            type: "POST",
                                            headers: {
                                                Accept: 'application/json'
                                            },
                                            url: session['context'] + '/approve/saveDocumentReference',
                                            data: jsonDocumentReference,
                                            async: false,
                                            complete: function (xhr) {
                                                if (xhr.readyState == 4) {
                                                    dataDocumentReference = JSON.parse(xhr.responseText);
                                                    $('.dv-background').hide();
                                                    $("#completeModal").modal('show');
                                                    findCarBookingByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            });
        },500);
    }
}

function renderDataCarBooking(dataCarBooking){
    var startDate = DateUtil.coverDateToString(dataCarBooking.startDate);
    var endDate   = DateUtil.coverDateToString(dataCarBooking.endDate);
    $("#originCarBooking").val(dataCarBooking.origin.id);
    $("#destinationCarBooking").val(dataCarBooking.destination.id);
    $("#dateStartCar").val(startDate+" to "+endDate);
    $("#timeStartCar").val(dataCarBooking.startTime);
    $("#timeEndCar").val(dataCarBooking.endTime);
    $("#numOfTravel").val(dataCarBooking.numMember);
    $("#remarkCar").val(dataCarBooking.remark);

    /* validate set car type */
    for(var i=0;i<$("[name=iconCar]").length;i++){
        var carType = dataCarBooking.carTypeCode;
        if(carType == $("[name=iconCar]")[i].getAttribute('id')){
            if('van' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN);
            }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN);
            }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK);
            }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER);
                $("#otherCarDiv").removeClass('hide');
                $("#otherCarType").val(dataCarBooking.otherCarType);
            }
        }
    }

    if($DATA_DOCUMENT.documentReference[0] != null){
        AutocompleteDocRefAppCar.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefInputDocRefAppCar');
    }
}

function findCarBookingByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/carBookings',
        complete: function (xhr) {
            $DATA_CAR_BOOKING = JSON.parse(xhr.responseText);
            if($DATA_CAR_BOOKING != null && $DATA_CAR_BOOKING.length > 0){
                renderDataCarBooking($DATA_CAR_BOOKING[0]);
            }
        }
    });
}

/* manage hotel booking */
function saveHotelBooking(){
    var documentReference 	= $("#docRefHotelInputDocRefAppHotel").attr('data-doc');    //$("#hotelDocRef").val();
    var startDate 			= $("#dateHotel").val();
    var endDate   			= $("#dateHotel").val();
    var singleRoom 			= $("#singleRoom").val();
    var twinRoom      		= $("#twinRoom").val();
    var informationHotel    = $("#hotelInputHotel").attr('data-hotel-code');
    var otherHotel          = $("#otherHotelDiv").val();
    var hotelBookingId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking.id;

    var errorMessage = "";
    if(documentReference == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
    }if(startDate == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
    }if( endDate == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
    }if(informationHotel == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_HOTEL_TO_STAY+"<br/>";
    }

    if(errorMessage){
        $('#warningModal .modal-body').html(errorMessage);
        $('#warningModal').modal('show');
    }else{

        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $DATA_DOCUMENT.id;
            jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocument = JSON.parse(xhr.responseText);
                        if (dataDocument != null) {

                            var jsonDocumentReference = {};
                            jsonDocumentReference['parentId'] = 1;
                            jsonDocumentReference[csrfParameter] = csrfToken;
                            jsonDocumentReference['docReferenceNumber'] = documentReference;
                            jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                            jsonDocumentReference['approveTypeCode'] = $APP_TYPE;
                            jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                            var jsonHotelBooking = {};
                            jsonHotelBooking['parentId'] = 1;
                            jsonHotelBooking[csrfParameter] = csrfToken;
                            jsonHotelBooking['id'] = hotelBookingId;
                            jsonHotelBooking['startDate'] = $("#dateHotel").val().split('to')[0].trim();
                            jsonHotelBooking['endDate'] = $("#dateHotel").val().split('to')[1].trim();
                            jsonHotelBooking['singleRoom'] = singleRoom;
                            jsonHotelBooking['twinRoom'] = twinRoom;
                            jsonHotelBooking['informationHotel'] = informationHotel;
                            jsonHotelBooking['otherHotel'] = otherHotel;
                            jsonHotelBooking['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                            var dataHotelBooking = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/approve/saveHotelBooking',
                                data: jsonHotelBooking,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        dataHotelBooking = JSON.parse(xhr.responseText);

                                        var dataDocumentReference = $.ajax({
                                            type: "POST",
                                            headers: {
                                                Accept: 'application/json'
                                            },
                                            url: session['context'] + '/approve/saveDocumentReference',
                                            data: jsonDocumentReference,
                                            async: false,
                                            complete: function (xhr) {
                                                if (xhr.readyState == 4) {
                                                    dataDocumentReference = JSON.parse(xhr.responseText);
                                                    $('.dv-background').hide();
                                                    $("#completeModal").modal('show');
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            });
        },500);
    }
}

function renderDataHotelBooking(dataHotelBooking){
    var startDate = DateUtil.coverDateToString(dataHotelBooking.startDate);
    var endDate   = DateUtil.coverDateToString(dataHotelBooking.endDate);
    $("#dateHotel").val(startDate+" to "+endDate);
    $("#singleRoom").val(dataHotelBooking.singleRoom != null ? dataHotelBooking.singleRoom : "");
    $("#twinRoom").val(dataHotelBooking.twinRoom != null ? dataHotelBooking.twinRoom : "");
    AutocompleteHotel.renderValue(dataHotelBooking.informationHotel);
    $("#hotelPriceDiv").removeClass('hide');
    $("#hotelPrice").text($("#hotelInputHotel").attr('data-hotel-rate'));
    if(dataHotelBooking.otherHotel != null){
        $("#otherHotelDiv").removeClass('hide');
        $("#otherHotel").val(dataHotelBooking.otherHotel);
    }

    if($DATA_DOCUMENT.documentReference[0] != null){
        AutocompleteDocRefAppHotel.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefHotelInputDocRefAppHotel');
    }
}

/* manage flight */
function getMasterDataAirline(){
    $("#airline").empty().append('<option value="">'+LB.LABEL_SELECT_AIR_LINE+'</option>');
    for(var i=0; i<MASTER_DATA.AIR_LINE.length;i++){
        $("#airline").append(
            '<option value='+MASTER_DATA.AIR_LINE[i].code+'>'+ MASTER_DATA.AIR_LINE[i].description + '</option>');
    }
}

function addTravelMemberFlight(btn){
    $('.dv-background').show();
    window.setTimeout(function() {
        if (btn == "add") {
            $TravelMemberId = null;
            $("#nameMemberInputEmployeeAll").val("");
            $("#empCodeMember").text("");
            $("#positionMember").text("");
            $("#departmentMember").text("");
            $('.dv-background').hide();
            $("#modalAddPerson").modal('show');
        }
    },500);
}

function saveFlightTicket(){
    var documentReference 	= $("#docRefFlightInputDocRefAppFlight").val() == "" ? "" : $("#docRefFlightInputDocRefAppFlight").attr('data-doc');   //$("#docRefFlight").val();
    var member 				= $("#collapseFlightPerson").attr('valueSize');
    var flightTicketId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket.id;

    var errorMessage = "";
    if(documentReference == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
    }if(member == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_TRAVEL_MEMBER_NAME+"<br/>";
    }

    if(errorMessage){
        $('#warningModal .modal-body').html(errorMessage);
        $('#warningModal').modal('show');
    }else{

        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $DATA_DOCUMENT.id;
            jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocument = JSON.parse(xhr.responseText);
                        if (dataDocument != null) {
                            var jsonDocumentReference = {};
                            jsonDocumentReference['parentId'] = 1;
                            jsonDocumentReference[csrfParameter] = csrfToken;
                            jsonDocumentReference['docReferenceNumber'] = documentReference;
                            jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                            jsonDocumentReference['approveTypeCode'] = $APP_TYPE;
                            jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                            var jsonFlightTicket = {};
                            jsonFlightTicket['parentId'] = 1;
                            jsonFlightTicket[csrfParameter] = csrfToken;
                            jsonFlightTicket['id'] = flightTicketId;
                            jsonFlightTicket['flagOneWay'] = $('input[name="travelType"]:checked').val();
                            jsonFlightTicket['fixFlight'] = $('input[name="flightType"]:checked').val();
                            jsonFlightTicket['airlineCode'] = $("#airline")[0].selectedOptions[0].value;
                            jsonFlightTicket['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                            var dataFlightTicket = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/approve/saveFlightTicket',
                                data: jsonFlightTicket,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        dataFlightTicket = JSON.parse(xhr.responseText);

                                        var dataDocumentReference = $.ajax({
                                            type: "POST",
                                            headers: {
                                                Accept: 'application/json'
                                            },
                                            url: session['context'] + '/approve/saveDocumentReference',
                                            data: jsonDocumentReference,
                                            async: false,
                                            complete: function (xhr) {
                                                if (xhr.readyState == 4) {
                                                    dataDocumentReference = JSON.parse(xhr.responseText);
                                                    $('.dv-background').hide();
                                                    $("#completeModal").modal('show');
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            });
        },500);
    }
}

function renderDataFlightTicket(dataFlightTicket){
    $("#airline").val(dataFlightTicket.airlineCode);
    var travelType = document.getElementsByName('travelType');
    for(var i =0;i < travelType.length;i++){
        if(travelType[i].value == dataFlightTicket.flagOneWay){
            travelType[i].checked = true;
            break;
        }
    }

    var flightType = document.getElementsByName('flightType');
    for(var i =0;i < flightType.length;i++){
        if(flightType[i].value == dataFlightTicket.fixFlight){
            flightType[i].checked = true;
            break;
        }
    }

    if($DATA_DOCUMENT.documentReference[0] != null){
        AutocompleteDocRefAppFlight.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefFlightInputDocRefAppFlight');
    }

}

/* manage document */
function updateDocumentApproveItem(){
    BTN = "SAVE";

    if(($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC || $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN) &&  $DATA_TRAVEL_DETAIL.length > 0){

        var reason = "";
        if ($APP_TYPE == MASTER_DATA.APR_TYPE_DOMESTIC) {
            reason = $("#travelReason").val();
        } else if ($APP_TYPE == MASTER_DATA.APR_TYPE_FOREIGN) {
            reason = $("#foreignReason").val();
        }

        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;
        jsonDocumentApproveItem['approveType'] = $APP_TYPE;
        jsonDocumentApproveItem['travelReason'] = reason;
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        $('.dv-background').show();
        window.setTimeout(function() {
            var dataDocumentApproveItem = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/saveDocumentApproveItem',
                data: jsonDocumentApproveItem,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocumentApproveItem = JSON.parse(xhr.responseText);
                        $('.dv-background').hide();
                        STATUS_SAVE_DOCUMENT = "SUCCESS";
                        if ($APP_TYPE == MASTER_DATA.APR_TYPE_CAR) {
                            saveCarBooking();
                        } else if ($APP_TYPE == MASTER_DATA.APR_TYPE_HOTEL) {
                            saveHotelBooking();
                        } else if ($APP_TYPE == MASTER_DATA.APR_TYPE_FIGHT_BOOKING) {
                            saveFlightTicket();
                        } else {
                            $("#completeModal").modal('show');
                        }
                    }
                }
            });
        },500);
    }else if(($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC || $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN) &&  $DATA_TRAVEL_DETAIL.length == 0){
        $('#warningModal .modal-body').html(MSG.MESSAGE_TRAVEL_DETAIL_NO_SIZE);
        $('#warningModal').modal('show');
    }else{

        var reason = "";

        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;
        jsonDocumentApproveItem['approveType'] = $APP_TYPE;
        jsonDocumentApproveItem['travelReason'] = reason;
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        $('.dv-background').show();
        window.setTimeout(function() {
            var dataDocumentApproveItem = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/saveDocumentApproveItem',
                data: jsonDocumentApproveItem,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocumentApproveItem = JSON.parse(xhr.responseText);
                        $('.dv-background').hide();
                        STATUS_SAVE_DOCUMENT = "SUCCESS";
                        if ($APP_TYPE == MASTER_DATA.APR_TYPE_CAR) {
                            saveCarBooking();
                        } else if ($APP_TYPE == MASTER_DATA.APR_TYPE_HOTEL) {
                            saveHotelBooking();
                        } else if ($APP_TYPE == MASTER_DATA.APR_TYPE_FIGHT_BOOKING) {
                            saveFlightTicket();
                        } else {
                            $("#completeModal").modal('show');
                        }
                    }
                }
            });
        },500);
    }
}

function confirmCancelDocument(){
    var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelDocument(){

    $('.dv-background').show();

    window.setTimeout(function() {
        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['id'] = $DATA_DOCUMENT.id;
        jsonData['documentStatus'] = MASTER_DATA.DOC_STATUS_CANCEL;

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/updateDocumentStatus',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);
                    $('#confirmModal').modal('hide');
                    $("#completeModal").modal('show');
                    $('.dv-background').hide();
                    backToMenu();
                    // window.location.href = session.context + '/approve/viewCreateDocDetail?doc=' + $DATA_DOCUMENT.id;
                    // validateDocStatus(dataDocument.documentStatus);
                }
            }
        });
    },500);
}

/* manage document attachment */
function documentAttachment(){
    getDataAttachmentType();
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}

function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function validateFileExtensions(){
    var extensionFile = $('#attachmentType option:selected').attr('extension');

    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

function saveDocumentAttachment(){
    var jsonData = {};
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['attachmentType']  = $("#attachmentType").val();
    jsonData['document']  		= $DATA_DOCUMENT.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename",  $fileUpload.name);
    formData.append("attachmentType",  $("#attachmentType").val());
    formData.append("document",  $DATA_DOCUMENT.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context']+ '/approve/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    $("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center"><button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button></td>' +
                '</tr>'
            );
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}

function deleteDocumentAttachment(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

var BTNDownloadFile;
function downloadDocumentFile(btn){
    BTNDownloadFile = btn;
    var id = BTNDownloadFile.attr('id');
    var fileName = BTNDownloadFile.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}

function copyDocument(){

    $('.dv-background').show();

    window.setTimeout(function() {
        var jsonDocument = {};
        jsonDocument['parentId'] = 1;
        jsonDocument[csrfParameter] = csrfToken;
        jsonDocument['id'] = $DATA_DOCUMENT.id;

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/copyDocument',
            data: jsonDocument,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);
                    $('.dv-background').hide();
                    window.location.href = session.context + '/approve/createDocDetail?doc=' + dataDocument.id;
                }
            }
        });
    },500);
}

function callQRCode(){
    let user_account;

    $DATA_REQUEST_APPROVER.forEach(function (item) {
        if(item.actionState.indexOf("ACC") >= 0){
            user_account = item.userNameApprover;
        }
    })
    window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
}

/* for get employee profile by user name */
function findEmployeeProfileByUserName(userName){

    $DATA_EMPLOYEE = null;
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;
    $DATA_EMPLOYEE = data;
}

function sendRequest(){

    BTN = "SEND";
    $('.dv-background').show();

    window.setTimeout(function() {

        /* update userNameMember */
        var jsonData = {};
        jsonData['parentId']     	= 1;
        jsonData[csrfParameter]  	= csrfToken;
        jsonData['id']    			= $DATA_DOCUMENT.id;
        jsonData['userNameMember']  = getUserNameTravelMember();

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/updateDocumentStatus',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);

                    /* SEND REQUEST */
                    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING || $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN){
                        checkLineApproveFlight();
                    }else{
                        var listJsonDetails = [];
                        var flowType = validateFlowTypeByApproveType($APP_TYPE);

                        var jsonDetails = {};
                        jsonDetails['amount'] = 1;
                        jsonDetails['flowType'] = flowType;
                        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;


                        listJsonDetails.push(jsonDetails);

                        var jsonData = {};
                        jsonData['id'] = $DATA_DOCUMENT.id;
                        jsonData['requester'] = $DATA_DOCUMENT.requester;
                        jsonData['company'] = 1;
                        jsonData['approveType'] = $DATA_DOCUMENT.approveType;
                        jsonData['documentType'] = $DATA_DOCUMENT.documentType;
                        jsonData['tmpDocNumber'] = $DATA_DOCUMENT.tmpDocNumber;
                        jsonData['details'] = JSON.stringify(listJsonDetails);


                        $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/requests/createRequest',
                            data: jsonData,
                            async: false,
                            complete: function (xhr) {
                                if (xhr.readyState == 4) {
                                    $DATA_REQUEST = JSON.parse(xhr.responseText);
                                    if ($DATA_REQUEST != null) {
                                        $('.dv-background').hide();
                                        window.location.href = session.context + '/approve/approveMainMenu';
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });
    },500);
}

function getAuthorizeForLineApprove(userName){

    var flowType;
    if($DATA_DOCUMENT.approveType != MASTER_DATA.APR_TYPE_FOREIGN && $DATA_DOCUMENT.approveType != MASTER_DATA.APR_TYPE_FIGHT_BOOKING) {
        flowType = validateFlowTypeByApproveType($APP_TYPE);

        var listJsonDetails = [];

        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = flowType;
        jsonDetails['costCenter']  = $DATA_DOCUMENT.costCenterCode;


        listJsonDetails.push(jsonDetails);

        var jsonData = {};
        jsonData['requester']   = userName;
        jsonData['approveType'] = $DATA_DOCUMENT.approveType;
        jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
        jsonData['details'] = JSON.stringify(listJsonDetails);

        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/getAuthorizeForLineApprove',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                    renderLineApprove($DATA_LINE_APPROVE);
                }
            }
        });
    }else{
        checkLineApproveFlight();
    }
}

function renderLineApprove(requestApprover){

    findEmployeeProfileByUserName($DATA_DOCUMENT.requester);

    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;
    $("#lineApproverDetail").empty();

    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
                '<span class="step"><span><img src='+validateIMG(requestApprover[i].actionRoleCode)+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
                '<span class="name-idle">'+
                    '<div>'+requestApprover[i].actionRoleName+'</div>'+
                    '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
                    '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
                '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src='+validateIMG(requestApprover[i].actionRoleCode)+' width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+requestApprover[i].actionRoleName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].name+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].position+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    }
}

function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINANCE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
}

function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_CAR){
        return MASTER_DATA.FLOW_TYPE_CAR;
    }else if(approveType == MASTER_DATA.APR_TYPE_HOTEL){
        return MASTER_DATA.FLOW_TYPE_HOTEL;
    }
}

function validateDefaultCarType(docNumber){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findTravelMemberAndExternalMemberBydocRef/'+docNumber,
        complete: function (xhr) {
            var data  = JSON.parse(xhr.responseText);
            NUMBER_MEMBER = data;
            $("#numOfTravel").val(NUMBER_MEMBER);
            if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_CAR && data != null && data  == "3"){
                for(var i=0;i<$("[name=iconCar]").length;i++){
                    if('van' == $("[name=iconCar]")[i].getAttribute('value')){
                        $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN);
                        $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
                        $CarTypeCode = $("[name=iconCar]")[i].getAttribute('id');
                    }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                        $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
                        $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
                    }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                        $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
                        $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
                    }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                        $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
                        $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
                    }
                }
            }
            validateDefaultHotel(docNumber);
        }
    });
}

var TRAVEL;
function validateDefaultHotel(docNumber){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/findTravelDetailsByDocRef/' + docNumber,
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            TRAVEL = data;
            for(var i=0;i<data.length;i++){
                if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_HOTEL) {
                    var startDate = DateUtil.coverDateToString(data[i].startDate);
                    var endDate = DateUtil.coverDateToString(data[i].endDate);
                    $("#dateHotel").val(startDate + " to " + endDate);

                    if(NUMBER_MEMBER != null){
                        $("#singleRoom").val(NUMBER_MEMBER);
                    }
                }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_CAR){
                    var startDate = DateUtil.coverDateToString(data[i].startDate);
                    var endDate = DateUtil.coverDateToString(data[i].endDate);
                    $("#dateStartCar").val(startDate + " to " + endDate);

                    $("#timeStartCar").val(data[i].startTime);
                    $("#timeEndCar").val(data[i].endTime);

                    for(var j=0;j<$DATA_LOCATION_JSON.length;j++){
                        if(data[i].origins.id == $DATA_LOCATION_JSON[j].id){
                            $("#originCarBooking").val(data[i].origins.id);
                        }
                        else if(data[i].destinations.id == $DATA_LOCATION_JSON[j].id){
                            $("#destinationCarBooking").val(data[i].destinations.id);
                        }
                    }
                }
            }
        }
    });
}

function validateMemberFlight(docNumber){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/findTravelMemberByDocRef/' + docNumber,
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            LIST_TRAVEL_MEMBER = data;

            checkLineApproveFlight();
        }
    });
}

function getUserNameTravelMember(){

    var userNameMember = "";
    if($DATA_TRAVEL_MEMBER.length > 0){
        $DATA_TRAVEL_MEMBER.forEach(function (data){
            userNameMember += data.memberUser+",";
        });
    }

    if(userNameMember != ""){
        return userNameMember.substr(0,userNameMember.lastIndexOf(","));
    }
}

var dataCheckLine;

function checkLineApprove(){
    getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
}
function checkLineApproveFlight(){

    var psa_origin = "";
    var psa_destination = "";
    var location_type = "";
    var zone_code = "";

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+$DATA_DOCUMENT.requester,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseText != null){
                var dataEmployee = JSON.parse(xhr.responseText);
                var cLevel = dataEmployee.EESG_ID;
                var psa_requester = dataEmployee.Personal_PSA_ID;

                if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING  && $("#docRefFlightInputDocRefAppFlight").val() != ""){

                    var docNumber = $("#docRefFlightInputDocRefAppFlight").attr("data-doc");

                    $.ajax({
                        type: "GET",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/findTravelDetailsByDocRef/' + docNumber,
                        complete: function (xhr) {
                            dataCheckLine = JSON.parse(xhr.responseText);
                            if (dataCheckLine != null) {
                                psa_origin = dataCheckLine[0].origins.psaCode;
                                psa_destination = dataCheckLine[0].destination.psaCode;
                                location_type = dataCheckLine[0].origins.locationType;
                                zone_code = dataCheckLine[0].destination.zoneCode;

                                validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code);
                            }
                        }
                    });
                }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN){
                    var documentAppItem = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;
                    $.ajax({
                        type: "GET",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
                        complete: function (xhr) {
                            if(xhr.responseText != null){
                                dataCheckLine = JSON.parse(xhr.responseText);
                                if (dataCheckLine.length > 0) {
                                    psa_origin = dataCheckLine[0].origins.psaCode;
                                    psa_destination = dataCheckLine[0].destination.psaCode;
                                    location_type = dataCheckLine[0].origins.locationType;
                                    zone_code = dataCheckLine[0].destination.zoneCode;

                                    validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code);
                                }else{
                                    var listJsonDetails = [];

                                    var jsonDetails = {};
                                    jsonDetails['amount'] = 1;
                                    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_H003;
                                    jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

                                    listJsonDetails.push(jsonDetails);

                                    getLineApprove(listJsonDetails);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}

function validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code,btn) {
    if (psa_origin != "" && psa_destination != "" && location_type != "" && zone_code != "") {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: session['context'] + '/approve/validateFlowTypeApprove',
            data: {
                cLevel: parseInt(cLevel),
                psa_requester: psa_requester,
                psa_origin: psa_origin,
                psa_destination: psa_destination,
                location_type: location_type,
                zone_code: zone_code
            },
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != null) {
                        var flowTypeName = xhr.responseText;
                        var flowType = "";
                        if (flowTypeName == "H001" || flowTypeName == "H002") {
                            flowType = MASTER_DATA.FLOW_TYPE_H001;
                        } else if (flowTypeName == "H003") {
                            flowType = MASTER_DATA.FLOW_TYPE_H003;
                        } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                            flowType = MASTER_DATA.FLOW_TYPE_H004;
                        }

                        if (flowType != "") {
                            var listJsonDetails = [];

                            var jsonDetails = {};
                            jsonDetails['amount'] = 1;
                            jsonDetails['flowType'] = flowType;
                            jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

                            listJsonDetails.push(jsonDetails);

                            if(BTN != "SEND"){
                                getLineApprove(listJsonDetails);
                            }else{

                                var jsonData = {};
                                jsonData['id'] = $DATA_DOCUMENT.id;
                                jsonData['requester'] = $DATA_DOCUMENT.requester;
                                jsonData['company'] = 1;
                                jsonData['approveType'] = $DATA_DOCUMENT.approveType;
                                jsonData['documentType'] = $DATA_DOCUMENT.documentType;
                                jsonData['tmpDocNumber'] = $DATA_DOCUMENT.tmpDocNumber;
                                jsonData['details'] = JSON.stringify(listJsonDetails);

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/requests/createRequest',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $DATA_REQUEST = JSON.parse(xhr.responseText);
                                            if ($DATA_REQUEST != null) {
                                                $('.dv-background').hide();
                                                window.location.href = session.context + '/approve/approveMainMenu';
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }
        });
    }
}

function backToMenu() {

    window.location.href = session.context + '/approve/approveMainMenu';
}

function linkCarForRent() {
    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterReserveCarLink",
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseText != null){
                var dataParam = JSON.parse(xhr.responseText);

                var data = $.ajax({
                    url: session.context + "/parameters/" + dataParam.id + "/details",
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var paramDetail = JSON.parse(xhr.responseText);
                            if(paramDetail.content != null && paramDetail.content.length > 0){
                                var dataDetail = paramDetail.content;
                                $('#gridLinkCar').empty();
                                if (dataDetail.length != 0) {
                                    for (var i = 0; i < dataDetail.length; i++) {
                                        $('#gridLinkCar').append('' +
                                            '<tr id="' + dataDetail[i].id + '">' +
                                            '<td align="center">' + dataDetail[i].code + '</td>' +
                                            '<td align="center">' + dataDetail[i].description + '</td>' +
                                            '<td align="center"><a href="'+(dataDetail[i].variable1 == null ? "-" : dataDetail[i].variable1)+'" style="color:blue" >'+(dataDetail[i].variable1 == null ? "-" : dataDetail[i].variable1)+'</a></td>' +
                                            '</tr>'
                                        );
                                    }
                                }
                                $("#modalLinkCar").modal('show');
                            }
                        }
                    }
                });
            }
        }
    });
}

/* function getAuthorize for speacial case */
function getLineApprove(listJsonDetails){
    var jsonData = {};
    jsonData['requester']   = $DATA_DOCUMENT.requester;
    jsonData['approveType'] = $DATA_DOCUMENT.approveType;
    jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
    jsonData['details']     = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/getAuthorizeForLineApprove',
        data: jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                renderLineApprove($DATA_LINE_APPROVE);
            }
        }
    });
}


function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#company").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#company").attr('pa',paData.PaCode);
    }else{
        $("#company").text("-");
        $("#company").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#department").attr('psa',psaData.PsaCode);
    }else{
        $("#department").text("-");
        $("#department").attr('psa',"");
    }
}