var type = null;
var $DATA_TRAVEL_DETAIL;
var $DATA_TRAVEL_MEMBER;
var $DATA_EXTERNAL_MEMBER;
var $DATA_DOCUMENT_ATTACHMENT;
var $DATA_LOCATION_JSON;
var $DATA_LOCATION_DESTINATION;
var $STATUS;
var $TravelDetailId;
var $TravelMemberId;
var $ExternalMemberId;
var $CarTypeCode = null;
var $fileUpload="";

var min,max;
var FLAG_CAR = null;
var FLAG_FLIGHT = null;
var FLAG_BED = null;
var FLAG_BED_TWIN = null;
var $DATA_EMPLOYEE;
var listJsonFlowDetails = [];
var DATA_PARAMETER_DUEDATE_ADVANCE;
var CAR_TYPE_ACTIVE = false;
var HOTEL_TYPE_ACTIVE = false;
var FLIGHT_TYPE_ACTIVE = false;
var ADVANCE_TYPE_ACTIVE = false;

var STATUS_SAVE_DOCUMENT;
var FLOW_CAR_ACTIVE = false;
var $DATA_DOCUMENT_SET;
var DATA_BANK_NUMBER;
var DATA_LINE_APR_MEMBER;

var BTN_ACTION;
var DATA_RESULT_CAR;
var DATA_RESULT_HOTEL;
var DATA_RESULT_FLIGHT;
var DATA_RESULT_ADVANCE;
var FLAG_UPDATE = 0;

var FLAG_REQUEST_MEMBER = 0;
var FLAG_CREATE_DOC_MEMBER = 0;
var SPEACIAL_FLOW;
var psa_default = "0001";
var FLAG_USER_SAME_COSTCENTER = 0;
var ADD_MAIL_ADMIN = true;
var MAIL_ADMIN = null;

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-50) + 'px';
    }
}

$(document).ready(function () {
    $('.dv-background').show();
	/* call master data */
    readMasterDataDocType();
    readMasterDataDocStatus();
    readMasterDataAprType();
    readMasterDataCarType();
    readMasterDataAirline();
    readMasterDataAttachmentType();
    readMasterDataHotel();
    readMasterDataFlowType();

	
	$.material.init();
    moveFixedActionButton();
    window.onresize = function (event) {
        moveFixedActionButton()
    };

    $("#222").keypress(function (e) {
        e.preventDefault();
    });

    /* validate BTN QR code */
    if($DATA_DOCUMENT.request != null){
		$("#divBtnQRCode").removeClass('hide');
	}


    window.setTimeout(function(){
   	 	validateApproveType();
   	 	validateDocStatus($DATA_DOCUMENT.documentStatus);
   	 	// getMasterDataAirline();
   	 	// setUpDefaultCarType();
   	 	// setUpDefaultBedType();
        $("#advancesMoney").autoNumeric('init',{vMin:0});
        $("#advancesAmount").autoNumeric('init',{vMin:0});
   },1000);
    
    /* -======================= event date time =========================- */

    $("#dateStart").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#dateStartCar").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#dateHotel").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
        
    });

    var firstDate = $("#borrowDate").flatpickr({
        noCalendar: false,
        enableTime: false,
        dateFormat : "d/m/Y",
        onChange: function(selectedDates, dateStr, instance) {
            min = dateStr;
            lastDate.set('minDate', min);
        }
      });
      
	var lastDate = $("#advanceEndDate").flatpickr({
	noCalendar: false,
	enableTime: false,
	dateFormat : "d/m/Y",
	onChange : function(selectDates,dateStr,instance){
		max = dateStr;
	  firstDate.set('maxDate',max)
	}
	});

    $('#timeStart').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true, 
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeStartCar').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeEnd').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true, 
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeEndCar').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    /* -============== end event date time ===============- */
    
    /* render travel detail */
    window.setTimeout(function(){
        /* render value document */
        defaultBankNumber($DATA_DOCUMENT.requester);
        findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
        $("#docNumber").text($DATA_DOCUMENT.docNumber);
        $("#sendTime").text($DATA_DOCUMENT.sendDate == null ? "SEND TIME" : $DATA_DOCUMENT.sendDate);
        $("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
        if($DATA_EMPLOYEE != null) {
            $("#requester").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
            $("#empCode").text($DATA_EMPLOYEE.Personal_ID);
            $("#position").text($DATA_EMPLOYEE.PositionTH);
            $("#costCenter").text($DATA_DOCUMENT.costCenterCode);

        }

        if($DATA_DOCUMENT.companyCode){
            findByPaCode($DATA_DOCUMENT.companyCode);
        }

        if($DATA_DOCUMENT.psa){
            findByPsaCode($DATA_DOCUMENT.psa);
        }

        if($DATA_DOCUMENT.createdBy == null){
            findEmployeeProfileByUserName($USERNAME);
            $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        }else{
            findEmployeeProfileByUserName($DATA_DOCUMENT.createdBy);
            $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        }

        if($DATA_DOCUMENT.documentApprove.documentApproveItem.length > 0){
    	    var dataDocApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    	    for(var i=0;i<dataDocApproveItem.length;i++){
    	        if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC ||
                    dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                    findTravelDetailsByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findTravelMembersByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findExternalMembersByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                }
    	        else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_CAR){
                    renderDataCarBooking(dataDocApproveItem[i].carBookings);
                }else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_HOTEL){
                    renderDataHotelBooking(dataDocApproveItem[i].hotelBooking);
                }else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
                    renderDataFlightTicket(dataDocApproveItem[i].flightTicket);
                }
            }
        }


        findAdminByPapsaAndRequester($DATA_DOCUMENT.requester);
        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
    	if($DATA_DOCUMENT.documentAdvance != null){
    		renderDocumentAdvance($DATA_DOCUMENT.documentAdvance);
    	}else{
            $("#advancesMoney").autoNumeric('set',0);
            $("#advancesAmount").autoNumeric('set',0);
            $("#borrowDate").val("");
            $("#advanceEndDate").val("");
            $("#dueDate").text("");
    	}

        $('.dv-background').hide();
   },3000);
    
    /* travel member panel */
    $("#nameMemberInputEmployeeAll").on('keyup',function(){
        AutocompleteEmployeeAll.setId('nameMemberInputEmployeeAll');
        AutocompleteEmployeeAll.search($("#nameMemberInputEmployeeAll").val(),"nameMemberInputEmployeeAll");
    });

    $("#nameMemberInputEmployeeAll").on('blur',function(){
        if($("#nameMemberInputEmployeeAll").val() != ""){
            $("#empCodeMember").text($("#nameMemberInputEmployeeAll").attr("data-personalId"));
            $("#empCodeMember").attr("data-empCode",$("#nameMemberInputEmployeeAll").attr("data-empCode"));
            $("#positionMember").text($("#nameMemberInputEmployeeAll").attr("data-position"));
            $("#departmentMember").text($("#nameMemberInputEmployeeAll").attr("data-deptName"));
        }else{
            $("#empCodeMember").text("");
            $("#positionMember").text("");
            $("#departmentMember").text("");
        }
    });


    $("#advancesAmount").on("blur",function (){
        var amount = $("#advancesAmount").autoNumeric('get');

        /* validate advance out standing */

        var dataAdvanceOutstandings = $.ajax({
            url: session.context + "/advanceOutstandings/findAdvanceOutstanding?venderNo="+$DATA_DOCUMENT.personalId+"&userName="+$DATA_DOCUMENT.requester.toLowerCase(),
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        console.info(dataAdvanceOutstandings);
        if(dataAdvanceOutstandings){
            if(dataAdvanceOutstandings.amount != 0){
                clearAdvanceValue();
                var errorMessage = MSG.MESSAGE_HAVE_ADVANCE_OUTSTANDING+"<br/>";
                $('#warningModal .modal-body').html(errorMessage);
                $('#warningModal').modal('show');
            }else{
                $("#advancesMoney").autoNumeric('set',amount);
                if($("#advancesAmount").autoNumeric('get') != ""){
                    ADVANCE_TYPE_ACTIVE = true;
                    setFlowTypeAdvance();
                }else{
                    ADVANCE_TYPE_ACTIVE = false;
                    setFlowTypeAdvance();
                }
            }
        }else{
            $("#advancesMoney").autoNumeric('set',amount);
            if($("#advancesAmount").autoNumeric('get') != ""){
                ADVANCE_TYPE_ACTIVE = true;
                setFlowTypeAdvance();
            }else{
                ADVANCE_TYPE_ACTIVE = false;
                setFlowTypeAdvance();
            }
        }
    });


	$("#advanceEndDate").on("blur",function(){
   	 	if($("#advanceEndDate").val() != ""){
            var advEndDate = DateUtil.coverStringToDate($("#advanceEndDate").val());
            advEndDate =  DateUtil.addDays(advEndDate , getParameterDueDateFromPACode());
            $("#dueDate").text(DateUtil.coverDateToString(advEndDate));
            ADVANCE_TYPE_ACTIVE = true;
            setFlowTypeAdvance();
        }else{
            $("#dueDate").text("");
            ADVANCE_TYPE_ACTIVE = false;
            setFlowTypeAdvance();
        }
    });

	$("#borrowDate").on('change',function () {
	    if($("#borrowDate").val() != ""){
            ADVANCE_TYPE_ACTIVE = true;
            setFlowTypeAdvance();
        }else{
            if($("#advanceEndDate").val() == ""){
                $("#dueDate").text("");
            }
            ADVANCE_TYPE_ACTIVE = false;
            setFlowTypeAdvance();
        }
    });

	$("#bankNumber").on('change',function () {
       if($("#bankNumber").val() != ""){
           ADVANCE_TYPE_ACTIVE = true;
           setFlowTypeAdvance();
       }else{
           ADVANCE_TYPE_ACTIVE = false;
           setFlowTypeAdvance();
       }
    });

    // $("#collapseAdvanceHeader").on('click',function (){
    //     defaultBankNumber($DATA_DOCUMENT.requester);
    // });
    
    /* event hide modal complete */
    $('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});
    
    /* manage cancel document */
    $("#confirmCancel").on('click',function(){
    	cancelDocument();
    });
    
    /* upload document attachment */
    $("#browseFile").on('click',function(){
    	if($("#attachmentType").val() == ""){
    		var errorMessage = MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ATTACHMENT_TYPE+"<br/>";
    		$('#warningModal .modal-body').html(errorMessage);
    		$('#warningModal').modal('show');
    	}else{
    		var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
    		$("#uploadFileDocument").attr('accept',extension);
    		$("#uploadFileDocument").click();
    	}
    });
    
    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
        // validateFileExtensions();
    });
    
    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });
 
    /* event confirm delete */
    $("#confirmDelete").on('click',function (){
    	if($('#typeDataDelete').val() == "TRAVEL_DETAIL"){
    		deleteTravelDetail($("#idItemDelete").val());
    	}else if($('#typeDataDelete').val() == "TRAVEL_MEMBER"){
    		deleteTravelMember($("#idItemDelete").val());
    	}else if($('#typeDataDelete').val() == "DOCUMENT_ATTACHMENT"){
    		deleteDocumentAttachment($("#idItemDelete").val());
    	}else{
    		deleteExternalMember($("#idItemDelete").val());
    	}
    });

    $("#origin").on('change',function(){
		for(var i=0;i<$DATA_LOCATION_JSON.length;i++){
			if($("#origin").val() == $DATA_LOCATION_JSON[i].id && $DATA_LOCATION_JSON[i].flagOther == "Y"){
				$("#otherOriginDiv").removeClass('hide');
			}else{
                $("#otherOriginDiv").addClass('hide');
            }
		}

		var locationType = "D";
		if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            locationType = "F";
        }
        $.ajax({
            url: session['context'] + "/locations/findByLocationType",
            type: "GET",
            data: {locationType: locationType},
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if(xhr.responseText){
                        var dataLocation = JSON.parse(xhr.responseText);

                        var flagSplice = false;
                        if($("#origin").val() != "") {
                            for (var j = 0; j < dataLocation.length; j++) {
                                if (dataLocation[j].id == parseInt($("#origin").val())) {
                                    dataLocation.splice(j, 1);
                                    flagSplice = true;
                                }
                            }

                            if(flagSplice){
                                $("#destination").empty().append('<option value=""></option>');
                                for(var i=0;i<dataLocation.length;i++){
                                    $("#destination").append(
                                        '<option value=' + dataLocation[i].id + '>' + dataLocation[i].description + '</option>');
                                }
                            }
                        }
                    }
                }
            }
        });
	});

    $("#destination").on('change',function(){
        for(var i=0;i<$DATA_LOCATION_JSON.length;i++){
            if($("#destination").val() == $DATA_LOCATION_JSON[i].id && $DATA_LOCATION_JSON[i].flagOther == "Y"){
                $("#otherDestinationDiv").removeClass('hide');
            }else{
                $("#otherDestinationDiv").addClass('hide');
            }
        }
    });

    $("#imageCalendar").on('click',function () {
        $("#dateStart").focus();
    });


    $("#imageCalendarBorrow").on('click',function(){
        $("#borrowDate").focus();
    });

    $("#imageCalendarAdvanceEndDate").on('click',function(){
        $("#advanceEndDate").focus();
    });

    $("#carReserve").on('click',function(){
        if($("#carReserve").is(':checked')){
            $("#linkCarMis").removeClass('hide');
            $("#linkCarMisForm").removeClass('hide');
        }else{
            $("#linkCarMis").addClass('hide');
            $("#linkCarMisForm").addClass('hide');

            if(!$("#hotelReserve").is(':checked') && !$("#flightReserve").is(':checked')){
                ADD_MAIL_ADMIN = false;
            }

            validateApproveItem("CAR");
        }
    });


    $("#hotelReserve").on('click',function(){
        if($("#hotelReserve").is(':checked')){
            $("#linkHotel").removeClass('hide');
        }else{
            $("#linkHotel").addClass('hide');

            if(!$("#carReserve").is(':checked') && !$("#flightReserve").is(':checked')){
                ADD_MAIL_ADMIN = false;
            }

            validateApproveItem("HOTEL");
        }
    });

    $("#flightReserve").on('click',function(){
        if($("#flightReserve").is(':checked')){
            $("#linkFlightTicket").removeClass('hide');
            FLIGHT_TYPE_ACTIVE = true;
            setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
        }else{
            FLIGHT_TYPE_ACTIVE = false;
            $("#linkFlightTicket").addClass('hide');

            if(!$("#carReserve").is(':checked') && !$("#hotelReserve").is(':checked')){
                ADD_MAIL_ADMIN = false;
            }

            validateApproveItem("FLIGHT");
        }
    });


});

function findCostCenter(costCenter) {
    $('.dv-background').show();
    setTimeout(function () {

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data){
            var costCenterSplit = data.profile.split('#');
            if(costCenterSplit[2] != "0"){
                var paPsaSplit = costCenterSplit[2].split('-');
                $("#company").text(paPsaSplit[0]+" : "+paPsaSplit[1]);
                $("#company").attr('pa',paPsaSplit[0]);
                $("#department").text(paPsaSplit[2]+" : "+paPsaSplit[3]);
                $("#department").attr('psa',paPsaSplit[2]);
                $('.dv-background').hide();
            }else{
                $('.dv-background').hide();
            }
        }

    },1000);

}

function validateDocStatus(documentStatus){
	/* update by siriradC.  2017.08.04 */
  	 if(documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
    	$("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
    	$("#ribbon").addClass("ribbon-status-cancel");
    	$("#ribbon").attr("data-content","CANCEL");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
    	$("#ribbon").addClass("ribbon-status-reject");
    	$("#ribbon").attr("data-content","REJECT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
    	$("#ribbon").addClass("ribbon-status-complete");
    	$("#ribbon").attr("data-content","COMPLETE");
    }
}

function validateApproveType(){
    
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
    	$("#approveTypeLB").text(LB.LABEL_TRAVEL_DEMESTIC);
    	getMasterDataLocation("D");
        $("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
    	$("#approveTypeLB").text(LB.LABEL_TRAVEL_FOREIGN);
    	getMasterDataLocation("F");
    	$("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
    }
    
}

/* PANEL TRAVEL DETAIL */
function addTravelDetail(btn){
	$STATUS = btn;
	if(btn == "add"){
		$TravelDetailId = null;
        /* validate default origin */
        if($DATA_TRAVEL_DETAIL.length == 0 && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
            findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
            for(var i=0;i<$DATA_LOCATION_JSON.length;i++){
                if($DATA_LOCATION_JSON[i].psaCode == $DATA_EMPLOYEE.Personal_PSA_ID){
                    $("#origin").val($DATA_LOCATION_JSON[i].id);
                }
            }
            var locationType = "D";

            $.ajax({
                url: session['context'] + "/locations/findByLocationType",
                type: "GET",
                data: {locationType: locationType},
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if(xhr.responseText){
                            var dataLocation = JSON.parse(xhr.responseText);

                            var flagSplice = false;
                            if($("#origin").val() != "") {
                                for (var j = 0; j < dataLocation.length; j++) {
                                    if (dataLocation[j].id == parseInt($("#origin").val())) {
                                        dataLocation.splice(j, 1);
                                        flagSplice = true;
                                    }
                                }

                                if(flagSplice){
                                    $("#destination").empty().append('<option value=""></option>');
                                    for(var i=0;i<dataLocation.length;i++){
                                        $("#destination").append(
                                            '<option value=' + dataLocation[i].id + '>' + dataLocation[i].description + '</option>');
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }else if($DATA_TRAVEL_DETAIL.length == 0 && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            for(var i=0;i<$DATA_LOCATION_JSON.length;i++){
                if($DATA_LOCATION_JSON[i].psaCode == psa_default){
                    $("#origin").val($DATA_LOCATION_JSON[i].id);
                }
            }
            var locationType = "F";

            $.ajax({
                url: session['context'] + "/locations/findByLocationType",
                type: "GET",
                data: {locationType: locationType},
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if(xhr.responseText){
                            var dataLocation = JSON.parse(xhr.responseText);

                            var flagSplice = false;
                            if($("#origin").val() != "") {
                                for (var j = 0; j < dataLocation.length; j++) {
                                    if (dataLocation[j].id == parseInt($("#origin").val())) {
                                        dataLocation.splice(j, 1);
                                        flagSplice = true;
                                    }
                                }

                                if(flagSplice){
                                    $("#destination").empty().append('<option value=""></option>');
                                    for(var i=0;i<dataLocation.length;i++){
                                        $("#destination").append(
                                            '<option value=' + dataLocation[i].id + '>' + dataLocation[i].description + '</option>');
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }else{
            $("#origin").val("");
        }
		$("#destination").val("");
		$("#dateStart").val("");
		$("#timeStart").val("");
		$("#timeEnd").val("");
		$("#remark").val("");
		$("#modalAddDetail").modal('show');
	}
}

function insertDataTravel(){
	
	var originData      = $("#origin")[0].selectedOptions[0].value;
	var destinationData = $("#destination")[0].selectedOptions[0].value;
	var dateStartData	= ($("#dateStart").val() == "" ? "" : $("#dateStart").val().split("to")[0].trim());
	var timeStartData	= $("#timeStart").val();
	var dateEndData	    = ($("#dateStart").val() == "" ? "" : $("#dateStart").val().split("to")[1].trim());
	var timeEndData	    = $("#timeEnd").val();
	var remarkData	    = $("#remark").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
	
	var errorMessage = "";
	
	if(originData == "" ){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ORIGIN+"<br/>";
	}if (destinationData == "" ){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DESTINATION+"<br/>";
	}if(dateStartData == ""){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
	}if(timeStartData == ""){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_TIME+"<br/>";
	}if( dateEndData == ""){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
	}if(timeEndData == ""){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_TIME+"<br/>";
	}
	
	if(errorMessage){
		$('#warningModal .modal-body').html(errorMessage);
		$('#warningModal').modal('show');
	}
	else{
        saveTravelDetail(originData,destinationData,dateStartData,timeStartData,dateEndData,timeEndData,remarkData);
    }
}

function getMasterDataLocation(locationType){
    var locationString = $.ajax({
        url: session['context'] + "/locations/findByLocationType",
        type: "GET",
        data: {locationType: locationType},
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if(xhr.responseText){
                    $DATA_LOCATION_JSON = JSON.parse(xhr.responseText);

                    $("#origin").empty().append('<option value=""></option>');
                    for (var i = 0; i < $DATA_LOCATION_JSON.length; i++) {
                        $("#origin").append(
                            '<option value=' + $DATA_LOCATION_JSON[i].id + '>' + $DATA_LOCATION_JSON[i].description + '</option>');
                    }
                }
            }
        }
    });

}

var docOverlap = [];
function saveTravelDetail(originData,destinationData,dateStartData,timeStartData,dateEndData,timeEndData,remarkData){

    var documentApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    var docApproveItemId = null;
    if(documentApproveItem.length == 1){
        docApproveItemId = documentApproveItem[0].id;
    }else{
        for(var i=0;i<documentApproveItem.length;i++){
            if($DATA_DOCUMENT.approveType == $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                    docApproveItemId = documentApproveItem[i].id;
                }
            }else if($DATA_DOCUMENT.approveType == $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                    docApproveItemId = documentApproveItem[i].id;
                }
            }
        }
    }

    if(docApproveItemId != null){
        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $TravelDetailId;
            jsonData['origin'] = originData;
            jsonData['destination'] = destinationData;
            jsonData['startDate'] = dateStartData;
            jsonData['endDate'] = dateEndData;
            jsonData['startTime'] = timeStartData;
            jsonData['endTime'] = timeEndData;
            jsonData['remark'] = remarkData;
            jsonData['otherOrigin'] = $("#otherOrigin").val();
            jsonData['otherDestination'] = $("#otherDestination").val();
            jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItem[0].id;

            var dataDiffDays = 0;
            $.ajax({
                type: "GET",
                headers: {
                    Accept: 'application/json'
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: session['context'] + '/approve/calculateDiffDate',
                data: {
                    startDate: dateStartData,
                    endDate: dateEndData,
                    startTime: timeStartData,
                    endTime: timeEndData
                },
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDiffDays = JSON.parse(xhr.responseText);
                        jsonData['travelDays'] = dataDiffDays;

                        var dataValidate = {};
                        dataValidate['dateStartData']   = dateStartData;
                        dataValidate['dateEndData']     = dateEndData;
                        dataValidate['timeStartData']   = timeStartData;
                        dataValidate['timeEndData']     = timeEndData;
                        dataValidate['requester']       = $DATA_DOCUMENT.requester;
                        dataValidate['docNumber']       = $DATA_DOCUMENT.docNumber;

                        $.ajax({
                            type: "GET",
                            headers: {
                                Accept: 'application/json'
                            },
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            url: session['context'] + '/approve/validateDateOverlap',
                            data: dataValidate,
                            async: false,
                            complete: function (xhr) {
                                if (xhr.readyState == 4) {
                                    if(xhr.responseText){
                                        var resultDateOverlap = JSON.parse(xhr.responseText);
                                        docOverlap = resultDateOverlap;
                                        console.info("><><<< validateDateOverlap ><><><");
                                        console.info(resultDateOverlap);
                                        if (resultDateOverlap == "SUCCESS") {
                                            var dataTravel = $.ajax({
                                                type: "POST",
                                                headers: {
                                                    Accept: 'application/json'
                                                },
                                                url: session['context'] + '/approve/saveTravelDetail',
                                                data: jsonData,
                                                async: false,
                                                complete: function (xhr) {
                                                    if (xhr.readyState == 4) {
                                                        if (xhr.status == 200) {
                                                            $('.dv-background').hide();
                                                            findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
                                            $('.dv-background').hide();
                                            var docNumberOverlap = resultDateOverlap.split("#");
                                            for(var i=0;i<docNumberOverlap.length;i++){
                                                if(docNumberOverlap[i] == "FAIL"){
                                                    docNumberOverlap.splice(i,1);
                                                }
                                            }
                                            docOverlap = docNumberOverlap;
                                            var errorMessage = MSG.MESSAGE_TIME_OVERLAP + "<br/>";
                                            if(docOverlap.length > 0){
                                                for(var i=0;i<docOverlap.length;i++){
                                                    errorMessage += docOverlap[i]+ "<br/>"
                                                }
                                            }

                                            if(errorMessage){
                                                $('#warningModal .modal-body').html(errorMessage);
                                                $('#warningModal').modal('show');
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });
        },500);
    }
}

var remark;
function remarkDetail(obj){
	remark = obj;   
    for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
    	if($DATA_TRAVEL_DETAIL[i].id == parseInt(obj.id)){
    		$("#alertModal").modal('show');
    	    $("label[id=detailAlert]").text($DATA_TRAVEL_DETAIL[i].remark);
    	}
    }
}

function renderTravelDetail(dataTravel){
	$("#collapseHeaderTravel").empty();
	if(dataTravel.length > 0){
		for(var i=0;i<dataTravel.length;i++){
			var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
			var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);	
			
			$("#collapseHeaderTravel").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                '<div class="form-group" style="margin-bottom: 0px">'+
                '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)"><img src='+IMG.DELETE+' title="DELETE" width="50px" id='+dataTravel[i].id+'  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataTravel[i].id+'\');$(\'#typeDataDelete\').val(\'TRAVEL_DETAIL\');" /></a></div>'+
                '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" >'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:18px;"><b>'+dataTravel[i].origin.description+'</b></label></div>'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-4" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" style="padding-left: 0px;padding-right: 0px;" >'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+dataTravel[i].destination.description+'</b></label></div>'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-2" id='+dataTravel[i].id+' onclick="editTravelDetail(this)" style="padding-left: 0px;padding-right: 0px;">'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;">'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>');
		}
	}
	$("#modalAddDetail").modal('hide');
}

function deleteTravelDetail(id){

	var jsonParams2 = {};
	jsonParams2['parentId']     = 1;
	jsonParams2[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteTravelDetail/'+id,
        data: jsonParams2,
        complete: function (xhr) {
        	findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
        	$('#deleteItemModal').modal('hide');
        }
    });
}

function editTravelDetail(obj){
	$STATUS = "update";
	$TravelDetailId = obj.id;
	$("#modalAddDetail").modal('show');
	renderDataToModalEditTravelDetail(obj.id);
}

function findTravelDetailsByDocumentApproveItemId(documentAppItem){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
        complete: function (xhr) {
        	$DATA_TRAVEL_DETAIL = JSON.parse(xhr.responseText);
            renderTravelDetail($DATA_TRAVEL_DETAIL);
            // if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                setFlowTypeDoc();
            // }
        }
    });  
}

function renderDataToModalEditTravelDetail(id){
	for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
    	if($DATA_TRAVEL_DETAIL[i].id == parseInt(id)){
    		$("#origin").val($DATA_TRAVEL_DETAIL[i].origin.id);
            $("#origin").change();
    		$("#destination").val($DATA_TRAVEL_DETAIL[i].destination.id);
    		if($DATA_TRAVEL_DETAIL[i].destination.flagOther == "Y"){
                $("#otherDestinationDiv").removeClass('hide');
                $("#otherDestination").val($DATA_TRAVEL_DETAIL[i].otherDestination)
            }
    		$("#dateStart").val(DateUtil.coverDateToString($DATA_TRAVEL_DETAIL[i].startDate)+" "+"to"+" "+DateUtil.coverDateToString($DATA_TRAVEL_DETAIL[i].endDate));
    		$("#timeStart").val($DATA_TRAVEL_DETAIL[i].startTime);
    		$("#timeEnd").val($DATA_TRAVEL_DETAIL[i].endTime);
    		$("#remark").val($DATA_TRAVEL_DETAIL[i].remark);
    	}
    }
}

/* PANEL MEMBER PERSON */
function addTravelMember(btn){
	if(btn == "add"){
		$TravelMemberId = null;
		$("#nameMemberInputEmployeeAll").val("");
		$("#empCodeMember").text("");
		$("#positionMember").text("");
		$("#departmentMember").text("");
		$("#modalAddPerson").modal('show');
	}
}

function renderDataTravelMember(dataTravelMember){
    $("#collapseHeaderMember").empty();
    $("#personMember").text(dataTravelMember.length);
    if(dataTravelMember.length > 0){
        for(var i=0;i<dataTravelMember.length;i++){
            findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
            var memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
            var positionMember = $DATA_EMPLOYEE.PositionTH;
            var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;

            if($DATA_TRAVEL_MEMBER[i].memberUser == $DATA_DOCUMENT.requester){
                $("#collapseHeaderMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                    '</div>'+
                    // '<div class="col-sm-3" style="padding-right: 0px;padding-left: 0px;">'+
                    // '<a href="javascript:void(0)">'+renderButton("flagFlight",dataTravelMember[i].flagFlight,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagCar",dataTravelMember[i].flagCar,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagBed",dataTravelMember[i].flagBed,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagBedTwin",dataTravelMember[i].flagBedTwin,dataTravelMember[i].id)+'</a>'+
                    // '</div >'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }else{
                $("#collapseHeaderMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-1" style="padding-left: 0px;width: 50px;"><a href="javascript:void(0)"><img src='+IMG.DELETE+' title="DELETE" style="padding-right: 10px;" width="50px" id='+dataTravelMember[i].id+'  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataTravelMember[i].id+'\');$(\'#typeDataDelete\').val(\'TRAVEL_MEMBER\');" /></a></div>'+
                    '<div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;margin-left: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)"  >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                    '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                    '</div>'+
                    // '<div class="col-sm-3" style="padding-right: 0px;padding-left: 0px;">'+
                    // '<a href="javascript:void(0)">'+renderButton("flagFlight",dataTravelMember[i].flagFlight,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagCar",dataTravelMember[i].flagCar,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagBed",dataTravelMember[i].flagBed,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                    // '<a href="javascript:void(0)">'+renderButton("flagBedTwin",dataTravelMember[i].flagBedTwin,dataTravelMember[i].id)+'</a>'+
                    // '</div >'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }
	
	
	$("#modalAddPerson").modal('hide');
}

function renderButton(typeBTN,valueType,id){
	if("flagFlight" == typeBTN){
		if(valueType == true){
            return '<img src='+IMG.BTN_PLANE+' id="btn_plane" data-id='+id+' name="vehicleType" width="40px" value="active" onclick="changeBtnType(this)" />'
        }else{
            return '<img src='+IMG.BTN_PLANE_A+' id="btn_plane"  data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnType(this)" />'
		}
	}if("flagCar" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_CAR+' id="btn_car" data-id='+id+' name="vehicleType" width="40px" value="active" onclick="changeBtnType(this)" />'
        }else{
            return '<img src='+IMG.BTN_CAR_A+' id="btn_car"  data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnType(this)" />'
        }
	}if("flagBed" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BED+' id="btn_bed" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnType(this)" />'
        }else{
            return '<img src='+IMG.BTN_BED_A+' id="btn_bed" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnType(this)" />'
        }
	}if("flagBedTwin" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BEDS+' id="btn_beds" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnType(this)" />'
        }else{
            return '<img src='+IMG.BTN_BEDS_A+' id="btn_beds" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnType(this)" />'
        }
	}
}

function insertDataTravelMember(){
	var nameMember = $("#nameMemberInputEmployeeAll").val();
	
	var errorMessage = "";
	
	if(nameMember == ""){
		
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_EMPLOYEE_NAME+"<br/>";
		
		$("#warningModal .modal-body").html(errorMessage);
		$("#warningModal").modal('show');
	}else{
		if(FLAG_CAR == null && FLAG_FLIGHT == null && FLAG_BED == null && FLAG_BED_TWIN == null){
			if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                FLAG_CAR = true;
                FLAG_FLIGHT = false;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
			}else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                FLAG_CAR = false;
                FLAG_FLIGHT = true;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
			}
		}

        $('.dv-background').show();
        window.setTimeout(function() {
            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $TravelMemberId;
            jsonData['memberCode'] = $("#empCodeMember").text();
            jsonData['memberUser'] = $("#nameMemberInputEmployeeAll").attr("data-userName");
            jsonData['memberPersonalId'] = $("#nameMemberInputEmployeeAll").attr("data-personalId");
            jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItem[0].id;

            var dataTravel = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/saveTravelMember',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('.dv-background').hide();
                        if (xhr.responseText == "DUPLICATE") {
                            var errorMessage = MSG.MESSAGE_DATA_DUPLICATE + "<br/>";
                            $('#warningModal .modal-body').html(errorMessage);
                            $("#warningModal").modal('show');
                        } else {
                            $('.dv-background').hide();
                            findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
                        }
                    }
                }
            });
        },500);
	}
}

function findTravelMembersByDocumentApproveItemId(documentAppItem){

    $('.dv-background').show();
    window.setTimeout(function() {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/' + documentAppItem + '/travelMembers',
            complete: function (xhr) {
                $DATA_TRAVEL_MEMBER = JSON.parse(xhr.responseText);

                var userMember = "";
                for(var i=0;i<$DATA_TRAVEL_MEMBER.length;i++){

                    var userName = $DATA_TRAVEL_MEMBER[i].memberUser;
                    $.ajax({
                        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
                        headers: {
                            Accept: "application/json"
                        },
                        type: "GET",
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                var dataRequester = JSON.parse(xhr.responseText);

                                $.ajax({
                                    url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
                                    headers: {
                                        Accept: "application/json"
                                    },
                                    type: "GET",
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            var dataMember = JSON.parse(xhr.responseText);
                                            if (dataRequester.Personal_Cost_center == dataMember.Personal_Cost_center) {
                                                userMember += userName+",";
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }

                if(userMember != ""){
                    userMember =  userMember.substr(0,userMember.lastIndexOf(","));
                }

                var jsonDocument = {};
                jsonDocument['parentId'] = 1;
                jsonDocument[csrfParameter] = csrfToken;
                jsonDocument['id'] = $DATA_DOCUMENT.id;
                jsonDocument['userNameMember'] = userMember;

                var dataDocument = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/approve/updateDocumentApprove',
                    data: jsonDocument,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            $('.dv-background').hide();
                            renderDataTravelMember($DATA_TRAVEL_MEMBER);
                        }
                    }
                });
            }
        });
    },500);
}

function editTravelMember(obj){
	$STATUS = "update";
	$TravelMemberId = obj.id;
	$("#modalAddPerson").modal('show');
	renderDataToModalEditTravelMember(obj.id);
}

function renderDataToModalEditTravelMember(id){
	for(var i = 0; i<$DATA_TRAVEL_MEMBER.length;i++){
    	if($DATA_TRAVEL_MEMBER[i].id == parseInt(id)){
            AutocompleteEmployeeAll.search($DATA_TRAVEL_MEMBER[i].memberUser);
            AutocompleteEmployeeAll.renderValue($DATA_TRAVEL_MEMBER[i].memberUser,"nameMemberInputEmployeeAll");

            $("#empCodeMember").text($("#nameMemberInputEmployeeAll").attr("data-personalId"));
            $("#empCodeMember").attr("data-empCode",$("#nameMemberInputEmployeeAll").attr("data-empCode"));
    		$("#positionMember").text($DATA_TRAVEL_MEMBER[i].positionMember);
    		$("#departmentMember").text($DATA_TRAVEL_MEMBER[i].departmentMember);
    	}
    }
}

function deleteTravelMember(id){
	
	var jsonParams2 = {};
	jsonParams2['parentId']     = 1;
	jsonParams2[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteTravelMember/'+id,
        data: jsonParams2,
        complete: function (xhr) {
        	findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
        	$('#deleteItemModal').modal('hide');
        }
    });
}

var BTN;
function changeBtnType(btn){

	BTN = btn;
    if(btn.getAttribute('id') == "btn_plane" && btn.getAttribute('value') == "inActive"){
    	$("#btn_plane").attr('src',IMG.BTN_PLANE);
    	$("#btn_plane").attr('value',"active");
    	$("#btn_car").attr('src',IMG.BTN_CAR_A);
    	$("#btn_car").attr('value',"inActive");
    	FLAG_FLIGHT = true;
    	FLAG_CAR = false;
	}else if(btn.getAttribute('id') == "btn_car" && btn.getAttribute('value') == "inActive"){
        $("#btn_car").attr('src',IMG.BTN_CAR);
        $("#btn_car").attr('value',"active");
        $("#btn_plane").attr('src',IMG.BTN_PLANE_A);
        $("#btn_plane").attr('value',"inActive");
        FLAG_CAR = true;
        FLAG_FLIGHT = false;
	}else if(btn.getAttribute('id') == "btn_bed" && btn.getAttribute('value') == "inActive"){
        $("#btn_bed").attr('src',IMG.BTN_BED);
        $("#btn_bed").attr('value',"active");
        $("#btn_beds").attr('src',IMG.BTN_BEDS_A);
        $("#btn_beds").attr('value',"inActive");
        FLAG_BED = true;
        FLAG_BED_TWIN = false;
	}else if(btn.getAttribute('id') == "btn_beds" && btn.getAttribute('value') == "inActive"){
        $("#btn_beds").attr('src',IMG.BTN_BEDS);
        $("#btn_beds").attr('value',"active");
        $("#btn_bed").attr('src',IMG.BTN_BED_A);
        $("#btn_bed").attr('value',"inActive");
        FLAG_BED_TWIN = true;
        FLAG_BED = false;
	}

    updateDataTravelMember(BTN.getAttribute('data-id'));
}

function updateDataTravelMember(id){

    $('.dv-background').show();
    window.setTimeout(function() {
        var memberName;
        var memberCode;
        var docApproveItem;

        for (var i = 0; i < $DATA_TRAVEL_MEMBER.length; i++) {
            if ($DATA_TRAVEL_MEMBER[i].id == id) {
                memberName = $DATA_TRAVEL_MEMBER[i].memberName;
                memberCode = $DATA_TRAVEL_MEMBER[i].memberCode;
                docApproveItem = $DATA_TRAVEL_MEMBER[i].documentApproveItem.id
            }
        }
        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['id'] = id;
        jsonData['memberName'] = memberName;
        jsonData['memberCode'] = memberCode;
        jsonData['flagCar'] = FLAG_CAR;
        jsonData['flagFlight'] = FLAG_FLIGHT;
        jsonData['flagBed'] = FLAG_BED;
        jsonData['flagBedTwin'] = FLAG_BED_TWIN;
        jsonData['documentApproveItem'] = docApproveItem;

        var dataTravel = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveTravelMember',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    findTravelMembersByDocumentApproveItemId(docApproveItem);
                }
            }
        });
    },500);
}

/* PANEL EXTERNAL MEMBER */
function addExternalMember(btn){
	if(btn == "add"){
		$ExternalMemberId = null;
		$("#externalName").val("");
		$("#externalCompany").val("");
		$("#externalPhoneNumber").val("");
		$("#externalEmail").val("");
		$("#externalRemark").val("");
		$("#modalAddOuterPerson").modal('show');
	}
}

function renderDataExternalMember(dataExternal){
	$("#collapseHeaderExternal").empty();
	$("#personExternal").text(dataExternal.length > 0 ? dataExternal.length : 0);
	if(dataExternal.length > 0){
		for(var i=0;i<dataExternal.length;i++){
			
			$("#collapseHeaderExternal").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
				'<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px;padding-left: 0px;padding-right: 0px;" >'+
					'<div class="container-fluid">'+
						'<div class="col-xs-12 col-xs-1" ><a href="javascript:void(0)"><img src='+IMG.DELETE+' title="DELETE" width="40px" data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataExternal[i].id+'\');$(\'#typeDataDelete\').val(\'EXTERNAL_MEMBER\');" /></a></div>'+
						'<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)" style="padding-left: 0px;padding-right: 0px;" >'+
							'<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
							'<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: #08f508;font-size:14px;"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
						'</div>'+
						'<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)" style="padding-left: 0px;padding-right: 0px;" >'+
							'<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
							'<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: #08f508;font-size:14px;"><b>'+dataExternal[i].email+'</b></label></div>'+
						'</div>'+
						// '<div class="col-xs-3 text-right">'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagFlight",dataExternal[i].flagFlight,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagCar",dataExternal[i].flagCar,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagBed",dataExternal[i].flagBed,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagBedTwin",dataExternal[i].flagBedTwin,dataExternal[i].id)+'</a>'+
						// '</div >'+
						'<div class="col-xs-12 col-sm-1" >'+
							'<div class="col-xs-12 col-sm-12"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' title="REMARK" width="40px"/></a></div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			 '</div>');
		}
	}	
	
	$("#modalAddOuterPerson").modal('hide');
}

function remarkExternal(obj){
    for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
    	if($DATA_EXTERNAL_MEMBER[i].id == parseInt(obj.id)){
    		$("#alertModal").modal('show');
    	    $("label[id=detailAlert]").text($DATA_EXTERNAL_MEMBER[i].remark);
    	}
    }
}

function insertDataExternalMember(){
	var externalName      		= $("#externalName").val();
	var externalCompany			= $("#externalCompany").val();
	var externalPhoneNumber	    = $("#externalPhoneNumber").val();
	var externalEmail	    	= $("#externalEmail").val();
	var externalRemark	    	= $("#externalRemark").val();

	var errorMessage = "";
	if(externalName == "") {
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD + " " + LB.LABEL_FULL_NAME + "<br/>";
    }
    if(externalCompany == "") {
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD + " " + LB.LABEL_COMPANY + "<br/>";
    }

    if(errorMessage != ""){
		$('#warningModal .modal-body').html(errorMessage);
		$("#warningModal").modal('show');
	}else{
        if(FLAG_CAR == null && FLAG_FLIGHT == null && FLAG_BED == null && FLAG_BED_TWIN == null){
            if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                FLAG_CAR = true;
                FLAG_FLIGHT = false;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
            }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                FLAG_CAR = false;
                FLAG_FLIGHT = true;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
            }
        }

        $('.dv-background').show();
        window.setTimeout(function() {

            var jsonData = {};
            jsonData['parentId'] = 1;
            jsonData[csrfParameter] = csrfToken;
            jsonData['id'] = $ExternalMemberId;
            jsonData['memberName'] = externalName;
            jsonData['companyMember'] = externalCompany;
            jsonData['phoneNumber'] = externalPhoneNumber;
            jsonData['email'] = externalEmail;
            jsonData['remark'] = externalRemark;
            jsonData['flagCar'] = FLAG_CAR;
            jsonData['flagFlight'] = FLAG_FLIGHT;
            jsonData['flagBed'] = FLAG_BED;
            jsonData['flagBedTwin'] = FLAG_BED_TWIN;
            jsonData['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItem[0].id;

            var dataTravel = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/saveExternalMember',
                data: jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('.dv-background').hide();
                        findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
                    }
                }
            });
        },500);
    }
}

function findExternalMembersByDocumentApproveItemId(documentAppItem){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/externalMembers',
        complete: function (xhr) {
        	$DATA_EXTERNAL_MEMBER = JSON.parse(xhr.responseText);
        	renderDataExternalMember($DATA_EXTERNAL_MEMBER);
        }
    });  
}

function editExternalMemberl(obj){
	$STATUS = "update";
	$ExternalMemberId = obj.id;
	$("#modalAddOuterPerson").modal('show');
	renderDataToModalEditExternalMember(obj.id);
}

function renderDataToModalEditExternalMember(id){
	for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
    	if($DATA_EXTERNAL_MEMBER[i].id == parseInt(id)){
    		$("#externalName").val($DATA_EXTERNAL_MEMBER[i].memberName);
    		$("#externalCompany").val($DATA_EXTERNAL_MEMBER[i].companyMember);
    		$("#externalPhoneNumber").val($DATA_EXTERNAL_MEMBER[i].phoneNumber);
    		$("#externalEmail").val($DATA_EXTERNAL_MEMBER[i].email);
    		$("#externalRemark").val($DATA_EXTERNAL_MEMBER[i].remark);
    	}
    }
}

function deleteExternalMember(id){
	
	var jsonParams2 = {};
	jsonParams2['parentId']     = 1;
	jsonParams2[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteExternalMember/'+id,
        data: jsonParams2,
        complete: function (xhr) {
        	findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
        	$('#deleteItemModal').modal('hide');
        }
    });
}

function changeBtnTypeExternal(btn){

    BTN = btn;
    if(btn.getAttribute('id') == "plane_btn" && btn.getAttribute('value') == "inActive"){
        $("#plane_btn").attr('src',IMG.BTN_PLANE);
        $("#plane_btn").attr('value',"active");
        $("#car_btn").attr('src',IMG.BTN_CAR_A);
        $("#car_btn").attr('value',"inActive");
        FLAG_FLIGHT = true;
        FLAG_CAR = false;
    }else if(btn.getAttribute('id') == "car_btn" && btn.getAttribute('value') == "inActive"){
        $("#car_btn").attr('src',IMG.BTN_CAR);
        $("#car_btn").attr('value',"active");
        $("#plane_btn").attr('src',IMG.BTN_PLANE_A);
        $("#plane_btn").attr('value',"inActive");
        FLAG_CAR = true;
        FLAG_FLIGHT = false;
    }else if(btn.getAttribute('id') == "bed_btn" && btn.getAttribute('value') == "inActive"){
        $("#bed_btn").attr('src',IMG.BTN_BED);
        $("#bed_btn").attr('value',"active");
        $("#beds_btn").attr('src',IMG.BTN_BEDS_A);
        $("#beds_btn").attr('value',"inActive");
        FLAG_BED = true;
        FLAG_BED_TWIN = false;
    }else if(btn.getAttribute('id') == "beds_btn" && btn.getAttribute('value') == "inActive"){
        $("#beds_btn").attr('src',IMG.BTN_BEDS);
        $("#beds_btn").attr('value',"active");
        $("#bed_btn").attr('src',IMG.BTN_BED_A);
        $("#bed_btn").attr('value',"inActive");
        FLAG_BED_TWIN = true;
        FLAG_BED = false;
    }
    updateDataExternalMember(BTN.getAttribute('data-id'));

}

function renderButtonExternal(typeBTN,valueType,id){
    if("flagFlight" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_PLANE+' id="plane_btn" data-id='+id+' name="vehicleType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_PLANE_A+' id="plane_btn" data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagCar" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_CAR+' id="car_btn" data-id='+id+'  name="vehicleType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_CAR_A+' id="car_btn" data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagBed" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BED+' id="bed_btn" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_BED_A+' id="bed_btn" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagBedTwin" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BEDS+' id="beds_btn" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_BEDS_A+' id="beds_btn" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }
}

function updateDataExternalMember(id){

    $('.dv-background').show();
    window.setTimeout(function() {
        var memberName;
        var companyMember;
        var phoneNumber;
        var email;
        var remark;
        var documentApproveItem;

        for (var i = 0; i < $DATA_EXTERNAL_MEMBER.length; i++) {
            if ($DATA_EXTERNAL_MEMBER[i].id == id) {
                memberName = $DATA_EXTERNAL_MEMBER[i].memberName;
                companyMember = $DATA_EXTERNAL_MEMBER[i].companyMember;
                phoneNumber = $DATA_EXTERNAL_MEMBER[i].phoneNumber;
                email = $DATA_EXTERNAL_MEMBER[i].email;
                remark = $DATA_EXTERNAL_MEMBER[i].remark;
                documentApproveItem = $DATA_EXTERNAL_MEMBER[i].documentApproveItem.id;
            }
        }


        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['id'] = id;
        jsonData['memberName'] = memberName;
        jsonData['companyMember'] = companyMember;
        jsonData['phoneNumber'] = phoneNumber;
        jsonData['email'] = email;
        jsonData['remark'] = remark;
        jsonData['flagCar'] = FLAG_CAR;
        jsonData['flagFlight'] = FLAG_FLIGHT;
        jsonData['flagBed'] = FLAG_BED;
        jsonData['flagBedTwin'] = FLAG_BED_TWIN;
        jsonData['documentApproveItem'] = documentApproveItem;

        var dataTravel = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveExternalMember',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    findExternalMembersByDocumentApproveItemId(documentApproveItem);
                }
            }
        });
    },500);
}

/* manage car */
function activeCar(btn) {
	if($("[name=iconCar]").hasClass('click') == 'false'){
		$("#"+btn.id).addClass('click');
		var carValue = $("#"+btn.id).attr('value');
		if(carValue == 'van'){
			$("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'sedan'){
			$("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'tuck'){
			$("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'other'){
			$("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
			$("#otherCarDiv").removeClass('hide');
		}
	}else{
		$("[name=iconCar]").removeClass('click');
		setUpDefaultCarType();
		$("#"+btn.id).addClass('click');
		var carValue = $("#"+btn.id).attr('value');
		if(carValue == 'van'){
			$("#"+btn.id).attr('src',IMAGE_CAR.VAN);
			$CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'sedan'){
			$("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
			$CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'tuck'){
			$("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
			$CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").addClass('hide');
		}else if(carValue == 'other'){
			$("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
			$CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarDiv").removeClass('hide');
		}
	}
    CAR_TYPE_ACTIVE = true;
	setFlowTypeCar();
}

function validateDefaultCarType(){
	for(var i=0;i<$("[name=iconCar]").length;i++){
		if('van' == $("[name=iconCar]")[i].getAttribute('value')){
			$("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
			$("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
			/* validate member over 3 person */
			var personMember = $("#personMember").text() == "" ? 0 : parseInt($("#personMember").text());
			var externalMember = $("#personExternal").text() == "" ? 0 : parseInt($("#personExternal").text());
			if(personMember + externalMember >= 3){
				$("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN);
				$CarTypeCode = $("[name=iconCar]")[i].getAttribute('id');
                $("#"+$CarTypeCode).addClass('click');
                CAR_TYPE_ACTIVE = true;
			}
		}else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
			$("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
			$("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
		}else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
			$("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
			$("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
		}else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
			$("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
			$("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
		}
	}
}

function setUpDefaultCarType(){
    for(var i=0;i<$("[name=iconCar]").length;i++){
        if('van' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
        }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
        }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
        }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
            $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
            $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
        }
    }
}

function saveCarBooking(){

    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_CAR);
        jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_CAR;
        jsonDocumentApproveItem['travelReason'] = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        var dataDocumentApproveItem = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocumentApproveItem',
            data: jsonDocumentApproveItem,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocumentApproveItem = JSON.parse(xhr.responseText);

                    var origin = $("#origin").val();
                    var destination = $("#destination").val();
                    var carBookingId = dataDocumentApproveItem.carBooking == null ? "" : dataDocumentApproveItem.carBooking.id;

                    var jsonCarBooking = {};
                    jsonCarBooking['parentId'] = 1;
                    jsonCarBooking[csrfParameter] = csrfToken;
                    jsonCarBooking['id'] = carBookingId;
                    jsonCarBooking['origin'] = origin;
                    jsonCarBooking['destination'] = destination;
                    jsonCarBooking['documentApproveItem'] = dataDocumentApproveItem.id;

                    var dataCarBooking = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveCarBooking',
                        data: jsonCarBooking,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                DATA_RESULT_CAR = JSON.parse(xhr.responseText);
                                $('.dv-background').hide();
                            }
                        }
                    });
                }
            }
        });
    },500);

}

function verifyDocApproveItem(appType) {
    var dataApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    for(var i=0;i<dataApproveItem.length;i++){
        if(appType == $DATA_DOCUMENT.approveType && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
            if(dataApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                return dataApproveItem[i].id;
            }
        }else if(appType == $DATA_DOCUMENT.approveType && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            if(dataApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                return dataApproveItem[i].id;
            }
        }
    }
}

function verifyDocApproveItemBySubType(appType){
    var dataApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    for(var i=0;i<dataApproveItem.length;i++) {
        if (dataApproveItem[i].approveType == appType) {
            return dataApproveItem[i].id;
        }
    }
}

function renderDataCarBooking(dataCarBooking){

    if(dataCarBooking != null){
        $("#carReserve").prop('checked',true);
        $("#linkCarMis").removeClass('hide');
        $("#linkCarMisForm").removeClass('hide');
    }

}


function saveHotelBooking(){

    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_HOTEL);
        jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_HOTEL;
        jsonDocumentApproveItem['travelReason'] = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        var dataDocumentApproveItem = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocumentApproveItem',
            data: jsonDocumentApproveItem,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocumentApproveItem = JSON.parse(xhr.responseText);

                    var singleRoom = $("#singleRoom").val();
                    var twinRoom = $("#twinRoom").val();
                    var informationHotel = $("#hotelInputHotel").attr('data-hotel-code');
                    var hotelBookingId = dataDocumentApproveItem.hotelBooking == null ? "" : dataDocumentApproveItem.hotelBooking.id;

                    var jsonHotelBooking = {};
                    jsonHotelBooking['parentId'] = 1;
                    jsonHotelBooking[csrfParameter] = csrfToken;
                    jsonHotelBooking['id'] = hotelBookingId;
                    jsonHotelBooking['documentApproveItem'] = dataDocumentApproveItem.id;

                    var dataHotelBooking = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveHotelBooking',
                        data: jsonHotelBooking,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                DATA_RESULT_HOTEL = JSON.parse(xhr.responseText);
                                $('.dv-background').hide();
                            }
                        }
                    });
                }
            }
        });
    },500);
}

function renderDataHotelBooking(dataHotelBooking){
	if(dataHotelBooking != null){
        $("#hotelReserve").prop('checked',true);
        // $("#singleRoom").val(dataHotelBooking.singleRoom != null ? dataHotelBooking.singleRoom : "");
        // $("#twinRoom").val(dataHotelBooking.twinRoom != null ? dataHotelBooking.twinRoom : "");
        // AutocompleteHotel.renderValue(dataHotelBooking.informationHotel);
        // if(dataHotelBooking.informationHotel != "Other"){
        //     $("#hotelPriceDiv").removeClass('hide');
        //     $("#hotelPrice").text($("#hotelInputHotel").attr('data-hotel-rate'));
        // }else {
        //     $("#otherHotelDiv").removeClass('hide');
        //     $("#otherHotel").val(dataHotelBooking.otherHotel);
        // }
        // if((dataHotelBooking.singleRoom != null || dataHotelBooking.twinRoom != null) && dataHotelBooking.informationHotel != null){
        //     HOTEL_TYPE_ACTIVE = true;
        //     setFlowTypeHotel();
        // }
    }

}

/* manage flight */
function getMasterDataAirline(){
	$("#airline").empty().append('<option value="">'+LB.LABEL_SELECT_AIR_LINE+'</option>');
    for(var i=0; i<MASTER_DATA.AIR_LINE.length;i++){
        $("#airline").append(
            '<option value='+MASTER_DATA.AIR_LINE[i].code+'>'+ MASTER_DATA.AIR_LINE[i].description + '</option>');
    }
}

function saveFlightTicket(){

    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_FIGHT_BOOKING);
        jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
        jsonDocumentApproveItem['travelReason'] = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        var dataDocumentApproveItem = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocumentApproveItem',
            data: jsonDocumentApproveItem,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocumentApproveItem = JSON.parse(xhr.responseText);

                    var flightTicketId = dataDocumentApproveItem.flightTicket == null ? "" : dataDocumentApproveItem.flightTicket.id;

                    var jsonFlightTicket = {};
                    jsonFlightTicket['parentId'] = 1;
                    jsonFlightTicket[csrfParameter] = csrfToken;
                    jsonFlightTicket['id'] = flightTicketId;
                    jsonFlightTicket['documentApproveItem'] = dataDocumentApproveItem.id;

                    var dataFlightTicket = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveFlightTicket',
                        data: jsonFlightTicket,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                DATA_RESULT_FLIGHT = JSON.parse(xhr.responseText);
                                $('.dv-background').hide();
                            }
                        }
                    });
                }
            }
        });
    },500);
}

function renderDataFlightTicket(dataFlightTicket){
    if(dataFlightTicket != null){
        $("#flightReserve").prop('checked',true);

        FLIGHT_TYPE_ACTIVE = true;
        setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
    }
}

/* manage document approve item */
function updateDocumentApproveItem(btn){

    if($DATA_TRAVEL_DETAIL.length > 0){
        if( $("#advancesAmount").autoNumeric('get') == 0) {
            $("#borrowDate").val("");
            $("#advanceEndDate").val("");
            $('.dv-background').show();
            window.setTimeout(function () {
                var reason = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

                if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC) {
                    approveType = MASTER_DATA.APR_TYPE_SET_DOMESTIC;
                } else {
                    approveType = MASTER_DATA.APR_TYPE_SET_FOREIGN;
                }


                var jsonDocumentApproveItem = {};
                jsonDocumentApproveItem['parentId'] = 1;
                jsonDocumentApproveItem[csrfParameter] = csrfToken;
                jsonDocumentApproveItem['id'] = verifyDocApproveItem($DATA_DOCUMENT.approveType);
                jsonDocumentApproveItem['approveType'] = approveType;
                jsonDocumentApproveItem['travelReason'] = reason;
                jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                var dataDocumentApproveItem = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/approve/saveDocumentApproveItem',
                    data: jsonDocumentApproveItem,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            dataDocumentApproveItem = JSON.parse(xhr.responseText);
                            STATUS_SAVE_DOCUMENT = "SUCCESS";
                            $('.dv-background').hide();
                            if ($("#carReserve").is(':checked')) {
                                saveCarBooking();
                            }
                            if ($("#hotelReserve").is(':checked')) {
                                saveHotelBooking();
                            }
                            if ($("#flightReserve").is(':checked')) {
                                saveFlightTicket();
                            }
                            saveAdvances();

                            if ($("#carReserve").is(':checked') || $("#hotelReserve").is(':checked') || $("#flightReserve").is(':checked')) {
                                var jsonDocument = {};
                                jsonDocument['parentId'] = 1;
                                jsonDocument[csrfParameter] = csrfToken;
                                jsonDocument['id'] = $DATA_DOCUMENT.id;
                                jsonDocument['ccMailAddress'] = MAIL_ADMIN;

                                var dataDocument = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/updateDocumentApprove',
                                    data: jsonDocument,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            console.info("><>< UPDATE MAIL ADMIN");
                                        }
                                    }
                                });
                            } else {
                                var jsonDocument = {};
                                jsonDocument['parentId'] = 1;
                                jsonDocument[csrfParameter] = csrfToken;
                                jsonDocument['id'] = $DATA_DOCUMENT.id;
                                jsonDocument['ccMailAddress'] = null;

                                var dataDocument = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/updateDocumentApprove',
                                    data: jsonDocument,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            console.info("><>< UPDATE MAIL ADMIN");
                                        }
                                    }
                                });
                            }


                            if (DATA_RESULT_ADVANCE != null && btn != "SEND") {
                                $("#completeDetail").text(MSG.MESSAGE_UPDATE_SUCCESS);
                                $("#completeModal").modal('show');
                            }
                        }
                    }
                });
            }, 500);
        }else{
            if($("#borrowDate").val() != "" && $("#advanceEndDate").val() != "" && $("#bankNumber").val() != ""){
                $('.dv-background').show();
                window.setTimeout(function () {
                    var reason = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

                    if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC) {
                        approveType = MASTER_DATA.APR_TYPE_SET_DOMESTIC;
                    } else {
                        approveType = MASTER_DATA.APR_TYPE_SET_FOREIGN;
                    }


                    var jsonDocumentApproveItem = {};
                    jsonDocumentApproveItem['parentId'] = 1;
                    jsonDocumentApproveItem[csrfParameter] = csrfToken;
                    jsonDocumentApproveItem['id'] = verifyDocApproveItem($DATA_DOCUMENT.approveType);
                    jsonDocumentApproveItem['approveType'] = approveType;
                    jsonDocumentApproveItem['travelReason'] = reason;
                    jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                    var dataDocumentApproveItem = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveDocumentApproveItem',
                        data: jsonDocumentApproveItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                dataDocumentApproveItem = JSON.parse(xhr.responseText);
                                STATUS_SAVE_DOCUMENT = "SUCCESS";
                                $('.dv-background').hide();
                                if ($("#carReserve").is(':checked')) {
                                    saveCarBooking();
                                }
                                if ($("#hotelReserve").is(':checked')) {
                                    saveHotelBooking();
                                }
                                if ($("#flightReserve").is(':checked')) {
                                    saveFlightTicket();
                                }
                                saveAdvances();

                                if ($("#carReserve").is(':checked') || $("#hotelReserve").is(':checked') || $("#flightReserve").is(':checked')) {
                                    var jsonDocument = {};
                                    jsonDocument['parentId'] = 1;
                                    jsonDocument[csrfParameter] = csrfToken;
                                    jsonDocument['id'] = $DATA_DOCUMENT.id;
                                    jsonDocument['ccMailAddress'] = MAIL_ADMIN;

                                    var dataDocument = $.ajax({
                                        type: "POST",
                                        headers: {
                                            Accept: 'application/json'
                                        },
                                        url: session['context'] + '/approve/updateDocumentApprove',
                                        data: jsonDocument,
                                        async: false,
                                        complete: function (xhr) {
                                            if (xhr.readyState == 4) {
                                                console.info("><>< UPDATE MAIL ADMIN");
                                            }
                                        }
                                    });
                                } else {
                                    var jsonDocument = {};
                                    jsonDocument['parentId'] = 1;
                                    jsonDocument[csrfParameter] = csrfToken;
                                    jsonDocument['id'] = $DATA_DOCUMENT.id;
                                    jsonDocument['ccMailAddress'] = null;

                                    var dataDocument = $.ajax({
                                        type: "POST",
                                        headers: {
                                            Accept: 'application/json'
                                        },
                                        url: session['context'] + '/approve/updateDocumentApprove',
                                        data: jsonDocument,
                                        async: false,
                                        complete: function (xhr) {
                                            if (xhr.readyState == 4) {
                                                console.info("><>< UPDATE MAIL ADMIN");
                                            }
                                        }
                                    });
                                }


                                if (DATA_RESULT_ADVANCE != null && btn != "SEND") {
                                    $("#completeDetail").text(MSG.MESSAGE_UPDATE_SUCCESS);
                                    $("#completeModal").modal('show');
                                }
                            }
                        }
                    });
                }, 500);
            }else{
                $('#warningModal .modal-body').html(LB.LABEL_PLEASE_FILL_ADVANCE_DATA);
                $('#warningModal').modal('show');
            }
        }
    }else {
        $('#warningModal .modal-body').html(MSG.MESSAGE_TRAVEL_DETAIL_NO_SIZE);
        $('#warningModal').modal('show');
    }
}

/* manage advances */
function saveAdvances(){
    var amount 		= ($("#advancesAmount").autoNumeric('get') == "" ? 0 : $("#advancesAmount").autoNumeric('get'));
    var startDate 	= $("#borrowDate").val();
    var endDate   	= $("#advanceEndDate").val();
    var dueDate	  	= $("#dueDate").text();
    var bankNumber 	= $("#bankNumber").val();
    var remark 	    = $("#remarkAdvance").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
    var advancePurpose = "";
    var documentId = 0;
    documentId = $DATA_DOCUMENT.id;
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
        advancePurpose = "เพื่อเดินทางปฏิบัติงานภายในประเทศ";
    }else{
        advancePurpose = "เพื่อเดินทางปฏิบัติงานต่างประเทศ";
    }

    if(startDate != ""){
        startDate = new Date(DateUtil.coverStringToDate(startDate)).format("yyyy-mm-dd HH:MM:ss");
    }
    if(endDate != ""){
        endDate = new Date(DateUtil.coverStringToDate(endDate)).format("yyyy-mm-dd HH:MM:ss");
    }
    if(dueDate != ""){
        dueDate = new Date(DateUtil.coverStringToDate(dueDate)).format("yyyy-mm-dd HH:MM:ss");
    }

    setFlowTypeAdvance();
    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonDocumentAdvance = {};
        jsonDocumentAdvance['parentId'] = 1;
        jsonDocumentAdvance[csrfParameter] = csrfToken;
        jsonDocumentAdvance['id'] = $DATA_DOCUMENT.documentAdvance == null ? "" : $DATA_DOCUMENT.documentAdvance.id;
        jsonDocumentAdvance['amount'] = amount;
        jsonDocumentAdvance['startDate'] = (startDate != "" ? startDate : null);
        jsonDocumentAdvance['endDate'] = (endDate != "" ? endDate : null);
        jsonDocumentAdvance['dueDate'] = (dueDate != "" ? dueDate : null);
        jsonDocumentAdvance['bankNumber'] = bankNumber;
        jsonDocumentAdvance['advancePurpose'] = advancePurpose;
        jsonDocumentAdvance['remark'] = remark;

        var jsonDocument = {};
        jsonDocument['parentId'] = 1;
        jsonDocument[csrfParameter] = csrfToken;
        jsonDocument['id'] = $DATA_DOCUMENT.id;
        jsonDocument['totalAmount'] = amount;
        jsonDocument['documentAdvance'] = JSON.stringify(jsonDocumentAdvance);

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/updateDocumentApprove',
            data: jsonDocument,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    DATA_RESULT_ADVANCE = JSON.parse(xhr.responseText);
                    $('.dv-background').hide();

                    if(BTN_ACTION != "SEND"){
                        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                    }else if(DATA_RESULT_ADVANCE != null && BTN_ACTION == "SEND") {
                        if($DATA_TRAVEL_MEMBER.length > 1){
                            for(var i=0;i<$DATA_TRAVEL_MEMBER.length;i++){
                                if($DATA_TRAVEL_MEMBER[i].memberUser != $DATA_DOCUMENT.requester){

                                    /** validate */
                                    validateLineApprove($DATA_TRAVEL_MEMBER[i].memberUser);
                                }
                            }

                            if(FLAG_CREATE_DOC_MEMBER == 0 && FLAG_USER_SAME_COSTCENTER > 0){
                                sendRequest();
                            }
                        }else {
                            sendRequest();
                        }

                    }
                }
            }
        });
    },500);

}

function renderDocumentAdvance(documentAdvance){
    var amount = documentAdvance.amount == null ? 0 : documentAdvance.amount;
    $("#advancesAmount").autoNumeric('set',amount);
	$("#borrowDate").val(DateUtil.coverDateToString(documentAdvance.startDate));
	$("#advanceEndDate").val(DateUtil.coverDateToString(documentAdvance.endDate));
	$("#dueDate").text(DateUtil.coverDateToString(documentAdvance.dueDate));
	$("#bankNumber").val(documentAdvance.bankNumber);
    $("#advancesMoney").autoNumeric('set',amount);
    $("#remarkAdvance").val(documentAdvance.remark == null ? "" : documentAdvance.remark);

    if(documentAdvance.amount != 0 && documentAdvance.startDate != null && documentAdvance.endDate != null
        && documentAdvance.dueDate != null && documentAdvance.bankNumber != ""){
        ADVANCE_TYPE_ACTIVE = true;
        setFlowTypeAdvance();
    }
}

/* manage copy document */
function copyDocument(){
	var jsonDocument = {};
	jsonDocument['parentId']     	= 1;
	jsonDocument[csrfParameter]  	= csrfToken;
	jsonDocument['id']   			= $DATA_DOCUMENT.id;

    var dataDocument = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/copyDocument',
        data:jsonDocument,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
            	dataDocument = JSON.parse(xhr.responseText);
            	var appType = dataDocument.documentApprove.documentApproveItem[0].approveType;
            	window.location.href = session.context+'/approve/createDocSetDetail?doc='+dataDocument.id;
            }
        }
    });
}

/* cancel document */
function confirmCancelDocument(){
	var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";
	
	$('#confirmModal .modal-body').html(warningMessage);
	$('#confirmModal').modal('show');
}

function cancelDocument(){
	var jsonData = {};
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']    			= $DATA_DOCUMENT.id;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;
    
    var dataDocument = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/updateDocumentStatus',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                dataDocument = JSON.parse(xhr.responseText);
                $('#confirmModal').modal('hide');
                $("#completeDetail").text(MSG.MESSAGE_UPDATE_SUCCESS);
                $("#completeModal").modal('show');
                backToMenu();
            }
        }
    });
}

/* manage document attachment */
function documentAttachment(){
	getDataAttachmentType();
	$("#modalDocumentAttachment").modal('show');
	$("#attachmentType").val("");
	$("#textFileName").val("");
}

function getDataAttachmentType(){
	$("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function validateFileExtensions(){
	var extensionFile = $('#attachmentType option:selected').attr('extension');
    
    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

function saveDocumentAttachment(){
	var jsonData = {};
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['attachmentType']  = $("#attachmentType").val();
    jsonData['document']  		= $DATA_DOCUMENT.id;
    
    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename",  $fileUpload.name);
    formData.append("attachmentType",  $("#attachmentType").val());
  	formData.append("document",  $DATA_DOCUMENT.id);
    
    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context']+ '/approve/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	dataDocumentAttachment = JSON.parse(xhr.responseText);
                	$("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function findDocumentAttachmentByDocumentId(documentId){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentId+'/documentAttachment',
        complete: function (xhr) {
        	$DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
        	renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });  
}

function renderDocumentAttachment(dataDocumentAttachment){
	$('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItem(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button>' +
                '</td>' +
                '</tr>'
            );
        }
    }
}

var BTNDownloadFile;
function downloadDocumentFile(btn){
    BTNDownloadFile = btn;
    var id = BTNDownloadFile.attr('id');
    var fileName = BTNDownloadFile.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}

function getAttachmentType(code){
	for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
		if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
			return MASTER_DATA.ATTACHMENT_TYPE[i].description;
			break;
		}
	}
}

function deleteDocumentAttachment(id){
	
	var jsonParams2 = {};
	jsonParams2['parentId']     = 1;
	jsonParams2[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
        	findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
        	$('#deleteItemModal').modal('hide');
        }
    });
}

function callQRCodeDocSet(){
    var user_account;

    $DATA_REQUEST_APPROVER.forEach(function (item) {
        if(item.actionState.indexOf("ACC") >= 0){
            user_account = item.userNameApprover;
        }
    })
    window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
}

/* for get employee profile by user name */
function findEmployeeProfileByUserName(userName) {

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;
}

function getAuthorizeForLineApprove(userName){

    $('.dv-background').show();

    window.setTimeout(function(){

        if(listJsonFlowDetails.length == 0){
            setFlowTypeDoc();
        }else {
            /** for case modify flow */
            var approveTypeFlow = null;
            if(FLIGHT_TYPE_ACTIVE){
                approveTypeFlow = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
            }else if(FLIGHT_TYPE_ACTIVE == false && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                approveTypeFlow = MASTER_DATA.APR_TYPE_SET_FOREIGN;
            }else{
                approveTypeFlow = $DATA_DOCUMENT.approveType;
            }
            var jsonData = {};
            jsonData['requester'] = userName;
            jsonData['approveType'] = approveTypeFlow;
            jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
            jsonData['details'] = JSON.stringify(listJsonFlowDetails);

            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/getAuthorizeForLineApprove',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if(xhr.status == 200 && xhr.responseText != null){
                            $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                            $('.dv-background').hide();
                            renderLineApprove($DATA_LINE_APPROVE);
                        }else{
                            getAuthorizeForLineApprove(userName);
                        }

                    }
                }
            });
        }
    },500);
}

function renderLineApprove(requestApprover){

    $('.dv-background').show();

    window.setTimeout(function() {
        findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
        var requesterName = $DATA_EMPLOYEE.FOA + $DATA_EMPLOYEE.FNameTH + " " + $DATA_EMPLOYEE.LNameTH;
        var requesterPosition = $DATA_EMPLOYEE.PositionTH;
        var dataStep = requestApprover.length + 1;

        $("#lineApproverDetail").attr('data-steps', dataStep);
        $("#lineApproverDetail").empty();
        $("#lineApproveMobile").empty();
        $("#lineApproverDetail").append(
            '<li class="idle" style="text-align: center;">' +
            '<span class="step"><span><img src=' + IMG.REQUESTER + ' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>' +
            '<span class="name-idle">' +
            '<div>' + LB.LABEL_REQUESTER + '</div>' +
            '<div style="color: blue;">' + requesterName + '</div>' +
            '<div style="color: lightseagreen;">' + requesterPosition + '</div>' +
            '</span>' +
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">' +
            '<div class="container-fluid">' +
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">' +
            '<img src="' + IMG.REQUESTER + '" width="45px"/>' +
            '</div>' +
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>' + LB.LABEL_REQUESTER + '</b></label>' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterName + '</b></label>' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterPosition + '</b></label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        for (var i = 0; i < requestApprover.length; i++) {
            $("#lineApproverDetail").append(
                '<li class="idle" style="text-align: center;">' +
                '<span class="step"><span><img src=' + validateIMG(requestApprover[i].actionRoleCode) + ' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>' +
                '<span class="name-idle">' +
                '<div>' + requestApprover[i].actionRoleName + '</div>' +
                '<div style="color: blue;">' + requestApprover[i].name + '</div>' +
                '<div style="color: lightseagreen;">' + requestApprover[i].position + '</div>' +
                '</span>' +
                '</li>'
            );

            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">' +
                '<div class="container-fluid">' +
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">' +
                '<img src=' + validateIMG(requestApprover[i].actionRoleCode) + ' width="45px"/>' +
                '</div>' +
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>' + requestApprover[i].actionRoleName + '</b></label>' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requestApprover[i].name + '</b></label>' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requestApprover[i].position + '</b></label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );
        }
        $('.dv-background').hide();
    },500);
}

function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINANCE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
}

function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_SET_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
        return MASTER_DATA.FLOW_TYPE_SET_FOREIGN;
    }
}

function sendRequestDocument(){
    BTN_ACTION = "SEND";

    if($DATA_TRAVEL_DETAIL.length > 0){
        if( $("#advancesAmount").autoNumeric('get') == 0) {
            $("#borrowDate").val("");
            $("#advanceEndDate").val("");
            $('.dv-background').show();
            window.setTimeout(function () {
                var reason = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
                var approveType;

                if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC) {
                    approveType = MASTER_DATA.APR_TYPE_SET_DOMESTIC;
                } else {
                    approveType = MASTER_DATA.APR_TYPE_SET_FOREIGN;
                }

                if ($("#carReserve").is(':checked') || $("#hotelReserve").is(':checked') || $("#flightReserve").is(':checked')) {
                    var jsonDocument = {};
                    jsonDocument['parentId'] = 1;
                    jsonDocument[csrfParameter] = csrfToken;
                    jsonDocument['id'] = $DATA_DOCUMENT.id;
                    jsonDocument['ccMailAddress'] = MAIL_ADMIN;

                    var dataDocument = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/updateDocumentApprove',
                        data: jsonDocument,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                console.info("><>< UPDATE MAIL ADMIN");
                            }
                        }
                    });
                } else {
                    var jsonDocument = {};
                    jsonDocument['parentId'] = 1;
                    jsonDocument[csrfParameter] = csrfToken;
                    jsonDocument['id'] = $DATA_DOCUMENT.id;
                    jsonDocument['ccMailAddress'] = null;

                    var dataDocument = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/updateDocumentApprove',
                        data: jsonDocument,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                console.info("><>< UPDATE MAIL ADMIN");
                            }
                        }
                    });
                }

                var jsonDocumentApproveItem = {};
                jsonDocumentApproveItem['parentId'] = 1;
                jsonDocumentApproveItem[csrfParameter] = csrfToken;
                jsonDocumentApproveItem['id'] = verifyDocApproveItem($DATA_DOCUMENT.approveType);
                jsonDocumentApproveItem['approveType'] = approveType;
                jsonDocumentApproveItem['travelReason'] = reason;
                jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                var dataDocumentApproveItem = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/approve/saveDocumentApproveItem',
                    data: jsonDocumentApproveItem,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            dataDocumentApproveItem = JSON.parse(xhr.responseText);
                            STATUS_SAVE_DOCUMENT = "SUCCESS";
                            $('.dv-background').hide();
                            if ($("#carReserve").is(':checked')) {
                                saveCarBooking();
                            }
                            if ($("#hotelReserve").is(':checked')) {
                                saveHotelBooking();
                            }
                            if ($("#flightReserve").is(':checked')) {
                                saveFlightTicket();
                            }
                            saveAdvances();


                            if (DATA_RESULT_ADVANCE != null && btn != "SEND") {
                                $("#completeDetail").text(MSG.MESSAGE_UPDATE_SUCCESS);
                                $("#completeModal").modal('show');
                            }
                        }
                    }
                });
            }, 500);
        }else{
            if($("#borrowDate").val() != "" && $("#advanceEndDate").val() != "" && $("#bankNumber").val() != ""){
                $('.dv-background').show();
                window.setTimeout(function () {
                    var reason = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
                    var approveType;

                    if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC) {
                        approveType = MASTER_DATA.APR_TYPE_SET_DOMESTIC;
                    } else {
                        approveType = MASTER_DATA.APR_TYPE_SET_FOREIGN;
                    }

                    if ($("#carReserve").is(':checked') || $("#hotelReserve").is(':checked') || $("#flightReserve").is(':checked')) {
                        var jsonDocument = {};
                        jsonDocument['parentId'] = 1;
                        jsonDocument[csrfParameter] = csrfToken;
                        jsonDocument['id'] = $DATA_DOCUMENT.id;
                        jsonDocument['ccMailAddress'] = MAIL_ADMIN;

                        var dataDocument = $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/approve/updateDocumentApprove',
                            data: jsonDocument,
                            async: false,
                            complete: function (xhr) {
                                if (xhr.readyState == 4) {
                                    console.info("><>< UPDATE MAIL ADMIN");
                                }
                            }
                        });
                    } else {
                        var jsonDocument = {};
                        jsonDocument['parentId'] = 1;
                        jsonDocument[csrfParameter] = csrfToken;
                        jsonDocument['id'] = $DATA_DOCUMENT.id;
                        jsonDocument['ccMailAddress'] = null;

                        var dataDocument = $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/approve/updateDocumentApprove',
                            data: jsonDocument,
                            async: false,
                            complete: function (xhr) {
                                if (xhr.readyState == 4) {
                                    console.info("><>< UPDATE MAIL ADMIN");
                                }
                            }
                        });
                    }

                    var jsonDocumentApproveItem = {};
                    jsonDocumentApproveItem['parentId'] = 1;
                    jsonDocumentApproveItem[csrfParameter] = csrfToken;
                    jsonDocumentApproveItem['id'] = verifyDocApproveItem($DATA_DOCUMENT.approveType);
                    jsonDocumentApproveItem['approveType'] = approveType;
                    jsonDocumentApproveItem['travelReason'] = reason;
                    jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                    var dataDocumentApproveItem = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveDocumentApproveItem',
                        data: jsonDocumentApproveItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                dataDocumentApproveItem = JSON.parse(xhr.responseText);
                                STATUS_SAVE_DOCUMENT = "SUCCESS";
                                $('.dv-background').hide();
                                if ($("#carReserve").is(':checked')) {
                                    saveCarBooking();
                                }
                                if ($("#hotelReserve").is(':checked')) {
                                    saveHotelBooking();
                                }
                                if ($("#flightReserve").is(':checked')) {
                                    saveFlightTicket();
                                }
                                saveAdvances();


                                if (DATA_RESULT_ADVANCE != null && btn != "SEND") {
                                    $("#completeDetail").text(MSG.MESSAGE_UPDATE_SUCCESS);
                                    $("#completeModal").modal('show');
                                }
                            }
                        }
                    });
                }, 500);
            }else{
                $('#warningModal .modal-body').html(LB.LABEL_PLEASE_FILL_ADVANCE_DATA);
                $('#warningModal').modal('show');
            }
        }
    }else {
        $('#warningModal .modal-body').html(MSG.MESSAGE_TRAVEL_DETAIL_NO_SIZE);
        $('#warningModal').modal('show');
    }
}

function validateLineApprove(username){

    var dataUserNameOtherCostCenter = null;
    $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                var dataRequester = JSON.parse(xhr.responseText);

                $.ajax({
                    url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + username,
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            var dataMember = JSON.parse(xhr.responseText);
                            if (dataRequester.Personal_Cost_center != dataMember.Personal_Cost_center) {
                                FLAG_CREATE_DOC_MEMBER++;
                                dataUserNameOtherCostCenter = username;
                            }else {
                                FLAG_USER_SAME_COSTCENTER++;
                            }
                        }
                    }
                });
            }
        }
    });

    if(dataUserNameOtherCostCenter != null){
        findEmployeeProfileByUserName(username);
        if($DATA_EMPLOYEE.User_name && $DATA_EMPLOYEE.LineOrgID != "30000001"){
            createDocForDocSet(username,$DATA_DOCUMENT.id);
        }else{
            sendRequest();
        }

    }

}

function createDocForDocSet(requester,docId){

    $('.dv-background').show();

    window.setTimeout(function() {
        var data = $.ajax({
            url: session.context + "/approve/findByDocumentId/" + docId,
            headers: {
                Accept: "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;
        $DATA_DOCUMENT_SET = data;

        var approveType = validateApproveTypeForSaveApproveItem($DATA_DOCUMENT.approveType);
        findEmployeeProfileByUserName(requester);

        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['approveType'] = approveType;
        jsonDocumentApproveItem['travelReason'] = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');

        var jsonDocumentApprove = {};
        jsonDocumentApprove['remark'] = "";

        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['documentType'] = MASTER_DATA.DOC_TYPE;
        jsonData['documentStatus'] = MASTER_DATA.DOC_STATUS_DRAFT;
        jsonData['requester'] = requester;
        jsonData['companyCode'] = $DATA_EMPLOYEE.Position_PA_ID;
        jsonData['departmentCode'] = $DATA_EMPLOYEE.Org_ID_800;
        jsonData['costCenterCode'] = $DATA_EMPLOYEE.Personal_Cost_center;
        jsonData['approveType'] = approveType;
        jsonData['titleDescription'] = $("#travelReason").val().replace(/\s+/g," ").replace(/[&\/\\,'"]/g,'');
        jsonData['psa'] = $DATA_DOCUMENT_SET.psa;
        jsonData['userNameMember'] = requester;
        jsonData['documentApprove'] = JSON.stringify(jsonDocumentApprove);

        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocument',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    var dataDocument = JSON.parse(xhr.responseText);

                    jsonDocumentApproveItem['document'] = dataDocument.id;

                    var dataDocumentApproveItem = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveDocumentApproveItem',
                        data: jsonDocumentApproveItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                dataDocumentApproveItem = JSON.parse(xhr.responseText);

                                if (approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN) {
                                    var jsonDataMember = {};
                                    jsonDataMember['parentId'] = 1;
                                    jsonDataMember[csrfParameter] = csrfToken;
                                    jsonDataMember['memberCode'] = $DATA_EMPLOYEE.EmployeeId;
                                    jsonDataMember['memberPersonalId'] = $DATA_EMPLOYEE.Personal_ID;
                                    jsonDataMember['memberUser'] = requester;
                                    jsonDataMember['documentApproveItem'] = dataDocumentApproveItem.id;

                                    var dataMember = $.ajax({
                                        type: "POST",
                                        headers: {
                                            Accept: 'application/json'
                                        },
                                        url: session['context'] + '/approve/saveTravelMember',
                                        data: jsonDataMember,
                                        async: false,
                                        complete: function (xhr) {
                                            if (xhr.readyState == 4) {

                                            }
                                        }
                                    });


                                    var dataDocumentSet = $DATA_DOCUMENT_SET.documentApprove.documentApproveItem;
                                    for (var i = 0; i < dataDocumentSet.length; i++) {
                                        if (dataDocumentSet[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || dataDocumentSet[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN) {
                                            var dataTravelDetailSet = dataDocumentSet[i].travelDetail;

                                            var jsonDataTravel = {};
                                            jsonDataTravel['parentId'] = 1;
                                            jsonDataTravel[csrfParameter] = csrfToken;
                                            jsonDataTravel['origin'] = dataTravelDetailSet[0].origins.id;
                                            jsonDataTravel['destination'] = dataTravelDetailSet[0].destinations.id;
                                            jsonDataTravel['startDate'] = DateUtil.coverDateToString(dataTravelDetailSet[0].startDate);
                                            jsonDataTravel['endDate'] = DateUtil.coverDateToString(dataTravelDetailSet[0].endDate);
                                            jsonDataTravel['startTime'] = dataTravelDetailSet[0].startTime;
                                            jsonDataTravel['endTime'] = dataTravelDetailSet[0].endTime;
                                            jsonDataTravel['remark'] = dataTravelDetailSet[0].remark;
                                            jsonDataTravel['otherOrigin'] = dataTravelDetailSet[0].otherOrigin;
                                            jsonDataTravel['otherDestination'] = dataTravelDetailSet[0].otherDestination;
                                            jsonDataTravel['documentApproveItem'] = dataDocumentApproveItem.id;

                                            var dataDiffDays = 0;
                                            $.ajax({
                                                type: "GET",
                                                headers: {
                                                    Accept: 'application/json'
                                                },
                                                contentType: "application/json; charset=utf-8",
                                                dataType: "json",
                                                url: session['context'] + '/approve/calculateDiffDate',
                                                data: {
                                                    startDate: DateUtil.coverDateToString(dataTravelDetailSet[0].startDate),
                                                    endDate: DateUtil.coverDateToString(dataTravelDetailSet[0].endDate),
                                                    startTime: dataTravelDetailSet[0].startTime,
                                                    endTime: dataTravelDetailSet[0].endTime
                                                },
                                                async: false,
                                                complete: function (xhr) {
                                                    xhr.status
                                                    if (xhr.readyState == 4) {
                                                        dataDiffDays = JSON.parse(xhr.responseText);
                                                        jsonDataTravel['travelDays'] = dataDiffDays;

                                                        var dataTravel = $.ajax({
                                                            type: "POST",
                                                            headers: {
                                                                Accept: 'application/json'
                                                            },
                                                            url: session['context'] + '/approve/saveTravelDetail',
                                                            data: jsonDataTravel,
                                                            async: false,
                                                            complete: function (xhr) {
                                                                if (xhr.readyState == 4) {
                                                                    $('.dv-background').show();
                                                                    window.setTimeout(function() {
                                                                        var jsonDetails = {};
                                                                        var listJsonDetails = [];

                                                                        if (dataDocument.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC) {
                                                                            jsonDetails['amount'] = 1;
                                                                            jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_DOMESTIC;
                                                                            jsonDetails['costCenter'] = dataDocument.costCenterCode;

                                                                            listJsonDetails.push(jsonDetails);
                                                                        } else if (dataDocument.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN) {
                                                                            jsonDetails['amount'] = 1;
                                                                            jsonDetails['flowType'] = SPEACIAL_FLOW;
                                                                            jsonDetails['costCenter'] = dataDocument.costCenterCode;

                                                                            listJsonDetails.push(jsonDetails);
                                                                        }

                                                                        var jsonDataRequest = {};

                                                                        jsonDataRequest['id'] = dataDocument.id;
                                                                        jsonDataRequest['requester'] = dataDocument.requester;
                                                                        jsonDataRequest['company'] = 1;
                                                                        jsonDataRequest['approveType']  = dataDocument.approveType;
                                                                        jsonDataRequest['documentType'] = dataDocument.documentType;
                                                                        jsonDataRequest['tmpDocNumber'] = dataDocument.tmpDocNumber;
                                                                        jsonDataRequest['details'] = JSON.stringify(listJsonDetails);
                                                                        jsonDataRequest['pa'] = $("#company").attr('pa');
                                                                        jsonDataRequest['psa'] = $("#department").attr('psa');

                                                                        var dataRequest = $.ajax({
                                                                            type: "POST",
                                                                            headers: {
                                                                                Accept: 'application/json'
                                                                            },
                                                                            url: session['context'] + '/requests/createRequest',
                                                                            data: jsonDataRequest,
                                                                            async: false,
                                                                            complete: function (xhr) {
                                                                                console.log("=================readyState===================");
                                                                                console.log(xhr.readyState);
                                                                                if (xhr.status == 200) {
                                                                                    dataRequest = JSON.parse(xhr.responseText);
                                                                                    FLAG_REQUEST_MEMBER++;
                                                                                    $('.dv-background').hide();
                                                                                    if(FLAG_CREATE_DOC_MEMBER > 0 && FLAG_CREATE_DOC_MEMBER == FLAG_REQUEST_MEMBER){
                                                                                        sendRequest();
                                                                                    }
                                                                                }
                                                                            }
                                                                        });
                                                                    },1000);
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });

    },500);
}

function sendRequest(){

    $('.dv-background').show();
    window.setTimeout(function() {
        var approveTypeFlow = $DATA_DOCUMENT.approveType;
        var flowType;
        if(FLIGHT_TYPE_ACTIVE){
            approveTypeFlow = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
        }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            approveTypeFlow = MASTER_DATA.APR_TYPE_SET_FOREIGN;
        }

        var jsonData = {};
        jsonData['id'] = $DATA_DOCUMENT.id;
        jsonData['requester'] = $DATA_DOCUMENT.requester;
        jsonData['company'] = 1;
        jsonData['approveType']  = approveTypeFlow;
        jsonData['documentType'] = $DATA_DOCUMENT.documentType;
        jsonData['tmpDocNumber'] = $DATA_DOCUMENT.tmpDocNumber;
        jsonData['details'] = JSON.stringify(listJsonFlowDetails);
        jsonData['pa'] = $("#company").attr('pa');
        jsonData['psa'] = $("#department").attr('psa');

        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/createRequest',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_REQUEST = JSON.parse(xhr.responseText);
                    if ($DATA_REQUEST != null) {
                        $('.dv-background').hide();
                        FLAG_REQUEST_MEMBER = 0;
                        FLAG_CREATE_DOC_MEMBER = 0;
                        window.location.href = session.context + '/approve/approveMainMenu';
                    }
                }
            }
        });
    },500);
}

function setFlowTypeCar(){

    var countFlow = 0;
    var flowCar  = 0;
    if(CAR_TYPE_ACTIVE == true && $CarTypeCode != null && FLOW_CAR_ACTIVE == false){
        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_CAR;
        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_CAR){
                countFlow++;
            }
        }

        if(countFlow > 1){
            for(var i=0;i<listJsonFlowDetails.length;i++){
                if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_CAR && flowCar == 0){
                    listJsonFlowDetails.splice(i,1);
                    flowCar++;
                }
            }
        }else{
            listJsonFlowDetails.push(jsonDetails);
        }
        FLOW_CAR_ACTIVE = true;
    }else{
        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_CAR && flowCar == 0){
                listJsonFlowDetails.splice(i,1);
                flowCar++;
            }
        }
    }

}

function setFlowTypeAdvance(){
    /* validate component on change */
    var countAdvance = 0;
    var flowAdvance = 0;
    if(ADVANCE_TYPE_ACTIVE == true ){
        if($("#advancesAmount").val() != "" &&
            $("#advancesAmount").autoNumeric('get') != "0" &&
            $("#borrowDate").val() != "" &&
            $("#advanceEndDate").val() != "" &&
            $("#bankNumber").val() != "") {

            var jsonDetails = {};
            jsonDetails['amount'] = $("#advancesAmount").autoNumeric('get');
            jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
            jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

            for(var i=0;i<listJsonFlowDetails.length;i++){
                if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                    countAdvance++;
                }
            }

            if(countAdvance > 1){
                for(var i=0;i<listJsonFlowDetails.length;i++){
                    if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE && flowAdvance == 0){
                        listJsonFlowDetails.splice(i,1);
                        flowAdvance ++;
                    }
                }
            }else{
                listJsonFlowDetails.push(jsonDetails);
            }
        }
    }else{
        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                listJsonFlowDetails.splice(i,1);
            }
        }
    }
}

function setFlowTypeFlight(userName,person){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.responseText != null) {
                var dataEmployee = JSON.parse(xhr.responseText);
                var cLevel = dataEmployee.EESG_ID;
                var psa_requester = dataEmployee.Personal_PSA_ID;

                if($DATA_DOCUMENT.documentApprove.documentApproveItem.length > 0){
                    var dataDocApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
                    for(var i=0;i<dataDocApproveItem.length;i++){
                        if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                            var approveType = dataDocApproveItem[i].approveType;
                            $.ajax({
                                type: "GET",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context']+'/approve/'+dataDocApproveItem[i].id+'/travelDetails',
                                complete: function (xhr) {
                                    var dataTravel = JSON.parse(xhr.responseText);
                                    if(dataTravel.length > 0) {
                                        var psa_origin = dataTravel[0].origins.psaCode;
                                        var psa_destination = dataTravel[0].destinations.psaCode;
                                        var location_type = dataTravel[0].origins.locationType;
                                        var zone_code = dataTravel[0].destinations.zoneCode;

                                        if (FLIGHT_TYPE_ACTIVE) {
                                            validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code, MASTER_DATA.APR_TYPE_FIGHT_BOOKING, userName);
                                        }else {
                                            validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code,approveType, userName);
                                        }
                                    }else{
                                        $('#warningModal .modal-body').html(MSG.MESSAGE_TRAVEL_DETAIL_NO_SIZE);
                                        $('#warningModal').modal('show');
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
    });
}

function validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code,approveTypeFlow,userName){

    $('.dv-background').show();
    window.setTimeout(function() {
        var costCenter = $DATA_DOCUMENT.costCenterCode;

        if (userName != $DATA_DOCUMENT.requester) {
            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false,
                complete: function (xhr) {
                    if (xhr.responseText != null) {
                        var dataEmployee = JSON.parse(xhr.responseText);
                        costCenter = dataEmployee.Personal_Cost_center;
                    }
                }
            });
        }else{
            $.ajax({
                type: "GET",
                headers: {
                    Accept: 'application/json'
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: session['context'] + '/approve/validateFlowTypeApprove',
                data: {
                    cLevel: parseInt(cLevel),
                    psa_requester: psa_requester,
                    psa_origin: psa_origin,
                    psa_destination: psa_destination,
                    location_type: location_type,
                    zone_code: zone_code
                },
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if (xhr.responseText != null) {
                            var flowTypeName = xhr.responseText;

                            if (flowTypeName == "H001" || flowTypeName == "H002") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H001;
                            } else if (flowTypeName == "H003") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H003;
                            } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H004;
                            }

                            if (SPEACIAL_FLOW) {

                                var jsonDetails = {};
                                jsonDetails['amount'] = 1;
                                jsonDetails['flowType'] = SPEACIAL_FLOW;
                                jsonDetails['costCenter'] = costCenter;

                                if(listJsonFlowDetails.length > 0){
                                    for(var i=0;i<listJsonFlowDetails.length;i++){
                                        if(listJsonFlowDetails[i].flowType == SPEACIAL_FLOW){
                                            break;
                                        }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                                            listJsonFlowDetails.clear();
                                            listJsonFlowDetails.push(jsonDetails);
                                        } else{
                                            listJsonFlowDetails.push(jsonDetails);
                                        }
                                    }
                                }else{
                                    listJsonFlowDetails.push(jsonDetails);
                                }

                                if (ADVANCE_TYPE_ACTIVE && $("#advancesAmount").autoNumeric('get') != '0') {
                                    var jsonDetails = {};
                                    jsonDetails['amount'] = $("#advancesAmount").autoNumeric('get');
                                    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
                                    jsonDetails['costCenter'] = costCenter;

                                    var countAdvance = 0;

                                    for(var i=0;i<listJsonFlowDetails.length;i++){
                                        if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                                            countAdvance++;
                                        }
                                    }

                                    if(countAdvance > 1){
                                        for(var i=0;i<listJsonFlowDetails.length;i++){
                                            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                                                listJsonFlowDetails.splice(i,1);
                                            }
                                        }
                                    }else{
                                        listJsonFlowDetails.push(jsonDetails);
                                    }
                                }

                                if(FLIGHT_TYPE_ACTIVE){
                                    approveTypeFlow = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;

                                }

                                var jsonData = {};
                                jsonData['requester'] = userName;
                                jsonData['approveType'] = approveTypeFlow;
                                jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
                                jsonData['details'] = JSON.stringify(listJsonFlowDetails);

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/getAuthorizeForLineApprove',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $('.dv-background').hide();

                                            if (BTN_ACTION == "SEND" && $DATA_TRAVEL_MEMBER.length > 1) {
                                                DATA_LINE_APR_MEMBER = JSON.parse(xhr.responseText);
                                                sendRequestDocument();
                                            } else {
                                                $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                                                renderLineApprove($DATA_LINE_APPROVE);
                                            }
                                        }
                                    }
                                });

                            }
                        }
                    }
                }
            });
        }


    },500);
}

function setFlowTypeHotel(){

    var countHotel = 0;
    var flowFlight = 0;
    if(HOTEL_TYPE_ACTIVE == true){
        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_HOTEL;
        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_HOTEL){
                countHotel++;
            }
        }

        if(countHotel > 1){
            for(var i=0;i<listJsonFlowDetails.length;i++){
                if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_HOTEL && flowFlight == 0){
                    listJsonFlowDetails.splice(i,1);
                    flowFlight++;
                }
            }
        }else{
            listJsonFlowDetails.push(jsonDetails);
        }

    }else{
        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_HOTEL && flowFlight == 0){
                listJsonFlowDetails.splice(i,1);
                flowFlight++;

            }
        }
    }
}

function setFlowTypeDoc(){
    var documentApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;

    for(var i=0;i<documentApproveItem.length;i++){
        var approveType = documentApproveItem[i].approveType;
        if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
            var jsonDetails = {};
            jsonDetails['amount'] = 1;
            jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_DOMESTIC;
            jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
            listJsonFlowDetails.push(jsonDetails);
            getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
        }else if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false,
                complete: function (xhr) {
                    if (xhr.responseText != null) {
                        var dataEmployee = JSON.parse(xhr.responseText);
                        var cLevel = dataEmployee.EESG_ID;
                        var psa_requester = dataEmployee.Personal_PSA_ID;

                        $.ajax({
                            type: "GET",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/approve/' + documentApproveItem[i].id + '/travelDetails',
                            complete: function (xhr) {
                                var dataTravel = JSON.parse(xhr.responseText);

                                if (dataTravel != null && dataTravel.length > 0) {
                                    var psa_origin = dataTravel[0].origins.psaCode;
                                    var psa_destination = dataTravel[0].destinations.psaCode;
                                    var location_type = dataTravel[0].origins.locationType;
                                    var zone_code = dataTravel[0].destinations.zoneCode;

                                    if(FLIGHT_TYPE_ACTIVE){
                                        setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
                                    }else{
                                        validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code, approveType, $DATA_DOCUMENT.requester);
                                    }
                                }
                                else {

                                    var jsonDetails = {};
                                    jsonDetails['amount'] = 1;
                                    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_H003;
                                    jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
                                    listJsonFlowDetails.push(jsonDetails);

                                    getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}

function getParameterDueDateFromPACode(){
    var paCode = $DATA_DOCUMENT.companyCode;

    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterDueDateAdvance",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;


    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterDetailDueDateAdvance/"+data.id,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    DATA_PARAMETER_DUEDATE_ADVANCE = data.content;

    for(var i=0;i<DATA_PARAMETER_DUEDATE_ADVANCE.length;i++){
        if(DATA_PARAMETER_DUEDATE_ADVANCE[i].code == paCode){
            return parseInt(DATA_PARAMETER_DUEDATE_ADVANCE[i].variable1);
            break;
        }
    }
}

function defaultBankNumber(userName) {

    var data = $.ajax({
        url: session.context + "/intermediaries/getBankAccount/"+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && (data.profile != null || data.profile != "")){
        DATA_BANK_NUMBER = data.profile;
        $("#bankNumber").val(DATA_BANK_NUMBER);
        ADVANCE_TYPE_ACTIVE = true;
        setFlowTypeAdvance();
    }else{
        $("#bankNumber").val("");
        ADVANCE_TYPE_ACTIVE = false;
        setFlowTypeAdvance();
    }
}

function validateApproveTypeForSaveApproveItem(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
        return MASTER_DATA.APR_TYPE_SET_DOMESTIC;
    }else{
        return MASTER_DATA.APR_TYPE_SET_FOREIGN;
    }
}

function clearCarValue(){
    setUpDefaultCarType();
    $CarTypeCode = "";
    CAR_TYPE_ACTIVE = false;
    FLOW_CAR_ACTIVE = false;
    setFlowTypeCar();
}

function clearAdvanceValue(){
    $("#advancesMoney").text(0);
    $("#advancesAmount").val("");
    $("#borrowDate").val("");
    $("#advanceEndDate").val("");
    $("#dueDate").text("");

    if(DATA_BANK_NUMBER == undefined){
        $("#bankNumber").val("");
    }

    ADVANCE_TYPE_ACTIVE = false;
    setFlowTypeAdvance();
}

function clearHotelValue() {
    $("#hotelInputHotel").val("");
    $("#hotelPriceDiv").addClass('hide');
    var personMember = $("#personMember").text() == "" ? 0 : parseInt($("#personMember").text());
    var externalMember = $("#personExternal").text() == "" ? 0 : parseInt($("#personExternal").text());
    var summaryPerson = personMember + externalMember;
    if(summaryPerson == 0){
        $("#singleRoom").val("");
    }
    $("#otherHotelDiv").addClass('hide');
    HOTEL_TYPE_ACTIVE = false;
    setFlowTypeHotel();
}

function clearFlightValue(){
    $("#airline").val("");
    FLIGHT_TYPE_ACTIVE = false;
    setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
}

function checkLineApproveSet(){
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN || FLIGHT_TYPE_ACTIVE == true){
        setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
    }else{
        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
    }
}

function backToMenu(){
    window.location.href = session.context+'/approve/approveMainMenu';
}

function linkCarForRent() {
    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterReserveCarLink",
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseText != null){
                var dataParam = JSON.parse(xhr.responseText);

                var data = $.ajax({
                    url: session.context + "/parameters/" + dataParam.id + "/details",
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var paramDetail = JSON.parse(xhr.responseText);
                            if(paramDetail.content != null && paramDetail.content.length > 0){
                                var dataDetail = paramDetail.content;
                                $('#gridLinkCar').empty();
                                if (dataDetail.length != 0) {
                                    for (var i = 0; i < dataDetail.length; i++) {
                                        $('#gridLinkCar').append('' +
                                            '<tr id="' + dataDetail[i].id + '">' +
                                            '<td align="center">' + dataDetail[i].code + '</td>' +
                                            '<td align="center">' + dataDetail[i].description + '</td>' +
                                            '<td align="center"><a href="javascript:void(0)" onclick="redirectToLinkCarMis(this)" data-link="'+dataDetail[i].variable1+'" style="color:blue" >'+(dataDetail[i].variable1 == null ? "-" : dataDetail[i].variable1)+'</a></td>' +
                                            '</tr>'
                                        );
                                    }
                                }

                                $("#modalLinkCar").modal('show');
                            }
                        }
                    }
                });
            }
        }
    });
}

var dataLink;
function redirectToLinkCarMis(index){
    var link = index.getAttribute('data-link');
    window.open(link,"_blank");
}

function linkHotelReservation(){

    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterHotelReservationTemplete",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.responseText != null) {
                var dataParam = JSON.parse(xhr.responseText);

                var data = $.ajax({
                    url: session.context + "/parameters/" + dataParam.id + "/details",
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var paramDetail = JSON.parse(xhr.responseText);
                            if (paramDetail.content != null && paramDetail.content.length > 0) {
                                var dataDetail = paramDetail.content;
                                location.href= session.context+'/intermediaries/downloadFileReservationTemplate?reservationType='+'H'+'&fileName='+dataDetail[0].variable1;
                            }
                        }
                    }
                });
            }
        }
    });
}

function linkFlightTicketForm(){
    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterFlightReservationFileDownload",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.responseText != null) {
                var dataParam = JSON.parse(xhr.responseText);

                var data = $.ajax({
                    url: session.context + "/parameters/" + dataParam.id + "/details",
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var paramDetail = JSON.parse(xhr.responseText);
                            if (paramDetail.content != null && paramDetail.content.length > 0) {
                                var dataDetail = paramDetail.content;
                                location.href= session.context+'/intermediaries/downloadFileReservationTemplate?reservationType='+'F'+'&fileName='+dataDetail[0].variable1;
                            }
                        }
                    }
                });
            }
        }
    });
}

function linkCarForRentForm(){

    var data = $.ajax({
        url: session.context + "/intermediaries/findParameterCarReservationFileDownload",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.responseText != null) {
                var dataParam = JSON.parse(xhr.responseText);

                var data = $.ajax({
                    url: session.context + "/parameters/" + dataParam.id + "/details",
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var paramDetail = JSON.parse(xhr.responseText);
                            if (paramDetail.content != null && paramDetail.content.length > 0) {
                                var dataDetail = paramDetail.content;
                                location.href= session.context+'/intermediaries/downloadFileReservationTemplate?reservationType='+'C'+'&fileName='+dataDetail[0].variable1;
                            }
                        }
                    }
                });
            }
        }
    });
}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#company").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#company").attr('pa',paData.PaCode);
    }else{
        $("#company").text("-");
        $("#company").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#department").attr('psa',psaData.PsaCode);
    }else{
        $("#department").text("-");
        $("#department").attr('psa',"");
    }

    var papsa = $("#company").attr('pa')+"-"+$("#department").attr('psa');

    var data = $.ajax({
        url: session.context + "/intermediaries/findAccountByPapsa/"+papsa+"/"+$DATA_DOCUMENT.personalId,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data) {
        var dataAccountSplit = data.profile.split(':');
        if (dataAccountSplit[1] != "0") {
            var accountName = dataAccountSplit[1].split("-");

            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + accountName[0],
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if (data) {

                if ($("#accountName").text() == "") {
                    $("#accountName").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);
                }
            }
        }
    }
}

var USERADMIN;
function findAdminByPapsaAndRequester(requester) {
    var papsa = $("#company").attr('pa')+"-"+$("#department").attr('psa');

    var data = $.ajax({
        url: session.context + "/intermediaries/findAdminByPapsa/"+papsa+"/"+requester,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data) {
        var dataAdminSplit = data.profile.split(':');
        if (dataAdminSplit[1] != "0") {
            var adminName = dataAdminSplit[1].split("-");

            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + adminName[0],
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(data){

                USERADMIN =  data;

                if($("#adminName").text() == ""){
                    $("#adminName").text(data.FOA+data.FNameTH+' '+data.LNameTH);
                }

                var emailAdmin = data.Email.toLowerCase();
                MAIL_ADMIN = data.Email.toLowerCase();

                // var jsonDocument = {};
                // jsonDocument['parentId'] = 1;
                // jsonDocument[csrfParameter] = csrfToken;
                // jsonDocument['id'] = $DATA_DOCUMENT.id;
                // jsonDocument['ccMailAddress'] = emailAdmin;
                //
                // var dataDocument = $.ajax({
                //     type: "POST",
                //     headers: {
                //         Accept: 'application/json'
                //     },
                //     url: session['context'] + '/approve/updateDocumentApprove',
                //     data: jsonDocument,
                //     async: false,
                //     complete: function (xhr) {
                //         if (xhr.readyState == 4) {
                //             console.info("><>< UPDATE MAIL ADMIN");
                //         }
                //     }
                // });
            }
        }
    }
}

function preViewAttachmentFileExpenseItem(id,fileName) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(fileType == 'pdf' || fileType == 'PDF' || fileType =='txt' || fileType == 'TXT'){
        var url = session['context']+'/approve/preViewPDFDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(url, '', properties);
    }

    if(fileType == 'png' || fileType == 'PNG' || fileType =='jpg' || fileType == 'JPG' || fileType == 'jpeg'  || fileType == 'JPEG'){


        var urlIMG = session['context']+'/approve/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName


        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(urlIMG, '', properties);
    }

}

function validateApproveItem(approveType) {

    if(approveType == "CAR"){
        approveType = MASTER_DATA.APR_TYPE_CAR;
    }else if(approveType == "HOTEL"){
        approveType = MASTER_DATA.APR_TYPE_HOTEL;
    }else if(approveType == "FLIGHT"){
        approveType = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
    }

    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/approve/deleteDocumentApproveItem',
        data: {
            documentId: $DATA_DOCUMENT.id,
            approveType: approveType
        },
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {

            }
        }
    });
}