var type = null;
var $DATA_TRAVEL_DETAIL;
var $DATA_TRAVEL_MEMBER;
var $DATA_EXTERNAL_MEMBER;
var $DATA_CAR_BOOKING;
var $DATA_DOCUMENT_ATTACHMENT;
var $STATUS;
var $TravelDetailId;
var $TravelMemberId;
var $ExternalMemberId;
var $CarTypeCode;
var $fileUpload="";
var $DATA_EMPLOYEE;
var $DATA_REQUEST;
var $DATA_REQUEST_APPROVER;
var $DATA_APPROVE;
var $APP_TYPE;
var checkStatus=0;
var checkStatusReject = 0;
var flagApprove = false;

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-80) + 'px';
    }
}

$(document).ready(function () {
    $('.dv-background').show();
    readMasterDataDocType();
    readMasterDataDocStatus();
    readMasterDataAprType();
    readMasterDataCarType();
    readMasterDataAttachmentType();
    readMasterDataActionReason();
    readMasterDataAirline();
    readMasterDataHotel();
    readMasterDataRequestStatus();

    $.material.init();
    moveFixedActionButton();
    window.onresize = function (event) {
        moveFixedActionButton()
    };

    $("#222").keypress(function (e) {
        e.preventDefault();
    });

    $("#dateStartCarADM").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#dateHotelADM").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $('#timeStartCarADM').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });

    $('#timeEndCarADM').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i",
        minuteIncrement : 1
    });


    /* render value document */
    findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
    $("#docNumber").text($DATA_DOCUMENT.docNumber);
    $("#sendTime").text($DATA_DOCUMENT.sendDate == null ? "SEND TIME" : $DATA_DOCUMENT.sendDate);
    if($DATA_EMPLOYEE != null) {
        $("#requester").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        $("#empCode").text($DATA_EMPLOYEE.Personal_ID);
        $("#position").text($DATA_EMPLOYEE.PositionTH);
        $("#costCenter").text($DATA_DOCUMENT.costCenterCode);
        findRequestByDocument($DATA_DOCUMENT.id);
    }

    if($DATA_DOCUMENT.companyCode){
        findByPaCode($DATA_DOCUMENT.companyCode);
    }

    if($DATA_DOCUMENT.psa){
        findByPsaCode($DATA_DOCUMENT.psa);
    }


    findEmployeeProfileByUserName($DATA_DOCUMENT.createdBy);
    $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);

    /* render travel detail */
    window.setTimeout(function(){

        validateApproveType();
        validateDocStatus($DATA_DOCUMENT.documentStatus);
        setUpDefaultCarType();

        if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id != null){
            findTravelDetailsByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findTravelMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findCarBookingByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItems[0].id);
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);

            if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking != null){
                renderDataHotelBooking($DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking);
                if($DATA_DOCUMENT.documentReference[0] != null){
                    $("#hotelDocRef").text($DATA_DOCUMENT.documentReference[0].docReferenceNumber);
                }

            }if($DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket != null){
                renderDataFlightTicket($DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket);
                if($DATA_DOCUMENT.documentReference[0] != null){
                    $("#docRefFlight").text($DATA_DOCUMENT.documentReference[0].docReferenceNumber);
                }
            }
        }

        $('.dv-background').hide();
    },4000);

    /* manage cancel document */
    $("#confirmCancel").on('click',function(){
        cancelRequest();
    });

    $("#imageCalendarCarADM").on('click',function () {
        $("#dateStartCarADM").focus()
    });

    $("#imageCalendarHotelADM").on('click',function () {
        $("#dateHotelADM").focus()
    });

});

function findCostCenter(costCenter) {
    $('.dv-background').show();
    setTimeout(function () {

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data){
            var costCenterSplit = data.profile.split('#');
            if(costCenterSplit[2] != "0"){
                var paPsaSplit = costCenterSplit[2].split('-');
                $("#company").text(paPsaSplit[0]+" : "+paPsaSplit[1]);
                $("#company").attr('pa',paPsaSplit[0]);
                $("#department").text(paPsaSplit[2]+" : "+paPsaSplit[3]);
                $("#department").attr('psa',paPsaSplit[2]);
                $('.dv-background').hide();
            }else{
                $('.dv-background').hide();
            }
        }

    },1000);

}

function validateAuthorize(userName,requester){
    if(userName != requester){
        validateBTN($DATA_DOCUMENT.documentStatus,"approver");
    }else{
        validateBTN($DATA_DOCUMENT.documentStatus,"requester");
    }
}

function validateApproveType(){

    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#divDOMESTIC").removeClass("hide");
        $("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason);
        getMasterDataLocation("D");
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN){
        $("#divFOREIGN").removeClass("hide");
        $("#foreignReason").val($DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].travelReason);
        getMasterDataLocation("F");
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_CAR){
        if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
            $("#divCAR").addClass('hide');
            $("#divCARADM").removeClass('hide');
        }else{
            $("#divCAR").removeClass("hide");
        }
        getMasterDataLocation("D");
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_HOTEL){
        if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
            $("#divHotelUser").addClass('hide');
            $("#divHOTELADM").removeClass('hide');
        }else{
            $("#divHotelUser").removeClass("hide");
        }
        getMasterDataLocation("D");
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
            $("#divFLIGHT").addClass('hide');
            $("#divFLIGHTADM").removeClass('hide');
        }else{
            $("#divFLIGHT").removeClass("hide");
        }
    }
}

function validateDocStatus(documentStatus){
    /* update by siriradC.  2017.08.04 */
    if(documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }

    validateAuthorize($USERNAME,$DATA_DOCUMENT.requester);
}


function validateBTN(){

    /* update by siriradC.  2017.08.04 */
    if($USERNAME == $DATA_DOCUMENT.requester) {
        if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
            $("#btnCancelDiv").removeClass('hide');
            $("#btnAttachFileDiv").removeClass('hide');
            $("#btnCopyDocDiv").removeClass('hide');
            $("#divBtnQRCode").removeClass('hide');
            $("#divBtnBlack").removeClass('hide');

            $("#divCancel").removeClass('hide');
            $("#divFile").removeClass('hide');
            $("#divCopy").removeClass('hide');
            $("#divQr").removeClass('hide');

            $("#btnCancelDiv").addClass('btn-0');
            $("#btnAttachFileDiv").addClass('btn-80');
            $("#btnCopyDocDiv").addClass('btn-160');
            $("#divBtnQRCode").addClass('btn-240');
            $("#divBtnBlack").addClass('btn-320');

            if(flagApprove){
                if($USERNAME == $DATA_DOCUMENT.requester){
                    $("#divCancel").addClass('hide');
                    $("#btnCancelDiv").addClass('hide');

                    $("#btnAttachFileDiv").removeClass('btn-80');
                    $("#btnAttachFileDiv").addClass('btn-0');
                    $("#btnCopyDocDiv").removeClass('btn-160');
                    $("#btnCopyDocDiv").addClass('btn-80');
                    $("#divBtnQRCode").removeClass('btn-240');
                    $("#divBtnQRCode").addClass('btn-160');
                    $("#divBtnBack").removeClass('btn-320');
                    $("#divBtnBack").addClass('btn-240');
                }
            }

        } else if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL ||
            $DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT ||
            $DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE) {

            $("#btnAttachFileDiv").removeClass('hide');
            $("#btnCopyDocDiv").removeClass('hide');

            $("#divFile").removeClass('hide');
            $("#divCopy").removeClass('hide');

            $("#btnAttachFileDiv").addClass('btn-0');
            $("#btnCopyDocDiv").addClass('btn-80');
        }
    }else if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
        $("#btnApproveDiv").removeClass('hide');
        $("#actionReasonDiv").removeClass('hide');
        $("#btnAttachFileDiv").removeClass('hide');

        $("#divTick").removeClass('hide');
        $("#divFile").removeClass('hide');

        $("#btnApproveDiv").addClass('btn-0');
        $("#btnAttachFileDiv").addClass('btn-80');
    }else{
        $("#actionReasonDiv").removeClass('hide');
        $("#btnApproveDiv").removeClass('hide');
        $("#btnCancelDiv").removeClass('hide');
        $("#btnAttachFileDiv").removeClass('hide');

        $("#btnApproveDiv").addClass('btn-0');
        $("#btnCancelDiv").addClass('btn-80');
        $("#btnAttachFileDiv").addClass('btn-160');

        /* mobile */
        $("#divTick").removeClass('hide');
        $("#divCancel").removeClass('hide');
        $("#divFile").removeClass('hide');

    }
}

function setUpDefaultCarType(){
    if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){
        for(var i=0;i<$("[name=iconCarADM]").length;i++){
            if('van' == $("[name=iconCarADM]")[i].getAttribute('value')){
                $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
                $("[name=iconCarADM]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
            }else if('sedan' == $("[name=iconCarADM]")[i].getAttribute('value')){
                $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
                $("[name=iconCarADM]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
            }else if('tuck' == $("[name=iconCarADM]")[i].getAttribute('value')){
                $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
                $("[name=iconCarADM]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
            }else if('other' == $("[name=iconCarADM]")[i].getAttribute('value')){
                $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
                $("[name=iconCarADM]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
            }
        }
    }else{
        for(var i=0;i<$("[name=iconCar]").length;i++){
            if('van' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
            }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
            }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
            }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
            }
        }
    }
}

/* for get employee profile by user name */
function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;
}

/* Manage Request */
function findRequestByDocument(id){
    var data = $.ajax({
        url: session.context + "/approve/findRequestByDocument/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST = data;

    if($DATA_REQUEST!= null){
        var data = $.ajax({
            url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST_APPROVER = data;
        renderLineApprove($DATA_REQUEST_APPROVER,"REQUEST");
    }else{
        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
    }
}

function getAuthorizeForLineApprove(userName){

    var flowType;
    if($DATA_DOCUMENT.approveType != MASTER_DATA.APR_TYPE_FOREIGN && $DATA_DOCUMENT.approveType != MASTER_DATA.APR_TYPE_FIGHT_BOOKING) {
        flowType = validateFlowTypeByApproveType($DATA_DOCUMENT.approveType);

        var listJsonDetails = [];

        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = flowType;
        jsonDetails['costCenter']  = $DATA_DOCUMENT.costCenterCode;


        listJsonDetails.push(jsonDetails);

        var jsonData = {};
        jsonData['requester']   = userName;
        jsonData['approveType'] = $DATA_DOCUMENT.approveType;
        jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
        jsonData['details'] = JSON.stringify(listJsonDetails);

        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/getAuthorizeForLineApprove',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                    renderLineApprove($DATA_LINE_APPROVE,"DOC");
                }
            }
        });
    }else{
        checkLineApproveSpecialCase();
    }
}


function renderLineApprove(requestApprover,status){
    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;

    $("#lineApproverDetail").attr('data-steps',dataStep);

    if(status == "REQUEST"){
        $("#lineApproverDetail").append(

            '<li class="idle-complete" style="text-align: center;">'+
            '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
            '<span class="name-idle">'+
            '<div>'+LB.LABEL_REQUESTER+'</div>'+
            '<div style="color: blue;">'+requesterName+'</div>'+
            '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );


        for(var i=0;i<requestApprover.length;i++){
            findEmployeeProfileByUserName(requestApprover[i].userNameApprover);
            $("#lineApproverDetail").append(
                '<li class='+validateStatus(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
                '<span class="step"><span>'+ validateStatusReject(requestApprover[i].actionState)+'</span></span>'+
                '<span class="name-idle">'+
                '<div>'+requestApprover[i].actionStateName+'</div>'+
                '<div style="color: blue;">'+requestApprover[i].approver+'</div>'+
                '<div style="color: lightseagreen;">'+$DATA_EMPLOYEE.PositionTH+'</div>'+
                '</span>'+
                '</li>'
            );

            if(requestApprover[i].requestStatusCode == null){
                $("#lineApproveMobile").append('' +
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                    '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                    '<div class="container-fluid">'+
                    '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                    '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                    '</div>'+
                    '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+requestApprover[i].actionStateName+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].approver+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+$DATA_EMPLOYEE.PositionTH+'</b></label>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }else if(requestApprover[i].requestStatusCode == "REJ"){
                $("#lineApproveMobile").append('' +
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                    '<div class="panel-heading collapseLightRed" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                    '<div class="container-fluid">'+
                    '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                    '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                    '</div>'+
                    '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+requestApprover[i].actionStateName+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].approver+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+$DATA_EMPLOYEE.PositionTH+'</b></label>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
            else{
                $("#lineApproveMobile").append('' +
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                    '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                    '<div class="container-fluid">'+
                    '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                    '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                    '</div>'+
                    '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+requestApprover[i].actionStateName+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].approver+'</b></label>'+
                    '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+$DATA_EMPLOYEE.PositionTH+'</b></label>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
            flagApprove = true;

        }
    }
    else{
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
            '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
            '<span class="name-idle">'+
            '<div>'+LB.LABEL_REQUESTER+'</div>'+
            '<div style="color: blue;">'+requesterName+'</div>'+
            '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );

        for(var i=0;i<requestApprover.length;i++){
            $("#lineApproverDetail").append(

                '<li class="idle" style="text-align: center;">'+
                '<span class="step"><span><img src='+validateIMG(requestApprover[i].actionRoleCode)+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
                '<span class="name-idle">'+
                '<div>'+requestApprover[i].actionRoleName+'</div>'+
                '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
                '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
                '</span>'+
                '</li>'
            );

            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionRoleCode)+' width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+requestApprover[i].actionRoleName+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].name+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].position+'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );
        }
    }
}



function validateStatus(status){

    if(status != null && status != "REJ"){
        return "idle-complete";
    }else if(status != null && status == "REJ"){
        checkStatusReject++;
        return "idle";
    }else{
        checkStatus++
        return "idle";
    }
}


function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINANCE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
}


function validateStatusReject(actionState){
    if(checkStatus === 1 && checkStatusReject === 0){
        return '<img class="animation-border two"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }else if(checkStatusReject === 1 && checkStatus === 0){
        return '<img class="animation-border three"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }else{
        return '<img src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span>';
    }
}

function findTravelDetailsByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
        complete: function (xhr) {
            $DATA_TRAVEL_DETAIL = JSON.parse(xhr.responseText);
            renderDataTravelDetail($DATA_TRAVEL_DETAIL);
        }
    });
}

function renderDataTravelDetail(dataTravel){
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderTravel").empty();
        if(dataTravel.length > 0){
            for(var i=0;i<dataTravel.length;i++){
                var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
                var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);

                $("#collapseHeaderTravel").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravel[i].id+'>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:18px;"><b>'+dataTravel[i].origin.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:16px;"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                    '</div>'+

                    '<div class="col-xs-12 col-sm-4" id='+dataTravel[i].id+'>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:18px;"><b>'+dataTravel[i].destination.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:16px;"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-6 col-sm-2" id='+dataTravel[i].id+'>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-6 col-sm-1">'+
                    '<div class="col-sm-12"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    }else{
        $("#collapseForiegnTravel").empty();
        if(dataTravel.length > 0){
            for(var i=0;i<dataTravel.length;i++){
                var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
                var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);

                $("#collapseForiegnTravel").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravel[i].id+'>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:18px;"><b>'+dataTravel[i].origin.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:16px;"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravel[i].id+'>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:18px;"><b>'+dataTravel[i].destination.description+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:16px;"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-6 col-sm-2" id='+dataTravel[i].id+'>'+
                    '<div class="col-sm-12"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-6 col-sm-1">'+
                    '<div class="col-sm-12"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
        }
    }


    $("#modalAddDetail").modal('hide');
}

var remark;
function remarkDetail(obj){
    remark = obj;
    for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
        if($DATA_TRAVEL_DETAIL[i].id == parseInt(obj.id)){
            $("#alertModal").modal('show');
            $("label[id=detailAlert]").text($DATA_TRAVEL_DETAIL[i].remark);
        }
    }
}

function findTravelMembersByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelMembers',
        complete: function (xhr) {
            $DATA_TRAVEL_MEMBER = JSON.parse(xhr.responseText);
            renderDataTravelMember($DATA_TRAVEL_MEMBER);

        }
    });
}

function renderDataTravelMember(dataTravelMember){
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderMember").empty();
        $("#personMember").text(dataTravelMember.length);
        if(dataTravelMember.length > 0){
            for(var i=0;i<dataTravelMember.length;i++){
                findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                var memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
                var positionMember = $DATA_EMPLOYEE.PositionTH;
                var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;

                $("#collapseHeaderMember").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+' >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;"><label class="control-label custom" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;"><label class="control-label custom" style="color: white;font-size:14px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;"><label class="control-label custom" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+' >'+
                    '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;"><label class="control-label  custom" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
        }
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){
            $("#collapseFlightPersonADM").empty();
            if(dataTravelMember.length > 0){
                $("#collapseFlightPersonADM").attr('valueSize',dataTravelMember.length);
                for(var i=0;i<dataTravelMember.length;i++) {

                    findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                    var memberName = $DATA_EMPLOYEE.FOA + $DATA_EMPLOYEE.FNameTH + " " + $DATA_EMPLOYEE.LNameTH;
                    var positionMember = $DATA_EMPLOYEE.PositionTH;
                    var memberEng = $DATA_EMPLOYEE.NameEN;


                    $("#collapseFlightPersonADM").append(
                        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id=' + dataTravelMember[i].id + '>' +
                        '<div class="panel-heading collapseGray" id=' + dataTravelMember[i].id + ' role="button" style="padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" >' +
                        '<div class="form-group" style="margin-bottom: 0px">' +
                        '<div class="col-xs-12 col-sm-6" style="padding-left: 0px;padding-top: 5px;padding-right: 0px;" id=' + dataTravelMember[i].id + '  >' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>' + memberName + '</b></label></div>' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;text-align:left;font-size:16px"><b>' + memberEng + '</b></label></div>' +
                        '</div>' +
                        '<div class="col-xs-12 col-sm-6" style="padding-left: 0px;padding-top: 5px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' >' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;float:right"><b>' + positionMember + '</b></label></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        }else{
            $("#collapseFlightPerson").empty();
            if(dataTravelMember.length > 0){
                $("#collapseFlightPerson").attr('valueSize',dataTravelMember.length);
                for(var i=0;i<dataTravelMember.length;i++) {

                    findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                    var memberName = $DATA_EMPLOYEE.FOA + $DATA_EMPLOYEE.FNameTH + " " + $DATA_EMPLOYEE.LNameTH;
                    var positionMember = $DATA_EMPLOYEE.PositionTH;
                    var memberEng = $DATA_EMPLOYEE.NameEN;


                    $("#collapseFlightPerson").append(
                        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id=' + dataTravelMember[i].id + '>' +
                        '<div class="panel-heading collapseGray" id=' + dataTravelMember[i].id + ' role="button" style="padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" >' +
                        '<div class="form-group" style="margin-bottom: 0px">' +
                        '<div class="col-xs-12 col-sm-6" style="padding-left: 0px;padding-top: 5px;padding-right: 0px;" id=' + dataTravelMember[i].id + '  >' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>' + memberName + '</b></label></div>' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;text-align:left;font-size:16px"><b>' + memberEng + '</b></label></div>' +
                        '</div>' +
                        '<div class="col-xs-12 col-sm-6" style="padding-left: 0px;padding-top: 5px;padding-right: 0px;" id=' + dataTravelMember[i].id + ' >' +
                        '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;float:right"><b>' + positionMember + '</b></label></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        }

    }
    else{
        $("#collapseForeignMember").empty();
        $("#foreignMember").text(dataTravelMember.length);
        if(dataTravelMember.length > 0){
            for(var i=0;i<dataTravelMember.length;i++){

                findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
                var memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
                var positionMember = $DATA_EMPLOYEE.PositionTH;
                var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;

                $("#collapseForeignMember").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: white;font-size:18px;"><b>'+memberName+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: white"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+positionMember+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-4" id='+dataTravelMember[i].id+'  >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label " style="color: #b2ff59;font-size:18px"><b>'+departmentMember+'</b></label></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
        }
    }


    $("#modalAddPerson").modal('hide');
}

function findExternalMembersByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/externalMembers',
        complete: function (xhr) {
            $DATA_EXTERNAL_MEMBER = JSON.parse(xhr.responseText);
            renderDataExternalMember($DATA_EXTERNAL_MEMBER);
        }
    });
}

function renderDataExternalMember(dataExternal){
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        $("#collapseHeaderExternal").empty();
        $("#personExternal").text(dataExternal.length);
        if(dataExternal.length > 0){
            for(var i=0;i<dataExternal.length;i++){

                $("#collapseHeaderExternal").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
                    '<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px" >'+
                    '<div class="form-group" style="margin-bottom: 0px">'+
                    '<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+'  >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:16px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #08f508;font-size:14px;"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+'   >'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:16px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
                    '<div class="col-xs-12 col-sm-12"><label class="control-label custom-label" style="color: #08f508;font-size:14px;"><b>'+dataExternal[i].email+'</b></label></div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-sm-2">'+
                    '<div class="col-xs-12 col-sm-12"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
            }
        }
    }else{
        $("#collapseForeignExternal").empty();
        $("#foreignExternal").text(dataExternal.length);
        if(dataExternal.length > 0){
            for(var i=0;i<dataExternal.length;i++){

                $("#collapseForeignExternal").append(
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
                        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
                            '<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px" >'+
                                '<div class="form-group" style="margin-bottom: 0px">'+
                                    '<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+'  >'+
                                        '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: white;font-size:16px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
                                        '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #08f508;font-size:14px;"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
                                    '</div>'+
                                    '<div class="col-xs-12 col-sm-5" id='+dataExternal[i].id+'  >'+
                                        '<div class="col-xs-12 col-sm-12"><label class="control-label custom" style="color: #04f9fd;font-size:16px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
                                        '<div class="col-xs-12 col-sm-12"><label class="control-label custom-label" style="color: #08f508;font-size:14px;"><b>'+dataExternal[i].email+'</b></label></div>'+
                                    '</div>'+
                                    '<div class="col-xs-12 col-sm-2">'+
                                        '<div class="col-xs-12 col-sm-12"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                );
            }
        }
    }


    $("#modalAddOuterPerson").modal('hide');
}

function remarkExternal(obj){
    for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
        if($DATA_EXTERNAL_MEMBER[i].id == parseInt(obj.id)){
            $("#alertModal").modal('show');
            $("label[id=detailAlert]").text($DATA_EXTERNAL_MEMBER[i].remark);
        }
    }
}

function findCarBookingByDocumentApproveItemId(documentAppItem){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/carBookings',
        complete: function (xhr) {
            $DATA_CAR_BOOKING = JSON.parse(xhr.responseText);
            if($DATA_CAR_BOOKING != null && $DATA_CAR_BOOKING.length > 0){
                renderDataCarBooking($DATA_CAR_BOOKING[0]);
            }
        }
    });
}

function renderDataCarBooking(dataCarBooking){

    if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){

        var startDate = DateUtil.coverDateToString(dataCarBooking.startDate);
        var endDate   = DateUtil.coverDateToString(dataCarBooking.endDate);
        $("#originCarBookingADM").val(dataCarBooking.origin.id);
        $("#destinationCarBookingADM").val(dataCarBooking.destination.id);
        $("#dateStartCarADM").val(startDate+" to "+endDate);
        $("#timeStartCarADM").val(dataCarBooking.startTime);
        $("#timeEndCarADM").val(dataCarBooking.endTime);
        $("#numOfTravelADM").val(dataCarBooking.numMember);
        $("#remarkCarADM").val(dataCarBooking.remark);

        /* validate set car type */
        for(var i=0;i<$("[name=iconCarADM]").length;i++){
            var carType = dataCarBooking.carTypeCode;
            if(carType == $("[name=iconCarADM]")[i].getAttribute('id')){
                if('van' == $("[name=iconCarADM]")[i].getAttribute('value')){
                    $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.VAN);
                }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.SEDAN);
                }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.TUCK);
                }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCarADM]")[i].setAttribute('src',IMAGE_CAR.OTHER);
                    $("#otherCarADMDiv").removeClass('hide');
                    $("#otherCarTypeADM").val(dataCarBooking.otherCarType);
                }
            }
        }

        if($DATA_DOCUMENT.documentReference[0] != null){
            AutocompleteDocRefAppCar.search('docRefADMInputDocRefAppCar',$DATA_DOCUMENT.requester,$DATA_DOCUMENT.documentReference[0].docReferenceNumber);
            AutocompleteDocRefAppCar.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefADMInputDocRefAppCar');
        }
    }else{
        if($DATA_DOCUMENT.documentReference != null){

            $("#docRef").text($DATA_DOCUMENT.documentReference[0].docReferenceNumber);
        }else{
            $("#docRef").text("-");
        }

        var startDate = DateUtil.coverDateToString(dataCarBooking.startDate);
        var endDate   = DateUtil.coverDateToString(dataCarBooking.endDate);
        $("#originCarBooking").text(dataCarBooking.origin.description);
        $("#destinationCarBooking").text(dataCarBooking.destination.description);
        $("#dateStartCar").text(startDate+" to "+endDate);
        $("#timeStartCar").text(dataCarBooking.startTime);
        $("#timeEndCar").text(dataCarBooking.endTime);
        $("#numOfTravel").text(dataCarBooking.numMember);
        $("#remarkCar").text(dataCarBooking.remark);

        /* validate set car type */
        for(var i=0;i<$("[name=iconCar]").length;i++){
            var carType = dataCarBooking.carTypeCode;
            if(carType == $("[name=iconCar]")[i].getAttribute('id')){
                if('van' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN);
                }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN);
                }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK);
                }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                    $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER);
                    $("#otherCarDiv").removeClass('hide');
                    $("#otherCarType").text(dataCarBooking.otherCarType);
                }
            }
        }
    }
}

function renderDataHotelBooking(dataHotelBooking){

    if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){
        $("#divHOTELADM").removeClass('hide');
        var startDate = DateUtil.coverDateToString(dataHotelBooking.startDate);
        var endDate   = DateUtil.coverDateToString(dataHotelBooking.endDate);
        AutocompleteHotel.renderValue(dataHotelBooking.informationHotel);
        $("#dateHotelADM").val(startDate+" to "+endDate);
        $("#singleRoomADM").val(dataHotelBooking.singleRoom != null ? dataHotelBooking.singleRoom : "");
        $("#twinRoomADM").val(dataHotelBooking.twinRoom != null ? dataHotelBooking.twinRoom : "");
        if($DATA_DOCUMENT.documentReference[0] != null){
            AutocompleteDocRefAppHotel.search('docRefHotelADMInputDocRefAppHotel',$DATA_DOCUMENT.requester,$DATA_DOCUMENT.documentReference[0].docReferenceNumber);
            AutocompleteDocRefAppHotel.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefHotelADMInputDocRefAppHotel');
        }
        $("#hotelPriceADMDiv").removeClass('hide');
        $("#hotelPriceADM").text($("#hotelADMInputHotel").attr('data-hotel-rate'));
    }else{
        $("#divHOTEL").removeClass('hide');
        var startDate = DateUtil.coverDateToString(dataHotelBooking.startDate);
        var endDate   = DateUtil.coverDateToString(dataHotelBooking.endDate);
        $("#dateHotel").text(startDate+" to "+endDate);
        $("#singleRoom").text(dataHotelBooking.singleRoom != null ? dataHotelBooking.singleRoom : "");
        $("#twinRoom").text(dataHotelBooking.twinRoom != null ? dataHotelBooking.twinRoom : "");
        $("#hotel").text(findHotelByCode(dataHotelBooking.informationHotel));
        if(dataHotelBooking.informationHotel != "Other"){
            $("#hotelPriceDiv").removeClass('hide');
            $("#hotelPrice").text($("#hotelInputHotel").attr('data-hotel-rate'));
        }else{
            $("#otherHotelDiv").removeClass('hide');
            $("#otherHotel").text(dataHotelBooking.otherHotel);
        }
    }

}

function findHotelByCode(hotelCode) {
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findMasterDataHotelByCode',
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            if(data != null){
                for(var i=0;i<data.length;i++){
                    if(data[i].code == hotelCode){
                        $("#hotel").text(data[i].description);
                        $("#hotelPrice").text(data[i].variable1);

                    }
                }
            }
        }
    });
}

function renderDataFlightTicket(dataFlightTicket){
    if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){
        $("#airlineADM").val(dataFlightTicket.airlineCode);
        var travelType = document.getElementsByName('travelTypeADM');
        for(var i =0;i < travelType.length;i++){
            if(travelType[i].value == dataFlightTicket.flagOneWay){
                travelType[i].checked = true;
                break;
            }
        }

        var flightType = document.getElementsByName('flightTypeADM');
        for(var i =0;i < flightType.length;i++){
            if(flightType[i].value == dataFlightTicket.fixFlight){
                flightType[i].checked = true;
                break;
            }
        }

        if($DATA_DOCUMENT.documentReference[0] != null){
            AutocompleteDocRefAppFlight.search('docRefFlightADMInputDocRefAppFlight',$DATA_DOCUMENT.requester,$DATA_DOCUMENT.documentReference[0].docReferenceNumber);
            AutocompleteDocRefAppFlight.renderValue($DATA_DOCUMENT.documentReference[0].docReferenceNumber,'docRefFlightADMInputDocRefAppFlight');
        }
    }else{
        $("#airline").text(findAirlineByCode(dataFlightTicket.airlineCode));
        var travelType = document.getElementsByName('travelType');
        for(var i =0;i < travelType.length;i++){
            if(travelType[i].value == dataFlightTicket.flagOneWay){
                travelType[i].checked = true;
                break;
            }
        }

        var flightType = document.getElementsByName('flightType');
        for(var i =0;i < flightType.length;i++){
            if(flightType[i].value == dataFlightTicket.fixFlight){
                flightType[i].checked = true;
                break;
            }
        }
    }
}

function findAirlineByCode(airlineCode) {
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findMasterDataAirlineByCode',
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            if(data != null){
                for(var i=0;i<data.length;i++){
                    if(data[i].code == airlineCode){
                        $("#airline").text(data[i].description);

                    }
                }
            }
        }
    });
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length > 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            var index = i+1;
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+index+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center"><button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '</tr>'
            );
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}


function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}


function documentAttachment(){
    getDataAttachmentType();
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}


function callQRCode(){
    let user_account;

    $DATA_REQUEST_APPROVER.forEach(function (item) {
        if(item.actionState.indexOf("ACC") >= 0){
            user_account = item.userNameApprover;
        }
    })
    window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
}

function confirmCancelDocument(){
    if($DATA_DOCUMENT.requester == $USERNAME){
        var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

        $('#confirmModal .modal-body').html(warningMessage);
        $('#confirmModal').modal('show');
    }else{
        rejectRequest();
    }

}

function cancelRequest(){

    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN || $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        checkLineApproveSpecialCase();
    }else{
        $('.dv-background').show();

        window.setTimeout(function() {

            var listJsonDetails = [];

            var jsonDetails = {};
            jsonDetails['amount'] = 1;
            jsonDetails['flowType'] = validateFlowTypeByApproveType($DATA_DOCUMENT.approveType);

            listJsonDetails.push(jsonDetails);

            var jsonData = {};
            jsonData['id'] = $DATA_DOCUMENT.id;
            jsonData['requester'] = $DATA_DOCUMENT.requester;
            jsonData['docNumber'] = $DATA_DOCUMENT.docNumber;
            jsonData['details'] = JSON.stringify(listJsonDetails);


            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/requests/cancelRequest',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $DATA_REQUEST = JSON.parse(xhr.responseText);
                        if($DATA_REQUEST != null){
                            $('.dv-background').hide();
                            cancelDocument($DATA_REQUEST.document.id);
                        }

                    }
                }
            });
        },500);
    }
}

function cancelDocument(){

    $('.dv-background').show();

    window.setTimeout(function() {

        var jsonData = {};
        jsonData['parentId']     	= 1;
        jsonData[csrfParameter]  	= csrfToken;
        jsonData['id']    			= $DATA_DOCUMENT.id;
        jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/updateDocumentStatus',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);
                    $('.dv-background').hide();
                    backToMenu();
                    // window.location.href = session.context+'/approve/viewCreateDocDetail?doc='+$DATA_DOCUMENT.id;
                }
            }
        });

    },500);
}

function approveDocument(){

    if(session.roleName.indexOf("ROLE_OFFICE_ADMIN") != -1){
        updateDocumentRoleAdmin();
    }else{
        $('.dv-background').show();

        window.setTimeout(function() {

            $.ajax({
                type: "GET",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/requests/' + $DATA_REQUEST.id,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if (xhr.responseText != "Error") {
                            var data_request = JSON.parse(xhr.responseText);
                            if (data_request.nextApprover != $USERNAME) {
                                $('.dv-background').hide();
                                $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_IS_APPROVED);
                                $("#warningModal").modal('show');
                            } else {
                                var jsonData = {};
                                jsonData['userName'] = $USERNAME;
                                jsonData['actionStatus'] = validateActionState($USERNAME);
                                jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                                jsonData['docType'] = $DATA_DOCUMENT.documentType;
                                jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                                jsonData['processId'] = $DATA_DOCUMENT.processId;
                                jsonData['documentId'] = $DATA_DOCUMENT.id;
                                jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                                jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();


                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/requests/approveRequest',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $DATA_APPROVE = JSON.parse(xhr.responseText);

                                            if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {

                                                $('.dv-background').hide();

                                                window.location.href = session.context;
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        },500);
    }
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function rejectRequest(){

    $('.dv-background').show();

    window.setTimeout(function() {

        /* verify request is active */
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+$DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if(xhr.responseText != "Error"){
                        var data_request = JSON.parse(xhr.responseText);
                        if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        }else if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        }else if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        }else{
                            var jsonData = {};
                            jsonData['userName'] = $USERNAME;
                            jsonData['actionStatus'] = validateActionState($USERNAME);
                            jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                            jsonData['docType'] = $DATA_DOCUMENT.documentType;
                            jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                            jsonData['processId'] = $DATA_DOCUMENT.processId;
                            jsonData['documentId'] = $DATA_DOCUMENT.id;
                            jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                            jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();


                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/rejectRequest',
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {

                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            $('.dv-background').hide();
                                            backToMenu();
                                            // window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

    },500);
}

function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_CAR){
        return MASTER_DATA.FLOW_TYPE_CAR;
    }else if(approveType == MASTER_DATA.APR_TYPE_HOTEL){
        return MASTER_DATA.FLOW_TYPE_HOTEL;
    }
}

function backToMenu(){
    window.location.href = session.context+'/approve/approveMainMenu';
}

function checkLineApproveSpecialCase(){
    var psa_origin = "";
    var psa_destination = "";
    var location_type = "";
    var zone_code = "";

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+$DATA_DOCUMENT.requester,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseText != null){
                var dataEmployee = JSON.parse(xhr.responseText);
                var cLevel = dataEmployee.EESG_ID;
                var psa_requester = dataEmployee.Personal_PSA_ID;

                if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){

                    var docNumber = $("#docRefFlight").text();

                    $.ajax({
                        type: "GET",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/findTravelDetailsByDocRef/' + docNumber,
                        complete: function (xhr) {
                            var dataCheckLine = JSON.parse(xhr.responseText);
                            if (dataCheckLine != null) {
                                psa_origin = dataCheckLine[0].origins.psaCode;
                                psa_destination = dataCheckLine[0].destination.psaCode;
                                location_type = dataCheckLine[0].origins.locationType;
                                zone_code = dataCheckLine[0].destination.zoneCode;

                                validateFlowTypeSpecial(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code);
                            }
                        }
                    });
                }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FOREIGN){
                    var documentAppItem = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;
                    $.ajax({
                        type: "GET",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
                        complete: function (xhr) {
                            if(xhr.responseText != null){
                                dataCheckLine = JSON.parse(xhr.responseText);
                                if (dataCheckLine.length > 0) {
                                    psa_origin = dataCheckLine[0].origins.psaCode;
                                    psa_destination = dataCheckLine[0].destination.psaCode;
                                    location_type = dataCheckLine[0].origins.locationType;
                                    zone_code = dataCheckLine[0].destination.zoneCode;

                                    validateFlowTypeSpecial(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}

function validateFlowTypeSpecial(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code) {
    if (psa_origin != "" && psa_destination != "" && location_type != "" && zone_code != "") {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: session['context'] + '/approve/validateFlowTypeApprove',
            data: {
                cLevel: parseInt(cLevel),
                psa_requester: psa_requester,
                psa_origin: psa_origin,
                psa_destination: psa_destination,
                location_type: location_type,
                zone_code: zone_code
            },
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != null) {
                        var flowTypeName = xhr.responseText;
                        var flowType = "";
                        if (flowTypeName == "H001" || flowTypeName == "H002") {
                            flowType = MASTER_DATA.FLOW_TYPE_H001;
                        } else if (flowTypeName == "H003") {
                            flowType = MASTER_DATA.FLOW_TYPE_H003;
                        } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                            flowType = MASTER_DATA.FLOW_TYPE_H004;
                        }

                        if (flowType != "") {
                            var listJsonDetails = [];

                            var jsonDetails = {};
                            jsonDetails['amount'] = 1;
                            jsonDetails['flowType'] = flowType;

                            listJsonDetails.push(jsonDetails);

                            var jsonData = {};
                            jsonData['id'] = $DATA_DOCUMENT.id;
                            jsonData['requester'] = $DATA_DOCUMENT.requester;
                            jsonData['docNumber'] = $DATA_DOCUMENT.docNumber;
                            jsonData['details'] = JSON.stringify(listJsonDetails);


                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/cancelRequest',
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $DATA_REQUEST = JSON.parse(xhr.responseText);
                                        if($DATA_REQUEST != null){
                                            $('.dv-background').hide();
                                            cancelDocument($DATA_REQUEST.document.id);
                                        }

                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }
}

function getMasterDataLocation(locationType){
    var locationString = $.ajax({
        url: session['context'] + "/locations/findByLocationType",
        type: "GET",
        data:{locationType:locationType},
        async: false
    }).responseText;
    $DATA_LOCATION_JSON = JSON.parse(locationString);

    $("#originCarBookingADM").empty().append('<option value="">'+LB.LABEL_ORIGIN+'</option>');
    $("#destinationCarBookingADM").empty().append('<option value="">'+LB.LABEL_DESTINATION+'</option>');

    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#originCarBookingADM").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }
    for(var i=0; i<$DATA_LOCATION_JSON.length;i++){
        $("#destinationCarBookingADM").append(
            '<option value='+$DATA_LOCATION_JSON[i].id+'>'+ $DATA_LOCATION_JSON[i].description + '</option>');
    }
}

function activeCar(btn) {
    if($("[name=iconCarADM]").hasClass('click') == 'false'){
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").removeClass('hide');
        }
    }else{
        $("[name=iconCarADM]").removeClass('click');
        setUpDefaultCarType();
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").removeClass('hide');
        }
    }
}

function updateDocumentRoleAdmin(){
    var reason = "";

    var jsonDocumentApproveItem = {};
    jsonDocumentApproveItem['parentId'] = 1;
    jsonDocumentApproveItem[csrfParameter] = csrfToken;
    jsonDocumentApproveItem['id'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;
    jsonDocumentApproveItem['approveType'] = $DATA_DOCUMENT.approveType;
    jsonDocumentApproveItem['travelReason'] = reason;
    jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

    $('.dv-background').show();
    window.setTimeout(function() {
        var dataDocumentApproveItem = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocumentApproveItem',
            data: jsonDocumentApproveItem,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocumentApproveItem = JSON.parse(xhr.responseText);
                    $('.dv-background').hide();
                    STATUS_SAVE_DOCUMENT = "SUCCESS";
                    if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_CAR) {
                        var documentReference 	= $("#docRefADMInputDocRefAppCar").val() == "" ? "" : $("#docRefInputDocRefAppCar").attr('data-doc');
                        var origin 				= $("#originCarBookingADM").val();
                        var destination 		= $("#destinationCarBookingADM").val();
                        var startDate 			= $("#dateStartCarADM").val();
                        var endDate   			= $("#dateStartCarADM").val();
                        var startTime			= $("#timeEndCarADM").val();
                        var endTime  			= $("#timeEndCarADM").val();
                        var carType  			= $CarTypeCode;
                        var numOfTravel 		= $("#numOfTravelADM").val();
                        var remark      		= $("#remarkCarADM").val();
                        var otherLocation      	= $("#otherLocationADM").val();
                        var otherCarType      	= $("#otherCarTypeADM").val();
                        var carBookingId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBooking == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].carBooking.id;


                        var errorMessage = "";
                        if(documentReference == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
                        }if(origin == "" ){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ORIGIN+"<br/>";
                        }if (destination == "" ){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DESTINATION+"<br/>";
                        }if(startDate == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
                        }if(startTime == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_TIME+"<br/>";
                        }if( endDate == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
                        }if(endTime == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_TIME+"<br/>";
                        }if(carType == "" || carType == undefined){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_CAR_TYPE+"<br/>";
                        }if(numOfTravel == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_NUMBER_OF_TRAVELERS+"<br/>";
                        }

                        if(errorMessage){
                            $('#warningModal .modal-body').html(errorMessage);
                            $('#warningModal').modal('show');
                        }else{

                            $('.dv-background').show();
                            window.setTimeout(function() {
                                var jsonData = {};
                                jsonData['parentId'] = 1;
                                jsonData[csrfParameter] = csrfToken;
                                jsonData['id'] = $DATA_DOCUMENT.id;
                                jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

                                var dataDocument = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/updateDocumentStatus',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            dataDocument = JSON.parse(xhr.responseText);
                                            if (dataDocument != null) {
                                                var jsonDocumentReference = {};
                                                jsonDocumentReference['parentId'] = 1;
                                                jsonDocumentReference[csrfParameter] = csrfToken;
                                                jsonDocumentReference['docReferenceNumber'] = documentReference;
                                                jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                                                jsonDocumentReference['approveTypeCode'] = $DATA_DOCUMENT.approveType;
                                                jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                                                var jsonCarBooking = {};
                                                jsonCarBooking['parentId'] = 1;
                                                jsonCarBooking[csrfParameter] = csrfToken;
                                                jsonCarBooking['id'] = carBookingId;
                                                jsonCarBooking['startDate'] = $("#dateStartCarADM").val().split('to')[0].trim();
                                                jsonCarBooking['endDate'] = $("#dateStartCarADM").val().split('to')[1].trim();
                                                jsonCarBooking['carTypeCode'] = carType;
                                                jsonCarBooking['startTime'] = startTime;
                                                jsonCarBooking['endTime'] = endTime;
                                                jsonCarBooking['numMember'] = numOfTravel;
                                                jsonCarBooking['remark'] = remark;
                                                jsonCarBooking['origin'] = origin;
                                                jsonCarBooking['destination'] = destination;
                                                jsonCarBooking['otherLocation'] = otherLocation;
                                                jsonCarBooking['otherCarType'] = otherCarType;
                                                jsonCarBooking['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                                                var dataCarBooking = $.ajax({
                                                    type: "POST",
                                                    headers: {
                                                        Accept: 'application/json'
                                                    },
                                                    url: session['context'] + '/approve/saveCarBooking',
                                                    data: jsonCarBooking,
                                                    async: false,
                                                    complete: function (xhr) {
                                                        if (xhr.readyState == 4) {
                                                            dataCarBooking = JSON.parse(xhr.responseText);

                                                            var dataDocumentReference = $.ajax({
                                                                type: "POST",
                                                                headers: {
                                                                    Accept: 'application/json'
                                                                },
                                                                url: session['context'] + '/approve/saveDocumentReference',
                                                                data: jsonDocumentReference,
                                                                async: false,
                                                                complete: function (xhr) {
                                                                    if (xhr.readyState == 4) {
                                                                        dataDocumentReference = JSON.parse(xhr.responseText);
                                                                        $('.dv-background').hide();
                                                                        approveRequestRoleAdmin();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            },500);
                        }
                    } else if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_HOTEL) {
                        var documentReference 	= $("#docRefHotelADMInputDocRefAppHotel").attr('data-doc');    //$("#hotelDocRef").val();
                        var startDate 			= $("#dateHotelADM").val();
                        var endDate   			= $("#dateHotelADM").val();
                        var singleRoom 			= $("#singleRoomADM").val();
                        var twinRoom      		= $("#twinRoomADM").val();
                        var informationHotel    = $("#hotelADMInputHotel").attr('data-hotel-code');
                        var otherHotel          = $("#otherHotelADMDiv").val();
                        var hotelBookingId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].hotelBooking.id;

                        var errorMessage = "";
                        if(documentReference == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
                        }if(startDate == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_START_DATE+"<br/>";
                        }if( endDate == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_END_DATE+"<br/>";
                        }if(informationHotel == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_HOTEL_TO_STAY+"<br/>";
                        }

                        if(errorMessage){
                            $('#warningModal .modal-body').html(errorMessage);
                            $('#warningModal').modal('show');
                        }else{

                            $('.dv-background').show();
                            window.setTimeout(function() {
                                var jsonData = {};
                                jsonData['parentId'] = 1;
                                jsonData[csrfParameter] = csrfToken;
                                jsonData['id'] = $DATA_DOCUMENT.id;
                                jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

                                var dataDocument = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/updateDocumentStatus',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            dataDocument = JSON.parse(xhr.responseText);
                                            if (dataDocument != null) {

                                                var jsonDocumentReference = {};
                                                jsonDocumentReference['parentId'] = 1;
                                                jsonDocumentReference[csrfParameter] = csrfToken;
                                                jsonDocumentReference['docReferenceNumber'] = documentReference;
                                                jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                                                jsonDocumentReference['approveTypeCode'] = $DATA_DOCUMENT.approveType;
                                                jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                                                var jsonHotelBooking = {};
                                                jsonHotelBooking['parentId'] = 1;
                                                jsonHotelBooking[csrfParameter] = csrfToken;
                                                jsonHotelBooking['id'] = hotelBookingId;
                                                jsonHotelBooking['startDate'] = $("#dateHotelADM").val().split('to')[0].trim();
                                                jsonHotelBooking['endDate'] = $("#dateHotelADM").val().split('to')[1].trim();
                                                jsonHotelBooking['singleRoom'] = singleRoom;
                                                jsonHotelBooking['twinRoom'] = twinRoom;
                                                jsonHotelBooking['informationHotel'] = informationHotel;
                                                jsonHotelBooking['otherHotel'] = otherHotel;
                                                jsonHotelBooking['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                                                var dataHotelBooking = $.ajax({
                                                    type: "POST",
                                                    headers: {
                                                        Accept: 'application/json'
                                                    },
                                                    url: session['context'] + '/approve/saveHotelBooking',
                                                    data: jsonHotelBooking,
                                                    async: false,
                                                    complete: function (xhr) {
                                                        if (xhr.readyState == 4) {
                                                            dataHotelBooking = JSON.parse(xhr.responseText);

                                                            var dataDocumentReference = $.ajax({
                                                                type: "POST",
                                                                headers: {
                                                                    Accept: 'application/json'
                                                                },
                                                                url: session['context'] + '/approve/saveDocumentReference',
                                                                data: jsonDocumentReference,
                                                                async: false,
                                                                complete: function (xhr) {
                                                                    if (xhr.readyState == 4) {
                                                                        dataDocumentReference = JSON.parse(xhr.responseText);
                                                                        $('.dv-background').hide();
                                                                        approveRequestRoleAdmin();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            },500);
                        }
                    } else if ($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING) {
                        var documentReference 	= $("#docRefFlightADMInputDocRefAppFlight").val() == "" ? "" : $("#docRefFlightADMInputDocRefAppFlight").attr('data-doc');   //$("#docRefFlight").val();
                        var member 				= $("#collapseFlightPersonADM").attr('valueSize');
                        var flightTicketId 		= $DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItems[0].flightTicket.id;

                        var errorMessage = "";
                        if(documentReference == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_DOCUMENT_REFERENCE+"<br/>";
                        }if(member == ""){
                            errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_TRAVEL_MEMBER_NAME+"<br/>";
                        }

                        if(errorMessage){
                            $('#warningModal .modal-body').html(errorMessage);
                            $('#warningModal').modal('show');
                        }else{

                            $('.dv-background').show();
                            window.setTimeout(function() {
                                var jsonData = {};
                                jsonData['parentId'] = 1;
                                jsonData[csrfParameter] = csrfToken;
                                jsonData['id'] = $DATA_DOCUMENT.id;
                                jsonData['userNameMember'] = $DATA_DOCUMENT.requester;

                                var dataDocument = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/updateDocumentStatus',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            dataDocument = JSON.parse(xhr.responseText);
                                            if (dataDocument != null) {
                                                var jsonDocumentReference = {};
                                                jsonDocumentReference['parentId'] = 1;
                                                jsonDocumentReference[csrfParameter] = csrfToken;
                                                jsonDocumentReference['docReferenceNumber'] = documentReference;
                                                jsonDocumentReference['documentTypeCode'] = $DATA_DOCUMENT.documentType;
                                                jsonDocumentReference['approveTypeCode'] = $DATA_DOCUMENT.approveType;
                                                jsonDocumentReference['document'] = $DATA_DOCUMENT.id;

                                                var jsonFlightTicket = {};
                                                jsonFlightTicket['parentId'] = 1;
                                                jsonFlightTicket[csrfParameter] = csrfToken;
                                                jsonFlightTicket['id'] = flightTicketId;
                                                jsonFlightTicket['flagOneWay'] = $('input[name="travelTypeADM"]:checked').val();
                                                jsonFlightTicket['fixFlight'] = $('input[name="flightTypeADM"]:checked').val();
                                                jsonFlightTicket['airlineCode'] = $("#airlineADM")[0].selectedOptions[0].value;
                                                jsonFlightTicket['documentApproveItem'] = $DATA_DOCUMENT.documentApprove.documentApproveItems[0].id;

                                                var dataFlightTicket = $.ajax({
                                                    type: "POST",
                                                    headers: {
                                                        Accept: 'application/json'
                                                    },
                                                    url: session['context'] + '/approve/saveFlightTicket',
                                                    data: jsonFlightTicket,
                                                    async: false,
                                                    complete: function (xhr) {
                                                        if (xhr.readyState == 4) {
                                                            dataFlightTicket = JSON.parse(xhr.responseText);

                                                            var dataDocumentReference = $.ajax({
                                                                type: "POST",
                                                                headers: {
                                                                    Accept: 'application/json'
                                                                },
                                                                url: session['context'] + '/approve/saveDocumentReference',
                                                                data: jsonDocumentReference,
                                                                async: false,
                                                                complete: function (xhr) {
                                                                    if (xhr.readyState == 4) {
                                                                        dataDocumentReference = JSON.parse(xhr.responseText);
                                                                        $('.dv-background').hide();
                                                                        approveRequestRoleAdmin();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            },500);
                        }
                    } else {
                        $("#completeModal").modal('show');
                    }
                }
            }
        });
    },500);
}

function approveRequestRoleAdmin(){

    BTN = "SEND";
    $('.dv-background').show();

    window.setTimeout(function() {

        /* update userNameMember */
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/' + $DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.nextApprover != $USERNAME) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_IS_APPROVED);
                            $("#warningModal").modal('show');
                        } else {
                            var jsonData = {};
                            jsonData['userName'] = $USERNAME;
                            jsonData['actionStatus'] = validateActionState($USERNAME);
                            jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                            jsonData['docType'] = $DATA_DOCUMENT.documentType;
                            jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                            jsonData['processId'] = $DATA_DOCUMENT.processId;
                            jsonData['documentId'] = $DATA_DOCUMENT.id;
                            jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                            jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();


                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/approveRequest',
                                data: jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {

                                            $('.dv-background').hide();

                                            window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },500);
}

function getUserNameTravelMember(){

    var userNameMember = "";
    if($DATA_TRAVEL_MEMBER.length > 0){
        $DATA_TRAVEL_MEMBER.forEach(function (data){
            userNameMember += data.memberUser+",";
        });
    }

    if(userNameMember != ""){
        return userNameMember.substr(0,userNameMember.lastIndexOf(","));
    }
}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#company").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#company").attr('pa',paData.PaCode);
    }else{
        $("#company").text("-");
        $("#company").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#department").attr('psa',psaData.PsaCode);
    }else{
        $("#department").text("-");
        $("#department").attr('psa',"");
    }
}