var docStatus = ''
var nextApprover = ''
var useApproveEmail = ''




$(document).ready(function () {

    useApproveEmail = $USERNAME

    if(useApproveEmail != null){
        useApproveEmail = useApproveEmail.toUpperCase();
    }


    findDocumentByDocNumber($DOC_NUM)


        if(docStatus != 'ONP' ){

                    if(docStatus == 'CMP'){

                        swal({
                            title: "เอกสารนี้ดำเนินการเสร็จแล้ว<br />ท่านไม่สามารถทำการอนุมัติได้อีก",
                            html: true,
                            type: "error",
                            confirmButtonClass: "btn-info",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        })

                    }else if(docStatus == 'REJ' || docStatus == 'CCL'){


                        swal({
                            title: "เอกสารนี้ถูกยกเลิกไปแล้ว<br />ท่านไม่สามารถทำการอนุมัติได้อีก",
                            html: true,
                            type: "error",
                            confirmButtonClass: "btn-info",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        })

                    }

        }else{

            if(useApproveEmail != nextApprover){

                swal({
                    title: "ท่านไม่สามารถพิจารณา<br />เอกสารฉบับนีได้",
                    html: true,
                    type: "error",
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                })

            }else{


                swal({
                        title: "Confirm E-mail : " + $USERNAME,

                        textSize: "25px",
                        type: "input",
                        // showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Enter Password...",
                        width: '850px',
                        inputType: "password"
                    },
                    function (inputValue) {

                        console.log('UserName = ' + $USERNAME)

                        console.log('Password = ' + inputValue)
                        if(inputValue != ""){

                            $.ajax({
                                type: "GET",
                                url: session['context'] + '/requests/checkAuthenUser?userName=' + $USERNAME + '&password=' + encodeURIComponent(inputValue),
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        if (xhr.status == 200) {
                                            var result = xhr.responseText;
                                            console.log('Result = ' + result)


                                            if (result != 1) {

                                                swal.showInputError("Invalid Password !!!");

                                            } else {
                                                var action = $ACTION_REQUEST

                                                if (action == "Approve") {
                                                    $.ajax({
                                                        type: "GET",
                                                        url: session['context'] + '/requests/sendApproveRequest?docNumber=' + $DOC_NUM + '&userName=' + $USERNAME + '&actionState=' + $STATE_REQUEST,
                                                        async: false,
                                                        complete: function (xhr) {
                                                            if (xhr.readyState == 4) {
                                                                if (xhr.status == 200) {
                                                                    var resultState = (xhr.responseText);
                                                                    if (resultState = "1") {
                                                                        swal({
                                                                            title: "Approve Success !!",
                                                                            html: true,
                                                                            type: "success",
                                                                            confirmButtonClass: "btn-info",
                                                                            confirmButtonText: "OK",
                                                                            closeOnConfirm: false
                                                                        })
                                                                    } else {
                                                                        swal({
                                                                            title: "Approve Fail !!",
                                                                            html: true,
                                                                            type: "error",
                                                                            confirmButtonClass: "btn-info",
                                                                            confirmButtonText: "OK",
                                                                            closeOnConfirm: false
                                                                        })
                                                                    }


                                                                }
                                                            }
                                                        }
                                                    }).done(function () {

                                                    });
                                                } else {

                                                    $.ajax({
                                                        type: "GET",
                                                        url: session['context'] + '/requests/sendRejectRequest?docNumber=' + $DOC_NUM + '&userName=' + $USERNAME + '&actionState=' + $STATE_REQUEST,
                                                        async: false,
                                                        complete: function (xhr) {
                                                            if (xhr.readyState == 4) {
                                                                if (xhr.status == 200) {
                                                                    var resultState = JSON.parse(xhr.responseText);
                                                                    if (resultState = "1") {
                                                                        swal({
                                                                            title: "Reject Success !!",
                                                                            html: true,
                                                                            type: "success",
                                                                            confirmButtonClass: "btn-info",
                                                                            confirmButtonText: "OK",
                                                                            closeOnConfirm: false
                                                                        })
                                                                    } else {
                                                                        swal({
                                                                            title: "Reject Fail !!",
                                                                            html: true,
                                                                            type: "error",
                                                                            confirmButtonClass: "btn-info",
                                                                            confirmButtonText: "OK",
                                                                            closeOnConfirm: false
                                                                        })
                                                                    }


                                                                }
                                                            }
                                                        }
                                                    }).done(function () {

                                                    });

                                                }


                                            }

                                        }
                                    }
                                }
                            }).done(function () {
                                //close loader
                            });

                        }else{

                            swal.showInputError("Please Enter Password !!!");

                        }



                    });

            }


        }




})


function goDashboard() {
    window.location.href = session.context;
}





function findDocumentByDocNumber(docNumber){

    $.ajax({
        type: "GET",
        url: session['context']+'/advance/findByDocNumber?docNumber='+docNumber ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                            var item = JSON.parse(xhr.responseText)


                        docStatus = item.documentStatus == null ? '' : item.documentStatus
                        if(docStatus != null){
                            docStatus = docStatus.toUpperCase()
                        }


                        nextApprover = item.requests == null ? '' : item.requests.nextApprover
                        if(nextApprover != null){
                            nextApprover = nextApprover.toUpperCase();
                        }


                    }else{

                        console.log('Not Found Doc Number : '+docNumber)

                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
