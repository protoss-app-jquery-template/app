var objectApprove = $.extend({},UtilPagination);
var objectExpense = $.extend({},UtilPagination);
var objectMyRequest = $.extend({},UtilPagination);

var $DATA_EMPLOYEE;
var $DATA_REQUEST;
var $DATA_REQUEST_APPROVER;
var objectRequestApprover;
var objectRequestApprover_Mobile;
var startCount=0
var startCount_Request=111
var DocumentTYPE = 'APP';

var DATA_MILK;
var docTypeAPP = 'APP'
var docTypeEXP = 'EXP'
var docTypeADV = 'ADV'

var userPA;
let $APPROVERS;
let $COLOR;
let APP_COLOR = [];
let EXP_COLOR = [];
let ADV_COLOR = [];
let sumDateProcess;
let userHead;
let dataApproves = [];
let dataExpense = [];
let dataApproveListMobile = [];


$(document).ready(function () {

    $('.dv-background').show();

    readDataZip(function () {
        readMasterDataDocAppType();
        readMasterDataDocExpType();
        readMasterDataDocAdvType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataDocStatusLost();
        // readMasterDataAprTypeDomestic();
        // readMasterDataAprTypeForeign();
        // readMasterDataAprTypeCar();
        // readMasterDataAprTypeHotel();
        readMasterDataAprTypeFightBooking();
        readMasterDataAprTypeSetDomestic();
        readMasterDataAprTypeSetForeign();
    });


    setTimeout(function () {

        DocumentTYPE = MASTER_DATA.DOC_APP_TYPE;
        findUserPA();
        loadColor();

        if(userPA.length){
            $COLOR = JSON.parse($COLOR.responseText);
            $COLOR = $COLOR.content.sort(function(a,b){
                return a.id > b.id;
            });
            $COLOR.forEach(function(item){
                if(item.code == MASTER_DATA.DOC_APP_TYPE + '_' + userPA[0].companyCode){
                    APP_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }else if(item.code == MASTER_DATA.DOC_EXP_TYPE + '_' + userPA[0].companyCode){
                    EXP_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }else if(item.code == MASTER_DATA.DOC_ADV_TYPE + '_' + userPA[0].companyCode){
                    ADV_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }
            })
        }

        DocumentTYPE = MASTER_DATA.DOC_APP_TYPE;

        try{
            findIncomingRequestApprove();
            findIncomingRequestExpense();
            // $.material.init()
        }catch (e){
            console.error('Incoming Request : ' + e);
        }

        try{
            findByRequesterAndDocumentType();
        }catch (e){
            console.error('My Request : ' + e);
        }

        $('input[name = "check_approve_app"]').on('change',function(){
            dataApproves.length = 0;
            // console.log("approve checkbox active");
            $.each( $( ".checkbox_status_app:checked" ), function( i, obj ) {
                dataApproves.push(obj.value);
                dataApproveListMobile.push(obj.value);
            });

            if(dataApproves.length > 0){
                $('#app_selected_btn').attr('disabled',false);
                $('#app_selected').css("filter","grayscale(0%)");
                $('#approve_selected_mobile_button_li').removeClass('hide');
            }else{
                $('#app_selected_btn').attr('disabled',true);
                $('#app_selected').css("filter","grayscale(100%)");
                $('#approve_selected_mobile_button_li').addClass('hide');

            }
        });

        $('input[name = "check_approve_exp"]').on('change',function(){
            dataExpense.length = 0;
            // console.log("expense checkbox active");
            $.each( $( ".checkbox_status_exp:checked" ), function( i, obj ) {
                dataExpense.push(obj.value);
                dataApproveListMobile.push(obj.value);
            });
            if(dataExpense.length > 0){
                $('#exp_selected_btn').attr('disabled',false);
                $('#exp_selected').css("filter","grayscale(0%)");
                $('#approve_selected_mobile_button_li').removeClass('hide');
            }else{
                $('#exp_selected_btn').attr('disabled',true);
                $('#exp_selected').css("filter","grayscale(100%)");
                $('#approve_selected_mobile_button_li').addClass('hide');

            }
        });


        $('.dv-background').hide();
    },2000);



    $('#app_selected_btn').on('click',function () {
        approveSelectedApprove(dataApproves);
        $('#app_selected_btn').attr('disabled',true);
        $('#app_selected').css("filter","grayscale(100%)");
        dataApproves.length = 0;
    });

    $('#exp_selected_btn').on('click',function () {
        approveSelectedExpense(dataExpense);
        $('#exp_selected_btn').attr('disabled',true);
        $('#exp_selected').css("filter","grayscale(100%)");
        dataExpense.length = 0;
    });

    $('#approve_selected_mobile_button_li').on('click',function () {
        approveSelectedMobile(dataApproveListMobile);
        $('#approve_selected_mobile_button_li').addClass('hide');
        $('#app_selected_btn').attr('disabled',true);
        $('#app_selected').css("filter","grayscale(100%)");
        $('#exp_selected_btn').attr('disabled',true);
        $('#exp_selected').css("filter","grayscale(100%)");
        dataApproveListMobile.length = 0;
    })


    $('#requestExpense').on('click',function () {
        $('.dv-background').show();
        DocumentTYPE = MASTER_DATA.DOC_EXP_TYPE;

        window.setTimeout(function () {
            findByRequesterAndDocumentType()
            $('.dv-background').hide();
        },500);

    })

    $('#requestApprove').addClass('bgblue');

    $('#requestApprove').on('click',function () {
        $('.dv-background').show();
        DocumentTYPE = MASTER_DATA.DOC_APP_TYPE;

        window.setTimeout(function(){
            findByRequesterAndDocumentType()
            $('.dv-background').hide();
        },500);

    })

    $('#requestADV').on('click',function () {
        $('.dv-background').show();
        DocumentTYPE = MASTER_DATA.DOC_ADV_TYPE;

        window.setTimeout(function(){
            findByRequesterAndDocumentType()
            $('.dv-background').hide();
        },500);

    })


    $(".rectangle-tab").click(function () {
        $(".rectangle-tab").removeClass('bgblue')
        $(this).addClass('bgblue')
    })


    $("#incomingFilter").click(function () {
        $(this).find('img').toggle();
    })

    $("#myRequest").click(function () {
        $(this).find('img').toggle();
    })

    $("#expense_collapse").click(function () {
        $(this).find('span').toggle();
    })

    $('#approve_collapse').click(function () {
        $(this).find('span').toggle();
    })

    $('#mobile_heading_advance').click(function () {
        $(this).find('span').toggle();
    })

    $('#mobile_heading_expense').click(function () {
        $(this).find('span').toggle();
    })

    $('#mobile_heading_approve').click(function () {
        $(this).find('span').toggle();
    })

    $('#employeeInputEmployeeAllInputEmployeeAll').keypress(function () {
        AutocompleteEmployeeAll.search($('#employeeInputEmployeeAllInputEmployeeAll').val(),"employeeInputEmployeeAllInputEmployeeAll");
    })

    // $('#employeeInputEmployeeAllMobileInputEmployeeAll_mobile').keypress(function () {
    //     AutocompleteEmployeeAll.search($('#employeeInputEmployeeAllMobileInputEmployeeAll_mobile').val());
    // })


    $('#advance_mobile_button').on('click',function () {
        DocumentTYPE = MASTER_DATA.DOC_ADV_TYPE;
        $('#mobile_heading_advance').removeClass('hide');
        $('#mobile_heading_expense').addClass('hide');
        $('#mobile_heading_approve').addClass('hide');

        $('.dv-background').show();
        window.setTimeout(function(){
            findByRequesterAndDocumentType()

        },500);
        // $('.dv-background').hide();
    })

    $('#expense_mobile_button').on('click',function () {
        DocumentTYPE = MASTER_DATA.DOC_EXP_TYPE;
        $('#mobile_heading_advance').addClass('hide');
        $('#mobile_heading_expense').removeClass('hide');
        $('#mobile_heading_approve').addClass('hide');

        $('.dv-background').show();
        window.setTimeout(function(){
            findByRequesterAndDocumentType()

        },500);
        // $('.dv-background').hide();
    })

    $('#approve_mobile_button').on('click',function () {
        DocumentTYPE = MASTER_DATA.DOC_APP_TYPE;
        $('#mobile_heading_advance').addClass('hide');
        $('#mobile_heading_expense').addClass('hide');
        $('#mobile_heading_approve').removeClass('hide');

        $('.dv-background').show();
        window.setTimeout(function(){
            findByRequesterAndDocumentType()

        },500);
        // $('.dv-background').hide();
    })

    $('[data-toggle="tooltip"]').tooltip();

    $(window).resize(function () {
        moveFixedActionButton();
    })

    $('.close').on('click', function () {
        // $('.top-notif').animate({'margin-right': '-524px'});
        $('.top-notif').animate({'margin-right': '-524px'});
        window.setTimeout(function () {
            $('.top-notif').hide();
        },800)
        // $('.top-notif').hide('slide',{direction:'right',distance:524},500);
    });

})





function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.left = (xPos-75) + 'px';
    }
}
function searchByNextApproverAndRequesterAndTitleDescription() {
    $('.dv-background').show();
    window.setTimeout(function(){
        findIncomingRequestApprove();
        findIncomingRequestExpense();

        $('.dv-background').hide();
    },500);
}
function searchByRequesterAndDocumentType(){
    $('.dv-background').show();
    window.setTimeout(function(){
        findByRequesterAndDocumentType();
        $('.dv-background').hide();
    },500);
}
function approveSelectedExpense(data) {

    $('.dv-background').show();

    let datas = {};
    datas['data'] = data.toString();

    // console.log(datas);
    $('.dv-background').show();
    window.setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/approveSelected',
            data: datas,
            async: false,
            complete: function (xhr) {
                findIncomingRequestExpense();
                $('.dv-background').hide();
                $('#approve_button').empty();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);

                $('input[name = "check_approve_app"]').on('change',function(){
                    dataApproves.length = 0;
                    // console.log("approve checkbox active");
                    $.each( $( ".checkbox_status_app:checked" ), function( i, obj ) {
                        dataApproves.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });

                    if(dataApproves.length > 0){
                        $('#app_selected_btn').attr('disabled',false);
                        $('#app_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#app_selected_btn').attr('disabled',true);
                        $('#app_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');

                    }
                });

                $('input[name = "check_approve_exp"]').on('change',function(){
                    dataExpense.length = 0;
                    // console.log("expense checkbox active");
                    $.each( $( ".checkbox_status_exp:checked" ), function( i, obj ) {
                        dataExpense.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });
                    if(dataExpense.length > 0){
                        $('#exp_selected_btn').attr('disabled',false);
                        $('#exp_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#exp_selected_btn').attr('disabled',true);
                        $('#exp_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');
                    }
                });
            }
        });
    },500);
}
function approveSelectedApprove(data) {
    $('.dv-background').show();

    let datas = {};
    datas['data'] = data.toString();

    // console.log(datas);
    $('.dv-background').show();
    window.setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/approveSelected',
            data: datas,
            async: false,
            complete: function (xhr) {
                findIncomingRequestApprove();
                $('.dv-background').hide();
                $('#approve_button').empty();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);

                $('input[name = "check_approve_app"]').on('change',function(){
                    dataApproves.length = 0;
                    // console.log("approve checkbox active");
                    $.each( $( ".checkbox_status_app:checked" ), function( i, obj ) {
                        dataApproves.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });

                    if(dataApproves.length > 0){
                        $('#app_selected_btn').attr('disabled',false);
                        $('#app_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#app_selected_btn').attr('disabled',true);
                        $('#app_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');

                    }
                });

                $('input[name = "check_approve_exp"]').on('change',function(){
                    dataExpense.length = 0;
                    // console.log("expense checkbox active");
                    $.each( $( ".checkbox_status_exp:checked" ), function( i, obj ) {
                        dataExpense.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });
                    if(dataExpense.length > 0){
                        $('#exp_selected_btn').attr('disabled',false);
                        $('#exp_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#exp_selected_btn').attr('disabled',true);
                        $('#exp_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');
                    }
                });
            }
        });
    },500);
}
function approveSelectedMobile(data) {
    $('.dv-background').show();

    let datas = {};
    datas['data'] = data.toString();

    // console.log(datas);
    $('.dv-background').show();
    window.setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/approveSelected',
            data: datas,
            async: false,
            complete: function (xhr) {
                findIncomingRequestApprove();
                findIncomingRequestExpense()
                $('.dv-background').hide();
                $('#approve_button').empty();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);

                $('input[name = "check_approve_app"]').on('change',function(){
                    dataApproves.length = 0;
                    // console.log("approve checkbox active");
                    $.each( $( ".checkbox_status_app:checked" ), function( i, obj ) {
                        dataApproves.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });

                    if(dataApproves.length > 0){
                        $('#app_selected_btn').attr('disabled',false);
                        $('#app_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#app_selected_btn').attr('disabled',true);
                        $('#app_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');

                    }
                });

                $('input[name = "check_approve_exp"]').on('change',function(){
                    dataExpense.length = 0;
                    // console.log("expense checkbox active");
                    $.each( $( ".checkbox_status_exp:checked" ), function( i, obj ) {
                        dataExpense.push(obj.value);
                        dataApproveListMobile.push(obj.value);
                    });
                    if(dataExpense.length > 0){
                        $('#exp_selected_btn').attr('disabled',false);
                        $('#exp_selected').css("filter","grayscale(0%)");
                        $('#approve_selected_mobile_button_li').removeClass('hide');
                    }else{
                        $('#exp_selected_btn').attr('disabled',true);
                        $('#exp_selected').css("filter","grayscale(100%)");
                        $('#approve_selected_mobile_button_li').addClass('hide');
                    }
                });
            }
        });
    },500);
}
function addComma(textValue){
    if(textValue !=null){
        return textValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }else{
        textValue
    }

}
function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_FOREIGN){
        return MASTER_DATA.FLOW_TYPE_FOREIGN;
    }else if(approveType == MASTER_DATA.APR_TYPE_CAR){
        return MASTER_DATA.FLOW_TYPE_CAR;
    }else if(approveType == MASTER_DATA.APR_TYPE_HOTEL){
        return MASTER_DATA.FLOW_TYPE_HOTEL;
    }else if(approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        return MASTER_DATA.FLOW_TYPE_FIGHT_BOOKING;
    }
}
function cancelRequest(id,docNumber,type,approveType){

    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = 1;
    jsonDetails['flowType'] = validateFlowTypeByApproveType(approveType);

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['id'] = id;
    jsonData['requester'] = $USER_NAME;
    jsonData['docNumber'] = docNumber;
    jsonData['details'] = JSON.stringify(listJsonDetails);


    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/requests/cancelRequest',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $DATA_REQUEST = JSON.parse(xhr.responseText);
                if($DATA_REQUEST != null){
                    cancelDocument(id,type,approveType);
                }

            }
            $('#noRequestModal').modal('hide')
            $('#cancleDocMobile').modal('hide');
            $('#haveRequestModal').modal('hide');
            $('#cancleRequest_mobile').modal('hide');
        }
    });



}
function cancelDocument(id,type,approveType){

    $('.dv-background').show();
    window.setTimeout(function () {
        if(type == 'APP') {


            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('#noRequestModal').modal('hide')
                        $('#cancleDocMobile').modal('hide');
                        $('#haveRequestModal').modal('hide');
                        $('#cancleRequest_mobile').modal('hide');
                        // findByRequester_Moblie_Mod();
                        findByRequesterAndDocumentType();
                        $('.dv-background').hide();
                    }

                }
            });

        }
        if(type == MASTER_DATA.DOC_EXP_TYPE) {


            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/expense/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {

                        $('#noRequestModal').modal('hide')
                        $('#cancleDocMobile').modal('hide');
                        $('#haveRequestModal').modal('hide');
                        $('#cancleRequest_mobile').modal('hide');
                        // findByRequester_Moblie_Mod();
                        findByRequesterAndDocumentType();
                        $('.dv-background').hide();
                    }

                }
            });

        }

        if(type == MASTER_DATA.DOC_ADV_TYPE) {


            var jsonData = {}
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id;
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '//advance/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('#noRequestModal').modal('hide')
                        $('#cancleDocMobile').modal('hide');
                        $('#haveRequestModal').modal('hide');
                        $('#cancleRequest_mobile').modal('hide');
                        // findByRequester_Moblie_Mod();
                        findByRequesterAndDocumentType();
                        $('.dv-background').hide();
                    }

                }

            });

        }

    },500);

}
function findByRequesterAndDocumentType(){
    // findExpenseTypeRequestAmount();
    // findApproveRequestAmount();
    // findAdvanceRequestAmount();
    // findSizeRequest();

    findMyRequestNotification();

    // let pathVariable = '?requester='+$USER_NAME+'&documentNumber='+$('#myRequest_docNumber').val()+'&titleDescription='+$('#myRequest_titleDescription').val()+'&docType='+DocumentTYPE;
    // let pathParam = '?requester=' + data.requester + '&docNumber=' + data.docNumber + '&titleDescription=' + data.titleDescription + "&documentStatus=" + data.documentStatus;


    let criteriaObject = {
        requester : $USER_NAME,
        docNumber : $('#myRequest_docNumber').val(),
        titleDescription : $('#myRequest_titleDescription').val(),
        documentType : DocumentTYPE,
        documentStatus : 'ONP,DRF'
    }

    if($(window).width() < 1300){
        objectMyRequest.setId("#paggingSearchMyRequestMobile");
    }else{
        objectMyRequest.setId("#paggingSearchMyRequestPC");
    }


    objectMyRequest.setUrlData("/dashboardMyRequest/findByCriteria");
    objectMyRequest.setUrlSize("/dashboardMyRequest/findSize");

    objectMyRequest.loadTable = function(items) {

        let item = items.content;

        let nextApprover;

        $('#dashboard_myRequestBody').empty();
        $('#myRequest_mobile_device').empty();

        sumDateProcess = 0;

        for (var i = 0 ; i < item.length; i++) {
            let record = '';
            let appendMobileBody = '';
            let isPaid = false;


            if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS || item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) {
                if (item[i].id) {

                    record += '' +
                        '<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<div class="col-sm-1" style="padding-top: 10px; padding-left: 0; padding-right: 0; text-align: right">';


                    appendMobileBody += '' +
                        '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-top: 10px;">' +
                        '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">';


                    if (item[i].requests) {

                        nextApprover = findEmployeeProfileByUserName(item[i].requests.nextApprover);

                        record += '' +
                            // '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '"  onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                            // '<img src="' + $IMAGE_CANCEL + '" width="40px"/>&#160;' +
                            // '</a>'+
                            '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                            '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                            '</a>';
                        // appendMobileBody += '' +
                        //     '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#numberDocument_mobile\').val(\'' + item[i].docNumber + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\')"  data-target="#cancleRequest_mobile" data-toggle="modal"><img src="' + $IMAGE_CANCEL + '" width="50px;"/></a>';
                    } else {
                        record += '<a title="' + $MESSAGE_CANCEL_DOC_TOOLTIP + '"  onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').empty(),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal" data-toggle="modal">' +
                            '<img src="' + $IMAGE_CANCEL + '" width="40px"/>&#160;' +
                            '</a>' +
                            '<a title="' + $MESSAGE_VIEW_DRAFT_DOC_TOOLTIP + '" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                            '<img src=' + $IMG_PENCIL + ' width="35px"/>&#160;' +
                            '</a>';

                        appendMobileBody += '' +
                            '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\'),$(\'#docnumber_noRequest_mobile\').empty(),$(\'#docnumber_noRequest_mobile\').append(\'' + item[i].docNumber + '\')"  data-target="#cancleDocMobile" data-toggle="modal"><img src="' + $IMAGE_CANCEL + '" width="50px;"/></a>';
                    }

                    record += '</div>' +
                        '<div class="col-sm-2" style="text-align: left; padding-right: 0px;">' +
                        '<div class="col-sm-12" style="padding-right: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].docNumber + '">' +
                        item[i].docNumber +
                        '</div>' +
                        '<div class="col-sm-12" style="color: orange; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;" title="' + (item[i].titleDescription ? item[i].titleDescription : '-') + '">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '<div class="col-sm-12" style="color: deepskyblue; font-size: 12px;"> ' +
                        ((item[i].createdDate) ? item[i].createdDate.substring(0, item[i].createdDate.lastIndexOf(':')) : '-') +
                        '</div>' +
                        '<div class="col-sm-12">' +
                        ((item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) ? '<span style="background-color: red; color: white; font-weight: 900;">DRAFT</span>' : '') +
                        '</div>' +
                        '</div>';

                    appendMobileBody += '' +
                        '</div>' +
                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                        '<div   class="col-xs-12" style="text-align: left; font-size: 17px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;">' +
                        '<div style="display: inline-block">' +
                        item[i].docNumber +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                        '<div style="display: inline-block;">' +
                        (item[i].titleDescription ? item[i].titleDescription : "-") +
                        '</div>' +
                        '</div>' +
                        '<div id="sumProcessDay' + i + '" class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                        '0 Days' +
                        '</div>' +
                        '</div>' +
                        '</a>' +
                        '</div>';


                    // if (item[i].requests !== undefined && item[i].requests != null && item[i].requests) {
                    if (item[i].requests) {
                        // if (item[i].requests.id !== undefined && item[i].requests.id != null && item[i].requests.id) {
                        if (item[i].requests.id) {
                            $.ajax({
                                url: session.context + '/myApprove/getApprover/' + item[i].requests.id,
                                type: "GET",
                                async: false,
                                complete: function (xhr) {

                                    let docRemark = $.ajax({
                                        url: session.context + '/documentAccountRemarks/findByDocumentId?docId=' + item[i].id,
                                        headers: {
                                            Accept: "application/json"
                                        },
                                        type: "GET",
                                        async: false
                                    }).responseJSON;

                                    let itemRemark, accountDays;
                                    if (docRemark.content.length > 0) {
                                        itemRemark = docRemark.content;
                                        for(let k = 0; k < itemRemark.length ; k++){
                                            if (itemRemark.startDate) {
                                                accountDays = accountProcessDays(itemRemark.startDate, itemRemark.endDate)
                                            }
                                        }
                                    }


                                    $APPROVERS = JSON.parse(xhr.responseText);


                                    $APPROVERS = $APPROVERS.requestApprovers;

                                    $APPROVERS.sort(function (a, b) {
                                        return a.sequence > b.sequence;
                                    });

                                    record += '' +
                                        '<div class="col-sm-4">' +
                                        '<ol class="progress" data-steps="' + $APPROVERS.length + '" style="height: 100%;">';

                                    let checkActive = 0;
                                    let oneDay = 24 * 60 * 60 * 1000;
                                    let diffDays;
                                    let nameLast;
                                    let dateLast;
                                    let start_time;
                                    let end_time;

                                    let to_day = new Date();
                                    let days = to_day.getDate();
                                    let month = to_day.getMonth() + 1; //January is 0!
                                    let year = to_day.getFullYear();
                                    let hours = to_day.getHours();
                                    let minutes = to_day.getMinutes();

                                    if (days < 10)
                                        days = '0' + days;
                                    if (month < 10)
                                        month = '0' + month;
                                    if (hours < 10)
                                        hours = '0' + hours;
                                    if (minutes < 10)
                                        minutes = '0' + minutes;


                                    to_day = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes;

                                    sumDateProcess = 0;

                                    appendMobileBody += '' +
                                        '<div id="detail' + (startCount_Request) + '" class="panel-collapse collapse" role="detail' + (startCount_Request) + '">' +
                                        '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px;  background-color: white; text-align: center;">';

                                    if ($APPROVERS[0].actionTime) {

                                        start_time = moment(item[i].sendDate, moment.ISO_8601);
                                        end_time = moment($APPROVERS[0].actionTime, moment.ISO_8601);

                                        diffDays = end_time.diff(start_time, 'days');

                                        sumDateProcess += diffDays;
                                        record += '' +
                                            '<li class="done" style="text-align: center;">' +
                                            '<div style="text-align: center">' +
                                            '<span class="step">' +
                                            '<span>' +
                                            $APPROVERS[0].actionState +
                                            '</span>' +
                                            '</span>' +
                                            '<span class="name">' +
                                            diffDays +
                                            '</span>' +
                                            '</div>' +
                                            '</li>';

                                        appendMobileBody += '' +
                                            '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #47e833">' +
                                            '<a style="font-weight: 600">' +
                                            $APPROVERS[0].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                            ($APPROVERS[0].approver ? $APPROVERS[0].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // item[i].createdDate.substring(0, item[i].createdDate.lastIndexOf(':')) +
                                            (item[i].sendDate ? item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':')) : '-') +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                            end_time.diff(start_time, 'days') + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';

                                    } else if ($APPROVERS[0].userNameApprover == item[i].requests.nextApprover) {


                                        start_time = moment(item[i].sendDate, moment.ISO_8601);
                                        end_time = moment(to_day, moment.ISO_8601);

                                        diffDays = end_time.diff(start_time, 'days');
                                        sumDateProcess += diffDays;

                                        record += '' +
                                            '<li class="active" style="text-align: left;">' +
                                            '<div style="text-align: center">' +
                                            '<span class="step">' +
                                            '<span>'
                                            + $APPROVERS[0].actionState +
                                            '</span>' +
                                            '</span>' +
                                            '<span class="name">' + diffDays + '</span>' +
                                            '</div>' +
                                            '</li>';

                                        appendMobileBody += '' +
                                            '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #ff9800">' +
                                            '<a style="font-weight: 600">' +
                                            $APPROVERS[0].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                            ($APPROVERS[0].approver ? $APPROVERS[0].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':')) +
                                            (item[i].sendDate ? item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':')) : '-') +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                            end_time.diff(start_time, 'days') + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';
                                    }

                                    for (let j = 1; j < $APPROVERS.length; j++) {
                                        let actionState = $APPROVERS[j].actionState;

                                        let sendDate_day = new Date($APPROVERS[j - 1].actionTime);
                                        let sendDate_days = sendDate_day.getDate();
                                        let sendDate_month = sendDate_day.getMonth() + 1;
                                        let sendDate_year = sendDate_day.getFullYear();
                                        let sendDate_hours = sendDate_day.getHours();
                                        let sendDate_minutes = sendDate_day.getMinutes();

                                        if (sendDate_days < 10)
                                            sendDate_days = '0' + sendDate_days;
                                        if (sendDate_month < 10)
                                            sendDate_month = '0' + sendDate_month;
                                        if (sendDate_hours < 10)
                                            sendDate_hours = '0' + sendDate_hours;
                                        if (sendDate_minutes < 10)
                                            sendDate_minutes = '0' + sendDate_minutes;

                                        sendDate_day = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes;

                                        if ($APPROVERS[j].actionState != null && $APPROVERS[j].requestStatusCode != null) {

                                            if (checkActive == 0) {

                                                start_time = moment($APPROVERS[j - 1].actionTime, moment.ISO_8601);
                                                end_time = moment($APPROVERS[j].actionTime, moment.ISO_8601);

                                                diffDays = end_time.diff(start_time, 'days');

                                                sumDateProcess = sumDateProcess + diffDays;
                                                record +=
                                                    '<li class="done" style="text-align: center;">' +
                                                    '<div style="text-align: center">' +
                                                    '<span class="step">' +
                                                    '<span>' +
                                                    $APPROVERS[j].actionState +
                                                    '</span>' +
                                                    '</span>' +
                                                    '<span class="name">' +
                                                    (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                                    '</span>' +
                                                    '</div>' +
                                                    '</li>';


                                                appendMobileBody += '' +
                                                    '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                    '<div class="circle" style="background-color: #47e833">' +
                                                    '<a style="font-weight: 600">' +
                                                    $APPROVERS[j].actionState +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left">' +
                                                    ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                                    '</div>' +
                                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                    // $APPROVERS[j].createdDate.substring(0, $APPROVERS[j].createdDate.lastIndexOf(':')) +
                                                    sendDate_day +
                                                    '</div>' +
                                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                    (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) + ' Day' +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '</a>' +
                                                    '</div>';

                                            } else {

                                                record +=
                                                    '<li  style="text-align: center;">' +
                                                    '<div style="text-align: center">' +
                                                    '<span class="step">' +
                                                    '<span>'
                                                    + $APPROVERS[j].actionState +
                                                    '</span>' +
                                                    '</span>' +
                                                    '<span class="name" style="color: transparent;">diffDays</span>' +
                                                    '</div>' +
                                                    '</li>';

                                                appendMobileBody += '' +
                                                    '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                    '<div class="circle" style="background-color: #c0c0c0">' +
                                                    '<a style="font-weight: 600">' +
                                                    $APPROVERS[j].actionState +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '<a data-toggle="collapse" data-target="#detail' + j + '">' +
                                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                                    ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                                    '</div>' +
                                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                    '<jsp:text/>' +
                                                    '</div>' +
                                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                    '<jsp:text/>' +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '</a>' +
                                                    '</div>';
                                            }
                                        } else {
                                            checkActive++
                                            if (checkActive > 1) {
                                                record +=
                                                    '<li>' +
                                                    '<span class="step">' +
                                                    '<span>'
                                                    + $APPROVERS[j].actionState +
                                                    '</span>' +
                                                    '</span>' +
                                                    '<span class="name" style="color: transparent">' +
                                                    'Waiting' +
                                                    '</span>' +
                                                    '</li>';

                                                appendMobileBody += '' +
                                                    '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                    '<div class="circle" style="background-color: #c0c0c0">' +
                                                    '<a style="font-weight: 600">' +
                                                    $APPROVERS[j].actionState +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '<a data-toggle="collapse" data-target="#detail' + j + '">' +
                                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                                    ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                                    '</div>' +
                                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                    '<jsp:text/>' +
                                                    '</div>' +
                                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                    '<jsp:text/>' +
                                                    '</a>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '</a>' +
                                                    '</div>';
                                            } else {

                                                nameLast = $APPROVERS[j].approver;
                                                dateLast = $APPROVERS[j].receiveTime;

                                                if ($APPROVERS[j].userNameApprover != $APPROVERS[j - 1].userNameApprover) {
                                                    if ($APPROVERS[j - 1].actionTime) {

                                                        if (($APPROVERS[j].actionState).toUpperCase().indexOf("PAID") >= 0) {
                                                            isPaid = true;
                                                        }

                                                        // start_time = moment($APPROVERS[j].createdDate, moment.ISO_8601);
                                                        start_time = moment($APPROVERS[j - 1].actionTime, moment.ISO_8601);
                                                        end_time = moment(to_day, moment.ISO_8601);

                                                        diffDays = end_time.diff(start_time, 'days');
                                                        sumDateProcess = sumDateProcess + diffDays;

                                                        record +=
                                                            '<li class="active" style="text-align: left;">' +
                                                            '<div style="text-align: center">' +
                                                            '<span class="step">' +
                                                            '<span>' +
                                                            $APPROVERS[j].actionState +
                                                            '</span>' +
                                                            '</span>' +
                                                            '<span class="name">' +
                                                            (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                                            '</span>' +
                                                            '</div>' +
                                                            '</li>';

                                                        appendMobileBody += '' +
                                                            '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                            '<div class="circle" style="background-color: #ff9800">' +
                                                            '<a style="font-weight: 600">' +
                                                            $APPROVERS[j].actionState +
                                                            '</a>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                                            (isPaid ? $LABEL_BANK_TRANSFER : ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-')) +
                                                            '</div>' +
                                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                            // $APPROVERS[j].createdDate.substring(0, $APPROVERS[j].createdDate.lastIndexOf(':')) +
                                                            sendDate_day +
                                                            '</div>' +
                                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                            (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) + ' Day' +
                                                            '</a>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</a>' +
                                                            '</div>';
                                                    } else {
                                                        record +=
                                                            '<li>' +
                                                            '<span class="step">' +
                                                            '<span>'
                                                            + $APPROVERS[j].actionState +
                                                            '</span>' +
                                                            '</span>' +
                                                            '<span class="name" style="color: transparent">' +
                                                            'Waiting' +
                                                            '</span>' +
                                                            '</li>';

                                                        appendMobileBody += '' +
                                                            '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                            '<div class="circle" style="background-color: #c0c0c0">' +
                                                            '<a style="font-weight: 600">' +
                                                            $APPROVERS[j].actionState +
                                                            '</a>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<a data-toggle="collapse" data-target="#detail' + j + '">' +
                                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                                            ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                                            '</div>' +
                                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                            '<jsp:text/>' +
                                                            '</div>' +
                                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                            '<jsp:text/>' +
                                                            '</a>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</a>' +
                                                            '</div>';
                                                    }

                                                } else {
                                                    record += '' +
                                                        '<li>' +
                                                        '<span class="step">' +
                                                        '<span>'
                                                        + $APPROVERS[j].actionState +
                                                        '</span>' +
                                                        '</span>' +
                                                        '<span class="name" style="color: transparent">a</span>' +
                                                        '</li>';

                                                    appendMobileBody += '' +
                                                        '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                                        '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                                        '<div class="circle" style="background-color: #c0c0c0">' +
                                                        '<a style="font-weight: 600">' +
                                                        $APPROVERS[j].actionState +
                                                        '</a>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                                        '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                                        ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                                        '</div>' +
                                                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                                        '<jsp:text/>' +
                                                        '</div>' +
                                                        '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                                        '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                                        '<jsp:text/>' +
                                                        '</a>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</a>' +
                                                        '</div>';
                                                }
                                            }
                                        }
                                    }
                                    record +=
                                        '</ol></div>';

                                    appendMobileBody += '' +
                                        '<a onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                        '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                        '</a>';
                                }
                            });
                        } else {

                            record += '' +
                                '<div class="col-sm-4">' +
                                '<ol class="progress" data-steps="3" style="height: 100%;">' +
                                '<li>' +
                                '<span class="step"><span>VRF</span></span>' +
                                '<span class="name" style="color: transparent">1 Day</span>' +
                                '</li>' +
                                '<li>' +
                                '<span class="step"><span>APR</span></span>' +
                                '<span  class="name" style="color: transparent" >5 Days</span>' +
                                '</li>' +
                                '<li>' +
                                '<span class="step"><span >ADM</span></span>' +
                                '<span class="name" style="color: transparent" >5 Days</span>' +
                                '</li>' +
                                '</ol>' +
                                '</div>';


                            appendMobileBody += '' +
                                '<div id="detail' + (startCount_Request) + '" class="panel-collapse collapse" role="detail' + (startCount_Request) + '">' +
                                '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; padding-bottom: 4px; background-color: white; text-align: center;">' +
                                '<div style="  height: 5em; width: 100%; background-color: #e7e124; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                '<a data-toggle="collapse" data-target="detail' + (startCount_Request) + '">' +
                                '<div class="col-xs-12" style="font-size: 4em; font-weight: bold; color: white; display: inline-block; text-align: center; vertical-align: middle;">' +
                                $LABEL_DRAFT_STATUS +
                                '</div>' +
                                '</a>' +
                                '</div>' +
                                '<a onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                '</a>';

                        }

                    } else {
                        record += '' +
                            '<div class="col-sm-4">' +
                            '<ol class="progress" data-steps="3" style="height: 100%;">' +
                            '<li>' +
                            '<span class="step"><span>VRF</span></span>' +
                            '<span class="name" style="color: transparent">1 Day</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span>APR</span></span>' +
                            '<span  class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span >ADM</span></span>' +
                            '<span class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '</ol>' +
                            '</div>';


                        appendMobileBody += '' +
                            '<div id="detail' + (startCount_Request) + '" class="panel-collapse collapse" role="detail' + (startCount_Request) + '">' +
                            '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; padding-bottom: 4px; background-color: white; text-align: center;">' +
                            '<div style="  height: 5em; width: 100%; background-color: #e7e124; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                            '<a data-toggle="collapse" data-target="detail' + (startCount_Request) + '">' +
                            '<div class="col-xs-12" style="font-size: 4em; font-weight: bold; color: white; display: inline-block; text-align: center; vertical-align: middle;">' +
                            $LABEL_DRAFT_STATUS +
                            '</div>' +
                            '</a>' +
                            '</div>' +
                            '<a onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                            '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                            '</a>';

                    }

                    let rgbCode;

                    if (DocumentTYPE == MASTER_DATA.DOC_APP_TYPE) {
                        APP_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    } else if (DocumentTYPE == MASTER_DATA.DOC_EXP_TYPE) {
                        EXP_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    } else {
                        ADV_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    }


                    record += '' +
                        '<div class="col-sm-1" style="text-align: center; padding-top: 0px; font-size: 40px; color:' + rgbCode + '">' + sumDateProcess + '</div>' +
                        '<div class="col-sm-2" style="text-align: center; padding-top: 0px; padding-left: 0px; padding-right: 0px; text-align: left;">' +
                        '<div class="col-sm-12">' +
                        (isPaid ? $LABEL_BANK_TRANSFER : (((nextApprover != 'NaN undefined' && nextApprover)) ? nextApprover : '-')) +
                        '</div>' +
                        '<div class="col-sm-12" style="font-size: 12px; color: deepskyblue;">' +
                        ((item[i].requests) ? (((item[i].requests.nextApprover) ? ((item[i].requests.lastActionTime) ? item[i].requests.lastActionTime.substring(0, item[i].requests.lastActionTime.lastIndexOf(':')) : item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':'))) : '-')) : '-') +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-2" style="padding-top: 0px; padding-right: 0px; color: limegreen; text-align: right;">' +
                        '<b style="font-size: x-large;">' + ((item[i].totalAmount) ? item[i].totalAmount.toLocaleString(undefined, {minimumFractionDigits: 2}) : (0).toLocaleString(undefined, {minimumFractionDigits: 2})) + ' ฿</b>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';

                    appendMobileBody += '</div>' +
                        '</div>';
                }
            }

            $('#dashboard_myRequestBody').append(record);
            $('#myRequest_mobile_device').append(appendMobileBody);
            startCount_Request++;
            $('#sumProcessDay' + i).empty();
            if (item[i].documentStatus != MASTER_DATA.DOC_STATUS_DRAFT) {
                $('#sumProcessDay' + i).append(sumDateProcess + ' Days');
            }

        }

        $('.dv-background').hide();
    }
    objectMyRequest.setDataSearch(criteriaObject);
    objectMyRequest.search(objectMyRequest);
}
function findExpenseTypeRequestAmount(){

    docTypeEXP =  MASTER_DATA.DOC_EXP_TYPE

    if(docTypeEXP == null || docTypeEXP == undefined || docTypeEXP == ''){
        docTypeEXP = 'EXP'
    }

    let pathVariable = '?requester='+$USER_NAME+'&documentNumber='+$('#myRequest_docNumber').val()+'&titleDescription='+$('#myRequest_titleDescription').val()+'&docType='+docTypeEXP;

    $.ajax({
        type: "GET",
        url: session['context']+'/advance/findByRequesterAndDocNumberAndTitleAndDocType' + pathVariable,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {


                    if(xhr.responseText){

                        let number = 0;
                        $('#amountExpenseRequest').empty();

                        var obj = JSON.parse(xhr.responseText);
                        var item = obj.content
                        item.forEach(function(k){
                            if(k.documentStatus){
                                if(k.documentStatus === MASTER_DATA.DOC_STATUS_ON_PROCESS || k.documentStatus === MASTER_DATA.DOC_STATUS_DRAFT){
                                    number++;
                                }
                            }
                        });
                        $('#amountExpenseRequest').addClass('circleMyRequest');
                        number ? $('#amountExpenseRequest').append('<b>' + number + '</b>') : $("#amountExpenseRequest").append('<b>0</b>');
                    }else{

                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findApproveRequestAmount(){

    docTypeAPP =  MASTER_DATA.DOC_APP_TYPE

    if(docTypeAPP == null || docTypeAPP == undefined || docTypeAPP == ''){
        docTypeAPP = 'APP'
    }

    let pathVariable = '?requester='+$USER_NAME+'&documentNumber='+$('#myRequest_docNumber').val()+'&titleDescription='+$('#myRequest_titleDescription').val()+'&docType=' + docTypeAPP;

    $.ajax({
        type: "GET",
        url: session['context']+'/advance/findByRequesterAndDocNumberAndTitleAndDocType'+pathVariable,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {


                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        var item = obj.content

                        let number = 0;
                        $('#amountApproveRequest').empty()

                        item.forEach(function(k){
                            if(k.documentStatus){
                                if(k.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS || k.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
                                    number++;
                                }
                            }

                        });


                        $('#amountApproveRequest').addClass('circleMyRequest');
                        number ? $('#amountApproveRequest').append('<b>' + number + '</b>') : $("#amountApproveRequest").append('<b>0</b>');



                    }else{

                    }
                }
            }
        }
    }).done(function (){

    });

}
function findAdvanceRequestAmount(){


    docTypeADV =  MASTER_DATA.DOC_ADV_TYPE

    if(docTypeADV == null || docTypeADV == undefined || docTypeADV == ''){
        docTypeADV = 'ADV'
    }

    let pathVariable = '?requester='+$USER_NAME+'&documentNumber='+$('#myRequest_docNumber').val()+'&titleDescription='+$('#myRequest_titleDescription').val()+'&docType=' + docTypeADV;
    $.ajax({
        type: "GET",
        url: session['context']+'/advance/findByRequesterAndDocNumberAndTitleAndDocType'+pathVariable,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {


                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);

                        var item = obj.content;
                        let number = 0;

                        $('#amountAdvanceRequest').empty();

                        item.forEach(function(k){
                            if(k.documentStatus){
                                if(k.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS || k.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
                                    number++;
                                }
                            }

                        });


                        $('#amountAdvanceRequest').addClass('circleMyRequest');
                        number ? $('#amountAdvanceRequest').append('<b>' + number + '</b>') : $("#amountAdvanceRequest").append('<b>0</b>');



                    }else{

                    }
                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function apppend(_callback) {
    _callback();
}
function findIncomingRequestExpense(){

    let requester = '';
    let titleDescription = '';
    let username = $USERNAME;
    let $REQUEST_APPROVERS;
    if($(window).width() > 1299) {
        (($('#employeeInputEmployeeAllInputEmployeeAll').attr('data-username') !== undefined) ? requester = $('#employeeInputEmployeeAllInputEmployeeAll').attr('data-username') : requester = '');
        titleDescription = $('#incoming_documentNumber').val() != '' ? titleDescription = $('#incoming_documentNumber').val() : '';
    }
    else {
        // (($('#employeeInputEmployeeAllMobileInputEmployeeAll').attr('data-username') !== undefined) ? requester = $('#employeeInputEmployeeAllMobileInputEmployeeAll').attr('data-username') : requester = '');
        titleDescription = $('#incoming_titleDescription_mobile').val() != '' ? titleDescription = $('#incoming_titleDescription_mobile').val() : '';
    }

    findUserHead();

    let companyCode = '';

    if(userHead.content[0].id){
        $.each(userHead.content,function (index,item) {
            username += ',' + item.variable1;
            if($USERNAME == item.code && item.variable10){
                if(companyCode){
                    companyCode += ','+item.variable10;
                }else{
                    companyCode = item.variable10;
                }

            }
        })
    }else{
        username = $USERNAME
    }

    // userHead ? username += ',' + userHead.variable1 : username = $USERNAME;

    if($(window).width() < 1300){
        objectExpense.setId("#paggingSearchIncomingExpenseMobile");
    }else{
        objectExpense.setId("#paggingSearchIncomingExpensePC");
    }

    let criteriaObject;

    // console.log('companyCode : ' +companyCode);

    if(companyCode){
        criteriaObject = {
            nextApprover : username,
            requester : requester,
            docNumber : $('#incoming_documentNumber').val() == '' ? '' : $('#incoming_documentNumber').val(),
            documentStatus : MASTER_DATA.DOC_STATUS_ON_PROCESS,
            documentType : MASTER_DATA.DOC_ADV_TYPE + ',' + MASTER_DATA.DOC_EXP_TYPE,
            companyCode : companyCode,
        }
        objectExpense.setUrlData("/dashBoardIncomingExpense/findCriteriaWithCompanyCode");
        objectExpense.setUrlSize("/dashBoardIncomingExpense/findSizeWithCompanyCode");
    }else{
        criteriaObject = {
            nextApprover : username,
            requester : requester,
            docNumber : $('#incoming_documentNumber').val() == '' ? '' : $('#incoming_documentNumber').val(),
            documentStatus : MASTER_DATA.DOC_STATUS_ON_PROCESS,
            documentType : MASTER_DATA.DOC_ADV_TYPE + ',' + MASTER_DATA.DOC_EXP_TYPE
        }
        objectExpense.setUrlData("/dashBoardIncomingExpense/findByCriteria");
        objectExpense.setUrlSize("/dashBoardIncomingExpense/findSize");
    }

    objectExpense.loadTable = function(items) {

        let item = items.content;

        $("#id_pc_expense").empty();
        $("#id_expense_mobile").empty();
        $("#id_expense_notificaiton").empty();

        let expenseRecord;
        let expenseRecordMobile;
        let expenseNotification = 0;
        let checkState;

        $("#id_expense_notificaiton").append('<span class="circle right" >' + items.page.totalElements + '</span>');

        console.log('-----');
        console.log(item);

        for (let i = 0; i < item.length; i++) {

            checkState = '';

            if(item[i].id) {

                // $.ajax({
                //     url: session.context + '/myApprove/getApprover/' + item[i].id,
                //     type: "GET",
                //     async: false,
                //     complete: function (xhr) {
                //         $REQUEST_APPROVERS = JSON.parse(xhr.responseText);
                //     }
                // })

                // $REQUEST_APPROVERS = $REQUEST_APPROVERS.requestApprovers;
                $REQUEST_APPROVERS = item[i].requestApprovers;

                if ($REQUEST_APPROVERS) {
                    $REQUEST_APPROVERS = $REQUEST_APPROVERS.sort(function (a, b) {
                        return a.id > b.id;
                    });

                    for (let temp = 0; temp < $REQUEST_APPROVERS.length; temp++) {
                        if (!$REQUEST_APPROVERS[temp].actionTime) {
                            checkState = $REQUEST_APPROVERS[temp].actionState;
                            break;
                        }
                    }
                }

                expenseRecord = '';
                expenseRecordMobile = '';
                if (item[i].document != null) {
                    if (item[i].document.documentType == MASTER_DATA.DOC_ADV_TYPE || item[i].document.documentType == MASTER_DATA.DOC_EXP_TYPE) {
                        if (item[i].document.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {

                            let requester = item[i].document.requesterName ?  item[i].document.requesterName : '-';
                            let processDay = processDays(item[i].lastActionTime, item[i].document.sendDate);
                            let rgbCode;

                            if (session.roleName.indexOf("ROLE_ACCOUNT") >= 0) {
                                let docRemark = $.ajax({
                                    url: session.context + '/documentAccountRemarks/findByDocumentId?docId=' + item[i].document.id,
                                    headers: {
                                        Accept: "application/json"
                                    },
                                    type: "GET",
                                    async: false
                                }).responseJSON;

                                if (docRemark) {
                                    let itemRemark = docRemark.content;
                                    for(let k = 0; k < itemRemark.length; k++){
                                        if (itemRemark[k].startDate) {
                                            processDay = processDay - accountProcessDays(itemRemark[k].startDate, itemRemark[k].endDate);
                                        }
                                    }
                                }
                            }

                            if (item[i].document.documentType == MASTER_DATA.DOC_EXP_TYPE) {
                                EXP_COLOR.forEach(function (item) {
                                    if (processDay >= item.minDay && processDay <= item.maxDay)
                                        rgbCode = item.color;
                                })
                            } else {
                                ADV_COLOR.forEach(function (item) {
                                    if (processDay >= item.minDay && processDay <= item.maxDay)
                                        rgbCode = item.color;
                                })
                            }

                            var totalAmount = item[i].document && item[i].document.totalAmount ? item[i].document.totalAmount : 0;

                            expenseNotification++;
                            expenseRecord += '' +
                                '<div class="row" >' +
                                '<div class="col-sm-12">' +
                                '<div class="col-sm-3" style="padding-top: 15px; padding-left: 0px; padding-right: 0px; text-align: center;">';

                            // if($USERNAME == item[i].nextApprover && (session.roleName.indexOf("ROLE_ACCOUNT") == -1 && session.roleName.indexOf("ROLE_FINANCE") == -1) &&  (checkState.indexOf("FNC") == -1 && checkState.indexOf("ACC"))){
                            if ($USERNAME == item[i].nextApprover && (checkState.indexOf("FNC") == -1 && checkState.indexOf("ACC") == -1 && checkState.indexOf("HR") == -1)) {
                                expenseRecord += '' +
                                    '<div class="col-sm-1 checkbox checkbox-info" style="margin-top: 5px;">' +
                                    '<label style="color: black;"><input type="checkbox" class="checkbox_status_exp" name="check_approve_exp"  id="checkbox_status_" value="' + item[i].document.id + '|' + item[i].document.docNumber + '|' + item[i].document.processId + '|' + item[i].document.documentType + '|' + $USERNAME + '"/><jsp:text/></label>' +
                                    '</div>' +
                                    '<a title="' + $MESSAGE_APPROVE_REQUEST_TOOLTIP + '" onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')">' +
                                    '<img src=' + $IMAGE_TICK + ' width="35px"/>&#160;' +
                                    '</a>' +
                                    '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                                    '</a>';
                            } else {
                                expenseRecord += '' +
                                    '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDocAccount(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                                    '</a>';
                            }

                            expenseRecord += '' +
                                '</div>' +
                                '<div class="col-sm-1" style="display: inline; color: red; text-align: center">' +
                                '   <div class="row" style="font-weight: 900; font-size: 30px; color:' + rgbCode + ';">' +
                                processDay +
                                '</div>' +
                                '<div class="row" style="font-weight: 900; font-size: 20px; color:' + rgbCode + ';">' +
                                $LABEL_DAY +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-3" style="text-align: left; padding-top: 15px; padding-right: 0px; padding-left: 0px;">' +
                                '<div class="col-sm-12" style="white-space: nowrap;  overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].document.docNumber + '">' +
                                item[i].document.docNumber +
                                '</div>' +
                                '<div class="col-sm-12" style="color: green; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;" title="' + ((item[i].document.titleDescription) ? item[i].document.titleDescription : "") + '">' +
                                ((item[i].document.titleDescription) ? item[i].document.titleDescription : "-") +
                                '</div>' +
                                    '' +
                                '' +
                                '<div title="'+item[i].document.verifyName+'" name="verifyDocAcc" class="col-sm-12 '+checkAccVerify(item[i].document.verifyName)+'">' +
                                    '<div><jspx:text/></div>' +
                                    '<div class="text-center" style=" border-radius: 10px;   font-size: 15px;font-weight: bold;color: white; background-color: '+renderColorAccVerify(item[i].document.verifyName)+'     ">' +
                                      ((item[i].document.verifyName) != null ? $LABEL_VERIFIED : $LABEL_VERIFIED) +'&#160;<img  width="15px;" height="15px" src=' + $IMAGE_CLICK + ' />'+
                                    '</div>' +
                                    '<div> <jspx:text/>  </div>' +
                                '</div>'+


                                '</div>' +
                                '<div class="col-sm-3" style="text-align: left; padding-top: 15px; padding-right: 0px; padding-left: 0px;">' +
                                '<div class="col-sm-12" style="padding-right:0px; padding-left: 0px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap">' +
                                requester +
                                '</div>' +
                                '<div class="col-sm-12" style="color: deepskyblue; padding-left: 0px;">' +
                                ((item[i].lastActionTime) ? item[i].lastActionTime.substring(0, item[i].lastActionTime.lastIndexOf(':')) : item[i].document.sendDate.substring(0, item[i].document.sendDate.lastIndexOf(':'))) +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-2" style="color: limegreen; padding-top: 15px; padding-right:0px;">' +
                                '<div class="pull-right">' + (totalAmount.toLocaleString(undefined, {minimumFractionDigits: 2})) + ' ฿</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';


                            expenseRecordMobile += '' +
                                '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px;">';


                            if ($USERNAME == item[i].nextApprover && (checkState.indexOf("FNC") == -1 && checkState.indexOf("ACC") == -1 && checkState.indexOf("HR") == -1)) {
                                expenseRecordMobile += '' +
                                    '<div class="col-xs-1 col-xs-1 checkbox checkbox-info" style="margin-top: 18px; padding-right: 0px; vertical-align: middle;">' +
                                    '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="' + item[i].document.id + '|' + item[i].document.docNumber + '|' + item[i].document.processId + '|' + item[i].document.documentType + '|' + $USERNAME + '"/><jsp:text/></label>' +
                                    '</div>&#160;' +
                                    '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 15px; width: 55px; display: inline-block">' +
                                    '<a onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')"><img src=' + $IMAGE_TICK + ' width="50px"/>&#160;</a>' +
                                    '</div>';
                            } else {
                                expenseRecordMobile += '' +
                                    '<div class="col-xs-1 col-xs-1 checkbox checkbox-info" style="margin-top: 18px; padding-right: 0px; vertical-align: middle;">' +
                                    // '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="'+item[i].document.id+'|'+item[i].document.docNumber+'|'+ item[i].document.processId +'|'+item[i].document.documentType+'|' + $USERNAME + '"/><jsp:text/></label>'+
                                    '</div>&#160;' +
                                    '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 15px; width: 55px; display: inline-block">' +
                                    // '<a onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')"><img src=' + $IMAGE_TICK + ' width="50px"/>&#160;</a>'+
                                    '</div>';
                            }

                            expenseRecordMobile += '' +
                                '<a data-toggle="collapse" data-target="#expense_detial_' + item[i].id + '">' +
                                '<div class="col-xs-9" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                '<div class="col-xs-10" style="font-size: 17px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                ((item[i].document.titleDescription) ? item[i].document.titleDescription : '-') +
                                '</div>' +
                      /*AA*/     '<div title="'+item[i].document.verifyName+'" class="col-xs-2 '+checkAccVerify(item[i].document.verifyName)+'" style=" border-radius: 10px;   font-weight: bold; background-color: #00a8ce; font-size: 14px; color: white; display: inline-block; text-align: center; padding-right: 0px; padding-left: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                      /*AA*/          $LABEL_VERIFIED+ '&#160;<img  width="15px;" height="15px" src=' + $IMAGE_CLICK + ' />'+
                      /*AA*/          '</div>' +

                                '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                requester +
                                '</div>' +
                                '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                ((item[i].document.totalAmount) ? item[i].document.totalAmount.toLocaleString(undefined, {minimumFractionDigits: 2}) : (0).toLocaleString(undefined, {minimumFractionDigits: 2})) + " ฿" +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>' +
                                '<div id="expense_detial_' + item[i].id + '" class="panel-collapse collapse" role="expense_detial_' + item[i].id + '">' +
                                '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; padding-bottom: 4px; background-color: white; text-align: center;">' +
                                '<div class="col-xs-12" style="font-size: 16px; text-align: left">' +
                                $LABEL_DOCUMENT_NUMBER +
                                '</div>' +
                                '<div class="col-xs-12" style="font-size: 16px; color:deepskyblue; text-align: right; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction:rtl;">' +
                                item[i].document.docNumber +
                                '</div>' +
                                '<div class="col-xs-12" style="font-size: 4em; font-weight: bold; color: white; display: inline-block; text-align: center; vertical-align: middle;">';

                            if ($USERNAME == item[i].nextApprover && (checkState.indexOf("FNC") == -1 && checkState.indexOf("ACC") == -1)) {
                                expenseRecordMobile += '' +
                                    '<a onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                    '</a>';
                            } else {
                                expenseRecordMobile += '' +
                                    '<a onclick="incomingDocAccount(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                    '</a>';
                            }

                            expenseRecordMobile += '' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br/>';


                        }
                    }
                }
            }


            apppend(function () {
                $("#id_pc_expense").append(expenseRecord);
                $("#id_expense_mobile").append(expenseRecordMobile);
            });
            // alert('LAP : ' + i);
        }
        $.material.init();
    }

    objectExpense.setDataSearch(criteriaObject);
    objectExpense.search(objectExpense);
}
function findIncomingRequestApprove() {
    let requester = '';
    let titleDescription = '';
    let username = $USERNAME;
    let $REQUEST_APPROVERS;

    if ($(window).width() > 1299) {
        (($('#employeeInputEmployeeAllInputEmployeeAll').attr('data-username') !== undefined) ? requester = $('#employeeInputEmployeeAllInputEmployeeAll').attr('data-username') : requester = '');
        titleDescription = $('#incoming_documentNumber').val() != '' ? titleDescription = $('#incoming_documentNumber').val() : '';
    }
    else {
        // (($('#employeeInputEmployeeAllMobileInputEmployeeAll').attr('data-username') !== undefined) ? requester = $('#employeeInputEmployeeAllMobileInputEmployeeAll').attr('data-username') : requester = '');
        titleDescription = $('#incoming_titleDescription_mobile').val() != '' ? titleDescription = $('#incoming_titleDescription_mobile').val() : '';
    }

    findUserHead();

    let companyCode = '';

    if(userHead.content[0].id){
        $.each(userHead.content,function (index,item) {
            username += ',' + item.variable1;
            if($USERNAME == item.code && item.variable10){
                if(companyCode){
                    companyCode += ','+item.variable10;
                }else{
                    companyCode = item.variable10;
                }
            }
        })
    }else{
        username = $USERNAME
    }


    let criteriaObject;

    if(companyCode){
        criteriaObject = {
            nextApprover : username,
            requester : requester,
            docNumber : $('#incoming_documentNumber').val() == '' ? '' : $('#incoming_documentNumber').val(),
            documentStatus : MASTER_DATA.DOC_STATUS_ON_PROCESS,
            documentType : MASTER_DATA.DOC_APP_TYPE,
            companyCode : companyCode
        }
        objectApprove.setUrlData("/dashBoardIncomingApprove/findByCriteria");
        objectApprove.setUrlSize("/dashBoardIncomingApprove/findSize");
    }else{
        criteriaObject = {
            nextApprover : username,
            requester : requester,
            docNumber : $('#incoming_documentNumber').val() == '' ? '' : $('#incoming_documentNumber').val(),
            documentStatus : MASTER_DATA.DOC_STATUS_ON_PROCESS,
            documentType : MASTER_DATA.DOC_APP_TYPE
        }
        objectApprove.setUrlData("/dashBoardIncomingApprove/findByCriteria");
        objectApprove.setUrlSize("/dashBoardIncomingApprove/findSize");
    }

    if ($(window).width() < 1300) {
        objectApprove.setId("#paggingSearchIncomingApproveMobile");
    } else {
        objectApprove.setId("#paggingSearchIncomingApprovePC");
    }

    objectApprove.loadTable = function (items) {
        let item = items.content;

        $("#id_pc_approve").empty();
        $("#id_approve_mobile").empty();
        $("#id_approve_notification").empty();

        let approverRecord;
        let approveRecordMobile;
        let checkState;
        // let processDay;
        $("#id_approve_notification").append('<span class="circle right" >' + items.page.totalElements + '</span>');

        console.log(item);

        for (let i = 0; i < item.length; i++) {
            approverRecord = '';
            approveRecordMobile = '';

            checkState = '';

            if(item[i].id) {

                // $.ajax({
                //     url: session.context + '/myApprove/getApprover/' + item[i].id,
                //     type: "GET",
                //     async: false,
                //     complete: function (xhr) {
                //         $REQUEST_APPROVERS = JSON.parse(xhr.responseText);
                //     }
                // })

                // $REQUEST_APPROVERS = $REQUEST_APPROVERS.requestApprovers;
                $REQUEST_APPROVERS = item[i].requestApprovers;

                if ($REQUEST_APPROVERS) {
                    $REQUEST_APPROVERS = $REQUEST_APPROVERS.sort(function (a, b) {
                        return a.id > b.id;
                    });

                    for (let temp = 0; temp < $REQUEST_APPROVERS.length; temp++) {
                        if (!$REQUEST_APPROVERS[temp].actionTime) {
                            checkState = $REQUEST_APPROVERS[temp].actionState;
                            break;
                        }
                    }
                }

                if (item[i].document != null) {
                    if (item[i].document.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
                        if (item[i].document.documentType == MASTER_DATA.DOC_APP_TYPE) {

                            let requester = item[i].document.requesterName ?  item[i].document.requesterName : '-';
                            var processDay = processDays(item[i].lastActionTime, item[i].document.sendDate);

                            let rgbCode;

                            APP_COLOR.forEach(function (item) {
                                if (processDay >= item.minDay && processDay <= item.maxDay)
                                    rgbCode = item.color;
                            })

                            approverRecord += '' +
                                '<div class="row" >' +
                                '<div class="col-sm-12">' +
                                '<div class="col-sm-3" style="padding-top: 15px; padding-left: 0px; padding-right: 0px; text-align: center;">';


                            if ($USERNAME == item[i].nextApprover && (checkState.indexOf("FNC") == -1)) {
                                approverRecord += '' +
                                    '<div class="col-sm-1 checkbox checkbox-info" style="margin-top: 5px;">' +
                                    '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="' + item[i].document.id + '|' + item[i].document.docNumber + '|' + item[i].document.processId + '|' + item[i].document.documentType + '|' + $USERNAME + '"/><jsp:text/></label>' +
                                    '</div>' +
                                    '<a title="' + $MESSAGE_APPROVE_REQUEST_TOOLTIP + '" onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')">' +
                                    '<img src=' + $IMAGE_TICK + ' width="35px"/>&#160;' +
                                    '</a>' +
                                    '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                                    '</a>';
                            } else {
                                approverRecord += '' +
                                    '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                    '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                                    '</a>';
                            }

                            // approverRecord += '' +
                            // '<div class="row" >' +
                            //     '<div class="col-sm-12">' +
                            //         '<div class="col-sm-3" style="padding-top: 15px; padding-left: 0px; padding-right: 0px; text-align: center;">'+
                            //             '<div class="col-sm-1 checkbox checkbox-info" style="margin-top: 5px;">'+
                            //                 '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="'+item[i].document.id+'|'+item[i].document.docNumber+'|'+ item[i].document.processId +'|'+item[i].document.documentType+'|' + $USERNAME + '"/><jsp:text/></label>'+
                            //             '</div>'+
                            //             '<a title="' + $MESSAGE_APPROVE_REQUEST_TOOLTIP + '" onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')">' +
                            //                 '<img src=' + $IMAGE_TICK + ' width="35px"/>&#160;' +
                            //             '</a>'+
                            //             '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                            //                 '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                            //             '</a>'+
                            approverRecord += '' +
                                '</div>' +
                                '<div class="col-sm-1" style="display: inline; color: red; text-align: center;">' +
                                '<div class="row" style="font-weight: 900; font-size: 30px; color:' + rgbCode + ';">' +
                                processDay +
                                '</div>' +
                                '<div class="row" style="font-weight: 900; font-size: 20px; color:' + rgbCode + ';">' +
                                $LABEL_DAY +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4" style="text-align: left; padding-top: 15px; padding-right: 0px; padding-left: 0px;" >' +
                                '<div class="col-sm-12" style="white-space: nowrap; width: 180px; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].document.docNumber + '">' +
                                item[i].document.docNumber +
                                '</div>' +
                                '<div class="col-sm-12" style="color: green; white-space: nowrap; width: 180px; overflow: hidden;text-overflow: ellipsis;" title="' + ((item[i].document.titleDescription) ? item[i].document.titleDescription : "") + '">' +
                                ((item[i].document.titleDescription) ? item[i].document.titleDescription : "-") +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4" style="text-align: left; padding-top: 15px; padding-right: 0px; padding-left: 0px;">' +
                                '<div class="col-sm-12">' +
                                requester +
                                '</div>' +
                                '<div class="col-sm-12" style="color: deepskyblue;">' +
                                ((item[i].lastActionTime) ? item[i].lastActionTime.substring(0, item[i].lastActionTime.lastIndexOf(':')) : item[i].document.sendDate.substring(0, item[i].document.sendDate.lastIndexOf(':'))) +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';

                            approveRecordMobile += '' +
                                '<div style="height: 60px; width: 100%; background-color: #858585;">';

                            if ($USERNAME == item[i].nextApprover && (checkState.indexOf("FNC") == -1)) {
                                approveRecordMobile += '' +
                                    '<div class="col-xs-1 col-xs-1 checkbox checkbox-info" style="margin-top: 18px; padding-right: 0px; vertical-align: middle;">' +
                                    '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="' + item[i].document.id + '|' + item[i].document.docNumber + '|' + item[i].document.processId + '|' + item[i].document.documentType + '|' + $USERNAME + '"/><jsp:text/></label>' +
                                    '</div>&#160;' +
                                    '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 15px; width: 55px; display: inline-block"><jsp:text/>' +
                                    '<a onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')"><img src=' + $IMAGE_TICK + ' width="50px"/>&#160;</a>' +
                                    '</div>';
                            } else {
                                approveRecordMobile += '' +
                                    '<div class="col-xs-1 col-xs-1 checkbox checkbox-info" style="margin-top: 18px; padding-right: 0px; vertical-align: middle;">' +
                                    // '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="'+item[i].document.id+'|'+item[i].document.docNumber+'|'+ item[i].document.processId +'|'+item[i].document.documentType+'|' + $USERNAME + '"/><jsp:text/></label>'+
                                    '</div>&#160;' +
                                    '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 15px; width: 55px; display: inline-block"><jsp:text/>' +
                                    // '<a onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')"><img src=' + $IMAGE_TICK + ' width="50px"/>&#160;</a>'+
                                    '</div>';
                            }

                            // approveRecordMobile += '' +
                            // '<div style="height: 60px; width: 100%; background-color: #858585;">'+
                            //     '<div class="col-xs-1 col-xs-1 checkbox checkbox-info" style="margin-top: 18px; padding-right: 0px; vertical-align: middle;">'+
                            //         '<label style="color: black;"><input type="checkbox" class="checkbox_status_app" name="check_approve_app"  id="checkbox_status_" value="'+item[i].document.id+'|'+item[i].document.docNumber+'|'+ item[i].document.processId +'|'+item[i].document.documentType+'|' + $USERNAME + '"/><jsp:text/></label>'+
                            //     '</div>&#160;'+
                            //     '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 15px; width: 55px; display: inline-block"><jsp:text/>'+
                            //         '<a onclick="findRequestByDocument(' + item[i].document.id + ',\'' + item[i].document.docNumber + '\',' + item[i].document.processId + ',\'' + item[i].document.documentType + '\')"><img src=' + $IMAGE_TICK + ' width="50px"/>&#160;</a>'+
                            //     '</div>'+
                            approveRecordMobile += '' +
                                '<a class="col-xs-8" style="padding-left: 0px;" data-toggle="collapse" data-target="#approve_mobile_' + item[i].id + '">' +
                                '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; white-space: nowrap; width: 300px; overflow: hidden;text-overflow: ellipsis;">' +
                                (item[i].document.docNumber ? item[i].document.docNumber : '-') +
                                '</div>' +
                                '<div class="col-xs-12" style="font-size: 17px; color: #52f7fc; display: inline-block">' +
                                ((item[i].document.titleDescription) ? item[i].document.titleDescription : "-") +
                                '</div>' +
                                '</a>' +
                                '</div>' +
                                '<div id="approve_mobile_' + item[i].id + '" class="panel-collapse collapse" role="approve_mobile_' + item[i].id + '">' +
                                '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; background-color: white;">' +
                                '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; background-color: white;">' +
                                '<div class="col-xs-12" style="font-size: 16px; text-align: left; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                $LABEL_REQUESTER +
                                '</div>' +
                                '<div class="col-xs-12" style="font-size: 16px; color:deepskyblue; text-align: right; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                requester +
                                '</div>' +
                                '<div clas="row" style="bottom: 0px">' +
                                '<div class="col-xs-12" style="text-align: center; display: inline-block">' +
                                '<a onclick="incomingDoc(' + item[i].document.id + ',\'' + item[i].document.documentType + '\',\'' + item[i].document.approveType + '\',\'' + item[i].document.createdBy + '\')">' +
                                '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br/>';
                        }
                    }
                }
            }


            apppend(function () {
                $("#id_pc_approve").append(approverRecord);
                $("#id_approve_mobile").append(approveRecordMobile);
            });
            // alert('LAP : ' + i);
        }
        $.material.init();
    }

    objectApprove.setDataSearch(criteriaObject);
    objectApprove.search(objectApprove);
}
function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;

    return $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
}
function findRequestByDocument(id,docNumber,processId,docType){
    $('.dv-background').show();
    window.setTimeout(function(){
        var data1 = $.ajax({
            url: session.context + "/approve/findRequestByDocument/"+ id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST = data1;

        var data2 = $.ajax({
            url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST_APPROVER = data2;

        let actionState
        let approver
        let documentId
        let rejectReasonCode
        let requestStatus
        let userNameApprover

        for(let i=0;i<$DATA_REQUEST_APPROVER.length; i++){
            if($DATA_REQUEST_APPROVER[i].userNameApprover == $USER_NAME && $DATA_REQUEST_APPROVER[i].requestStatusCode === null){

                actionState = $DATA_REQUEST_APPROVER[i].actionState;
                approver = $DATA_REQUEST_APPROVER[i].approver;
                documentId = $DATA_REQUEST_APPROVER[i].id;
                rejectReasonCode = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                requestStatus = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                userNameApprover = $DATA_REQUEST_APPROVER[i].userNameApprover;
                break;
            }
        }

        var jsonData = {};
        jsonData['userName'] = $USERNAME;
        jsonData['actionStatus'] = validateActionState($USERNAME);
        jsonData['documentNumber'] = docNumber;
        jsonData['docType'] = docType;
        jsonData['documentFlow'] = $DATA_REQUEST.docFlow;
        jsonData['processId'] = processId;
        jsonData['documentId'] = id;
        jsonData['actionReasonCode'] = '';
        jsonData['actionReasonDetail'] = '';


        $.ajax({
            type : "POST",
            url: session.context + "/requests/approveRequest",
            data:jsonData,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);

                searchByNextApproverAndRequesterAndTitleDescription();
                // $.material.init()


            }
        });
    },500);


}
function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}
function modalSuccess(msg) {
    $("#modalbodySuccess").empty();
    $("#modalbodySuccess").append(msg);
    $("#successModal").modal('show');
    setTimeout(function() {$('#successModal').modal('hide');}, 1250);
}
function loadColor() {
    if(userPA){
        if(userPA[0]){
            $COLOR = $.ajax({
                url : session.context + '/parameters/findByParameterDetailCode?code='+ userPA[0].companyCode,
                type : "GET",
                async : false ,
                complete : function (xhr) {
                    if(xhr.readyState == 4){
                        if(xhr.statusCode() == 200){
                            if(xhr.responseText){

                            }
                        }
                    }
                }
            })
        }
    }
}
function findUserPA(){
    userPA = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByKeySearchAndUserName?userName=" + $USER_NAME + '&keySearch=%',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;
}
function processDays(lastActionTime,sendDate){

    let to_day = new Date();
    let days = to_day.getDate();
    let month = to_day.getMonth()+1; //January is 0!
    let year = to_day.getFullYear();
    let hours = to_day.getHours();
    let minutes = to_day.getMinutes();

    if(days<10)
        days = '0'+days;
    if(month<10)
        month = '0'+month;
    if(hours<10)
        hours = '0'+hours;
    if(minutes<10)
        minutes = '0' + minutes;

    to_day = year+'-'+month+'-'+days+' '+hours+':'+minutes;

    let endDate = moment(to_day,moment.ISO_8601);
    let startDate;
    if(lastActionTime)
        startDate = moment(lastActionTime,moment.ISO_8601);
    else
        startDate = moment(sendDate,moment.ISO_8601);

    return endDate.diff(startDate,'days');
}

function accountProcessDays(startedDate,endedDate) {
    let to_day = new Date();
    let days = to_day.getDate();
    let month = to_day.getMonth()+1; //January is 0!
    let year = to_day.getFullYear();
    let hours = to_day.getHours();
    let minutes = to_day.getMinutes();

    if(days<10)
        days = '0'+days;
    if(month<10)
        month = '0'+month;
    if(hours<10)
        hours = '0'+hours;
    if(minutes<10)
        minutes = '0' + minutes;

    to_day = year+'-'+month+'-'+days+' '+hours+':'+minutes;

    let endDate; //= moment(to_day,moment.ISO_8601);
    let startDate;
    if(endedDate){
        startDate = moment(startedDate,moment.ISO_8601);
        endDate = moment(endedDate,moment.ISO_8601);
        return endDate.diff(startDate,'days');
    }else{
        startDate = moment(startedDate,moment.ISO_8601);
        endDate = moment(to_day,moment.ISO_8601);
        return endDate.diff(startDate,'days');
    }
}


function viewDraft(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/createDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/createDoc?doc=' + id;
    }
}
function incomingDoc(id,documentType,approveType,createdBy){
    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        if(session.roleName.indexOf("ROLE_HR") >= 0){
            window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
        }
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}
function incomingDocAccount(id,documentType,approveType,createdBy){
    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        if(session.roleName.indexOf("ROLE_ACCOUNT") >= 0 || session.roleName.indexOf("ROLE_HR") >= 0){
            window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
        }
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}

function ViewDoc(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}
function findUserHead() {
    userHead = $.ajax({
        url: session.context + '/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCodeList?masterdata=M020&code='+$USERNAME,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    // userHead.variable1 ? userHead : userHead = $USER_NAME;
}




function findSizeRequest(){

    $('#amountApproveRequest').empty()
    $('#amountExpenseRequest').empty()
    $('#amountAdvanceRequest').empty()

    $.ajax({
        type: "GET",
        url: session['context']+'/expenseItem/findDocumentAmountSeperateByAmount',
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);

                    $('#amountApproveRequest').addClass('circleMyRequest');
                    $('#amountExpenseRequest').addClass('circleMyRequest');
                    $('#amountAdvanceRequest').addClass('circleMyRequest');

                    $('#amountApproveRequest').append(obj[0])
                    $('#amountExpenseRequest').append(obj[1])
                    $('#amountAdvanceRequest').append(obj[2])





                }
            }
        }
    }).done(function (){
        //close loader
    });

}

function findMyRequestNotification() {
    let data = {
        requester : $USER_NAME,
        docNumber : $('#myRequest_docNumber').val() == '' ? '' : $('#myRequest_docNumber').val(),
        titleDescription : $('#myRequest_titleDescription').val() == '' ? '' : $('#myRequest_titleDescription').val(),
        documentStatus : MASTER_DATA.DOC_STATUS_ON_PROCESS + ',' + MASTER_DATA.DOC_STATUS_DRAFT
    };

    let pathParam = '?requester=' + data.requester + '&docNumber=' + data.docNumber + '&titleDescription=' + data.titleDescription + "&documentStatus=" + data.documentStatus;

    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url : session.context + "/dashboardMyRequest/findMyRequestNotification" + pathParam,
        async: false,
        complete: function (xhr) {

            let item = JSON.parse(xhr.responseText);

            $('#amountApproveRequest').empty();
            $('#amountApproveRequest').addClass('circleMyRequest');
            $('#amountApproveRequest').append('<b>' + item.appSize + '</b>');

            $('#amountExpenseRequest').empty();
            $('#amountExpenseRequest').addClass('circleMyRequest');
            $('#amountExpenseRequest').append('<b>' + item.expSize + '</b>');

            $('#amountAdvanceRequest').empty();
            $('#amountAdvanceRequest').addClass('circleMyRequest');
            $('#amountAdvanceRequest').append('<b>' + item.advSize + '</b>');
        }
    })

}


function renderColorAccVerify(verifyName) {


    if(verifyName != null){

        return '#00a8ce'

    }else{
        return '#fff000'

    }




}

function checkAccVerify(verifyName){
    if(verifyName != null){

        return ''

    }else{
        return 'hide'

    }
}