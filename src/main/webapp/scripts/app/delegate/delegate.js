/**
 * Created by ratthanan-w on 15/9/2560.
 */

var object = $.extend({},UtilPagination);
var object_tmp = $.extend({},UtilPagination);
var assigneeObject = ''
var assigneeObject_tmp = ''
var assignerObject = ''
var expenseTypeObject = ''
var empCodeAssign = ''
var empUserAssignee = ''
var checkEmpCodeAssignee = ''
var $DATA_EMPLOYEE_FOA='';
var $DATA_EMPLOYEE_FNameTH='';
var $DATA_EMPLOYEE_LNameTH=''

var $DATA_EMPLOYEE_Assigner_FOA='';
var $DATA_EMPLOYEE_Assigner_FNameTH='';
var $DATA_EMPLOYEE_Assigner_LNameTH='';
var checkDupAssigner
var checkDupAssignee
var checkDupAssigner_edit
var keepNameAssigner
var keepCodeAssigner
var checkDuplicate_EmpCodeAssignee
var lastRow=0
var flowTypeDesCription=''
$(document).ready(function () {



   




    initUtilAssignee()
    initUtilAssigner()

    $('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });






    $('#addModalAssignee').on('click',function () {


            initUtilAssignee()
        $('#add_assignee_codeInputEmployeeAll').val('')
        $('#add_assignee_id').val('')

    })



    $("#startDateEndDate_span").on('click',function(){
        $("#startDateEndDate").focus();
    });



    $("#add_assigner_amount").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

        $('#search_button_assigner_head').on('click',function () {
            initUtilAssigner()
            $('#add_assigner_codeInputEmployeeAll_tmp').val('')
            $('#add_assigner_flowType').val('')
            $('#add_assigner_amount').val('')
            $('#startDateEndDate').val('')
            $('#add_assigner_id').val('')

        })


    $("#startDateEndDate").on('change',function () {
        var value = $("#startDateEndDate").val();
        if(value.indexOf("to") !== -1){
            var endDate = new Date(DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]));
            var dueDate = new Date(endDate.setDate(endDate.getDate() + 1));
            $("#dueDate").text(dueDate.format('dd/mm/yyyy'))
        }

    });

    initCalendat()
    findAllEmployee()

    findFlowType()
    searchAssignee()

    $('#search_button_assignee').on('click',function () {
        searchAssignee()
    })

    $('#search_button_assigner').on('click',function () {
        searchAssigner()
    })


});

function initCalendat() {

    $("#startDateEndDate").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

}


function searchAssignee(){

    var criteriaObject = {
        // empCodeAssignee        :  $CRITERIA_EMP_CODE_ASSIGNEE == "" ? "" : $CRITERIA_EMP_CODE_ASSIGNEE
        empCodeAssignee        :  $('#search_assignee').val() == "" ? "" : $('#search_assignee').val()
        // empCodeAssignee        :  ''
    };

    console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryAssigneeByCriteria(criteriaObject);

}
function queryAssigneeByCriteria(criteriaObject){
    object.setId("#pagingSearchDelegateBody");
    object.setUrlData("/delegate/findByCriteria");
    object.setUrlSize("/delegate/findSize");




    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();


        $('#delegateBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;

            var checkDupAssignee = []

            console.log('SIZE ASSIGNEE =====>'+item.length)

            var checkSize = item.length;
            var checkDupCode = '';
            var itemArray = []



            lastRow=0

            for(var j=0;j<item.length;j++){

                console.log("Query Item empCodeAssignee ===> "+item[j].empCodeAssignee)
                console.log("Query Item empUserNameAssignee ===> "+item[j].empUserNameAssignee)

                empCodeAssignee = item[j].empCodeAssignee == null ? "" : item[j].empCodeAssignee
                empUserNameAssignee = item[j].empUserNameAssignee == null ? "" : item[j].empUserNameAssignee

                if(empCodeAssignee != "" && empCodeAssignee != null){
                    findDelegateByEmpCodeAssignee(empCodeAssignee)
                    findEmployeeProfileByUserName(assigneeObject_tmp.empUserNameAssignee)
                }





                        $('#delegateBody').append(''+
                            '<tr id="'+empCodeAssignee+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                            '<td class="text-center">'+'<image class="img-circle" width="45px;" src="'+$IMG_ASSIGNEE+'"  />'+'</td>'+
                            // '<td class="text-center" style="padding-top: 25px; color: #8bd5d2;  font-weight: 800">'+empCodeAssignee+'</td>'+
                            '<td class="text-center" style="padding-top: 25px;">'+$DATA_EMPLOYEE_FOA+' '+$DATA_EMPLOYEE_FNameTH+' '+$DATA_EMPLOYEE_LNameTH+'</td>'+

                            '<td class="text-center"  style="padding-top: 15px;">'+

                            '	<a onclick="$(\'#idItemDelete\').val(\''+item[j].empCodeAssignee+'\')" data-target="#deleteExpenseType" data-toggle="modal" "><span><image style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DELETE_ITEM+'"  /></span></a>'+
                            '	<a   onclick="showDetail(\''+empCodeAssignee+'\',\''+item[j].empCodeAssignee+'\',\''+assigneeObject_tmp.empUserNameAssignee+'\')"><span><image style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DETAIL_ITEM+'"  /></span></a>'+

                            '</td>'+
                            '</tr>'
                        );



                lastRow = j

            }


        }else{
            //Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
    object.loadPage(($CRITERIA_PAGE*1)+1,object);


    var totalResult = $('#pagingSearchDelegateBodyToDisplayRecords').text()
                        $('#pagingSearchDelegateBodyDisplayTotalRecord').empty()
                        $('#pagingSearchDelegateBodyDisplayTotalRecord').append(totalResult)


    var checkZero = $('#pagingSearchDelegateBodyToDisplayRecords').text()

    if(checkZero == '0'){
        $('#pagingSearchDelegateBodyStartDisplayRecords').empty()
        $('#pagingSearchDelegateBodyStartDisplayRecords').append('0')
    }

}
function findAssigneeByEmpCode(empCode){

    $.ajax({
        type: "GET",
        url: session['context']+'/employees/findByEmpCode?empCode='+empCode ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        assigneeObject = obj
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findAssignerByEmpCode(empCode){

    $.ajax({
        type: "GET",
        url: session['context']+'/employees/findByEmpCode?empCode='+empCode ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        assignerObject = obj
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findAllEmployee(){

    $('#add_assignee').empty()
    $('#add_assigner_name').empty()
    $.ajax({
        type: "GET",
        url: session['context']+'/employees/findAllEmployees',
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log('Company Object size ===> '+item.length)

                    for(var i=0; i< item.length; i++){
                        // console.log(item[i].id)
                        $("#add_assignee").append('<option value='+item[i].empCode+'>'+item[i].empName+ ' : '+item[i].empCode+ '</option>')
                        $("#add_assigner_name").append('<option value='+item[i].empCode+'>'+item[i].empName+ ' : '+item[i].empCode+ '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function saveAssignee(){




    var jsonParams = {};

    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['id']     		     = $('#add_assignee_id').val();
    jsonParams['createdBy'] 	     = $('#add_assignee_createdBy').val();
    jsonParams['createdDate'] 	     = $('#add_assigneet_createdDate').val();
    jsonParams['updateBy'] 		     = $('#add_assignee_updateBy').val();
    jsonParams['updateDate'] 	     = $('#add_assigneet_updateDate').val();
    jsonParams['empCodeAssignee']    = $("#add_assignee_codeInputEmployeeAll").attr("data-empCode")  ;
    jsonParams['empUserNameAssignee']= $("#add_assignee_codeInputEmployeeAll").attr("data-username")  ;



    console.log('id ==> '+jsonParams.id)
    console.log('empCodeAssignee ==> '+jsonParams.empCodeAssignee)
    console.log('empUserNameAssignee ==> '+jsonParams.empUserNameAssignee)



    if($("#add_assignee_codeInputEmployeeAll").val() != ""){



        searchAllAssignee(jsonParams.empCodeAssignee)

                    if(checkDuplicate_EmpCodeAssignee == false){



                        $('.dv-background').show();

                        var itemData = $.ajax({
                            async:false,
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context']+'/delegate',
                            data: jsonParams,
                            complete: function (xhr) {


                                if(obj = JSON.parse(xhr.responseText)){

                                    $('#addExpenseType').modal('hide');

                                    $('#saveSuccess').modal('show');
                                    setTimeout(function () {
                                        $('#saveSuccess').modal('hide');
                                    },1500)

                                console.log('empCodeReplace after save'+obj.empCodeAssignee)
                                console.log('empUserNameAssignee after save'+obj.empUserNameAssignee)

                                    empCodeAssign = obj.empCodeAssignee
                                    empUserAssignee = obj.empUserNameAssignee

                                    searchAssignee();
                                    searchAssigner()
                                    $('#search_button_assigner').hide()
                                    $('#search_button_assigner_head').hide()
                                    showDetail_AfterSave(obj.empCodeAssignee,obj.empCodeAssignee,obj.empUserNameAssignee)
                                    // $("tr#"+this).addClass("backgroundSelected")
                                    $('#search_button_assigner').fadeIn('slow');
                                    $('#search_button_assigner_head').fadeIn('slow');
                                }




                            }

                        }).done(function (){

                                $('.dv-background').hide();


                        });
                    }else{
                        searchAssignee()
                        $('#warningModal .modal-body').html("")
                        $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_ASSIGNEE+"<br/>");
                        $('#warningModal').modal('show');
                    }


    }else{

        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");
        $('#warningModal .modal-body').append("      - "+$LABEL_ASSIGNEE_NAME);

        $('#warningModal').modal('show');


    }




    // }else{
    //     $('#warningModal .modal-body').html("")
    //     $('#warningModal .modal-body').append("   Dupplicate Assignee"+"<br/>");
    //     $('#warningModal').modal('show');
    // }











    $('.dv-background').hide();

}
function deleteAssignee(id) {


    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;

    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/delegate/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchAssigner();
            $('#deleteAssigner').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function showDetail(id,empCodeAssignee,empUserAssign) {





    $('#search_button_assigner').fadeIn('slow');
    $('#search_button_assigner_head').fadeIn('slow');

    empUserAssignee = empUserAssign
    empCodeAssign = empCodeAssignee

    $("tr").removeClass("backgroundSelected")
    $("tr#"+id).addClass("backgroundSelected")
    searchAssigner()

console.log('empCodeAssignee ===> '+empCodeAssignee)



}
function searchAssigner(){

    var criteriaObject = {
        empCodeAssignee        : empCodeAssign,
        empCodeAssigner        : $("#search_assigner").val()=="" ? "": $("#search_assigner").val(),
        flowType        : $("#search_flowType").val()=="" ? "": $("#search_flowType").val()
    };

    console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryAssignerByCriteria(criteriaObject);

}
function queryAssignerByCriteria(criteriaObject){
    object.setId("#pagingSearchAssignerBody");
    object.setUrlData("/delegate/findAssignerByCriteria");
    object.setUrlSize("/delegate/findAssignerSize");


    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();


        $('#assignerBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;

            var checkDupAssignee = []


            for(var j=0;j<item.length;j++){



                var empCodeAssigner    = item[j].empCodeAssigner==null?"":item[j].empCodeAssigner;
                var empUserNameAssigner    = item[j].empUserNameAssigner==null?"":item[j].empUserNameAssigner;
                var amount    = item[j].amount==null?"": (item[j].amount)   ;
                var flowType    = item[j].flowType==null?"":item[j].flowType;
                var startDate    = item[j].startDate==null?"": new Date(item[j].startDate).format('dd/mm/yyyy');
                var endDate    = item[j].endDate==null?"": new Date(item[j].endDate).format('dd/mm/yyyy');



                if( empUserNameAssigner != "" && empUserNameAssigner != null){
                    findEmployeeProfileByUserName_Assigner(empUserNameAssigner)

                }


                    if(flowType != null && flowType != ""){
                        findDescriptionFlowType_ByCode(flowType)
                    }




                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#assignerBody').append(''+
                        '<tr style="width: 100%" id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td style="width: 10%" class="text-center">'+'<a onclick="editAssigner(\''+item[j].id+'\')" data-target="#addAssigner" data-toggle="modal"><image width="45px;"  class="img-circle" src="'+$IMG_ASSIGNER+'"  /></a>'+'</td>'+

                        // '<td style="width: 10%" class="text-center" style="padding-top: 25px; color: #8bd5d2; font-size: 18px; font-weight: 800">'+empCodeAssigner+'</td>'+
                        '<td style="width: 15%" class="text-center" style="padding-top: 25px;">'+$DATA_EMPLOYEE_Assigner_FOA+' '+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH+'</td>'+

                        '<td style="width: 5%" class="text-center" style="padding-top: 25px;">'+flowTypeDesCription+'</td>'+
                        '<td style="width: 10%" class="text-center" style="padding-top: 25px;">'+startDate+' - '+endDate+'</td>'+
                        '<td style="width: 5%" class="text-center" style="padding-top: 25px;">'+'<p style="margin-top: 15px;">'+addComma(amount)+'</p>'+'</td>'+
                        '<td style="width: 10%"    class="text-center"  style="padding-top: 15px;">'+

                        '	<a onclick="$(\'#idDeleteAssigner\').val(\''+item[j].id+'\')" data-target="#deleteAssigner" data-toggle="modal" "><span><image class="img-circle" width="35px;" style="margin-top: 5px;" src="'+$IMG_DELETE_ITEM+'"  /></span></a>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
    object.loadPage(($CRITERIA_PAGE_ASSIGNER*1)+1,object);
}
function saveAssigner(){

    var startDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[0]);
    var endDate = DateUtil.coverStringToDate($("#startDateEndDate").val().split(" to ")[1]);

    console.log('id before save'+ $('#add_assigner_id').val())

    var jsonSaveAssigner = {};

    jsonSaveAssigner['parentId']     	     = 1;
    jsonSaveAssigner[csrfParameter]  	     = csrfToken;
    jsonSaveAssigner['id']     		         = $('#add_assigner_id').val();
    jsonSaveAssigner['createdBy'] 	         = $('#add_assignee_createdBy').val();
    jsonSaveAssigner['createdDate'] 	     = $('#add_assigneet_createdDate').val();
    jsonSaveAssigner['updateBy'] 		     = $('#add_assignee_updateBy').val();
    jsonSaveAssigner['updateDate'] 	         = $('#add_assigneet_updateDate').val();
    jsonSaveAssigner['empCodeAssignee']      = empCodeAssign  ;
    jsonSaveAssigner['empUserNameAssignee']  = empUserAssignee  ;
    jsonSaveAssigner['empCodeAssigner']      = $('#add_assigner_codeInputEmployeeAll_tmp').attr("data-empCode")  ;
    jsonSaveAssigner['empUserNameAssigner']  = $('#add_assigner_codeInputEmployeeAll_tmp').attr("data-username")  ;
    jsonSaveAssigner['flowType']             = $('#add_assigner_flowType').val()  ;
    jsonSaveAssigner['amount']               = ($('#add_assigner_amount').val())     ;
    jsonSaveAssigner['startDate']            = new Date(startDate).format('yyyy-mm-dd HH:MM:ss');
    jsonSaveAssigner['endDate']              = new Date(endDate).format('yyyy-mm-dd HH:MM:ss');


        console.log('id   =>'+jsonSaveAssigner.id)
        console.log('assignee   =>'+jsonSaveAssigner.empCodeAssignee)
        console.log('assigner   =>'+jsonSaveAssigner.empCodeAssigner)
        console.log('flow type   =>'+jsonSaveAssigner.flowType)
        console.log('amount   =>'+jsonSaveAssigner.amount)
        console.log('startDate   =>'+jsonSaveAssigner.startDate)
        console.log('endDate   =>'+jsonSaveAssigner.endDate)


                    //
                    // console.log('start Date ====> '+jsonSaveAssigner.startDate)
                    // console.log('end Date ====> '+jsonSaveAssigner.endDate)

                        var startDateCheck = jsonSaveAssigner.startDate
                         var tmpStartDat =        startDateCheck.split(" ")
                       startDateCheck     =  tmpStartDat[0]


    var endDateCheck = jsonSaveAssigner.endDate
    var tmpEndDat =        endDateCheck.split(" ")
    endDateCheck     =  tmpEndDat[0]





if( $('#startDateEndDate').val() != "" &&  $('#add_assigner_amount').val() != "" &&  $('#add_assigner_CodeInputEmployeeAll').val() != "" &&  $('#add_assigner_flowType').val() != ""){


    findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(jsonSaveAssigner.empCodeAssignee,jsonSaveAssigner.empCodeAssigner,jsonSaveAssigner.flowType,startDateCheck,endDateCheck)



    if(checkDupAssigner != true){



        console.log('CASE SAVE !!!')
        console.log('item for check dup assignee  : '+jsonSaveAssigner.empCodeAssignee)
        console.log('item for check dup assigner  : '+jsonSaveAssigner.empCodeAssigner)
        console.log('item for check dup flowType  : '+jsonSaveAssigner.flowType)
        console.log('item for check dup startDate : '+startDateCheck)
        console.log('item for check dup endDate   : '+endDateCheck)










        $('.dv-background').show();
        var itemDatas = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context']+'/delegate',
            data: jsonSaveAssigner,
            complete: function (xhr) {


                searchAssigner();
                $('#add_assigner_name').val('')
                $('#add_assigner_flowType').val('')
                $('#add_assigner_amount').val('')
                $('#startDateEndDate').val('')

                $('#addAssigner').modal('hide');

                $('#saveSuccess').modal('show');
                setTimeout(function () {
                    $('#saveSuccess').modal('hide');
                },1500)




            }
        }).done(function (){
            $('.dv-background').hide();
        });



    }else{

        if(checkDupAssigner_edit == $('#add_assigner_codeInputEmployeeAll_tmp').val()){

            jsonSaveAssigner['empCodeAssigner']      = keepCodeAssigner
            jsonSaveAssigner['empUserNameAssigner']  = keepNameAssigner



                    console.log('CASE DUPLICATE WHEN UPDATE !!!')
            console.log('item for check dup assignee  : '+jsonSaveAssigner.empCodeAssignee)
            console.log('item for check dup assigner  : '+jsonSaveAssigner.empCodeAssigner)
            console.log('item for check dup flowType  : '+jsonSaveAssigner.flowType)
            console.log('item for check dup startDate : '+startDateCheck)
            console.log('item for check dup endDate   : '+endDateCheck)




            var itemDatas = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context']+'/delegate',
                data: jsonSaveAssigner,
                complete: function (xhr) {
                    searchAssigner();
                    $('#add_assigner_name').val('')
                    $('#add_assigner_flowType').val('')
                    $('#add_assigner_amount').val('')
                    $('#startDateEndDate').val('')

                    $('#addAssigner').modal('hide');

                    $('#saveSuccess').modal('show');
                    setTimeout(function () {
                        $('#saveSuccess').modal('hide');
                    },1500)




                }
            }).done(function (){
                //close loader
            });
        }else{
            $('#warningModal .modal-body').html("")
            $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_ASSIGNER+"<br/>");
            $('#warningModal').modal('show');
        }




    }



}else{
    $('#warningModal .modal-body').html("")
    $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");

    if(($('#add_assigner_CodeInputEmployeeAll').val()) == ""){
        $('#warningModal .modal-body').append("      - "+$LABEL_ASSIGNER_NAME+"<br />");
    }

    if(($('#add_assigner_flowType').val()) == ""){
        $('#warningModal .modal-body').append("      - "+$LABEL_AUTHORIZATION+"<br />");
    }

    if(($('#startDateEndDate').val()) == ""){
        $('#warningModal .modal-body').append("      - "+$LABEL_DATE_BETWEEN+"<br/>");

    }
    if(($('#add_assigner_amount').val()) == ""){
        $('#warningModal .modal-body').append("      - "+$LABEL_AMOUNT_OF_DELEGATE+"<br/>");
    }



    $('#warningModal').modal('show');
}











}
function showDetail_AfterSave(name,empCodeAssignee,empUserAssign) {





    $('#search_button_assigner').fadeIn('slow');
    $('#search_button_assigner_head').fadeIn('slow');

    empUserAssignee = empUserAssign
    empCodeAssign = empCodeAssignee

    $("tr").removeClass("backgroundSelected")
    $("tr#"+name).addClass("backgroundSelected")
    searchAssigner()

    console.log('empCodeAssignee ===> '+empCodeAssignee)



}
function deleteAllEmpCodeAssignee(empCodeAssignee){
    $('.dv-background').show();
    $.ajax({
        type: "GET",
        url: session['context']+'/delegate/findAllDelegateByEmpCodeAssignee?empCodeAssignee='+empCodeAssignee,
        async:false,
        complete: function (xhr) {


            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    // console.log('Company Object size ===> '+item.length)

                    for(var i=0; i< item.length; i++){

                        var idForDelete = item[i].id
                        var jsonParams = {};
                        jsonParams['parentId']     = 1;
                        jsonParams[csrfParameter]  = csrfToken;

                        $.ajax({
                            type: "DELETE",
                            url: session['context']+'/delegate/'+idForDelete,
                            data: jsonParams,
                            complete: function (xhr) {

                                searchAssignee()
                                $('#deleteSuccess').modal('show')
                                setTimeout(function () {
                                    $('#deleteSuccess').modal('hide')
                                },1500)



                            }
                        }).done(function (){

                        });




                    }


                }
            }


            searchAssignee();
            searchAssigner();
            $('#deleteExpenseType').modal('hide');
            $('#search_button_assigner').hide()
            $('#search_button_assigner_head').hide()

        }
    }).done(function (){
        $('.dv-background').hide();
    });



}
function findExpenseTypeByFlowType(flowType){




                if(flowType != null && flowType != ""){
                    $.ajax({
                        type: "GET",
                        url: session['context']+'/expenseType/findByFlowType?flowType='+flowType ,
                        async:false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    if(xhr.responseText){
                                        var obj = JSON.parse(xhr.responseText);
                                        expenseTypeObject = obj
                                    }else{
                                        console.log('NOT FOUND')
                                    }








                                }
                            }
                        }
                    }).done(function (){
                        //close loader
                    });
                }else{
                    expenseTypeObject=''
                }



}
function findDelegateByEmpCodeAssignee(empCodeAssignee){
    checkEmpCodeAssignee = false

    $.ajax({
        type: "GET",
        url: session['context']+'/delegate/findByEmpCodeAssignee?empCodeAssignee='+empCodeAssignee,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        var item = obj.content
                        checkEmpCodeAssignee = true
                        assigneeObject_tmp = item[0]
                        console.log('OBJECT FIND ASSIGNEER IS ====> '+item[0].empCodeAssignee)
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function addComma(textValue){
    return textValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function editAssigner(id){


    $('#add_assigner_codeInputEmployeeAll_tmp').val('');



    initUtilAssigner()
    $.ajax({
        type: "GET",
        url: session['context']+'/delegate/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)
                        console.log('ID IS------> '+obj.id)
                    $('#add_assigner_id').val(obj.id);
                    $('#add_assigner_createdBy').val(obj.createdBy);
                    $('#add_assigner_createdDate').val(obj.createdDate);
                    $('#add_assigner_updateBy').val(obj.updateBy);
                    $('#add_assigner_updateDate').val(obj.updateDate);
                    $('#add_assigner_codeInputEmployeeAll_tmp').val(obj.empCodeAssigner);
                    $('#add_assigner_flowType').val(obj.flowType);
                    findEmployeeProfileByUserName(obj.empUserNameAssigner)
                    $('#add_assigner_codeInputEmployeeAll_tmp').val($DATA_EMPLOYEE_Assigner_FOA+''+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH);
                    // checkDupAssigner_edit =  obj.empCodeAssigner
                    checkDupAssigner_edit =  $DATA_EMPLOYEE_Assigner_FOA+''+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH
                    keepCodeAssigner = obj.empCodeAssigner
                    keepNameAssigner = obj.empUserNameAssigner

                    console.log('Keep Emp Code is ===> '+keepCodeAssigner)



                    var dateStart =  DateUtil.coverDateToString(obj.startDate)
                    var dateEnd =  DateUtil.coverDateToString(obj.endDate)
                console.log('dateStart ====> '+dateStart)
                console.log('dateEnd ====> '+dateEnd)
                    $('#startDateEndDate').val(dateStart+' to '+dateEnd);
                    $('#add_assigner_amount').val(obj.amount);



                    console.log('id after get'+ $('#add_assigner_id').val())

                    AutocompleteEmployeeAll_tmp.search(obj.empUserNameAssigner,'add_assigner_codeInputEmployeeAll_tmp')
                    AutocompleteEmployeeAll_tmp.renderValue(obj.empUserNameAssigner)

                }
            }
        }
    }).done(function (){
        //close loader
    });


}
function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    console.info(data);
    // console.info(data.FOA);
    // $DATA_EMPLOYEE = data;
    $DATA_EMPLOYEE_FOA = data.FOA;
    $DATA_EMPLOYEE_FNameTH = data.FNameTH;
    $DATA_EMPLOYEE_LNameTH = data.LNameTH;
}
function findEmployeeProfileByUserName_Assigner(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    console.info(data);
    // console.info(data.FOA);
    // $DATA_EMPLOYEE = data;
    $DATA_EMPLOYEE_Assigner_FOA = data.FOA;
    $DATA_EMPLOYEE_Assigner_FNameTH = data.FNameTH;
    $DATA_EMPLOYEE_Assigner_LNameTH = data.LNameTH;
}
function findFlowType(){


    $("#add_assigner_flowType").empty()
    $("#search_flowType").empty()
    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInOrderByCode?masterdata='+MASTER_DATA.FLOW_TYPE_CODE,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log(item.length)
                    $("#add_assigner_flowType").append('<option value='+""+'>'+''+ '</option>')
                    $("#search_flowType").append('<option value='+""+'>'+''+ '</option>')


                    for(var i=0; i< item.length; i++){
                        // console.log(item[i].id)
                        $("#add_assigner_flowType").append('<option value='+item[i].code+'>'+item[i].description+ '</option>')
                        $("#search_flowType").append('<option value='+item[i].code+'>'+item[i].description+  '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(empCodeAssignee,empCodeAssigner,flowType,startDate,endDate){

    checkDupAssigner = false

    $.ajax({
        type: "GET",
        url: session['context']+'/delegate/findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate?empCodeAssignee='+empCodeAssignee+'&empCodeAssigner='+empCodeAssigner+'&flowType='+flowType+'&startDate='+startDate+'&endDate='+endDate ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        console.log('FOUND')
                        checkDupAssigner = true

                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function searchAllAssignee(empCodeAssignee){

    var criteriaObject = {
        empCodeAssignee        :  ''
    };

    queryAllAssignee(criteriaObject,empCodeAssignee);

}
function queryAllAssignee(criteriaObject,empCodeAssignee){
    // object_tmp.setId("#pagingSearchDelegateBody");
    object_tmp.setUrlData("/delegate/findByCriteria");
    object_tmp.setUrlSize("/delegate/findSize");

    checkDuplicate_EmpCodeAssignee = false


    object_tmp.loadTable = function(items){
        var item = items.content;
        $('.dv-background').show();


        $('#delegateBody').empty();
        if(item.length > 0){


            console.log('SIZE ASSIGNEE <<<<<<<<<<<<<<<<<<<<<<<< =====>'+item.length)





            for(var j=0;j<item.length;j++){



                if(item[j].empCodeAssignee == empCodeAssignee){
                    checkDuplicate_EmpCodeAssignee =true
                }




            }


        }else{
            //Not Found
        }

    };

    object_tmp.setDataSearch(criteriaObject);
    object_tmp.search(object_tmp);
    object_tmp.loadPage(($CRITERIA_PAGE*1)+1,object_tmp);



}
function initUtilAssignee(){



    $('#add_assignee_codeInputEmployeeAll').on('keyup',function () {

        AutocompleteEmployeeAll.setId('add_assignee_codeInputEmployeeAll')
        AutocompleteEmployeeAll.search($('#add_assignee_codeInputEmployeeAll').val(),'add_assignee_codeInputEmployeeAll')

    })

}
function initUtilAssigner(){



    $('#add_assigner_codeInputEmployeeAll_tmp').on('keyup',function () {


        AutocompleteEmployeeAll_tmp.search($('#add_assigner_codeInputEmployeeAll_tmp').val(),'add_assigner_codeInputEmployeeAll_tmp')

    })


}


function findDescriptionFlowType_ByCode(code){




    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode?masterdata='+MASTER_DATA.FLOW_TYPE_CODE+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj
                    flowTypeDesCription = item.description









                }
            }
        }
    }).done(function (){
        //close loader
    });
    console.log('Out find Number Sequence')
}






