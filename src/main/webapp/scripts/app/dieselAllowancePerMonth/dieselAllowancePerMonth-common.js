/**
 * 
 */

var object = $.extend({},UtilPagination);
var dieselJson;
// var listEm = listNameEmployee();
var startPageDe = 1;

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}

}

function saveDieselAllowancePerMonth(){
	//Define flagOther
	var flagActive = $('#modal_edit_flag_active').prop('checked')?"Y":"N";
	var empCode,empUserName ;
	if( ($("#modal_edit_empNameInputEmployeeAll").attr("data-empCode")==""||$("#modal_edit_empNameInputEmployeeAll").attr("data-empCode")==undefined)&&$("#modal_edit_empNameInputEmployeeAll").val()!=""){
		empCode = $("#modal_edit_empCode").val();
	}else {
		empCode = $("#modal_edit_empNameInputEmployeeAll").attr("data-empCode");
	}
	if( ($("#modal_edit_empNameInputEmployeeAll").attr("data-userName")==""||$("#modal_edit_empNameInputEmployeeAll").attr("data-userName")==undefined)&&$("#modal_edit_empNameInputEmployeeAll").val()!=""){
		empUserName = $("#modal_edit_empUserName").val();
	}else {
		empUserName = $("#modal_edit_empNameInputEmployeeAll").attr("data-userName");
	}


	var jsonParams = {};

	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();

	jsonParams['empCode']   	= empCode;
	jsonParams['flagActive']  	= flagActive;
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();
	jsonParams['literPerMonth']     	= $('#modal_edit_literPerMonth').val();/*JSON.stringify(jsonDieselAllowancePerMonthFrom);*/
	jsonParams['balanceLiterPerMonth']     	= $('#modal_edit_literPerMonth').val();
	jsonParams['empUserName']     	= empUserName;

	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/dieselAllowancePerMonths',
        data: jsonParams,
        complete: function (xhr) {
			searchDieselAllowancePerMonth();
			$('#editItemModal').modal('hide');
			$("#completeModal").modal('show');

        }
    }).done(function (){
        //close loader
    });

}

function editDieselAllowancePerMonth(id,empCode){
	$.ajax({
        type: "GET",
        url: session['context']+'/dieselAllowancePerMonths/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var dieselObj = JSON.parse(xhr.responseText);
					var nameEmp = empNameAdd(dieselObj.empUserName);
                    $('#modal_edit_id').val(id);
            		$('#modal_edit_createdBy').val(dieselObj.createdBy);
            		$('#modal_edit_createdDate').val(dieselObj.createdDate);
            		$('#modal_edit_updateBy').val(dieselObj.updateBy);
            		$('#modal_edit_updateDate').val(dieselObj.updateDate);
            		$('#modal_edit_empNameInputEmployeeAll').val(nameEmp);
            		$('#modal_edit_empCode').val(dieselObj.empCode);
					$('#modal_edit_literPerMonth').val(dieselObj.literPerMonth);
					$('#modal_edit_empUserName').val(dieselObj.empUserName);
					if(dieselObj.flagActive=="Y"){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}

                }
            }
        }
    }).done(function (){
        //close loader
		$('#editItemModal').modal('show');
    });

}

function deleteDieselAllowancePerMonth(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;

	$.ajax({
		type: "DELETE",
		url: session['context']+'/dieselAllowancePerMonths/'+id,
		data: jsonParams,
		complete: function (xhr) {
			searchDieselAllowancePerMonth();
			$('#deleteItemModal').modal('hide');
			$('#completeModal').modal('show');
		}
	}).done(function (){
		//close loader
	});
}



function searchDieselAllowancePerMonth(){
	var empCode = "";
	// var empName = "";
	// var empLastName = "";
	// if($("#search_fullname").val()){
	// 	if($("#search_fullname").val().split(" ").length > 0){
	// 		empName = $("#search_fullname").val().split(" ")[0];
	// 	}
	// 	if($("#search_fullname").val().split(" ").length > 1){
	// 		empLastName = $("#search_fullname").val().split(" ")[1];
	// 	}
	// }
	if($("#search_empCode").val().length > 0){
		empCode = $("#search_empCode").val();
	}

	var criteriaObject = {
		empCode     : empCode
	};
	queryDieselAllowancePerMonthByCriteria(criteriaObject);
}

function queryDieselAllowancePerMonthByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");
    object.setUrlData("/dieselAllowancePerMonths/findByCriteria");
    object.setUrlSize("/dieselAllowancePerMonths/findSize");

    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
				var empCode ="";
				var empCodeToHistory = "";

				empCode = item[j].empCode==null?"":item[j].empCode;
				var empName = item[j].empName==null?"":item[j].empName;


				var literPerMonth = item[j].literPerMonth==null?"":item[j].literPerMonth;
                var balanceLiterPerMonth = item[j].balanceLiterPerMonth==null?"":item[j].balanceLiterPerMonth;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;


				itemIdTmp = item[j].id==null?"":item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+itemIdTmp+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix"  style="width: 53px; ">' : '<td class="text-center headcol tdFixGrey"  style="width: 53px;">')+
							'<div class="checkbox" style="color: #2f2f2f;  margin-top: 0px;"><label><input type="checkbox" class="checkbox1" value="'+itemIdTmp+'"/><span class="checkbox-material " style="color: :#2f2f2f;"><span class="check" style="color: :#2f2f2f;" /></span></label></div>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 100px; left: 68px;">' : '<td class="text-center headcol tdFixGrey" style="width: 100px; left: 68px;">')+
							'	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editDieselAllowancePerMonth('+itemIdTmp+','+item[j].empCode+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 110px; left: 163px;">' : '<td class="text-center headcol tdFixGrey" style="width: 110px; left: 163px;">')+
							((flagActive)=='Y' ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;"   />')+
							'</td>'+
                            '<td class="text-center">'+empCode+'</td>'+
                            '<td class="text-center">'+empName+'</td>'+
                            '<td class="text-center">'+literPerMonth+'</td>'+
                            '<td class="text-center">'+balanceLiterPerMonth+'</td>'+
                            '<td class="text-center">'+'<img class="img-circle" src="'+$IMAGE_HISTORY+'"  style="width: 25px;height: 25px; cursor:pointer;" onclick="dieselAllowancePerMonthHistory(\''+empCode+'\')" />'+'</td>'+
                            '</tr>'
                        );
                }

            }

        }else{
        	//Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
	if(startPageDe==1){
		startPageDe++;
		object.loadPage(($CRITERIA_PAGE*1)+1,object);
	}else{
		object.loadPage(1,object);
	}
}

function  empNameAdd(userName) {
	var data = $.ajax({
		url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
		headers: {
			Accept : "application/json"
		},
		type: "GET",
		async: false
	}).responseJSON;

	console.info(data);
	return data.FOA+data.FNameTH+" "+data.LNameTH;
}
function checkDieselAllowancePerMonthDuplicate(empCode,dieselId){
	$.ajax({
        type: "GET",
        url: session['context']+'/dieselAllowancePerMonths/findDieselAllowancePerMonthByEmpCode?empCode='+empCode,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var dieselObj = JSON.parse(xhr.responseText);
						var idDiesel =dieselObj.id;
                        if(dieselId !=  idDiesel) {
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_EMPLOYEE);
                			$('#warningModal').modal('show');
                        }else{
                        	saveDieselAllowancePerMonth();
                        }
                	}else{
                		saveDieselAllowancePerMonth();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function checkAllDiesel() {
	$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkbox1').each(function() {
				this.checked = true;
			});
		}else{
			$('.checkbox1').each(function() {
				this.checked = false;
			});
		}
	});

	$("#checkAll").prop("checked", false);
	$('.checkbox1').click(function() {
		if($('.checkbox1:checked').length == $('.checkbox1').length){
			$("#checkAll").prop("checked", true);
		}else{
			$("#checkAll").prop("checked", false);
		}
	});
}

function btnDelete(){
	var arrayCheckStatusCheckbox = [] ;
	for (var i = 0; i < $('.checkbox1').length; i++) {
		arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
	}
	var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
	if(statusCheckboxTrue >= 0) {
		$('#deleteItemModal').modal('show');

	}else{
		$('#deleteCheckModal').modal('show');

	}
}

function getIdForDel(){
	var lengthOfResponseText;
	// var deleteSuccess = 0;
	// var deleteFail = 0;

	if($('.checkbox1').length != 0) {
		for (var i = 0; i < $('.checkbox1').length; i++) {
			if ($('.checkbox1')[i].checked == true) {
				var id = $('.checkbox1')[i].value;
				lengthOfResponseText = deleteDieselAllowancePerMonth(id);
			}
		}

		searchDieselAllowancePerMonth();
	}
}


$(document).ready(function () {
	


	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });

	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchDieselAllowancePerMonth();
		checkAllDiesel();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_empNameInputEmployeeAll').val('');
		$('#modal_edit_literPerMonth').val('');
		$('#modal_edit_empCode').val('');
		$('#modal_edit_flag_active').prop('checked', true);

	});
	$("#modal_edit_empNameInputEmployeeAll").on('keyup',function(){
		AutocompleteEmployeeAll.search($("#modal_edit_empNameInputEmployeeAll").val());
	});

	$("#modal_edit_empNameInputEmployeeAll").on('blur',function(){
		$("#modal_edit_empCode").text($("#modal_edit_empNameInputEmployeeAll").attr("data-empCode"));
	});
	$('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});

	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";


		if(!$('#modal_edit_empNameInputEmployeeAll').val()){
			errorMessage += $MESSAGE_REQUIRE_EMPLOYEE+"<br/>";
		}
		if(!$('#modal_edit_literPerMonth').val()){
			errorMessage += $MESSAGE_REQUIRE_LITER_PER_MONTH+"<br/>";
		}
		checkAllDiesel();


		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			var empCode;
			//Check Mandatory Field
			if( ($("#modal_edit_empNameInputEmployeeAll").attr("data-empCode")==""||$("#modal_edit_empNameInputEmployeeAll").attr("data-empCode")==undefined)&&$("#modal_edit_empNameInputEmployeeAll").val()!=""){
				empCode=$("#modal_edit_empCode").val();

			}
			else {empCode=$("#modal_edit_empNameInputEmployeeAll").attr("data-empCode");}
			checkDieselAllowancePerMonthDuplicate(empCode,$('#modal_edit_id').val());
		}

	});
	searchDieselAllowancePerMonth();
	checkAllDiesel();
});


function dieselAllowancePerMonthHistory(code){
			location.href =  session['context']+'/dieselAllowancePerMonthHistories/listView?empCode='+code;

}