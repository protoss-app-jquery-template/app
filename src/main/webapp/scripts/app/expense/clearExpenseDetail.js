
var listIdTime = [];
var listIdDate = [];
var $DATA_EMPLOYEE;
var $DATA_REQUEST;
var $DATA_LINE_APPROVE;
var $DATA_REQUEST_APPROVER;
var $DATA_EMPLOYEE_FOR_LINE_APPROVE;
var dieselLiter = 0;
var petrolLiter = 0;
var monthlyPhoneBill;
var checkStatus=0;
var listConditionalIO = [];
var amountExpenseItem;
var userHead;
var accommodationAuthorize = true;
var checkAccommodationAuthorizeForCollapse = false;
var checkStatusReject = 0;
var startAmountNet = 0;
var startIdAttachmentFile = 0;
var checkRequireFileLength = 0;
var startAmountTocalVat=0;
var checkResendFlow = false;
var checkOverDiesel = false;
var checkOverPetrol = false;
var checkOverPhone = false;
var checkOverPhoneBackend = false;
var monthlyPhoneBillPerMonth;

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-50) + 'px';
    }
}

function validateDocStatus(){
    /* update by siriradC.  2017.08.04 */
    if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }
}

function initClearExpenseDetail() {
    var dataDocumentReference = $DATA_DOCUMENT.documentReference==null?"":$DATA_DOCUMENT.documentReference;

    if(dataDocumentReference != ""){
        $.each(dataDocumentReference,function (index,item) {
            if(item.documentTypeCode == "APP"){
                $("#docRefAppBtn").attr('data-attn',item.docReferenceNumber);
                AutocompleteDocRefAppForExpense.renderValue(item.docReferenceNumber);
            }
            if(item.documentTypeCode == "ADV"){
                $("#docRefAdvBtn").attr('data-attn',item.docReferenceNumber);
                AutocompleteDocRefAdvForExpense.renderValue(item.docReferenceNumber);
            }
        });
    }


    $("#docNumberLabel").text($DATA_DOCUMENT.docNumber);
    if($DATA_DOCUMENT.sendDate){
        $("#docDateLabel").text($DATA_DOCUMENT.sendDate);
    }else{
        $("#docDateLabel").text("SEND TIME");
    }
    if($DATA_DOCUMENT.verifyStatus){
        $("#verifyName").text($DATA_DOCUMENT.verifyName+" ตรวจสอบ");
        if($DATA_DOCUMENT.verifyStatus == "Y"){
            $("#imgVerifyCancel").addClass('hide');
            $("#imgVerifyCheck").removeClass('hide');
        }else{
            $("#imgVerifyCheck").addClass('hide');
            $("#imgVerifyCancel").removeClass('hide');
        }
        $("#verifyName").removeClass('hide');
    }

    var costCenter = $("#costCenterLabel").text();
    var charIndex4 = costCenter.charAt(3);

    var dataDocumentExpenseGroup = $DATA_DOCUMENT.documentExpense==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup;
    $("#accordion1").empty();
    if("" != dataDocumentExpenseGroup){
        $.each(dataDocumentExpenseGroup,function (index,item) {
            var gl = charIndex4 == "9" || costCenter == '1017920010'?item.expenseTypeByCompany.expenseTypes.headOfficeGL:item.expenseTypeByCompany.expenseTypes.factoryGL;
            $("#accordion1").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseGray" id="collapseHeaderDetail" >'+
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">'+
                '<div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">'+
                '<a onclick="openModalDeleteExpenseGroup('+item.id+')"><img src="'+IMG.DELETE+'" width="35px"/></a>&#160;&#160;'+//<img src="'+$IMAGE_EXPENSE+'" width="40px"/>
                '</div>'+
                '<div class="col-xs-10 col-sm-11" style="padding-right: 0;padding-left: 0" id="expenseGroup'+index+'" role="button" data-toggle="collapse" href="#collapseExpense'+item.id+'" aria-expanded="false" aria-controls="collapseExpense'+item.id+'">'+
                '<a class="collapsed" style="color:#ffffff;font-size: 13px" >'+item.expenseTypeByCompany.expenseTypes.description+'</a>'+'<sup><img width="20px;" height="20px" src="'+IMG.CLICK+'" /></sup>'+'<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="summaryAmount" id-expense-com="'+item.expenseTypeByCompany.id+'" expenseTypeCode="'+item.expenseTypeByCompany.expenseTypes.code+'" id="summaryAmount'+item.expenseTypeByCompany.expenseTypes.code+'" flowType="'+item.expenseTypeByCompany.expenseTypes.flowType+'"><jsp:text/></span><br/>'+
                '<a class="collapsed" style="color:#ffffff;font-size: 13px" >'+gl+'</a>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div id="collapseExpense'+item.id+'" class="panel-collapse collapse in" role="tabpanel">'+
                '<div class="panel-body panel-white-perl">'+
                '<div class="row">'+
                '<div class="form-horizontal">'+
                '<div class="form-group">'+
                '<div class="container-fluid">'+
                '<a title="ADD" onclick="renderModalExpense(\''+item.expenseTypeByCompany.expenseTypes.id+'\',\''+"add"+'\',\''+""+'\',\''+item.expenseTypeByCompany.id+'\',\''+gl+'\')" ><img src="'+$IMAGE_PLUS+'" width="35"><jsp:text/></img></a>'+
                '</div>'+
                '</div>'+
                '<div class="form-group" id="body'+item.expenseTypeByCompany.expenseTypes.code+'" idForItem="subExpenseItem'+item+'" name="bodyDocExpenseItem">'+
                '<jsp:text/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            )

            $("#summaryAmount"+item.expenseTypeByCompany.expenseTypes.code).text(0);
            $("#summaryAmount"+item.expenseTypeByCompany.expenseTypes.code).autoNumeric('init')
        });

        renderDocumentExpenseItem();
    }

}

function cancelExpense() {
    var warningMessage = MSG_EXPENSE.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelDocument(){
    $('.dv-background').show();
    var jsonData = {}
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']    			= $DATA_DOCUMENT.id;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/expense/updateDocumentStatus',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    $('#confirmModal').modal('hide');
                    backToMenu();
                }
            }
        });
    },1000);

}

/* manage document attachment */
function documentAttachment(){
    getDataAttachmentType();
    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}

function getDataAttachmentType(){
    $("#attachmentType,#attachmentFileGL").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType,#attachmentFileGL").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'//expense/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            var $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    var checkFileBill = false;
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItemAttachment(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button>' +
                '</td>' +
                '</tr>'
            );

            if("M151" == dataDocumentAttachment[i].attachmentType){
                $("#billUploadTxt").val(dataDocumentAttachment[i].fileName);
                $("#previewBill").attr('id-img',dataDocumentAttachment[i].id);
                $("#previewBill").attr('id-name',dataDocumentAttachment[i].fileName);
                checkFileBill = true;
            }
        }

        if(checkFileBill){
            $("#spanPreviewBill").removeClass('hide');
        }else {
            $("#billUploadTxt").val("");
            $("#spanPreviewBill").addClass('hide');
        }
    }else{
        $("#billUploadTxt").val("");
        $("#spanPreviewBill").addClass('hide');
    }
}

function downloadDocumentFile(btn){
    var id = btn.attr('id');
    var fileName = btn.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}

function saveDocumentAttachment() {
    var jsonData = {}
    jsonData['parentId'] = 1;
    jsonData[csrfParameter] = csrfToken;
    jsonData['attachmentType'] = $("#attachmentType").val();
    jsonData['document'] = $DATA_DOCUMENT.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename", $fileUpload.name);
    formData.append("attachmentType", $("#attachmentType").val());
    formData.append("document", $DATA_DOCUMENT.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/expense/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    $("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    $("#textFileName").val("");
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                    renderDocumentAttachment(dataDocumentAttachment);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function saveDocumentAttachmentInCollapse() {
    var jsonData = {}
    jsonData['parentId'] = 1;
    jsonData[csrfParameter] = csrfToken;
    jsonData['attachmentType'] = 'M151';
    jsonData['document'] = $DATA_DOCUMENT.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename", $fileUpload.name);
    formData.append("attachmentType", 'M151');
    formData.append("document", $DATA_DOCUMENT.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/expense/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function deleteDocumentAttachment(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/expense/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

function validateFileExtensions(){
    var extensionFile = $('#attachmentType option:selected').attr('extension');

    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG_EXPENSE.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

/* manage copy document */
function copyDocument(){
    var jsonDocument = {};
    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']   			= $DATA_DOCUMENT.id;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/copyDocument',
        data:jsonDocument,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                window.location.href = session.context+'/expense/clearExpenseDetail?doc='+xhr.responseJSON.id;
            }
        }
    });
}

function hideInternalOrder(check) {
    if(check){
        $("#rowInternalOrder").addClass('hide');
        $("#internalOrder").removeClass('requiredValue');
        $("#internalOrder").removeClass('use');
    }else{
        $("#rowInternalOrder").removeClass('hide');
        $("#internalOrder").addClass('requiredValue');
        $("#internalOrder").addClass('use');
    }
}

function renderModalExpense(id,type,idExpenseItem,idExpenseCompany,gl) {
    $('.dv-background').show();

    $("#collapseExternalMember").removeClass('in');
    $("#collapseInternalMember").removeClass('in');
    $("#collapseExpenseTypeFile").addClass('in');
    listIdTime = [];
    listIdDate = [];

    $("#documentDateModalAdd").val("");
    $("#docNumberModalAdd").val("");
    $("#sellerModalAdd").val("");
    $("#taxIDNumberModalAdd").val("");
    $("#branchModalAdd").val("");
    $("#addressModalAdd").val("");
    $("#amountModalAdd").val("");
    $("#vatModalAdd").val("");
    $("#taxModalAdd").val("");
    $("#netAmountModalAdd").val("");
    $("#internalOrder").val("");
    $("#remark").val("");
    $("#branchPurchaserModalAdd").val("");
    $("#vatType").val("");
    $("#whtType").val("");
    $("#whtCode").val("");

    $("#dynamicField").empty();
    var data = $.ajax({
        url: session.context + "//expenseType/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false

    }).responseJSON;
    console.log("--------------------------")
    checkRequireFileLength = data.expenseTypeFile.length
    console.log(data);
    $("#modalAddDetail").attr('typeMethod',type);
    $("#modalAddDetail").attr('idExpenseItem',idExpenseItem);
    if(data != undefined){

        $("[name=fixCodeExpenseType]").addClass('hide');
        $("#rowInternalOrderAmount").addClass('hide');
        $(".fix_value").removeClass('use');
        $(".requiredValue,.notRequiredValue").val("");
        $("#headExpense").text(data.description);
        $("#headExpense").attr('id-expense',id);
        $("#headExpense").attr('id-expense-company',idExpenseCompany);
        $("#headExpense").attr('gl',gl);
        var dynamicField = '';
        var dataExpenseScreen = data.expenseTypeScreen;
        dataExpenseScreen.sort(compareExpenseScreen);

        /** check io **/
        var checkRequriedIo = false;
        if(listConditionalIO){
            if(listConditionalIO[0].gl){
                $.each(listConditionalIO,function (index,item) {
                    if(item.gl.length == 1 && item.gl == '*'){
                        checkRequriedIo = true;
                        return;
                    }else{
                        var checkGl = item.gl.split("*");
                        if(($('#headExpense').attr('gl')).startsWith(checkGl[0]) && gl != '624104' && gl != '624105'){
                            checkRequriedIo = true;
                            return;

                        }
                    }
                })
            }
        }

        /** fix screen **/
        if(data.fixCode){
            $("div."+data.fixCode).removeClass('hide');
            $("."+data.fixCode+"_VALUE").addClass('use');
            $("."+data.fixCode+"_VALUE_OTHER").addClass('use');
            hideInternalOrder(false);

            if(checkRequriedIo){
                $("#spanInternalOrder").text("*");
                $("#internalOrder").addClass("requiredValue");
            }else{
                $("#spanInternalOrder").text("");
                $("#internalOrder").removeClass("requiredValue");
            }

            if(data.fixCode == "EXPF_001"){
                findDieselAllowancePerMonth();
            }
            if(data.fixCode == "EXPF_002"){
                findPetrolAllowancePerMonth();
            }
            if(data.fixCode == "EXPF_012" || data.fixCode == "EXPF_013" || data.fixCode == "EXPF_014"){
                renderCarLicence();
            }
            if(data.fixCode == "EXPF_016"){
                renderSubvention();
            }
            if(data.fixCode == "EXPF_004"){
                hideInternalOrder(true);
            }
            if(data.fixCode == "EXPF_015"){
                findMonthlyPhoneBill();
            }
        }else{
            hideInternalOrder(false);

            if(checkRequriedIo){
                $("#spanInternalOrder").text("*");
                $("#internalOrder").addClass("requiredValue");
            }else{
                $("#spanInternalOrder").text("");
                $("#internalOrder").removeClass("requiredValue");
            }

            /************** render dynamic field *****************/
            $.each(dataExpenseScreen, function (index, item) {
                if ("String".toUpperCase() == item.type.toUpperCase()) {
                    if (item.require) {
                        if (item.structureField.indexOf("time") !== -1) {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border requiredValue use" description="' + item.nameENG + '" name="' + item.structureField + '" type="text" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            listIdTime.push(item.structureField);
                        } else {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border requiredValue use" description="' + item.nameENG + '" name="' + item.structureField + '" type="text" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        }
                    } else {
                        if (item.structureField.indexOf("time") !== -1) {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border notRequiredValue use" type="text" description="' + item.nameENG + '" name="' + item.structureField + '" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            listIdTime.push(item.structureField);
                        } else {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border notRequiredValue use" type="text" description="' + item.nameENG + '" name="' + item.structureField + '" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        }

                    }

                } else if ("Date".toUpperCase() == item.type.toUpperCase()) {
                    listIdDate.push(item.structureField);
                    if (item.require) {
                        dynamicField +=
                            '<div class="row">' +
                            '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-md-8" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border requiredValue use" description="' + item.nameENG + '" name="' + item.structureField + '" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                            '<span class="input-group-addon"><a onclick="focusDate(\'' + item.structureField + '\')"><i class="fa fa-calendar fa-2x" style="color: #00bfff"><jsp:text/></i></a></span></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    } else {
                        dynamicField +=
                            '<div class="row">' +
                            '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-md-8" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border notRequiredValue use" description="' + item.nameENG + '" name="' + item.structureField + '" id="' + item.structureField + '" type="text" style="color: #03a9f4"/>' +
                            '<span class="input-group-addon"><a onclick="focusDate(\'' + item.structureField + '\')"><i class="fa fa-calendar fa-2x" style="color: #00bfff"><jsp:text/></i></a></span></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    }
                }
            });

        }


        $("#dynamicField").append(dynamicField)

        /********** render expense type file ************/
        $("#expenseTypeFileRequired").empty();
        var dataExpenseTypeFile = data.expenseTypeFile;
        var dynamicFieldExpenseTypeFile = '';
        var checkRequriedFromFile = false;
        $.each(dataExpenseTypeFile,function (index,item) {
            if(item.attachmentTypeCode == "M151"){
                checkRequriedFromFile = true
            }
            $.each(MASTER_DATA.ATTACHMENT_TYPE,function (index2,item2) {
                startIdAttachmentFile =1
                if(item.attachmentTypeCode == item2.code){
                    startIdAttachmentFile++
                    if(item.require){
                        dynamicFieldExpenseTypeFile +=
                            '<div class="col-sm-12">'+
                            '<div class="col-sm-2" style="padding: 0px; width: 65px; text-align: center">'+
                            '<jsp:text/>'+
                            '</div>'+
                            '<div class="form-group has-info" >'+
                            '<div class="col-sm-4" style="padding: 0px;">'+
                            '<label class="col-sm-12" style="padding-top:5px;text-align: left">'+item2.description+'&#160;&#160;<span style="color:red">*</span></label>'+
                            '</div>'+
                            '<div class="col-md-6" style="margin-bottom: 8px" >'+
                            '<div class="input-group">'+
                            '<input class="form-control border requiredFile" placeholder="'+$MESSAGE_PLACEHOLDER_BROWSE_FILE+'" description="'+item2.description+'" type="text" id="dynamicFileTxt'+(index)+'" index="'+index+'" disabled="true"/>'+
                            '<input class="form-control hide" placeholder="'+$MESSAGE_PLACEHOLDER_BROWSE_FILE+'" type="file" id="dynamicFile'+(index)+'" name="uploadFileExpenseItem" uploadType="'+item.attachmentTypeCode+'"/>'+
                            '<span class="input-group-btn input-group-sm">'+
                            '<a ><img src="'+$IMAGE_PAPER_CLIP+'" width="30px" id="dynamicFileBtn'+(index)+'" index="'+index+'" title="UPLOAD FILE" onclick="uploadFileExpenseItemDetail(this)"/></a>' +
                            '</span>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>';
                    }else{
                        dynamicFieldExpenseTypeFile +=
                            '<div class="col-sm-12">'+
                            '<div class="col-sm-2" style="padding: 0px; width: 65px; text-align: center">'+
                            '<jsp:text/>'+
                            '</div>'+
                            '<div class="form-group has-info" >'+
                            '<div class="col-sm-4" style="padding: 0px;">'+
                            '<label class="col-sm-12" style="padding-top:5px;text-align: left">'+item2.description+'</label>'+
                            '</div>'+
                            '<div class="col-md-6" style="margin-bottom: 8px" >'+
                            '<div class="input-group">'+
                            '<input class="form-control border" placeholder="'+$MESSAGE_PLACEHOLDER_BROWSE_FILE+'" description="'+item2.description+'" type="text" id="dynamicFileTxt'+(index)+'" index="'+index+'" disabled="true"/>'+
                            '<input class="form-control hide" placeholder="'+$MESSAGE_PLACEHOLDER_BROWSE_FILE+'" type="file" id="dynamicFile'+(index)+'" name="uploadFileExpenseItem" uploadType="'+item.attachmentTypeCode+'"/>'+
                            '<span class="input-group-btn input-group-sm">'+
                            '<a ><img src="'+$IMAGE_PAPER_CLIP+'" width="30px" id="dynamicFileBtn'+(index)+'" index="'+index+'" title="UPLOAD FILE" onclick="uploadFileExpenseItemDetail(this)"/></a>' +
                            '</span>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>';
                    }

                }
            })
        });
        if(checkRequriedFromFile){
            if (session.roleName.indexOf("ROLE_ACCOUNT") != -1) {
                $("#docNumberModalAdd").addClass("requiredValue");
            }else{
                $("#docNumberModalAdd").addClass("requiredValue");
            }
        }else{
            $("#docNumberModalAdd").removeClass("requiredValue");
        }

        $("#expenseTypeFileRequired").append(dynamicFieldExpenseTypeFile);

        if("edit" == type){
            renderDataForEditDocumentExpenseItem(data.fixCode,idExpenseItem);
        }
    }

    initTimePicker(listIdTime);
    initDatePicker(listIdDate);

    if(data && data.expenseTypeReference.length == 0){
        if(data.fixCode == "EXPF_009" && type == "add"){
            saveBeforeOpenModalForEXPF009(idExpenseCompany,gl,function () {
                $("#externalMemberBody").empty();
                $("#internalMemberBody").empty();
                findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa);
                $("#modalAddDetail").modal('show');
            });
        }else{
            findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa);
            $("#modalAddDetail").modal('show');
        }
    } else if(data.expenseTypeReference.length != 0){
        if($("#docRefAppInputDocRefAppForExpense").val() == ""){
            $('#warningModal .modal-body').html("Please fill document reference approve");
            $('#warningModal').modal('show');
        }else{
            var dataApprove = $.ajax({
                url: session.context + "/expense/findByDocNumber?docNumber="+$("#docRefAppInputDocRefAppForExpense").val(),
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            var checkDocumentReference = false;

            if(data.expenseTypeReference.length >1){
                $.each(data.expenseTypeReference,function (index,item) {
                    if(dataApprove.approveType == item.approveTypeCode){
                        checkDocumentReference = true;
                        return false;
                    }
                })

                if(checkDocumentReference){
                    if(data.fixCode == "EXPF_009" && type == "add"){
                        saveBeforeOpenModalForEXPF009(idExpenseCompany,gl,function () {
                            $("#externalMemberBody").empty();
                            $("#internalMemberBody").empty();
                            findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa)
                            $("#modalAddDetail").modal('show');
                        });
                    }else{
                        findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa)
                        $("#modalAddDetail").modal('show');
                    }
                }else{
                    $('#warningModal .modal-body').html("Document reference approve does not match ");
                    $('#warningModal').modal('show');
                }
            }else{
                if(dataApprove.approveType == data.expenseTypeReference[0].approveTypeCode){
                    if(data.fixCode == "EXPF_009" && type == "add"){
                        saveBeforeOpenModalForEXPF009(idExpenseCompany,gl,function () {
                            $("#externalMemberBody").empty();
                            $("#internalMemberBody").empty();
                            findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa)
                            $("#modalAddDetail").modal('show');
                        });
                    }else{
                        findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa)
                        $("#modalAddDetail").modal('show');
                    }
                }else{
                    $('#warningModal .modal-body').html("Document reference approve does not match ");
                    $('#warningModal').modal('show');
                }
            }

        }
    }
    startAmountTocalVat = parseFloat($('#netAmountModalAdd').autoNumeric('get'))+parseFloat($("#taxModalAdd").autoNumeric('get'));
    $('.dv-background').hide();


    if($("#vatType option:selected").val() != "" && $("#vatType option:selected").val() != "VX"){

    }else{
        $("#vatModalAdd").attr('disabled',true)
        $("#vatModalAdd").val('')
        $('#amountModalAdd').autoNumeric("set",startAmountTocalVat)
        calculateWHT();

    }

}

function saveBeforeOpenModalForEXPF009(expTypeByCompany,gl,callback) {
    $('.dv-background').show();
    var jsonDocumentItemDetail = {};
    var jsonDocumentExpenseItem = {};

    jsonDocumentItemDetail['parentId']     	= 1;
    jsonDocumentItemDetail[csrfParameter]  	= csrfToken;

    jsonDocumentExpenseItem['parentId']     	= 1;
    jsonDocumentExpenseItem[csrfParameter]  	= csrfToken;
    jsonDocumentExpenseItem['docExpItemDetail'] = JSON.stringify(jsonDocumentItemDetail);
    jsonDocumentExpenseItem['expTypeByCompany'] = expTypeByCompany;
    jsonDocumentExpenseItem['glCode'] = gl;
    jsonDocumentExpenseItem['document'] = $DATA_DOCUMENT.id;
    jsonDocumentExpenseItem['remark'] = "";
    jsonDocumentExpenseItem['amount'] = 0;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/expense/saveDocumentExpenseItemDetail',
            data: jsonDocumentExpenseItem,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                var id = xhr.responseJSON.id;
                $("#modalAddDetail").attr('typeMethod',"edit");
                $("#modalAddDetail").attr('idExpenseItem',id);
                renderDocumentExpenseItem();
                callback();
            }
        });
    },1000)

}

function addInternalMember() {
    $('#nameMemberInputEmployeeAll').val("");
    $('#amountInternalMember').val("");
    $("#modalInternalMmber").modal('show');
}

function saveInternalMember() {

    $('.dv-background').show();
    var jsonParams = {};
    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['lvEmp']     		     = $('#nameMemberInputEmployeeAll').attr('data-empLv');
    jsonParams['id']     		     = null;
    jsonParams['docNumber'] 	     = $DATA_DOCUMENT.docNumber;
    jsonParams['itemId'] 	     = $('#modalAddDetail').attr('idexpenseitem');
    jsonParams['empUser'] 		     = $('#nameMemberInputEmployeeAll').attr('data-username');
    jsonParams['cost'] 	     = $('#amountInternalMember').autoNumeric('get');

    setTimeout(function () {
        if($('#nameMemberInputEmployeeAll').val() != "" && $('#amountInternalMember').autoNumeric('get') != ""){
            $.ajax({
                async: false,
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/internalMembers',
                data: jsonParams,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    findInternalMember('add');
                    checkAccommodationAuthorize();
                    $("#modalbodySuccess").text('Save Success');
                    $("#successModal").modal('show');
                    setTimeout(function () {
                        $("#successModal").modal('hide');
                        $("#modalInternalMmber").modal('hide')
                    },1000)
                }
            });
        }else{
            $('.dv-background').hide();
            var messageRequired ="";

            if($('#nameMemberInputEmployeeAll').val() == ""){
                messageRequired += "&#8195;&#8195;&#8195;"+LB_EXPENSE.LABEL_EMPLOYEE_NAME+"<br/>";
            }

            if($('#amountInternalMember').autoNumeric('get') == ""){
                messageRequired += "&#8195;&#8195;&#8195;"+LB_EXPENSE.LABEL_MONEY_AMOUNT_BATH+"<br/>";
            }

            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +"<br/>"+messageRequired);
            $('#warningModal').modal('show');
        }

    },1000)
}

function findInternalMember(type) {
    $("#internalMemberBody").empty();
    var dataInternalMember = $.ajax({
        url:session['context']+'/internalMembers/findByDocNumberAndItemId?docNumber='+$DATA_DOCUMENT.docNumber+"&itemId="+$('#modalAddDetail').attr('idexpenseitem'),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataInternalMember.content[0].id){
        var append = "";
        $.each(dataInternalMember.content,function (index,item) {
            append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                '<div class="col-xs-2 col-sm-2 text-center" style="padding-right: 0;padding-left: 0">' +
                '<a onclick="deleteInternalMember(' + item.id + ')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                '</div>' +
                '<div class="col-xs-10 col-sm-10" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold">' +
                '' + findInternalMemberProfileByUserName(item.empUser) + '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountInternalAndExternal">' + NumberUtil.formatCurrency(item.cost) + '</span>' +''+
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        })

        $("#internalMemberBody").append(append);
    }

    if(type != 'edit'){
        if($('[name=amountInternalAndExternal]').length > 0){
            $('[name=amountInternalAndExternal]').autoNumeric('init');
            var sum = 0;
            $.each($('[name=amountInternalAndExternal]'),function (index,item) {
                sum += parseFloat($(item).autoNumeric('get'))
            });

            $("#amountModalAdd").autoNumeric('set',sum);
            $("#netAmountModalAdd").autoNumeric('set',sum);
        }else{
            $("#amountModalAdd").val("");
            $("#netAmountModalAdd").val("");
        }
    }
}

function findInternalMemberProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data){
        return data.FOA+data.FNameTH+" "+data.LNameTH;
    }else{
        return "";
    }
}

function deleteInternalMember(id) {
    $('.dv-background').show();

    setTimeout(function () {
        $.ajax({
            type: "DELETE",
            url: session['context']+'/internalMembers/'+id,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                findInternalMember('delete');
                checkAccommodationAuthorize();
                $('#deleteSuccess').modal('show');
                setTimeout(function () {
                    $('#deleteSuccess').modal('hide');
                },1000);
            }
        });
    },1000)
}

function addExternalMember() {
    $('#externalMemberName').val("");
    $('#amountExternalMember').val("");
    $("#modalExternalMmber").modal('show');
}

function saveExternalMember() {

    $('.dv-background').show();
    var jsonParams = {};
    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['id']     		     = null;
    jsonParams['docNumber'] 	     = $DATA_DOCUMENT.docNumber;
    jsonParams['itemId'] 	     = $('#modalAddDetail').attr('idexpenseitem');
    jsonParams['name'] 		     = $('#externalMemberName').val();
    jsonParams['cost'] 	     = $('#amountExternalMember').autoNumeric('get');

    setTimeout(function () {
        if($('#externalMemberName').val() != "" && $('#amountExternalMember').autoNumeric('get') != ""){
            $.ajax({
                async: false,
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/externalMembers',
                data: jsonParams,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    findExternalMember("add");
                    checkAccommodationAuthorize();
                    $("#modalbodySuccess").text('Save Success');
                    $("#successModal").modal('show');
                    setTimeout(function () {
                        $("#successModal").modal('hide');
                        $("#modalExternalMmber").modal('hide')
                    },1000)
                }
            });
        }else{
            $('.dv-background').hide();
            var messageRequired ="";

            if($('#externalMemberName').val() == ""){
                messageRequired += "&#8195;&#8195;&#8195;"+LB_EXPENSE.LABEL_NAME+"<br/>";
            }

            if($('#amountExternalMember').autoNumeric('get') == ""){
                messageRequired += "&#8195;&#8195;&#8195;"+LB_EXPENSE.LABEL_MONEY_AMOUNT_BATH+"<br/>";
            }

            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +"<br/>"+messageRequired);
            $('#warningModal').modal('show');
        }

    },1000)
}

function findExternalMember(type) {
    $("#externalMemberBody").empty();
    var dataInternalMember = $.ajax({
        url:session['context']+'/externalMembers/findByDocNumberAndItemId?docNumber='+$DATA_DOCUMENT.docNumber+"&itemId="+$('#modalAddDetail').attr('idexpenseitem'),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataInternalMember.content[0].id){
        var append = "";
        $.each(dataInternalMember.content,function (index,item) {
            append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                '<div class="col-xs-2 col-sm-2 text-center" style="padding-right: 0;padding-left: 0">' +
                '<a onclick="deleteExternalMember(' + item.id + ')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                '</div>' +
                '<div class="col-xs-10 col-sm-10" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold">' +
                '' + item.name + '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountInternalAndExternal">' + NumberUtil.formatCurrency(item.cost) + '</span>' +''+
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        });

        $("#externalMemberBody").append(append);
    }

    if(type != 'edit'){
        if($('[name=amountInternalAndExternal]').length > 0){
            $('[name=amountInternalAndExternal]').autoNumeric('init');
            var sum = 0;
            $.each($('[name=amountInternalAndExternal]'),function (index,item) {
                sum += parseFloat($(item).autoNumeric('get'))
            });

            $("#amountModalAdd").autoNumeric('set',sum);
            $("#netAmountModalAdd").autoNumeric('set',sum);
        }else{
            $("#amountModalAdd").val("");
            $("#netAmountModalAdd").val("");
        }
    }

}

function deleteExternalMember(id) {
    $('.dv-background').show();

    setTimeout(function () {
        $.ajax({
            type: "DELETE",
            url: session['context']+'/externalMembers/'+id,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                findExternalMember('delete');
                checkAccommodationAuthorize();
                $('#deleteSuccess').modal('show');
                setTimeout(function () {
                    $('#deleteSuccess').modal('hide');
                },1000);
            }
        });
    },1000)
}

function uploadFileExpenseItemDetail(element) {
    var index = $("#"+element.id).attr('index');
    $("#dynamicFile"+index).click();

    $("#dynamicFile"+index).change(function () {
        if($("#dynamicFile"+index)[0].files.length != 0){
            $("#dynamicFileTxt"+index).val($("#dynamicFile"+index)[0].files[0].name)
        }
    })
}

function uploadFileExpenseItemDetailAndUpDate(element) {
    var index = $("#"+element.id).attr('index');
    $("#dynamicFile"+index).click();

    $("#dynamicFile"+index).change(function () {
        if($("#dynamicFile"+index)[0].files.length != 0){
            $("#dynamicFileTxt"+index).val($("#dynamicFile"+index)[0].files[0].name)
        }
    })
}

function compareExpenseScreen(a,b) {
    if (a.id < b.id)
        return -1;
    if (a.id > b.id)
        return 1;
    return 0;
}

function initTimePicker(listId) {
    $.each(listId,function (index,item) {
        $('#'+item).flatpickr({
            enableTime: true,
            noCalendar: true,
            locale:"en",
            enableSeconds: false,
            time_24hr: true,
            dateFormat: "H:i"
        });
    });
}

function focusDate(id) {
    $("#"+id).focus();
}

function initDatePicker(listId) {
    $.each(listId,function (index,item) {
        $('#'+item).flatpickr({
            dateFormat: "d/m/Y",
            locale:"en"
        });
    });
}

function openModalDeleteExpenseGroup(id) {
    $("#idItemExpenseTypeGroupDelete").val(id);
    $("#deleteExpenseTypeGroupModal").modal('show');
}

function deleteDocumentExpenseGroup(id) {
    $('.dv-background').show();

    setTimeout(function () {
        $.ajax({
            type: "DELETE",
            url: session['context']+'/documentExpenseGroups/deleteDocumentExpenseGroup/'+id,
            async: false,
            complete: function (xhr) {
                $.ajax({
                    url:session['context']+'/expense/'+$DATA_DOCUMENT.id,
                    headers: {
                        Accept : "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        $DATA_DOCUMENT = xhr.responseJSON;
                        initClearExpenseDetail();
                        $("#deleteExpenseTypeGroupModal").modal('hide');
                        setTimeout(function () {
                            $('.dv-background').hide();
                        },1000);
                    }
                });
            }
        });
    },1000)
}

function saveDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem) {
    var formData = new FormData();
    var listCheckFile = [];

    var file = $("[name=uploadFileExpenseItem]");

    if(file.length != 0){
        $.each(file,function (index,item) {
            if(item.files[0]){
                listCheckFile.push(item.files[0].name);
                formData.append("file", item.files[0]);
                formData.append("filename", item.files[0].name);
                formData.append("attachmentCode", $("#"+item.id).attr('uploadType'));
            }
        });
    }

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/saveDocumentExpenseItemDetail',
        data: jsonDocumentExpenseItem,
        async: false,
        complete: function (xhr) {
            var id = xhr.responseJSON.id;

            var formDataFile_Add;

            var file = $("[name=uploadFileExpenseItem]");
            if (listCheckFile.length != 0) {

                $.each(file,function (index,item) {
                    if(item.files[0]){

                        formDataFile_Add = new FormData();
                        formDataFile_Add.append("file", item.files[0]);
                        formDataFile_Add.append("filename", item.files[0].name);
                        formDataFile_Add.append("attachmentCode", $("#"+item.id).attr('uploadType'));
                        formDataFile_Add.append("documentExpenseItem", id);

                        $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            url: session['context'] + '/expense/saveDocumentExpenseItemAttachment',
                            processData: false,
                            contentType: false,
                            data: formDataFile_Add,
                            async: false,
                            complete: function (xhr) {
                                formDataFile_Add = null
                                if (xhr.readyState == 4) {
                                    if (xhr.status == 200) {
                                        $('.dv-background').hide();
                                        $("#modalbodySuccess").text('Save Success');
                                        $("#successModal").modal('show');
                                        renderDocumentExpenseItem();
                                        if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                                            // doc from emed
                                        }else{
                                            getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                                        }
                                        setTimeout(function () {
                                            $("#successModal").modal('hide');
                                            $("#modalAddDetail").modal('hide')
                                        }, 2000)
                                    }
                                }
                            }
                        });
                    }
                })

            } else {
                $("#modalbodySuccess").text('Save Success');
                $("#successModal").modal('show');
                renderDocumentExpenseItem();
                if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                    // doc from emed
                }else{
                    getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                }
                $('.dv-background').hide();
                setTimeout(function () {
                    $("#successModal").modal('hide');
                    $("#modalAddDetail").modal('hide');
                }, 2000)
            }
        }
    });
    $('.dv-background').hide();
}

function updateDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem,idExpenseItem) {
    var formData = new FormData();
    var listCheckFile = [];

    var file = $("[name=uploadFileExpenseItem]");

    if(file.length != 0){
        $.each(file,function (index,item) {
            if(item.files[0]){
                listCheckFile.push(item.files[0].name);
                formData.append("file", item.files[0]);
                formData.append("filename", item.files[0].name);
                formData.append("attachmentCode", $("#"+item.id).attr('uploadType'));
            }
        });
    }

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/updateDocumentExpenseItemDetail',
        data: jsonDocumentExpenseItem,
        async: false,
        complete: function (xhr) {
            if (listCheckFile.length != 0) {

                var formDataFile;

                var file = $("[name=uploadFileExpenseItem]");

                if (file.length != 0) {
                    $.each(file, function (index, item) {
                        if (item.files[0]) {
                            formDataFile = new FormData();
                            formDataFile.append("file", item.files[0]);
                            formDataFile.append("filename", item.files[0].name);
                            formDataFile.append("attachmentCode", $("#" + item.id).attr('uploadType'));
                            formDataFile.append("documentExpenseItem", idExpenseItem);

                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                url: session['context'] + '/expense/saveDocumentExpenseItemAttachment',
                                processData: false,
                                contentType: false,
                                data: formDataFile,
                                async: false,
                                complete: function (xhr) {
                                    formDataFile = null;
                                    if (xhr.readyState == 4) {
                                        if (xhr.status == 200) {
                                            $('.dv-background').hide();
                                            $("#modalbodySuccess").text('Update Success');
                                            $("#successModal").modal('show');
                                            renderDocumentExpenseItem();
                                            if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                                                // doc from emed
                                            }else{
                                                getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                                            }
                                            setTimeout(function () {
                                                $("#successModal").modal('hide');
                                                $("#modalAddDetail").modal('hide')
                                            }, 2000)
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            } else {
                $("#modalbodySuccess").text('Update Success');
                $("#successModal").modal('show');
                renderDocumentExpenseItem();
                if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                    // doc from emed
                }else{
                    getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                }
                setTimeout(function () {
                    $("#successModal").modal('hide');
                    $("#modalAddDetail").modal('hide')
                    $('.dv-background').hide();
                }, 2000)
            }


        }
    });
}

function saveDocumentExpenseItemDetail() {
    $('.dv-background').show();
    var jsonDocumentItemDetail = {};
    var jsonDocumentExpenseItem = {};
    var checkEXPF_009 = false;
    var checkNationalID = false;
    checkOverPhoneBackend = false;

    var listRequired = [];
    $.each($(".use"),function (index,item) {
        item=$(item);
        if(item.hasClass('requiredValue')){
            if(item.val()){
                if("internalOrder" == item.attr('name') || "remark" == item.attr('name') || "documentDate" == item.attr('name') || "docNumber" == item.attr('name')
                    || "seller" == item.attr('name') || "taxIDNumber" == item.attr('name') || "branch" == item.attr('name') || "address" == item.attr('name')){

                    if("documentDate" == item.attr('name')){
                        jsonDocumentExpenseItem[item.attr('name')] = item.val()==""?"":new Date(DateUtil.coverStringToDate(item.val())).format('yyyy-mm-dd HH:mm:ss');
                    }else{
                        if("internalOrder" == item.attr('name')){
                            jsonDocumentExpenseItem[item.attr('name')] = item.val().toUpperCase();
                        }else {
                            jsonDocumentExpenseItem[item.attr('name')] = item.val();
                        }

                    }
                }else if("date1" == item.attr('name') || "date2" == item.attr('name')){
                    jsonDocumentItemDetail[item.attr('name')] = item.val()==""?"":new Date(DateUtil.coverStringToDate(item.val())).format('yyyy-mm-dd HH:mm:ss');
                }else{
                    jsonDocumentItemDetail[item.attr('name')] = item.val();
                }
            }else{
                listRequired.push(item.attr('description'));
            }
        }else{
            if("internalOrder" == item.attr('name') || "remark" == item.attr('name') || "documentDate" == item.attr('name') || "docNumber" == item.attr('name')
                || "seller" == item.attr('name') || "taxIDNumber" == item.attr('name') || "branch" == item.attr('name') || "address" == item.attr('name')){

                if("documentDate" == item.attr('name')){
                    jsonDocumentExpenseItem[item.attr('name')] = item.val()==""?"":new Date(DateUtil.coverStringToDate(item.val())).format('yyyy-mm-dd HH:mm:ss');
                }else{
                    jsonDocumentExpenseItem[item.attr('name')] = item.val();
                }
            }else if("date1" == item.attr('name') || "date2" == item.attr('name')){
                jsonDocumentItemDetail[item.attr('name')] = item.val()==""?"":new Date(DateUtil.coverStringToDate(item.val())).format('yyyy-mm-dd HH:mm:ss');
            }else{
                jsonDocumentItemDetail[item.attr('name')] = item.val();
            }
        }
    });

    if($(".requiredFile").length > 0){
        $.each($(".requiredFile"),function (index,item) {
            item=$(item);
            if(!item.val()){
                listRequired.push(item.attr('description'));
            }
        })
    }

    if(!$(".EXPF_009").hasClass('hide')){
        if($("#internalMemberBody").find('.panel').length == 0 && $("#externalMemberBody").find('.panel').length == 0){
            checkEXPF_009 = true;
        }
    }

    if(!$("#rowNationalId").hasClass('hide') && $("#docNumberModalAdd").hasClass("requiredValue")){
        if($("#taxIDNumberModalAdd").val().length < 13){
            checkNationalID = true;
        }
    }
    

    jsonDocumentItemDetail['parentId']     	= 1;
    jsonDocumentItemDetail[csrfParameter]  	= csrfToken;

    jsonDocumentExpenseItem['parentId']     	= 1;
    jsonDocumentExpenseItem[csrfParameter]  	= csrfToken;
    jsonDocumentExpenseItem['amount'] = $("#amountModalAdd").autoNumeric('get')==""?0:parseFloat($("#amountModalAdd").autoNumeric('get'));
    jsonDocumentExpenseItem['vat'] = $("#vatModalAdd").autoNumeric('get')==""?null:parseFloat($("#vatModalAdd").autoNumeric('get'));
    jsonDocumentExpenseItem['vatCode'] = $("#vatType option:selected").val()==""?null:$("#vatType option:selected").val();
    jsonDocumentExpenseItem['vatCodeValue'] = $("#vatType option:selected").attr('vatValue')==""?null:$("#vatType option:selected").attr('vatValue');
    jsonDocumentExpenseItem['netAmount'] = $("#netAmountModalAdd").autoNumeric('get')==""?null:parseFloat($("#netAmountModalAdd").autoNumeric('get'));
    jsonDocumentExpenseItem['whtAmount'] = $("#taxModalAdd").autoNumeric('get')==""?null:parseFloat($("#taxModalAdd").autoNumeric('get'));
    jsonDocumentExpenseItem['whtType'] = $("#whtType option:selected").val()==""?null:$("#whtType option:selected").val();
    jsonDocumentExpenseItem['docExpItemDetail'] = JSON.stringify(jsonDocumentItemDetail);
    jsonDocumentExpenseItem['expTypeByCompany'] = $("#headExpense").attr('id-expense-company');
    jsonDocumentExpenseItem['glCode'] = $("#headExpense").attr('gl');
    jsonDocumentExpenseItem['businessPlace'] = $("#branchPurchaserModalAdd").val()==""?null:$("#branchPurchaserModalAdd").val();
    jsonDocumentExpenseItem['exchangeRate'] = $("#currencyRateEXPF_004").val()?$("#currencyRateEXPF_004").val():1;
    jsonDocumentExpenseItem['whtCode'] =  $("#whtCode option:selected").val()==""?null:$("#whtCode option:selected").val();
    jsonDocumentExpenseItem['document'] = $DATA_DOCUMENT.id;

    var type = $("#modalAddDetail").attr('typeMethod');
    var idExpenseItem = $("#modalAddDetail").attr('idExpenseItem');

    setTimeout(function () {
        if("add" == type){
            if(listRequired.length ==0) {
                if(checkOverDiesel || checkOverPetrol || checkOverPhone || checkEXPF_009 || checkNationalID){
                    $('.dv-background').hide();
                    if(checkOverDiesel){
                        $('#warningModal .modal-body').html("จำนวนลิตรเกินสิทธิ์คงเหลือ");
                        $('#warningModal').modal('show');
                    }
                    if(checkOverPetrol){
                        $('#warningModal .modal-body').html("จำนวนลิตรเกินสิทธิ์คงเหลือ");
                        $('#warningModal').modal('show');
                    }
                    if(checkOverPhone){
                        $('#warningModal .modal-body').html("จำนวนเงินเกิน");
                        $('#warningModal').modal('show');
                    }
                    if(checkEXPF_009){
                        $('#warningModal .modal-body').html("กรุณาระบุผู้เดินทาง");
                        $('#warningModal').modal('show');
                    }
                    if(checkNationalID){
                        $('#warningModal .modal-body').html("กรุณาระบุเลขที่ประจำตัวผู้เสียภาษี");
                        $('#warningModal').modal('show');
                    }
                }else{
                    if(!$(".EXPF_001").hasClass('hide')){
                        saveDieselAllowancePerMonthHistoryForExpense();
                        saveDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem);
                    }else if(!$(".EXPF_002").hasClass('hide')){
                        savePetrolAllowancePerMonthHistoryForExpense();
                        saveDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem);
                    }else if(!$(".EXPF_015").hasClass('hide')){
                        saveMonthlyPhoneBillHistoryForExpense(function () {
                            if(checkOverPhoneBackend){
                                $('.dv-background').hide();
                                $('#warningModal .modal-body').html("เกินจำนวนเงินคงเหลือของเดือนที่เลือก");
                                $('#warningModal').modal('show');
                            }else{
                                saveDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem);
                            }
                        });
                    }else{
                        saveDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem);
                    }
                }
                
            }else{
                $('.dv-background').hide();
                var messageRequired ="";

                $.each(listRequired,function (index,item) {
                    messageRequired += "&#8195;&#8195;&#8195;"+item+"<br/>";
                });

                $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +"<br/>"+messageRequired);
                $('#warningModal').modal('show');
            }
        }else{
            jsonDocumentExpenseItem['id'] = idExpenseItem;
            if(listRequired.length ==0) {
                if(checkOverDiesel || checkOverPetrol || checkOverPhone || checkEXPF_009 || checkNationalID){
                    $('.dv-background').hide();
                    if(checkOverDiesel){
                        $('#warningModal .modal-body').html("จำนวนลิตรเกินสิทธิ์คงเหลือ");
                        $('#warningModal').modal('show');
                    }
                    if(checkOverPetrol){
                        $('#warningModal .modal-body').html("จำนวนลิตรเกินสิทธิ์คงเหลือ");
                        $('#warningModal').modal('show');
                    }
                    if(checkOverPhone){
                        $('#warningModal .modal-body').html("จำนวนเงินเกินสิทธิ์");
                        $('#warningModal').modal('show');
                    }
                    if(checkEXPF_009){
                        $('#warningModal .modal-body').html("กรุณาระบุผู้เดินทาง");
                        $('#warningModal').modal('show');
                    }
                     if(checkNationalID){
                        $('#warningModal .modal-body').html("กรุณาระบุเลขที่ประจำตัวผู้เสียภาษี");
                        $('#warningModal').modal('show');
                    }
                }else {
                    if(!$(".EXPF_002").hasClass('hide')){
                        updatePetrolAllowancePerMonthHistoryForExpense(idExpenseItem);
                        updateDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem,idExpenseItem);
                    }else if(!$(".EXPF_001").hasClass('hide')){
                        updateDieselAllowancePerMonthHistoryForExpense(idExpenseItem);
                        updateDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem,idExpenseItem);
                    }else if(!$(".EXPF_015").hasClass('hide')){
                        var month = $("#month_EXPF_015").val();
                        var year = $("#year_EXPF_015").val();
                        updateMonthlyPhoneBillHistoryForExpense(idExpenseItem,month,year);
                        updateDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem,idExpenseItem);
                    }else{
                        updateDocumentExpenseItemDetailFnc(jsonDocumentExpenseItem,idExpenseItem);
                    }

                }
            }else{
                $('.dv-background').hide();
                var messageRequired ="";
                $.each(listRequired,function (index,item) {
                    messageRequired += "&#8195;&#8195;&#8195;"+item+"<br/>";
                });

                $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +"<br/>"+messageRequired);
                $('#warningModal').modal('show');
            }
        }
        $("#docRefAdvInputDocRefAdvForExpense").blur();
    },1000);
}

function compare(a,b) {
    if(a){
        if (a.id < b.id)
            return -1;
        if (a.id > b.id)
            return 1;
        return 0;
    }
}

function renderDocumentExpenseItem() {

    var data = $.ajax({
        url: session.context + "/expenseItem/findDocumentExpenseItemByDocumentExp?id="+$DATA_DOCUMENT.documentExpense.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false

    }).responseJSON;

    console.log("********--------------*********");
    console.log(data);

    var costCenter = $("#costCenterLabel").text();
    var charIndex4 = costCenter.charAt(3);

    var dataExpense = data.content;
    $('div[name=bodyDocExpenseItem]').empty();
    var sumAmount = 0;
    var append = '';
    var id;
    dataExpense.sort(compare);

    var checkAccommodation ;
    $.each(dataExpense,function (index,item) {
        console.log("============ check acoommodation render item ==============");
        console.log(item.accommodaionStatus);
        if(item.accommodaionStatus){
            if(checkAccommodation){
                if(checkAccommodation == false){

                }else{
                    if(item.accommodaionStatus == false){
                        checkAccommodation = item.accommodaionStatus;
                    }else{
                        checkAccommodation = true;
                    }
                }

            }else{
                if(checkAccommodation == false){

                }else{
                    if(item.accommodaionStatus == false){
                        checkAccommodation = item.accommodaionStatus;
                    }else{
                        checkAccommodation = true;
                    }
                }

            }

        }else{
            if(checkAccommodation == false){

            }else{
                if(item.accommodaionStatus == false){
                    checkAccommodation = item.accommodaionStatus;
                }else{
                    checkAccommodation = true;
                }
            }
        }
        console.log("checkAccomodation   :   "+checkAccommodation);
        var year = '';
        var month = '';
        if(item.docExpItemDetail){
            year = item.docExpItemDetail.variable19?item.docExpItemDetail.variable19:"";
            month = item.docExpItemDetail.month?item.docExpItemDetail.month:"";
        }

        if(item.expenseTypeByCompanys) {
            var gl = charIndex4 == "9" || costCenter == '1017920010'?item.expenseTypeByCompanys.expenseTypes.headOfficeGL:item.expenseTypeByCompanys.expenseTypes.factoryGL;
            var externalNumber = item.externalDocNumber?item.externalDocNumber:"";
            var externalPaymentNumber = item.externalPaymentDocNumber?item.externalPaymentDocNumber:"";
            var externalClearingNumber = item.externalClearingDocNumber?item.externalClearingDocNumber:"";
            var textFI = "";
            if(externalNumber){
                if(textFI){
                    textFI = textFI+" , "+externalNumber;
                }else{
                    textFI = externalNumber;
                }
            }
            if(externalPaymentNumber){
                if(textFI){
                    textFI = textFI+" , "+externalPaymentNumber;
                }else{
                    textFI = externalPaymentNumber;
                }
            }
            if(externalClearingNumber){
                if(textFI){
                    textFI = textFI+" , "+externalClearingNumber;
                }else{
                    textFI = externalClearingNumber;
                }
            }

            var fullTextFi = textFI?"("+textFI+")":"";

            if (id == undefined || id == item.expenseTypeByCompanys.expenseTypes.code) {
                var netAmount = item.netAmount;
                var whtAmount = 0;
                if(item.whtAmount){
                    whtAmount = item.whtAmount;
                }
                sumAmount += (netAmount+whtAmount);
                id = item.expenseTypeByCompanys.expenseTypes.code;
                append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" name="collapseDocumentExpenseItem">' +
                    '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                    '<div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '<a onclick="deleteDocumentExpenseItem(\'' + item.id + '\',\'' + id + '\',\'' + month + '\',\''+year+'\')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                    '</div>' +
                    '<div class="col-xs-10 col-sm-11" style="padding-right: 0;padding-left: 0;color: black;font-size: 14px" onclick="renderModalExpense(\'' + item.expenseTypeByCompanys.expenseTypes.id + '\',\'' + "edit" + '\',\'' + item.id + '\',\''+""+'\',\''+gl+'\')">' +
                    '' + item.remark +" "+fullTextFi+ '<sup><img width="20px;" height="20px" src="'+IMG.CLICK+'" /></sup>'+'<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountExpenseItem">' + (netAmount+whtAmount) + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                if (index == dataExpense.length - 1) {
                    $("#body" + id).append(append);
                    $("#summaryAmount" + id).text(sumAmount);
                    $("#summaryAmount" + id).autoNumeric('init');
                    $("[name=amountExpenseItem]").autoNumeric('init');
                    $("#summaryAmount" + id).autoNumeric('set',sumAmount);
                }
            } else {
                $("#body" + id).append(append);
                $("#summaryAmount" + id).text(sumAmount);
                $("#summaryAmount" + id).autoNumeric('init');
                $("[name=amountExpenseItem]").autoNumeric('init');
                $("#summaryAmount" + id).autoNumeric('set',sumAmount);

                var netAmount = item.netAmount;
                var whtAmount = 0;
                sumAmount = 0;
                if(item.whtAmount){
                    whtAmount = item.whtAmount;
                }
                sumAmount += (netAmount+whtAmount);
                id = item.expenseTypeByCompanys.expenseTypes.code;
                append = '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" name="collapseDocumentExpenseItem">' +
                    '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                    '<div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '<a onclick="deleteDocumentExpenseItem(\'' + item.id + '\',\'' + id + '\',\'' + month + '\',\''+year+'\')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                    '</div>' +
                    '<div class="col-xs-10 col-sm-11" style="padding-right: 0;padding-left: 0;color: black" onclick="renderModalExpense(\'' + item.expenseTypeByCompanys.expenseTypes.id + '\',\'' + "edit" + '\',\'' + item.id + '\',\''+""+'\',\''+gl+'\')">' +
                    '' + item.remark +" "+fullTextFi+'<sup><img width="20px;" height="20px" src="'+IMG.CLICK+'" /></sup>'+'<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountExpenseItem">' + (netAmount+whtAmount) + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                if (index == dataExpense.length - 1) {
                    $("#body" + id).append(append);
                    $("#summaryAmount" + id).text(sumAmount);
                    $("#summaryAmount" + id).autoNumeric('init');
                    $("[name=amountExpenseItem]").autoNumeric('init');
                    $("#summaryAmount" + id).autoNumeric('set',sumAmount);
                }
            }
        }else{
            $.each($('[name=summaryAmount]'),function (index,item) {
                $("#"+item.id).autoNumeric('init');
                $("#"+item.id).autoNumeric('set',0);
            })

        }

    });
    accommodationAuthorize = checkAccommodation;
    sumAllAmountExpense();

    // }
}

function deleteDocumentExpenseItem(id,expenseTypeCode,month,year) {
    $('.dv-background').show();
    if(expenseTypeCode == 'E00003'){
        deletePetrolAllowancePerMonthHistoryForExpense(id,function () {
            $.ajax({
                type: "DELETE",
                url: session['context']+'/expenseItem/'+id,
                async: false,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    $('[name=summaryAmount]').autoNumeric('set',0);
                    renderDocumentExpenseItem();
                    $('#deleteSuccess').modal('show');
                    setTimeout(function () {
                        $('#deleteSuccess').modal('hide');

                    },1000)

                    $('#deleteExpenseType').modal('hide');
                }
            });
        })
    }else if(expenseTypeCode == 'E00002'){
        deleteDieselAllowancePerMonthHistoryForExpense(id,function () {
            $.ajax({
                type: "DELETE",
                url: session['context']+'/expenseItem/'+id,
                async: false,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    $('[name=summaryAmount]').autoNumeric('set',0);
                    renderDocumentExpenseItem();
                    $('#deleteSuccess').modal('show');
                    setTimeout(function () {
                        $('#deleteSuccess').modal('hide');

                    },1000)

                    $('#deleteExpenseType').modal('hide');
                }
            });
        })
    }else if(expenseTypeCode == 'E00016'){
        deleteMonthlyPhoneBillHistoryForExpense(id,month,year,function () {
            $.ajax({
                type: "DELETE",
                url: session['context']+'/expenseItem/'+id,
                async: false,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    $('[name=summaryAmount]').autoNumeric('set',0);
                    renderDocumentExpenseItem();
                    $('#deleteSuccess').modal('show');
                    setTimeout(function () {
                        $('#deleteSuccess').modal('hide');

                    },1000)

                    $('#deleteExpenseType').modal('hide');
                }
            });
        })
    }else{
        setTimeout(function () {
            $.ajax({
                type: "DELETE",
                url: session['context']+'/expenseItem/'+id,
                async: false,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    $('[name=summaryAmount]').autoNumeric('set',0);
                    renderDocumentExpenseItem();
                    $('#deleteSuccess').modal('show');
                    setTimeout(function () {
                        $('#deleteSuccess').modal('hide');
                    },1000)

                    $('#deleteExpenseType').modal('hide');
                }
            });
        },1000)
    }


}

function renderDataForEditDocumentExpenseItem(fixCode,id) {
    var data = $.ajax({
        type: "GET",
        url: session['context']+'/expenseItem/'+id,
        headers: {
            Accept : "application/json"
        },
        async: false
    }).responseJSON;

    if(data != undefined){
        amountExpenseItem = data.amount;
        startAmountNet = data.amount

        if(session.roleName.indexOf("ROLE_ACCOUNT") != -1){
            if(!data.netAmount){
                $("#netAmountModalAdd").autoNumeric('set',data.amount);
            }else{
                $("#netAmountModalAdd").autoNumeric('set',data.netAmount);
            }

            if(!data.vatCode){
                $("#vatType").val("VX");
                $("#taxIDNumberModalAdd").val("0000000000000");
                $("#branchModalAdd").val("NVAT");
            }else{
                $("#vatType").val(data.vatCode);
            }


        }else{
            $("#netAmountModalAdd").autoNumeric('set',data.netAmount);
            $("#vatType").val(data.vatCode);
        }

        $("#remark").val(data.remark);
        $("#sellerModalAdd").val(data.seller);
        $("#internalOrder").val(data.internalOrder);
        $("#docNumberModalAdd").val(data.docNumber);
        $("#documentDateModalAdd").val(DateUtil.coverDateToString(data.documentDate));
        $("#amountModalAdd").autoNumeric('set',data.amount);
        if(data.taxIDNumber){
            $("#taxIDNumberModalAdd").val(data.taxIDNumber);
        }
        if(data.branch){
            $("#branchModalAdd").val(data.branch);
        }

        $("#branchPurchaserModalAdd").val(data.businessPlace);

        $("#addressModalAdd").val(data.address);
        $("#vatModalAdd").autoNumeric('set',data.vat);
        $("#taxModalAdd").autoNumeric('set',data.whtAmount);
        $("#whtType").val(data.whtType);
        $("#whtCode").val(data.whtCode);

        var listDocExpItemDetail =  Object.keys(data.docExpItemDetail);

        if(fixCode == "EXPF_003"){
            if(data.docExpItemDetail['place1'] == "อื่นๆ"){
                $("#place1_EXPF_003").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_003_other").removeClass('hide');
                $("#placeOther1_EXPF_003").val(data.docExpItemDetail['placeOther1']);
            }else{
                $("#place1_EXPF_003").val(data.docExpItemDetail['place1']);
            }
        }else if(fixCode == "EXPF_008"){
            if(data.docExpItemDetail['place1'] == "อื่นๆ"){
                $("#place1_EXPF_008").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_008_other").removeClass('hide');
                $("#placeOther1_EXPF_008").val(data.docExpItemDetail['placeOther1']);
            }else{
                $("#place1_EXPF_008").val(data.docExpItemDetail['place1']);
            }
        }else if(fixCode == "EXPF_001"){
            $("#liter_EXPF_001").val(data.docExpItemDetail['liter'])
        }else if(fixCode == "EXPF_002"){
            $("#liter_EXPF_002").val(data.docExpItemDetail['liter'])
        }else if(fixCode == "EXPF_004"){
            if(data.docExpItemDetail['country']){
                AutocompleteLocationForeignForExpense.renderValue(data.docExpItemDetail['country'])
            }
            $("#date1EXPF_004").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_004").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#time1EXPF_004").val(data.docExpItemDetail['time1']);
            $("#time2EXPF_004").val(data.docExpItemDetail['time2']);
            $("#percentRateEXPF_004").val(data.docExpItemDetail['variable1']);
            $("#currencyRateEXPF_004").val(data.exchangeRate);

        }
        else if(fixCode == "EXPF_006") {
            $("#place1_EXPF_006").val(data.docExpItemDetail['place1']);
            $("#date1EXPF_006").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_006").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#time1EXPF_006").val(data.docExpItemDetail['time1']);
            $("#time2EXPF_006").val(data.docExpItemDetail['time2']);
            $("#percentEXPF_006").val(data.docExpItemDetail['variable1']);
        }else if(fixCode == "EXPF_005") {
            if (data.docExpItemDetail['place1'] == "0022") {
                $("#place1_EXPF_005").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_005_other").removeClass('hide');
                $("#placeOther1_EXPF_005").val(data.docExpItemDetail['placeOther1']);
            } else {
                $("#place1_EXPF_005").val(data.docExpItemDetail['place1']);
            }

            if (data.docExpItemDetail['place2'] == "0022") {
                $("#place2_EXPF_005").val(data.docExpItemDetail['place2']);
                $("#place2EXPF_005_other").removeClass('hide');
                $("#placeOther2_EXPF_005").val(data.docExpItemDetail['placeOther2']);
            } else {
                $("#place2_EXPF_005").val(data.docExpItemDetail['place2']);
            }
        }else if(fixCode == "EXPF_007") {
            $("#date1EXPF_007").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));

            if(data.docExpItemDetail['variable4'] != null && data.docExpItemDetail['variable4'] != 0){
                $("#distance_EXPF_007").val(data.docExpItemDetail['variable4']);
                $(".EXPF_007_Distance").removeClass('hide');
            }

            $("#place1_EXPF_007").val(data.docExpItemDetail['place1']);

            if(data.docExpItemDetail['place2'] == "0022") {
                $("#place2_EXPF_007").val(data.docExpItemDetail['place2']);
                $("#place2EXPF_007_other").removeClass('hide');
                $("#placeOther2_EXPF_007").val(data.docExpItemDetail['placeOther2']);
            } else {
                $("#place2_EXPF_007").val(data.docExpItemDetail['place2']);
            }
        }else if(fixCode == "EXPF_010") {
            $("#place1_EXPF_010").val(data.docExpItemDetail['place1']);
            $("#place2_EXPF_010").val(data.docExpItemDetail['place2']);
            $("#month_EXPF_010").val(data.docExpItemDetail['month']);
            $("#year_EXPF_010").val(data.docExpItemDetail['variable19']);
        }else if(fixCode == "EXPF_012") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_012").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_012_other").removeClass('hide');
                $("#carLicenceOther_EXPF_012").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_012").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_013") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_013").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_013_other").removeClass('hide');
                $("#carLicenceOther_EXPF_013").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_013").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_014") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_014").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_014_other").removeClass('hide');
                $("#carLicenceOther_EXPF_014").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_014").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_016") {
            $("#addEmployeeInputEmployeeAll").val(data.docExpItemDetail['employee']);
            if (data.docExpItemDetail['variable5'] == "SV002") {
                $("#variable5_EXPF_016").val(data.docExpItemDetail['variable5']);
                $("#EXPF_016_other").removeClass('hide');
                $("#other_EXPF_016").val(data.docExpItemDetail['variable18']);
            } else {
                $("#variable5_EXPF_016").val(data.docExpItemDetail['variable5']);
            }
        }else if(fixCode == "EXPF_011") {
            $("#liter_EXPF_011").val(data.docExpItemDetail['liter']);
            $("#month_EXPF_011").val(data.docExpItemDetail['month']);
            $("#year_EXPF_011").val(data.docExpItemDetail['variable19']);
        }else if(fixCode == "EXPF_009"){
            if(data.docExpItemDetail['variable6']){
                AutocompleteHotelForExpense.renderValue(data.docExpItemDetail['variable6'])
            }
            $("#date1EXPF_009").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_009").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#externalMemberBody").empty();
            $("#internalMemberBody").empty();
            findInternalMember('edit');
            findExternalMember("edit");
            checkAccommodationAuthorize();
        }else if(fixCode == "EXPF_015"){
            $("#phoneNumber_EXPF_015").val(data.docExpItemDetail['phoneNumber']);
            $("#month_EXPF_015").val(data.docExpItemDetail['month']);
            $("#year_EXPF_015").val(data.docExpItemDetail['variable19']);
        }else{
            $.each(listDocExpItemDetail,function (index,item) {
                if(item == "date1" || item == "date2"){
                    $("#"+item).val(DateUtil.coverDateToString(data.docExpItemDetail[item]));
                }else{
                    $("#"+item).val(data.docExpItemDetail[item]);
                }

            });
        }
        var deleteItem = 'deleteItem'
        /** render file document expense item **/
        if(data.docExpItemAttachment.length != 0){
            $("#expenseTypeFileRequired").empty();
            startIdAttachmentFile=0
            $.each(data.docExpItemAttachment,function (index,item) {
                $.each(MASTER_DATA.ATTACHMENT_TYPE,function (index2,item2) {
                    if(item.attachmentCode == item2.code) {

                        var flagEmed = item.flagEmed?item.flagEmed:"";
                        var docEmed = item.docEmed?item.docEmed:"";
                        var sequence = item.sequence?item.sequence:"";

                        startIdAttachmentFile++
                        $("#expenseTypeFileRequired").append(

                            '<div id="'+deleteItem+index+'" class="col-sm-12">' +
                            '<div class="col-sm-1" style="padding: 0px; width: 30px;">' +
                            '<a ><img src="' + IMG.DELETE + '" width="30px" id="deleteDocumentExpenseItemAttachment'+index+'" idDocumentExpenseItemAttachment="'+item.id+'" index="' + index + '" onclick="deleteDocumentExpenseItemAttachment(this,\''+index+'\',\''+deleteItem+index+'\')"/></a>' +

                            '</div>' +
                            '<div class="col-sm-1" style="padding: 0px; width: 35px;">' +
                            '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+index+'" idDocumentExpenseItemAttachmentDownload="'+item.id+'" index="' + index + '" onclick="preViewAttachmentFileExpenseItem(\''+item.id+'\',\''+item.fileName+'\',\''+flagEmed+'\',\''+docEmed+'\',\''+sequence+'\')"/></a>' +

                            '</div>' +
                            '<div class="col-sm-1" style="padding: 0px; width: 35px;">' +
                            '<a ><img style="margin-left: 5px;" src="' + $IMAGE_DOWNLOAD + '" width="30px" id="downloadDocumentExpenseItemAttachment'+index+'" idDocumentExpenseItemAttachmentDownload="'+item.id+'" index="' + index + '" onclick="downloadAttachmentFileExpenseItem(\''+item.id+'\',\''+item.fileName+'\',\''+flagEmed+'\',\''+docEmed+'\',\''+sequence+'\')"/></a>' +

                            '</div>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-sm-3" style="padding: 0px;">' +
                            '<label class="col-sm-12" style="padding-top:5px;text-align: left">' + item2.description + '</label>' +
                            '</div>' +
                            '<div class="col-md-6" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border" type="text" id="dynamicFileTxt' + (index) + '" index="' + index + '" value="'+item.fileName+'" disabled="true">'+
                            '<input class="form-control hide" type="file" id="dynamicFile' + (index) + '" name="uploadFileExpenseItem" uploadType="' + item.attachmentTypeCode + '"/>' +
                            '<span class="input-group-btn input-group-sm">' +
                            '<a ><img src="' + $IMAGE_PAPER_CLIP + '" width="30px" id="dynamicFileBtn' + (index) + '" index="' + index + '" onclick="uploadFileExpenseItemDetail(this)"/></a>' +
                            '</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    }
                })
            })
        }
        startIdAttachmentFile--
    }
}

function deleteDocumentExpenseItemAttachment(btn,index,detail) {

    var checkRequire = index+1
    if(checkRequire > checkRequireFileLength){
       var idDelete = $("#"+btn.id).attr('idDocumentExpenseItemAttachment')

     deleteItemFile(detail,idDelete)

    }else{
        $('#downloadDocumentExpenseItemAttachment'+index).hide()
        var index = $("#"+btn.id).attr('index');
        var id = $("#"+btn.id).attr('idDocumentExpenseItemAttachment');
        var jsonParams2 = {};
        jsonParams2['parentId']     = 1;
        jsonParams2[csrfParameter]  = csrfToken;

        $.ajax({
            type: "DELETE",
            url: session['context']+'/expense/deleteDocumentExpenseItemAttachment/'+id,
            data: jsonParams2,
            complete: function (xhr) {
                $("#dynamicFileTxt"+index).val("");
                $("#"+btn.id).addClass('hide')
            }
        });
    }

}

function findRequestByDocument(id){
    var dataRequest = $.ajax({
        url: session.context + "/approve/findRequestByDocument/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST = dataRequest;

    var dataRequestApprover = $.ajax({
        url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST_APPROVER = dataRequestApprover;
    renderLineApproveACC($DATA_REQUEST_APPROVER)
}

var sumAll = 0;
function sumAllAmountExpense() {
    sumAll = 0
    $("#sumAllExpenseAmount").empty();
    $.each($('[name=summaryAmount]'),function (index,item) {
        console.log(item.id)
        var sum = $("#"+item.id)==undefined?0:parseFloat($("#"+item.id).autoNumeric('get'));
        sumAll += sum;
    })
    console.log("sumAll   : "+sumAll);
    $("#sumAllExpenseAmount").autoNumeric('set',sumAll);
    checkOutstanding();

    if($DATA_DOCUMENT.payinAmount){
        checkOutstandingFromPayInAmount();
    }

    if($DATA_DOCUMENT.requests){
        findRequestByDocument($DATA_DOCUMENT.id);
    }else{
        getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
    }

}

function findEmployeeProfileByUserName(userName,callback){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;
    $("#empNameLabel").text(data.FOA+data.FNameTH+" "+data.LNameTH);
    $("#empCodeLabel").text(data.Personal_ID);
    $("#positionLabel").text(data.PositionTH);
    $("#costCenterLabel").text(data.Position_Cost_Center);
    // $("#comapnyModalAddExpenseItem").text(data.Personal_PA_ID+" "+data.Personal_PA_Name);
    $("#costCenterLabel").text($DATA_DOCUMENT.costCenterCode);

    if($DATA_DOCUMENT.companyCode){
        findByPaCode($DATA_DOCUMENT.companyCode);
    }

    if($DATA_DOCUMENT.psa){
        findByPsaCode($DATA_DOCUMENT.psa);
    }

    setTimeout(function (){
        initClearExpenseDetail();
        $('.dv-background').hide();
        callback();
    },1000);

}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#comAgencyLabel").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#comapnyModalAddExpenseItem").text(paData.PaCode+" "+paData.PaNameTh);
        $("#comAgencyLabel").attr('pa',paData.PaCode);
    }else{
        $("#comAgencyLabel").text("-");
        $("#comAgencyLabel").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#departmentLabel").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#departmentLabel").attr('psa',psaData.PsaCode);
    }else{
        $("#departmentLabel").text("-");
        $("#departmentLabel").attr('psa',"");
    }
}

function findCreatorProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#creator").text(data.FOA+data.FNameTH+" "+data.LNameTH);
}

function getAuthorizeForLineApprove(userName){

    var listJsonDetails = [];

    var jsonData = {};
    jsonData['requester'] = userName;
    jsonData['papsa'] = $("#comAgencyLabel").attr('pa')+"-"+$("#departmentLabel").attr('psa');
    if(accommodationAuthorize){
        jsonData['approveType'] = $DATA_DOCUMENT.approveType;
    }else{
        jsonData['approveType'] = "Y";
    }

    $.each($("[name=summaryAmount]"),function (index,item) {
        var flowType = $("#"+item.id).attr("flowtype");
        var jsonDetails = {};
        jsonDetails['amount'] = $("#"+item.id).autoNumeric('get')==0?1000:$("#"+item.id).autoNumeric('get');
        jsonDetails['flowType'] = flowType;
        jsonDetails['costCenter']  = $DATA_DOCUMENT.costCenterCode;
        listJsonDetails.push(jsonDetails);
    });

    jsonData['details'] = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/getAuthorizeForLineApprove',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if(xhr.responseText){
                    $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                    checkLineApproveForResendFlow();
                    if(checkResendFlow){
                        renderLineApprove($DATA_LINE_APPROVE);
                    }
                }
            }
        }
    });
}

function renderLineApproveACC(requestApprover){
    checkStatus= 0;
    $("#lineApproverDetail").empty();
    // $("#lineApproveMobile").empty();

    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;

    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle-complete" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB_EXPENSE.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+LB_EXPENSE.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        findEmployeeProfileByUserNameLineApprove(requestApprover[i].userNameApprover);
        if(requestApprover[i].actionStateName != "Paid"){
            $("#lineApproverDetail").append(
                '<li class='+validateStatusACC(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
                '<span class="step"><a><span>'+ validateStatusReject(requestApprover[i].actionState,requestApprover[i].actionReasonDetail) +'</span></span>'+
                '<span class="name-idle">'+
                '<div>'+requestApprover[i].actionStateName+'</div>'+
                '<div style="color: blue;">'+nvl(requestApprover[i].approver)+'</div>'+
                '<div style="color: lightseagreen;">'+nvl($DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH)+'</div>'+
                '</span>'+
                '</li>'
            );

            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading '+validateStatusMobileACC(requestApprover[i].requestStatusCode)+'" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionStateName+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].approver+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+$DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH+'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );
        }else{
            $("#lineApproverDetail").append(
                '<li class='+validateStatusACC(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
                '<span class="step"><a><span>'+ validateStatusReject(requestApprover[i].actionState,requestApprover[i].actionReasonDetail) +'</span></span>'+
                '<span class="name-idle">'+
                '<div>'+requestApprover[i].actionStateName+'</div>'+
                '<div style="color: blue;">'+nvl(requestApprover[i].approver)+'</div>'+
                '<div style="color: lightseagreen;">'+nvl("")+'</div>'+
                '</span>'+
                '</li>'
            );

            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading '+validateStatusMobileACC(requestApprover[i].requestStatusCode)+'" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionStateName+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl(requestApprover[i].approver)+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl("")+'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );
        }



    }

}

function validateStatusMobileACC(status) {
    if(status != null){
        return "collapseLightGreen";
    }else{
        return "collapseLightBlue";
    }
}

function validateStatusACC(status){
    if(status != null){
        return "idle-complete";
    }else if(status != null && status == "REJ"){
        checkStatusReject++;
        return "idle";
    }else{
        checkStatus++;
        return "idle";
    }
}

function validateStatusReject(actionState,detail){
    if(checkStatus === 1 && checkStatusReject === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" class="animation-border two"   src="'+validateIMGACC(actionState)+'" style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }else if(checkStatusReject === 1 && checkStatus === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" class="animation-border three"   src="'+validateIMGACC(actionState)+'" style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }else{
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" src="'+validateIMGACC(actionState)+'" style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }
}

function validateIMGACC(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}
}

function findEmployeeProfileByUserNameLineApprove(userName){

    if(userName){
        var data = $.ajax({
            url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_EMPLOYEE_FOR_LINE_APPROVE = data;
    }

}

function renderLineApprove(requestApprover){
    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;
    $("#lineApproverDetail").empty();
    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB_EXPENSE.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+LB_EXPENSE.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
            '<span class="step"><span><img title="'+nvl(requestApprover[i].actionReasonDetail)+'"  src="'+validateIMG(requestApprover[i].actionRoleCode)+'" style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionRoleName+'</div>'+
            '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
            '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src='+validateIMG(requestApprover[i].actionRoleCode)+' width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionRoleName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].name+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].position+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    }

}

function validateIMG(role){

    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}


}

function verifyCancel() {
    $('.dv-background').show();

    var jsonDocument = {};
    var jsonDocumentReference = {};

    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']    =  $DATA_DOCUMENT.id;
    jsonDocument['advanceAmount']    =  $("#advanceAmount").autoNumeric('get');
    jsonDocument['payinAmount']    =  $("#payInAmount").autoNumeric('get')==""?null:$("#payInAmount").autoNumeric('get');
    jsonDocument['payinDate']    =  $("#payInDate").val()?new Date(DateUtil.coverStringToDate($("#payInDate").val())).format('yyyy-mm-dd HH:mm:ss'):"";
    if(userHead){
        jsonDocument['verifyStatus']    =  "N";
        jsonDocument['verifyRemark']    =  $("#actionReason option:selected").val();
        jsonDocument['verifyDescription']    =  $("#actionReasonDetail").val();
        jsonDocument['verifyName']    =  findInternalMemberProfileByUserName($USERNAME);
    }

    jsonDocumentReference['document'] 			= $DATA_DOCUMENT.id;
    var documentReference="";

    if($("#docRefAppInputDocRefAppForExpense").val()!= "" && $("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
        documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP"+","+$("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
    }else{
        if($("#docRefAppInputDocRefAppForExpense").val()!= ""){
            documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP";
        }
        if($("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
            documentReference = $("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
        }
    }

    jsonDocumentReference['documentReference'] 	= documentReference;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/expense/updateDocumentReferenceList',
            data:jsonDocumentReference,
            async: false,
            complete: function (xhr2) {
                if (xhr2.readyState == 4) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/expense/updateDocumentStatus',
                        data:jsonDocument,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                if(xhr.responseJSON){
                                    $("#verifyName").text(xhr.responseJSON.verifyName+" ตรวจสอบ");
                                    if(xhr.responseJSON.verifyStatus == "Y"){
                                        $("#imgVerifyCheck").removeClass('hide');
                                        $("#imgVerifyCancel").addClass('hide');
                                    }else{
                                        $("#imgVerifyCancel").removeClass('hide');
                                        $("#imgVerifyCheck").addClass('hide');
                                    }
                                    $("#verifyName").removeClass('hide');
                                }
                                $('.dv-background').hide();
                                $("#modalbodySuccess").text('Update Success');
                                $("#successModal").modal('show');
                                setTimeout(function () {
                                    $("#successModal").modal('hide');
                                },2000)
                            }
                        }
                    });
                }
            }
        });
    },1000);
}

function updateExpense() {
    $('.dv-background').show();

    var jsonDocument = {};
    var jsonDocumentReference = {};

    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']    =  $DATA_DOCUMENT.id;
    jsonDocument['advanceAmount']    =  $("#advanceAmount").autoNumeric('get');
    jsonDocument['payinAmount']    =  $("#payInAmount").autoNumeric('get')==""?null:$("#payInAmount").autoNumeric('get');
    jsonDocument['payinDate']    =  $("#payInDate").val()?new Date(DateUtil.coverStringToDate($("#payInDate").val())).format('yyyy-mm-dd HH:mm:ss'):"";
    if(userHead){
        jsonDocument['verifyStatus']    =  "Y";
        jsonDocument['verifyRemark']    =  $("#actionReason option:selected").val();
        jsonDocument['verifyDescription']    =  $("#actionReasonDetail").val();
        jsonDocument['verifyName']    =  findInternalMemberProfileByUserName($USERNAME);
    }
    jsonDocumentReference['document'] 			= $DATA_DOCUMENT.id;
    var documentReference="";

    if($("#docRefAppInputDocRefAppForExpense").val()!= "" && $("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
        documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP"+","+$("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
    }else{
        if($("#docRefAppInputDocRefAppForExpense").val()!= ""){
            documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP";
        }
        if($("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
            documentReference = $("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
        }
    }

    jsonDocumentReference['documentReference'] 	= documentReference;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/expense/updateDocumentReferenceList',
            data:jsonDocumentReference,
            async: false,
            complete: function (xhr2) {
                if (xhr2.readyState == 4) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/expense/updateDocumentStatus',
                        data:jsonDocument,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                if(xhr.responseJSON){
                                    $("#verifyName").text(xhr.responseJSON.verifyName+" ตรวจสอบ");
                                    if(xhr.responseJSON.verifyStatus == "Y"){
                                        $("#imgVerifyCheck").removeClass('hide');
                                        $("#imgVerifyCancel").addClass('hide');
                                        $("#verifyName").removeClass('hide');
                                    }
                                }
                                $('.dv-background').hide();
                                $("#modalbodySuccess").text('Update Success');
                                $("#successModal").modal('show');
                                setTimeout(function () {
                                    $("#successModal").modal('hide');
                                },2000)
                            }
                        }
                    });
                }
            }
        });
    },1000);

}

function sendRequest() {
    $('.dv-background').show();
    var listJsonDetails = [];

    $.each($('[name=summaryAmount]'),function (index,item) {
        var jsonDetails = {};
        jsonDetails['amount'] = $("#"+ item.id).autoNumeric('get');
        jsonDetails['flowType'] = $("#"+ item.id).attr('flowType');
        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
        listJsonDetails.push(jsonDetails);
    });

    var jsonData = {};
    jsonData['id'] = $DATA_DOCUMENT.id;
    jsonData['requester'] = $DATA_DOCUMENT.requester;
    if(accommodationAuthorize){
        jsonData['approveType'] = $DATA_DOCUMENT.approveType;
    }else{
        jsonData['approveType'] = "Y";
    }
    jsonData['company'] = 1;
    jsonData['documentType'] = $DATA_DOCUMENT.documentType;
    jsonData['tmpDocNumber'] = $DATA_DOCUMENT.tmpDocNumber;
    jsonData['details'] = JSON.stringify(listJsonDetails);
    jsonData['pa'] = $("#comAgencyLabel").attr('pa');
    jsonData['psa'] = $("#departmentLabel").attr('psa');

    var jsonDocument = {};
    var jsonDocumentReference = {};

    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']    =  $DATA_DOCUMENT.id;
    jsonDocument['advanceAmount']    =  $("#advanceAmount").autoNumeric('get');
    jsonDocument['payinAmount']    =  $("#payInAmount").autoNumeric('get')==""?null:$("#payInAmount").autoNumeric('get');
    jsonDocument['payinDate']    =  $("#payInDate").val()?new Date(DateUtil.coverStringToDate($("#payInDate").val())).format('yyyy-mm-dd HH:mm:ss'):"";

    jsonDocumentReference['document'] 			= $DATA_DOCUMENT.id;
    var documentReference="";

    if($("#docRefAppInputDocRefAppForExpense").val()!= "" && $("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
        documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP"+","+$("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
    }else{
        if($("#docRefAppInputDocRefAppForExpense").val()!= ""){
            documentReference = $("#docRefAppInputDocRefAppForExpense").val()+":APP";
        }
        if($("#docRefAdvInputDocRefAdvForExpense").val()!= ""){
            documentReference = $("#docRefAdvInputDocRefAdvForExpense").val()+":ADV";
        }
    }

    jsonDocumentReference['documentReference'] 	= documentReference;

    setTimeout(function () {
        if($("[name=collapseDocumentExpenseItem]").length >0){
            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/expense/updateDocumentReferenceList',
                data: jsonDocumentReference,
                async: false,
                complete: function (xhr1) {
                    if (xhr1.readyState == 4) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context'] + '/expense/updateDocumentStatus',
                            data: jsonDocument,
                            async: false,
                            complete: function (xhr2) {
                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/requests/createRequest',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr3) {
                                        if (xhr3.readyState == 4) {
                                            $('.dv-background').hide();
                                            window.location.href = session.context + "/expense/expenseMainMenu";
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }else{
            $('.dv-background').hide();
            $('#warningModal .modal-body').html("Please fill expense detail");
            $('#warningModal').modal('show');
        }
    },1000);
}

function findLocationTypePD() {
    var locationPD = $.ajax({
        url:session['context']+'/locations/findByLocationType?locationType=PD',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#place1_EXPF_008").empty();
    $("#place1_EXPF_008").append('<option value="">'+""+'</option>>');
    if(locationPD){
        $.each(locationPD,function (index,item) {
            $("#place1_EXPF_008").append('' +
                '<option value="'+item.description+'">'+item.description+'</option>'
            );
        })

    }

}

function findLocationTypePF() {
    var locationPD = $.ajax({
        url:session['context']+'/locations/findByLocationType?locationType=PF',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#place1_EXPF_003").empty();
    $("#place1_EXPF_003").append('<option value="">'+""+'</option>>');
    if(locationPD){
        $.each(locationPD,function (index,item) {
            $("#place1_EXPF_003").append('' +
                '<option value="'+item.description+'">'+item.description+'</option>'
            );
        })

    }

}

function findLocationTypeD() {
    var locationD = $.ajax({
        url:session['context']+'/locations/findByLocationType?locationType=D',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#place1_EXPF_005,#place2_EXPF_005,#place1_EXPF_007,#place2_EXPF_007,#place1_EXPF_006").empty();
    $("#place1_EXPF_005,#place2_EXPF_005,#place1_EXPF_007,#place2_EXPF_007,#place1_EXPF_006").append('<option value="">'+""+'</option>>');
    if(locationD){
        $.each(locationD,function (index,item) {
            $("#place1_EXPF_005,#place2_EXPF_005,#place1_EXPF_007,#place2_EXPF_007,#place1_EXPF_006").append('' +
                '<option value="'+item.code+'">'+item.description+'</option>'
            );
        })

    }
}

function renderCarLicence() {
    if($MASTER_DATA_CAR_LICENCE){
        $("#carLicence_EXPF_012,#carLicence_EXPF_013,#carLicence_EXPF_014").empty();
        $("#carLicence_EXPF_012,#carLicence_EXPF_013,#carLicence_EXPF_014").append('<option value=""><jsp:text/></option>');
        $.each($MASTER_DATA_CAR_LICENCE,function (index,item) {
            $("#carLicence_EXPF_012,#carLicence_EXPF_013,#carLicence_EXPF_014").append('' +
                '<option value="'+item.code+'">'+item.description+'</option>')
        })
    }
}

function renderSubvention() {
    if($MASTER_DATA_SUBVENTION){
        $("#variable5_EXPF_016").empty();
        $("#variable5_EXPF_016").append('<option value=""><jsp:text/></option>');
        $.each($MASTER_DATA_SUBVENTION,function (index,item) {
            $("#variable5_EXPF_016").append('' +
                '<option value="'+item.code+'">'+item.description+'</option>')
        })
    }
}

function findIO() {
    if("" != $("#internalOrder").val()){
        $('.dv-background').show();
        setTimeout(function () {
            var dataIo = $.ajax({
                url:session['context']+'/internalOrders/findByIoCodeAndCompanyCode?ioCode='+$("#internalOrder").val()+"&companyCode="+$DATA_DOCUMENT.companyCode,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(dataIo.content[0].balanceAmount){
                if($("#internalOrder").hasClass('requiredValue')){
                    $("#rowInternalOrderAmount").removeClass('hide');
                    $("#spanInternalOrderAmount").autoNumeric('set',dataIo.content[0].balanceAmount);
                }else{
                    $("#rowInternalOrderAmount").addClass('hide');
                }
                $('.dv-background').hide();
            }else{
                $('#warningModal .modal-body').html("IO is not defined");
                $('#warningModal').modal('show');
                $("#rowInternalOrderAmount").addClass('hide');
                $('.dv-background').hide();
            }
        },1000);
    }
}

function checkIO() {
    if("" != $("#internalOrder").val() && "" != $("#amountModalAdd").autoNumeric('get')){
        $('.dv-background').show();
        setTimeout(function () {
            var dataIo = $.ajax({
                url:session['context']+'/internalOrders/findByIoCodeAndCompanyCode?ioCode='+$("#internalOrder").val()+"&companyCode="+$DATA_DOCUMENT.companyCode,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(dataIo.content[0].balanceAmount){
                if(dataIo.content[0].balanceAmount < $("#amountModalAdd").autoNumeric('get')){
                    $('#warningModal .modal-body').html("Amount over IO<br/>Current Amount "+dataIo.content[0].balanceAmount);
                    $('#warningModal').modal('show');
                    // $('#taxModalAdd').val('')
                }
                $('.dv-background').hide();
            }else{
                $('#warningModal .modal-body').html("IO is not defined");
                $('#warningModal').modal('show');
                // $('#taxModalAdd').val('')
                $('.dv-background').hide();
            }
        },1000);

    }
}

function findDieselAllowancePerMonth() {
    var dataDiesel = $.ajax({
        url:session['context']+'/dieselAllowancePerMonths/findDieselAllowancePerMonthByEmpUserName?empUserName='+$DATA_DOCUMENT.requester,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataDiesel){
        dieselLiter = dataDiesel.balanceLiterPerMonth;
        $("#dieselPerMonth").text(dataDiesel.balanceLiterPerMonth);
    }
}

function findPetrolAllowancePerMonth() {
    var dataPetrol = $.ajax({
        url:session['context']+'/petrolAllowancePerMonths/findPetrolAllowancePerMonthByEmpUserName?empUserName='+$DATA_DOCUMENT.requester,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataPetrol){
        petrolLiter = dataPetrol.balanceLiterPerMonth;
        $("#petrolPerMonth").text(dataPetrol.balanceLiterPerMonth);
    }
}

function findMonthlyPhoneBill() {

}

function openModalConfirmSendToSap() {
    checkLineApproveForResendFlow();
    if(checkResendFlow){
        $("#modalResendFlow").modal('show');
    }else{
        $("#warningModalSendToSap").modal('show');
    }

}

function SendSap() {
    $('.dv-background').show();
    var jsonUpdateDocument = {};
    jsonUpdateDocument['parentId']     	= 1;
    jsonUpdateDocument[csrfParameter]  	= csrfToken;
    jsonUpdateDocument['id']  	= $DATA_DOCUMENT.id;
    jsonUpdateDocument['postingDate']  	= new Date(DateUtil.coverStringToDate($("#postingDate").val())).format('yyyy-mm-dd HH:mm:ss');
    
    var jsonData = {};
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']  	= $DATA_DOCUMENT.id;

    var jsonApprove = {};
    jsonApprove['userName'] = $USERNAME;
    jsonApprove['actionStatus'] = validateActionState($USERNAME);
    jsonApprove['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonApprove['docType'] = $DATA_DOCUMENT.documentType;
    jsonApprove['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonApprove['processId'] = $DATA_DOCUMENT.processId;
    jsonApprove['documentId'] = $DATA_DOCUMENT.id;
    jsonApprove['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonApprove['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $("#warningModalSendToSap").modal('hide');
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/expense/updateDocumentStatus',
            data:jsonUpdateDocument,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/expense/expenseTransfer',
                        data: jsonData,
                        async: false,
                        complete: function (xhr2) {
                            if (xhr2.readyState == 4) {
                                if(xhr2.responseText == "") {
                                    var url = session['context'] + '/requests/approveRequest';

                                    if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                                        url = session['context'] + '/requests/approveRequestEmed';
                                    }

                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            Accept: 'application/json'
                                        },
                                        url: url,
                                        data: jsonApprove,
                                        async: false,
                                        complete: function (xhr3) {
                                            if (xhr3.readyState == 4) {
                                                $('.dv-background').hide();
                                                window.location.href = session.context;
                                            }
                                        }
                                    });
                                }else{
                                    $('.dv-background').hide();
                                    $('#warningModal .modal-body').html(xhr2.responseText);
                                    $("#warningModal").modal('show');
                                }
                            }
                        }
                    });
                }
            }
        });

    },1000);
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function beforeRejectRequest(checkRole) {

    var actionReasonCode = $("#actionReason")[0].selectedOptions[0].value;
    var actionReasonDetail = $("#actionReasonDetail").val();

    if(actionReasonCode == "" || actionReasonDetail == ""){
        $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_INPUT_ACTION_REASON);
        $("#warningModal").modal('show');
    }else{
        var warningMessage = MSG_EXPENSE.MESSAGE_CONFIRM_REJECT_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

        $('#confirmRejectModal .modal-body').html(warningMessage);
        $("#confirmRejectModal").attr('checkRole',checkRole);
        $('#confirmRejectModal').modal('show');
    }

}

function rejectExpense(checkRole) {
    $('.dv-background').show();
    var jsonData = {};

    if(checkRole == 'verify'){
        if(userHead.length > 0){
            jsonData['userName'] = userHead[0].variable1;
            jsonData['actionStatus'] = validateActionState(userHead[0].variable1);
        }else{
            jsonData['userName'] = $USERNAME;
            jsonData['actionStatus'] = validateActionState($USERNAME);
        }
    }else{
        jsonData['userName'] = $USERNAME;
        jsonData['actionStatus'] = validateActionState($USERNAME);
    }

    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+$DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        } else {
                            var url = "";
                            if($DATA_DOCUMENT.externalDocChanel && $DATA_DOCUMENT.externalDocChanel == "EMED"){
                                url = session['context'] + '/requests/rejectRequestEmed';
                            }else{
                                url = session['context'] + '/requests/rejectRequest'
                            }
                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: url,
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            backToMenu();
                                            // window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },1000);
}

function findUserHead(callback) {
    $.ajax({
        url: session.context + '/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCodeList?masterdata=M020&code='+$USERNAME,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseJSON.content[0].id){
                userHead = xhr.responseJSON.content
            }
            callback();
        }
    });


}

function checkBtn() {
    
    if(userHead) {
        if ($DATA_DOCUMENT.requests) {
            if(userHead[0].variable2 != "Y"){
                $("[name=btnAcc]").addClass('hide');
                $("[name=btnStandard]").addClass('hide');
                $("[name=btnHR]").addClass('hide');
                $("[name=btnVerify2]").addClass('hide');
                $("[name=btnVerify]").removeClass('hide');
                $("#actionReasonDiv").removeClass('hide');
                $("[name=Acc]").removeClass('hide');
            }else{
                $("[name=btnAcc]").addClass('hide');
                $("[name=btnStandard]").addClass('hide');
                $("[name=btnHR]").addClass('hide');
                $("[name=btnVerify]").addClass('hide');
                $("[name=btnVerify2]").removeClass('hide');
                $("#actionReasonDiv").removeClass('hide');
                $("[name=Acc]").removeClass('hide');
            }
        } else {
            $("[name=btnAcc]").addClass('hide');
            $("[name=btnHR]").addClass('hide');
            $("[name=btnStandard]").removeClass('hide');
            $("[name=btnVerify]").addClass('hide');
            $("[name=btnVerify2]").addClass('hide');
            $("[name=Acc]").removeClass('hide');
        }
    }else{
        if (session.roleName.indexOf("ROLE_ACCOUNT") != -1) {
            console.log("ROLE ACCOUNT");
            if ($DATA_DOCUMENT.requests) {
                $("[name=btnAcc]").removeClass('hide');
                $("[name=btnStandard]").addClass('hide');
                $("[name=btnVerify]").addClass('hide');
                $("[name=btnVerify2]").addClass('hide');
                $("#actionReasonDiv").removeClass('hide');
                $("[name=Acc]").removeClass('hide');
                $("[name=btnHR]").addClass('hide');
            } else {
                $("[name=btnAcc]").addClass('hide');
                $("[name=btnStandard]").removeClass('hide');
                $("[name=btnVerify]").addClass('hide');
                $("[name=btnVerify2]").addClass('hide');
                $("[name=Acc]").removeClass('hide');
                $("[name=btnHR]").addClass('hide');
            }
        } else if (session.roleName.indexOf("ROLE_HR") != -1){
            console.log("ROLE HR");
            if ($DATA_DOCUMENT.requests) {
                $("[name=btnAcc]").addClass('hide');
                $("[name=btnStandard]").addClass('hide');
                $("[name=btnVerify]").addClass('hide');
                $("[name=btnVerify2]").addClass('hide');
                $("#actionReasonDiv").removeClass('hide');
                $("[name=btnHR]").removeClass('hide');
            } else {
                $("[name=btnAcc]").addClass('hide');
                $("[name=btnStandard]").removeClass('hide');
                $("[name=btnVerify]").addClass('hide');
                $("[name=btnVerify2]").addClass('hide');
                $("[name=btnHR]").addClass('hide');
            }
        }else{
            console.log("ROLE OTHER");
            $("[name=btnAcc]").addClass('hide');
            $("[name=btnStandard]").removeClass('hide');
            $("[name=btnVerify]").addClass('hide');
            $("[name=btnVerify2]").addClass('hide');
            $("[name=btnHR]").addClass('hide');
        }
    }
}

function findAllVat() {
    $("#vatType").empty();
    var dataVat = $.ajax({
        url:session['context']+'/vats/findAllVat',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataVat){
        $("#vatType").append('<option value="" vatValue="0"><jsp:text/></option>');
        $.each(dataVat.content,function (index,item) {
            $("#vatType").append('' +
                '<option value="'+item.code+'" vatValue="'+item.value+'">'+item.code+'</option>'
            )
        })
    }
}

function findAllWitholdingTax() {
    $("#whtType").empty();
    var dataWht = $.ajax({
        url:session['context']+'/whts/findAllWitholdingTax',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataWht){
        $("#whtType").append('<option value=""><jsp:text/></option>');
        $.each(dataWht.content,function (index,item) {
            $("#whtType").append('' +
                '<option value="'+item.code+'">'+item.description+'</option>'
            )
        })
    }
}

function findAllWitholdingTaxCode() {
    $("#whtCode").empty();
    var dataWhtCode = $.ajax({
        url:session['context']+'/whtCodes/findAllWithholdingTaxCode',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataWhtCode){
        $("#whtCode").append('<option value="" whtValue=""><jsp:text/></option>');
        $.each(dataWhtCode.content,function (index,item) {
            $("#whtCode").append('' +
                '<option value="'+item.code+'" whtValue="'+item.value+'">'+item.description+'</option>'
                // '<option value="'+item.code+'" whtValue=3>'+item.description+'</option>'
            )
        })
    }
}

function addExpense() {
    $('.dv-background').show();
    var jsonDocumentExpenseItem = {};
    var expenseItem = $("#expenseInputExpense").attr("data-expense-company-id");
    jsonDocumentExpenseItem['expenseItem'] = expenseItem;
    jsonDocumentExpenseItem['document'] = $DATA_DOCUMENT.id;
    var dataDocumentExpenseGroup = $DATA_DOCUMENT.documentExpense==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup;
    var checkDuplicate = true;
    if(expenseItem){
        if("" != dataDocumentExpenseGroup){
            $.each(dataDocumentExpenseGroup,function (index,item) {
                if(item.expenseTypeByCompany.id.toString() == expenseItem.toString()){
                    checkDuplicate = false;
                }
            });
            if(checkDuplicate){
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/expense/saveDocumentExpenseItem',
                        data: jsonDocumentExpenseItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                $("#expenseInputExpense").val("");
                                $.ajax({
                                    url:session['context']+'/expense/'+$DATA_DOCUMENT.id,
                                    headers: {
                                        Accept : "application/json"
                                    },
                                    type: "GET",
                                    async: false,
                                    complete: function (xhr) {
                                        $DATA_DOCUMENT = xhr.responseJSON;
                                        initClearExpenseDetail();
                                        setTimeout(function () {
                                            $('.dv-background').hide();
                                        },1000);
                                    }
                                });
                            }
                        }
                    });
                },1000)
            }else{
                $('.dv-background').hide();
                $('#warningModal .modal-body').html("Duplicate Expense");
                $("#warningModal").modal('show');
            }
        }else{
            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/expense/saveDocumentExpenseItem',
                    data: jsonDocumentExpenseItem,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            $("#expenseInputExpense").val("");
                            $.ajax({
                                url:session['context']+'/expense/'+$DATA_DOCUMENT.id,
                                headers: {
                                    Accept : "application/json"
                                },
                                type: "GET",
                                async: false,
                                complete: function (xhr) {
                                    $DATA_DOCUMENT = xhr.responseJSON;
                                    initClearExpenseDetail();
                                    setTimeout(function () {
                                        $('.dv-background').hide();
                                    },1000);
                                }
                            });
                        }
                    }
                });
            },1000)
        }
    }else{
        $('.dv-background').hide();
        $('#warningModal .modal-body').html("Select Expense");
        $("#warningModal").modal('show');
    }
}

function findCurrencyRate(date) {
    var dataCurrencyRate = $.ajax({
        url:session['context']+'/currencyRates/findByActionDate?actionDate='+date,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataCurrencyRate){
        $("#currencyRateEXPF_004").val(dataCurrencyRate.rateTranfer);
    }else{
        $("#currencyRateEXPF_004").val("");
    }

}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function processCalculatePerDiemForeign(empLv,startDate,endDate,startTime,endTime,rateType,locationCode,sellingRate) {
    $('.dv-background').show();

    var jsonPost = {};
    jsonPost['parentId']     	    = 1;
    jsonPost[csrfParameter]  	    = csrfToken;
    jsonPost['empLevel'] = empLv;
    jsonPost['sellingRate'] = sellingRate;
    jsonPost['startDate'] = startDate;
    jsonPost['endDate'] = endDate;
    jsonPost['startTime'] = startTime;
    jsonPost['endTime'] = endTime;
    jsonPost['rateType'] = rateType;
    jsonPost['location'] = locationCode;
    jsonPost['documentExpense'] = $DATA_DOCUMENT.documentExpense.id;
    jsonPost['documentExpenseItem'] = $("#modalAddDetail").attr('idexpenseitem');

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/perDiemProcess/processCalculatePerDiemForeign',
            data: jsonPost,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    if(xhr.responseText){
                        var data = xhr.responseJSON;
                        $("#amountModalAdd").autoNumeric('set',data.baht);
                        $("#netAmountModalAdd").autoNumeric('set',data.baht);
                    }else{
                        $("#amountModalAdd").autoNumeric('set',0);
                        $("#netAmountModalAdd").autoNumeric('set',data.baht);
                    }
                }
            }
        });
    },1000);
}

function processCalculatePerDiemDomestic(empLv,startDate,endDate,startTime,endTime,rateType,locationCode) {
    $('.dv-background').show();

    var jsonPost = {};
    jsonPost['parentId']     	    = 1;
    jsonPost[csrfParameter]  	    = csrfToken;
    jsonPost['empLevel'] = empLv;
    jsonPost['startDate'] = startDate;
    jsonPost['endDate'] = endDate;
    jsonPost['startTime'] = startTime;
    jsonPost['endTime'] = endTime;
    jsonPost['rateType'] = rateType;
    jsonPost['location'] = locationCode;
    jsonPost['documentExpense'] = $DATA_DOCUMENT.documentExpense.id;
    jsonPost['documentExpenseItem'] = $("#modalAddDetail").attr('idexpenseitem');

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/perDiemProcess/processCalculatePerDiemDomestic',
            data: jsonPost,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $('.dv-background').hide();
                    if(xhr.responseText){
                        var data = xhr.responseJSON;
                        $("#amountModalAdd").autoNumeric('set',data.summary);
                        $("#netAmountModalAdd").autoNumeric('set',data.summary);
                    }else{
                        $("#amountModalAdd").autoNumeric('set',0);
                        $("#netAmountModalAdd").autoNumeric('set',0);
                    }
                }
            }
        });
    },1000);
}


function checkAccommodationAuthorize() {
    if($("#hotelEXPF_009InputHotelForExpense").val() != ""){
        var docNumber = $DATA_DOCUMENT.docNumber;
        var itemId = $("#modalAddDetail").attr('idexpenseitem')
        var isIbisStd = false;
        if($("#hotelEXPF_009InputHotelForExpense").attr('data-hotel-isibis') == "true" && $("#hotelEXPF_009InputHotelForExpense").attr('data-hotel-roomtype') == "Standard"){
            isIbisStd =true;
        }
        var zoneType = $("#hotelEXPF_009InputHotelForExpense").attr('data-hotel-zonetype');

        var startDate = new Date($('#date1EXPF_009').val().split('/')[2],parseInt($('#date1EXPF_009').val().split('/')[1])-1,$('#date1EXPF_009').val().split('/')[0])
        var endDate = new Date($('#date2EXPF_009').val().split('/')[2],parseInt($('#date2EXPF_009').val().split('/')[1])-1,$('#date2EXPF_009').val().split('/')[0])
        var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        var checkAccommodation = $.ajax({
            url:session['context']+'/checkAccommodations/checkAccommodationAuthorize?docNumber='+docNumber+"&itemId="+itemId+"&isIbisStd="+isIbisStd+"&zoneType="+zoneType+'&diffDays='+diffDays.toString(),
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        console.log("========== checkAccommodation ===========");
        if(checkAccommodation){
            checkAccommodationAuthorizeForCollapse = checkAccommodation;
            $("#accommodationOver").addClass('hide');
            console.log(checkAccommodation);
        }else{
            accommodationAuthorize = checkAccommodation;
            checkAccommodationAuthorizeForCollapse = checkAccommodation;
            $("#accommodationOver").removeClass('hide');
            console.log(checkAccommodation);
        }

    }
}

function deleteItemFile(item,idDelete){
    $('#'+item).empty()
    if(idDelete != ''){

        var jsonParams2 = {};
        jsonParams2['parentId']     = 1;
        jsonParams2[csrfParameter]  = csrfToken;

        $.ajax({
            type: "DELETE",
            url: session['context']+'/expense/deleteDocumentExpenseItemAttachment/'+idDelete,
            data: jsonParams2,
            complete: function (xhr) {

            }
        });
    }else{
        startIdAttachmentFile--
    }

}

function calculateAcc() {
    if(startAmountTocalVat){

        $('#amountModalAdd').autoNumeric("set",startAmountTocalVat)
        var startAmount = $('#amountModalAdd').autoNumeric('get')
        var vatValue  = $('#vatType option:selected').attr('vatValue')
        var vatCode = $('#vatType').val();
        var netAmount = startAmountTocalVat

        if(isNumber(vatValue) ){
            var numberValue = parseInt(vatValue)
            if(vatCode != "VX"){
                $('#vatModalAdd').autoNumeric("set",(startAmount*numberValue)/(100+numberValue))
                $('#amountModalAdd').autoNumeric("set",(netAmount-((startAmount*numberValue)/(100+numberValue))))
            }


        }else{
            $('#vatModalAdd').val('');
            $('#amountModalAdd').autoNumeric("set",$('#netAmountModalAdd').val())
        }




        calculateWHT();
        startAmountNet =  netAmount
    }
}

function backToMenu(){

    window.location.href = session.context+'/expense/expenseMainMenu';

}

function findConditionalIObyCompanyCode(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/conditionalIOs/findByCompanyCode?code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);
                    var item = obj.content
                    listConditionalIO = item
                }
            }
        }
    }).done(function (){
        //close loader
    });

}

function nvl(e){
    if(e == null || e == undefined){
        return "";
    }else{
        return e;
    }
}

function findMasterdataDetailApproveByCode(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCode?masterdata='+$MASTER_DATA_BRNACH_PSA+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        $('#branchPurchaserModalAdd').val('');
                        $('#branchPurchaserModalAdd').val(obj.variable3);
                        $('#bankAccountHo').text(obj.variable6+" - "+obj.variable9);
                    }
                }
            }
        }
    }).done(function (){
        //close loader
    });

}

function calculateWHT(){
    var baseAmount = $('#amountModalAdd').autoNumeric('get')
    var whtValue = $('#whtCode option:selected').attr('whtValue')
    var result =   (baseAmount*whtValue)/100
    var netAmount =  $('#netAmountModalAdd').autoNumeric('get')
    $('#taxModalAdd').autoNumeric('set',result)

    $('#netAmountModalAdd').autoNumeric('set',startAmountTocalVat-result)
}

function preViewAttachmentFileExpenseItem(id,fileName,flagEmed,docEmed,seq) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(flagEmed != "Y") {
        if (fileType == 'pdf' || fileType == 'PDF' || fileType == 'txt' || fileType == 'TXT') {
            var url = session['context'] + '/expenseItem/preViewPDFDocumentExpItemAttachment?id=' + id + '&fileName=' + fileName

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }

        if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpg' || fileType == 'JPG' || fileType == 'jpeg' || fileType == 'JPEG') {

            var urlIMG = session['context']+'/expenseItem/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName;

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(urlIMG, '', properties);
        }
    }else{
        var url = $URL_EMED+"?docNo="+docEmed+"&seq="+seq;
        if (fileType == 'pdf' || fileType == 'PDF' || fileType == 'txt' || fileType == 'TXT') {

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }

        if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpg' || fileType == 'JPG' || fileType == 'jpeg' || fileType == 'JPEG') {

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }
    }

}

function preViewAttachmentFileExpenseItemAttachment(id,fileName) {

    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(fileType == 'pdf' || fileType == 'PDF' || fileType =='txt' || fileType == 'TXT'){
        var url = session['context']+'/approve/preViewPDFDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(url, '', properties);
    }

    if(fileType == 'png' || fileType == 'PNG' || fileType =='jpg' || fileType == 'JPG' || fileType == 'jpeg'  || fileType == 'JPEG'){


        var urlIMG = session['context']+'/approve/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName


        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(urlIMG, '', properties);
    }

}

function downloadAttachmentFileExpenseItem(id,fileName,flagEmed,docEmed,seq) {

    if(flagEmed != "Y") {
        window.location.href = session['context'] + '/expenseItem/downloadDocumentExpItemAttachment?id=' + id + '&fileName=' + fileName;
    }else{
        window.location.href = $URL_EMED+"?docNo="+docEmed+"&seq="+seq;
    }
}

function openDocumentAccountRemark() {
    renderDocumentAccountRemark();
    $("#remarkAccountInput").val("");
    $("#modalDocumentAccountRemark").modal('show')
}

function renderDocumentAccountRemark() {
    $("#accountRemarkBody").empty();
    var dataDocumentAccountRemark = $.ajax({
        url:session['context']+'/documentAccountRemarks/findByDocumentId?docId='+$DATA_DOCUMENT.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataDocumentAccountRemark.content[0].id){
        var append = "";
        $.each(dataDocumentAccountRemark.content,function (index,item) {
            if(item.endDate){
                append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                    '<div class="panel-heading collapseTeal" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0" >' +
                    '<div class="col-xs-1 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '<a onclick="deleteDocumentAccountRemark(' + item.id + ')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                    '</div>' +
                    '<div class="col-xs-11" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold" startDate="'+item.startDate+'" remarkDescription="'+item.remarkDescription+'" itemId="'+item.id+'" onclick="">' +
                    '' + item.remarkDescription +''+
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }else{
                append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                    '<div class="panel-heading collapseGray" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0" >' +
                    '<div class="col-xs-1 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '<a onclick="deleteDocumentAccountRemark(' + item.id + ')"><img src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                    '</div>' +
                    '<div class="col-xs-11" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold" startDate="'+item.startDate+'" remarkDescription="'+item.remarkDescription+'" itemId="'+item.id+'" onclick="updateDocumentAccountRemark(this)">' +
                    '' + item.remarkDescription +''+
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }

        });
        $("#accountRemarkBody").append(append);
    }
    console.log(dataDocumentAccountRemark);
}

function addDocumentAccountRemark() {
    $('.dv-background').show();
    var jsonParams = {};
    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['startDate']          = new Date().format("yyyy-mm-dd HH:MM:ss");
    jsonParams['id']     		     = null;
    jsonParams['remarkDescription']  = $("#remarkAccountInput").val();
    jsonParams['documentId'] 	     = $DATA_DOCUMENT.id;

    setTimeout(function () {
        if($("#remarkAccountInput").val() != ""){
            $.ajax({
                async: false,
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/documentAccountRemarks/save',
                data: jsonParams,
                complete: function (xhr) {
                    $('.dv-background').hide();
                    renderDocumentAccountRemark();
                    $("#remarkAccountInput").val("");
                }
            });
        }else{
            $('.dv-background').hide();
            var messageRequired ="";

            if($('#remarkAccountInput').val() == ""){
                messageRequired += "&#8195;&#8195;&#8195;"+LB_EXPENSE.LABEL_REMARK +"<br/>";
            }

            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +"<br/>"+messageRequired);
            $('#warningModal').modal('show');
        }

    },1000)

}

function updateDocumentAccountRemark(tag) {
    $('.dv-background').show();
    var jsonParams = {};
    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['startDate']          = $(tag).attr('startDate');
    jsonParams['id']     		     = $(tag).attr('itemId');
    jsonParams['remarkDescription']  = $(tag).attr('remarkDescription');
    jsonParams['endDate']            = new Date().format("yyyy-mm-dd HH:MM:ss");
    jsonParams['documentId'] 	     = $DATA_DOCUMENT.id;

    setTimeout(function () {
        $.ajax({
            async: false,
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/documentAccountRemarks',
            data: jsonParams,
            complete: function (xhr) {
                $('.dv-background').hide();
                renderDocumentAccountRemark();
            }
        });
    },1000)
}

function deleteDocumentAccountRemark(id) {
    $('.dv-background').show();

    setTimeout(function () {
        $.ajax({
            type: "DELETE",
            url: session['context']+'/documentAccountRemarks/'+id,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                renderDocumentAccountRemark();
            }
        });
    },1000)
}

function validateDocumentApproveByDocNumber(docNumber) {

    var psa_origin = "";
    var psa_destination = "";
    var location_type = "";
    var zone_code = "";

    $('.dv-background').show();
    setTimeout(function () {
        var data = $.ajax({
            url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
            headers: {
                Accept: "application/json"
            },
            type: "GET",
            async: false,
            complete: function (xhr) {
                if (xhr.responseText != null) {
                    var dataEmployee = JSON.parse(xhr.responseText);
                    var cLevel = dataEmployee.EESG_ID;
                    var psa_requester = dataEmployee.Personal_PSA_ID;

                    $.ajax({
                        type: "GET",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/findTravelDetailsByDocRef/' + docNumber,
                        complete: function (xhr) {
                            if(xhr.status == 200){
                                if(xhr.responseText){
                                    var dataCheckLine = JSON.parse(xhr.responseText);
                                    if (dataCheckLine != null) {
                                        psa_origin = dataCheckLine[0].origins.psaCode;
                                        psa_destination = dataCheckLine[0].destination.psaCode;
                                        location_type = dataCheckLine[0].origins.locationType;
                                        zone_code = dataCheckLine[0].destination.zoneCode;
                                        $('.dv-background').hide();
                                        validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code);
                                    }
                                }else{
                                    $('.dv-background').hide();
                                }
                            }else{
                                $('.dv-background').hide();
                            }
                        }
                    });
                }
            }
        });
    },1000);
}

var glCodeVal;
var resultVal;
function validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code) {
    if (psa_origin != "" && psa_destination != "" && location_type != "" && zone_code != "") {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: session['context'] + '/approve/validateFlowTypeApprove',
            data: {
                cLevel: parseInt(cLevel),
                psa_requester: psa_requester,
                psa_origin: psa_origin,
                psa_destination: psa_destination,
                location_type: location_type,
                zone_code: zone_code
            },
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != null) {
                        var flowTypeName = xhr.responseText;
                        var flowType = "";
                        if (flowTypeName == "H001" || flowTypeName == "H002") {
                            flowType = MASTER_DATA.FLOW_TYPE_H001;
                        } else if (flowTypeName == "H003") {
                            flowType = MASTER_DATA.FLOW_TYPE_H003;
                        } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                            flowType = MASTER_DATA.FLOW_TYPE_H004;
                        }

                        if (flowType != "") {
                            var glCode = "";
                            $.each($("[name=summaryAmount]"),function (index,item) {
                                console.info(item.id);
                                glCode += item.id.split("summaryAmount")[1]+",";
                                // var flowType = $("#"+item.id).attr("flowtype");

                            });
                            console.info(glCode);
                            if(glCode != ""){
                                $.ajax({
                                    type: "GET",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    url: session['context'] + '/expense/getExpenseTypeCodeForeign',
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            if (xhr.responseText != null) {
                                                console.info(xhr.responseText.split(","));
                                                var dataValue = xhr.responseText.split(",");
                                                resultVal = [];
                                                for(var i=0;i<dataValue.length;i++){
                                                    resultVal.push("summaryAmount"+dataValue[i]);
                                                }

                                                if(resultVal.length > 0){
                                                    $.each($("[name=summaryAmount]"),function (index,item) {
                                                        if(resultVal.indexOf(item.id) > -1){
                                                            $("#"+item.id).attr("flowtype",flowType);
                                                            getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }
        });
    }
}

function checkLineApproveForResendFlow() {
    checkResendFlow = false;
    if($DATA_REQUEST_APPROVER && $DATA_LINE_APPROVE){
        var checkRequestApprove = $DATA_REQUEST_APPROVER;
        var checkPaidState = checkRequestApprove.findIndex(checkRequestApprove => checkRequestApprove.actionState === "PAID");
        if(checkPaidState != -1){
            checkRequestApprove.splice(checkPaidState);
        }

        if(checkRequestApprove.length == $DATA_LINE_APPROVE.length){
            for (var i = 0; i < checkRequestApprove.length; i++) {
                if(checkRequestApprove[i].userNameApprover != $DATA_LINE_APPROVE[i].userName){
                    checkResendFlow = true;
                    break;
                }
            }
        }else{
            checkResendFlow = true;
        }

    }else{
        if(!$DATA_REQUEST_APPROVER){
            checkResendFlow = true;
        }
    }
}

function remarkRequestDetail(txt){

    console.log('>>Message Reason>>>'+txt)

    if(txt != null && txt != '' && txt!= undefined && txt != 'null'){

        $("#alertModal").modal('show');
        $("label[id=detailAlert]").html('Remark : '+txt);

    }

}

function resendFlow() {
    $('.dv-background').show();
    var dataTerminate = {};
    dataTerminate['id'] = $DATA_DOCUMENT.id;
    dataTerminate['requester'] = $DATA_DOCUMENT.requester;
    dataTerminate['docNumber'] = $DATA_DOCUMENT.docNumber;

    var listJsonDetails = [];

    $.each($('[name=summaryAmount]'),function (index,item) {
        var jsonDetails = {};
        jsonDetails['amount'] = $("#"+ item.id).autoNumeric('get');
        jsonDetails['flowType'] = $("#"+ item.id).attr('flowType');
        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
        listJsonDetails.push(jsonDetails);
    });

    var jsonResendFlow = {};
    jsonResendFlow['id'] = $DATA_DOCUMENT.id;
    jsonResendFlow['requester'] = $DATA_DOCUMENT.requester;
    if(accommodationAuthorize){
        jsonResendFlow['approveType'] = $DATA_DOCUMENT.approveType;
    }else{
        jsonResendFlow['approveType'] = "Y";
    }
    jsonResendFlow['documentType'] = $DATA_DOCUMENT.documentType;
    jsonResendFlow['tmpDocNumber'] = $DATA_DOCUMENT.tmpDocNumber;
    jsonResendFlow['details'] = JSON.stringify(listJsonDetails);
    jsonResendFlow['pa'] = $("#comAgencyLabel").attr('pa');
    jsonResendFlow['psa'] = $("#departmentLabel").attr('psa');

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/terminateFlow',
            data:dataTerminate,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    console.log(xhr);
                    $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/requests/resendFlow',
                        data:jsonResendFlow,
                        async: false,
                        complete: function (xhr2) {
                            if (xhr2.readyState == 4) {
                                $('.dv-background').hide();
                                window.location.href = session.context;
                            }
                        }
                    });
                }
            }
        });
    },1000);
}

function approveDocument(){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = $USERNAME;
    jsonData['actionStatus'] = validateActionState($USERNAME);
    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/approveRequest',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_APPROVE = JSON.parse(xhr.responseText);

                    if($DATA_APPROVE != null || $DATA_APPROVE != undefined){
                        window.location.href = session.context;
                    }
                    $('.dv-background').hide();
                }
            }
        });
    },1000);

}

function changeSizeSelectListOnblur(){
    $('#year_EXPF_015').attr('size', 0)
    $('#year_EXPF_015').css('margin-top', '15px')
}

var firstSelect =1
function changeSizeSelectListOnMouseDown(select){
    if(firstSelect=1){
        firstSelect++
        $('#year_EXPF_015').val("2017")
    }

    var index = ($("#year_EXPF_015")[0].selectedIndex)

    if(select.options.length>11){

        $('#year_EXPF_015').attr('size', 11)
        $("select#year_EXPF_015").prop('selectedIndex', index);
    }
    $('#year_EXPF_015').css('margin-top', '15px')

}

function expandOption(){
    $('#year_EXPF_015').click()
}

function saveDieselAllowancePerMonthHistoryForExpense() {
    var jsonDieselHistory = {};
    jsonDieselHistory['empCode']        =   $DATA_EMPLOYEE.EmployeeId
    jsonDieselHistory['month']          =   new Date().format('mm');
    jsonDieselHistory['year']           =   new Date().format('yyyy');
    jsonDieselHistory['literNumber']    =   $("#liter_EXPF_001").val();
    jsonDieselHistory['empUserName']    =   $DATA_DOCUMENT.requester;
    
    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/dieselAllowancePerMonthHistories/saveDieselAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                
            }
        }
    });
    
}

function deleteDieselAllowancePerMonthHistoryForExpense(id,callback) {
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/dieselAllowancePerMonthHistories/deleteDieselAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                callback()
            }
        }
    });

}

function updateDieselAllowancePerMonthHistoryForExpense(id) {
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;
    jsonDieselHistory['literNumber']    =   $("#liter_EXPF_002").val();

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/dieselAllowancePerMonthHistories/updateDieselAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
            }
        }
    });

}

function savePetrolAllowancePerMonthHistoryForExpense() {
    var jsonDieselHistory = {};
    jsonDieselHistory['empCode']        =   $DATA_EMPLOYEE.EmployeeId
    jsonDieselHistory['month']          =   new Date().format('mm');
    jsonDieselHistory['year']           =   new Date().format('yyyy');
    jsonDieselHistory['literNumber']    =   $("#liter_EXPF_002").val();
    jsonDieselHistory['empUserName']    =   $DATA_DOCUMENT.requester;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/petrolAllowancePerMonthHistories/savePetrolAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {

            }
        }
    });

}

function deletePetrolAllowancePerMonthHistoryForExpense(id,callback) {
    console.log("=====================  id  :   "+id)
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/petrolAllowancePerMonthHistories/deletePetrolAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                callback();
            }
        }
    });

}

function updatePetrolAllowancePerMonthHistoryForExpense(id) {
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;
    jsonDieselHistory['literNumber']    =   $("#liter_EXPF_002").val();
    
    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/petrolAllowancePerMonthHistories/updatePetrolAllowancePerMonthHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
            }
        }
    });

}

function saveMonthlyPhoneBillHistoryForExpense(callback) {
    var jsonDieselHistory = {};
    jsonDieselHistory['empCode']        =   $DATA_EMPLOYEE.EmployeeId
    jsonDieselHistory['month']          =   $("#month_EXPF_015 option:selected").val();
    jsonDieselHistory['year']           =   $("#year_EXPF_015").val();
    jsonDieselHistory['amount']         =   $("#amountModalAdd").autoNumeric('get');
    jsonDieselHistory['empUserName']    =   $DATA_DOCUMENT.requester;
    jsonDieselHistory['phoneNumber']    =   $("#phoneNumber_EXPF_015").val();

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/monthlyPhoneBillHistories/saveMonthlyPhoneBillHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if("Over" == xhr.responseText){
                checkOverPhoneBackend = true;
            }
            callback();
        }
    });

}

function deleteMonthlyPhoneBillHistoryForExpense(id,month,year,callback) {
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;
    jsonDieselHistory['month']              =   month;
    jsonDieselHistory['year']               =   year;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/monthlyPhoneBillHistories/deleteMonthlyPhoneBillHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                callback()
            }
        }
    });

}

function updateMonthlyPhoneBillHistoryForExpense(id,month,year) {
    var jsonDieselHistory = {};
    jsonDieselHistory['expenseItem']        =   id;
    jsonDieselHistory['amount']             =   $("#amountModalAdd").autoNumeric('get');
    jsonDieselHistory['month']              =   month;
    jsonDieselHistory['year']               =   year;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/monthlyPhoneBillHistories/updateMonthlyPhoneBillHistoryForExpense',
        data:jsonDieselHistory,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
            }
        }
    });

}

function checkOutstanding() {
    var advanceAmount = $("#advanceAmount").autoNumeric('get')==""?0:$("#advanceAmount").autoNumeric('get');
    var amountExpenseAll = $("#sumAllExpenseAmount").autoNumeric('get')==""?0:$("#sumAllExpenseAmount").autoNumeric('get');
    var total = parseFloat(advanceAmount) - parseFloat(amountExpenseAll);

    if(total < 0){
        $("#outstanding").css('color','green');
        $("#addMoney").addClass('hide');
        $("#getMoney").removeClass('hide');
        total = total*-1
    }else{
        $("#outstanding").css('color','red');
        $("#addMoney").removeClass('hide');
        $("#getMoney").addClass('hide');
    }
    $("#outstanding").autoNumeric('set',total);
}

function checkOutstandingFromPayInAmount() {
    var payInAmount = $("#payInAmount").autoNumeric('get')==""?0:$("#payInAmount").autoNumeric('get');
    var advanceAmount = $("#advanceAmount").autoNumeric('get')==""?0:$("#advanceAmount").autoNumeric('get');
    var amountExpenseAll = $("#sumAllExpenseAmount").autoNumeric('get')==""?0:$("#sumAllExpenseAmount").autoNumeric('get');
    var total;
    if(0==payInAmount){
        total = parseFloat(advanceAmount) - parseFloat(amountExpenseAll);
    }else{
        total = (parseFloat(advanceAmount)-parseFloat(amountExpenseAll)) - parseFloat(payInAmount);
    }


    if(total < 0){
        $("#outstanding").css('color','green');
        $("#addMoney").addClass('hide');
        $("#getMoney").removeClass('hide');
        total = total*-1
    }else{
        $("#outstanding").css('color','red');
        $("#addMoney").removeClass('hide');
        $("#getMoney").addClass('hide');
    }
    $("#outstanding").autoNumeric('set',total);
}

$(document).ready(function () {

    findMasterdataDetailApproveByCode($DATA_DOCUMENT.companyCode+"-"+$DATA_DOCUMENT.psa);
    $('.dv-background').show();
    $("#amountModalAdd").autoNumeric('init',{vMin:0});
    $("#outstanding").autoNumeric('init',{vMin:0});
    $("#sumAllExpenseAmount").autoNumeric('init',{vMin:0});
    $("#advanceAmount").autoNumeric('init',{vMin:0});
    $("#vatModalAdd").autoNumeric('init',{vMin:0});
    $("#taxModalAdd").autoNumeric('init',{vMin:0});
    $("#netAmountModalAdd").autoNumeric('init',{vMin:0});
    $("#distance_EXPF_007").autoNumeric('init',{vMin:0,mDec:0});
    $("#payInAmount").autoNumeric('init',{vMin:0});
    $("#amountInternalMember").autoNumeric('init',{vMin:0});
    $("#amountExternalMember").autoNumeric('init',{vMin:0});
    $("#spanInternalOrderAmount").autoNumeric('init',{vMin:0});
    $("#phonePerMonth").autoNumeric('init',{vMin:0});
    moveFixedActionButton();
    findUserHead(function () {
        checkBtn();
    });
    findLocationTypePD();
    findLocationTypePF();
    findLocationTypeD();
    findConditionalIObyCompanyCode($DATA_DOCUMENT.companyCode);

    $('#vatType').on('change',function () {

        if($("#vatType option:selected").val() != "" && $("#vatType option:selected").val() != "VX"){
            $("#taxIDNumberModalAdd").val("");
            $("#branchModalAdd").val("");
            $("#vatModalAdd").attr('disabled',false)

            $('#amountModalAdd').val(startAmountTocalVat)
            var startAmount = $('#amountModalAdd').autoNumeric('get')
            var vatValue  = $('#vatType option:selected').attr('vatValue')
            var netAmount = startAmountTocalVat

            if(isNumber(vatValue)){
                var numberValue = parseInt(vatValue)
                $('#vatModalAdd').autoNumeric("set",(startAmount*numberValue)/(100+numberValue) )
                $('#amountModalAdd').autoNumeric("set",(netAmount-((startAmount*numberValue)/(100+numberValue))))

            }else{
                $('#vatModalAdd').val('')
                $('#amountModalAdd').autoNumeric("set",startAmountTocalVat)
            }

            calculateWHT();


        }else{
            $("#vatModalAdd").attr('disabled',true);
            $("#vatModalAdd").val('');
            $('#amountModalAdd').autoNumeric("set",startAmountTocalVat);
            $("#taxIDNumberModalAdd").val("0000000000000");
            $("#branchModalAdd").val("NVAT");
            calculateWHT();
        }



    });



    $('#vatModalAdd').on('change',function () {

        $('#amountModalAdd').val(startAmountTocalVat)
        var startAmount = $('#amountModalAdd').autoNumeric('get')
        var vatValue  = $('#vatModalAdd').autoNumeric('get')
        var netAmount = startAmountTocalVat

        if(isNumber(vatValue)){
            var numberValue = parseInt(vatValue)
            $('#amountModalAdd').autoNumeric("set",(startAmount-vatValue))
            $('#vatModalAdd').autoNumeric("set",(netAmount-(startAmount-vatValue)))

        }else{
            $('#vatModalAdd').val('')
            $('#amountModalAdd').val(startAmountTocalVat)
        }

        calculateWHT();
    });

    $('#whtCode').on('change',function () {
        calculateWHT();
    });


    $('#addMoreFile').on('click', function () {

        var nameFile = $('#attachmentFileGL option:selected').text()
        var uploadType = $('#attachmentFileGL option:selected').val()


        if (nameFile != "") {
            var dynamicFileTxt = 'dynamicFileTxt'
            var dynamicFile = 'dynamicFile'
            var dynamicFileBtn = 'dynamicFileBtn'
            var detail = 'detailFile_'
            startIdAttachmentFile++

            dynamicFileTxt = dynamicFileTxt + (startIdAttachmentFile)
            dynamicFile = dynamicFile + (startIdAttachmentFile)
            detail = detail + (startIdAttachmentFile)
            dynamicFileBtn = dynamicFileBtn + (startIdAttachmentFile)
            var emptyStr = ''
            $('#expenseTypeFileRequired').append('' +
                '<div id="' + detail + '" class="col-sm-12">' +
                '  <div class="col-sm-2" style="padding: 0px; width: 65px; text-align: center">' +
                '<a onclick="deleteItemFile(\'' + detail + '\',\'' + emptyStr + '\')"><img  src="' + $IMAGE_DELETE + '" width="30px"/></a>' +
                ' </div>' +
                '<div class="form-group has-info" >' +
                '  <div class="col-sm-4" style="padding: 0px;" >' +

                '<label class="col-sm-12" style="padding-top:5px;text-align: left">' + nameFile + '</label>' +

                ' </div>' +
                '<div class="col-md-6" style="margin-bottom: 8px">' +
                ' <div class="input-group">' +
                '<input index="' + startIdAttachmentFile + '" disabled id="' + dynamicFileTxt + '" class="form-control border" placeholder="' + $MESSAGE_PLACEHOLDER_BROWSE_FILE + '" type="text" />' +
                '<input id="' + dynamicFile + '"   name="uploadFileExpenseItem" class="form-control hide" type="file"   uploadtype="'+uploadType+'"  />' +
                '<span class="input-group-btn input-group-sm">' +
                '<a><img index="' + startIdAttachmentFile + '" onclick="uploadFileExpenseItemDetailAndUpDate(this)" id="' + dynamicFileBtn + '" src="' + $IMAGE_PAPER_CLIP + '" width="30px"/></a></span>' +
                '</div>' +
                '</div>' +
                ' </div>' +
                ' </div>' +
                '')

            $('#attachmentFileGL').val('')
        }else{


            $('#warningModal .modal-body').html("Select File Type");
            $('#warningModal').modal('show');


        }


    })

    readDataZip(function () {
        readMasterDataDocType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataAttachmentType();
        readMasterDataActionReason();
        readMasterDataRequestStatus();
        readMasterDataFlowType();
        findAllVat();
        findAllWitholdingTax();
        findAllWitholdingTaxCode();
        window.setTimeout(function(){
            AutocompleteDocRefAdvForExpense.init("docRefAdvInputDocRefAdvForExpense",$DATA_DOCUMENT.requester,'');
            AutocompleteDocRefAppForExpense.init("docRefAppInputDocRefAppForExpense",$DATA_DOCUMENT.requester);
            findEmployeeProfileByUserName($DATA_DOCUMENT.requester,function () {
                console.log("callback");
                // AutocompleteExpense.init("expenseInputExpense", $DATA_DOCUMENT.companyCode, $DATA_DOCUMENT.psa, "","","", session.userName);
            });
            findCreatorProfileByUserName($DATA_DOCUMENT.createdBy);
            validateDocStatus();
            getDataAttachmentType();
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            if($DATA_DOCUMENT.documentReference.length != 0){
                $.each($DATA_DOCUMENT.documentReference,function (index,item) {
                    if(item.documentTypeCode == "ADV") {
                        AutocompleteDocRefAdvForExpense.renderValue(item.docReferenceNumber);
                        $("#amountForAdvance").removeClass('hide');
                        $("#uploadForDocRef").removeClass('hide');
                        $("#docRefAdvInputDocRefAdvForExpense").blur();
                    }
                });
            }

            if($DATA_DOCUMENT.payinAmount){
                $("#payInAmount").autoNumeric('set',$DATA_DOCUMENT.payinAmount);
            }

            if($DATA_DOCUMENT.payinDate){
                $("#payInDate").val(DateUtil.coverDateToString($DATA_DOCUMENT.payinDate));
            }

        },1000);

    });


    $("#payInAmount").on('blur',function () {
        checkOutstandingFromPayInAmount();
    });

    $("#confirmReject").on('click',function(){
        var checkRole = $("#confirmRejectModal").attr('checkrole');
        rejectExpense(checkRole);
    });

    $("#expenseInputExpense").on('keyup',function(){
        AutocompleteExpense.init("expenseInputExpense",$("#comAgencyLabel").attr('pa'),$("#departmentLabel").attr('psa'),$("#expenseInputExpense").val(),$("#expenseInputExpense").val(),$("#expenseInputExpense").val(),session.userName,$("#costCenterLabel").text());
    });

    $("#confirmCancel").on('click',function(){
        cancelDocument();
    });

    $("#documentDateModalAdd,#payInDate,#postingDate").flatpickr({
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("[name=date1]").flatpickr({
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("[name=date2]").flatpickr({
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $('#time1EXPF_004,#time1EXPF_006,#time2EXPF_006,#time2EXPF_004').flatpickr({
        enableTime: true,
        noCalendar: true,
        locale:"en",
        enableSeconds: false,
        time_24hr: true,
        dateFormat: "H:i"
    });

    $('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });

    $("#billUploadFile").on('change',function () {
        $fileUpload = this.files[0];
        $("#billUploadTxt").val(this.files[0].name);
        saveDocumentAttachmentInCollapse();
    });

    $("#imageCalendar").on('click',function () {
        $("#payInDate").focus()
    });

    $("#imagePostingDate").on('click',function () {
        $("#postingDate").focus()
    });

    $("#browseFile").on('click',function(){
        if($("#attachmentType").val() == ""){
            var errorMessage = MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +" "+LB_EXPENSE.LABEL_ATTACHMENT_TYPE+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }else{
            var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
            $("#uploadFileDocument").attr('accept',extension);
            $("#uploadFileDocument").click();
        }
    });

    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
    });

    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });

    $("#confirmDelete").on('click',function (){
        deleteDocumentAttachment($("#idItemDelete").val());
    });

    $("#billUploadTxt").on('click',function () {
        $("#billUploadFile").click();
    });

    $("#billUploadImage").on('click',function () {
        $("#billUploadFile").click();
    });

    $("#btnSendRequest,#btnSendRequest2").on('click',function () {
        sendRequest();
    });

    $("#docRefAppInputDocRefAppForExpense").on('blur',function(){
        $("#docRefAppBtn").attr('data-attn',$("#docRefAppInputDocRefAppForExpense").val());
    });

    $("#docRefAdvInputDocRefAdvForExpense").on('blur',function () {
        $("#docRefAdvBtn").attr('data-attn',$("#docRefAdvInputDocRefAdvForExpense").val());

        var advanceAmount = $("#docRefAdvInputDocRefAdvForExpense").attr('data-amount')==undefined?"":$("#docRefAdvInputDocRefAdvForExpense").attr('data-amount');
        var amountExpenseAll = $("#sumAllExpenseAmount").autoNumeric('get');
        var total = parseFloat(advanceAmount) - parseFloat(amountExpenseAll);

        var docRefApp = $("#docRefAdvInputDocRefAdvForExpense").attr('data-docRefApp')==undefined?"":$("#docRefAdvInputDocRefAdvForExpense").attr('data-docRefApp');
        if("" != docRefApp){
            $("#docRefAppInputDocRefAppForExpense").val(docRefApp);
        }
        $("#advanceAmount").autoNumeric('set',advanceAmount);
        if(total < 0){
            $("#outstanding").css('color','green')
            total = total*-1
        }else{
            $("#outstanding").css('color','red')
        }
        $("#outstanding").autoNumeric('set',total);

        if($("#docRefAdvInputDocRefAdvForExpense").val() != ""){
            $("#uploadForDocRef").removeClass('hide');
            $("#amountForAdvance").removeClass('hide');
        }else{
            $("#uploadForDocRef").addClass('hide');
            $("#amountForAdvance").addClass('hide');
        }
    });

    $("#date1EXPF_004").on('blur',function () {
        var currencyRate = $("#date1EXPF_004").val();
        if(currencyRate){
            findCurrencyRate(currencyRate);
        }else{
            $("#currencyRateEXPF_004").val("")
        }

    });

    $("#currencyRateEXPF_004,#percentRateEXPF_004,#time2EXPF_004,#date2EXPF_004,#time1EXPF_004,#date1EXPF_004,#locationInputLocationForeign").on('blur',function () {
        if($("#currencyRateEXPF_004").val() != "" && $("#percentRateEXPF_004").val() != "" &&
           $("#time2EXPF_004").val() != "" && $("#date2EXPF_004").val() != "" && $("#time1EXPF_004").val() != "" &&
           $("#date1EXPF_004").val() != "" && $("#locationInputLocationForeign").val() != ""){

            var startDate = $("#date1EXPF_004").val();
            var endDate = $("#date2EXPF_004").val();
            var startTime = $("#time1EXPF_004").val();
            var endTime = $("#time2EXPF_004").val();
            var rateType = $("#percentRateEXPF_004").val();
            var sellingRate = $("#currencyRateEXPF_004").val();
            var empLv = $DATA_EMPLOYEE.EESG_ID;
            var location = $("#locationInputLocationForeign").attr('data-code');
            processCalculatePerDiemForeign(empLv,startDate,endDate,startTime,endTime,rateType,location,sellingRate);

        }else{
            $("#amountModalAdd").autoNumeric('set',0);
        }
    });

    $("#place1_EXPF_006,#date1EXPF_006,#time1EXPF_006,#date2EXPF_006,#time2EXPF_006,#percentEXPF_006").on('blur',function () {
        if($("#place1_EXPF_006").val() != "" && $("#date1EXPF_006").val() != "" &&
            $("#time1EXPF_006").val() != "" && $("#date2EXPF_006").val() != "" && $("#time2EXPF_006").val() != "" &&
            $("#percentEXPF_006").val() != ""){

            var startDate = $("#date1EXPF_006").val();
            var endDate = $("#date2EXPF_006").val();
            var startTime = $("#time1EXPF_006").val();
            var endTime = $("#time2EXPF_006").val();
            var rateType = $("#percentEXPF_006").val();
            var location = $("#place1_EXPF_006").val();
            var empLv = $DATA_EMPLOYEE.EESG_ID;
            processCalculatePerDiemDomestic(empLv,startDate,endDate,startTime,endTime,rateType,location);

        }else{
            $("#amountModalAdd").autoNumeric('set',0);
        }
    });

    $("#internalOrder,#amountModalAdd").on('blur',function () {
        if($("#internalOrder").hasClass('requiredValue')){
            checkIO();
        }
    });

    $("#internalOrder").on('change',function () {
        if($("#internalOrder").hasClass('requiredValue')){
            findIO();
        }
    })

    $("#place1_EXPF_003").on('change',function () {
        if($("#place1_EXPF_003").val() == "อื่นๆ"){
            $("#place1EXPF_003_other").removeClass('hide');
        }else{
            $("#placeOther1_EXPF_003").val("");
            $("#place1EXPF_003_other").addClass('hide');
        }
    });

    $("#place1_EXPF_008").on('change',function () {
        if($("#place1_EXPF_008").val() == "อื่นๆ"){
            $("#place1EXPF_008_other").removeClass('hide');
        }else{
            $("#placeOther1_EXPF_008").val("");
            $("#place1EXPF_008_other").addClass('hide');
        }
    });

    $("#variable5_EXPF_016").on('change',function () {
        if($("#variable5_EXPF_016").val() == "SV002"){
            $("#EXPF_016_other").removeClass('hide');
        }else{
            $("#other_EXPF_016").val("");
            $("#EXPF_016_other").addClass('hide');
        }
    });

    $("#place1_EXPF_005").on('change',function () {
        if($("#place1_EXPF_005").val() == "0022"){
            $("#place1EXPF_005_other").removeClass('hide');
        }else{
            $("#placeOther1_EXPF_005").val("");
            $("#place1EXPF_005_other").addClass('hide');
        }
    });

    $("#place2_EXPF_005").on('change',function () {
        if($("#place2_EXPF_005").val() == "0022"){
            $("#place2EXPF_005_other").removeClass('hide');
        }else{
            $("#placeOther2_EXPF_005").val("");
            $("#place2EXPF_005_other").addClass('hide');
        }
    });

    $("#place2_EXPF_007").on('change',function () {
        if($("#place2_EXPF_007").val() == "0022"){
            $("#place2EXPF_007_other").removeClass('hide');
        }else{
            $("#placeOther2_EXPF_007").val("");
            $("#place2EXPF_007_other").addClass('hide');
        }
    });

    $("#carLicence_EXPF_012").on('change',function () {
        if($("#carLicence_EXPF_012").val() == "CL002"){
            $("#carLicenceEXPF_012_other").removeClass('hide');
        }else{
            $("#carLicenceOther_EXPF_012").val("");
            $("#carLicenceEXPF_012_other").addClass('hide');
        }
    });

    $("#carLicence_EXPF_013").on('change',function () {
        if($("#carLicence_EXPF_013").val() == "CL002"){
            $("#carLicenceEXPF_013_other").removeClass('hide');
        }else{
            $("#carLicenceOther_EXPF_013").val("");
            $("#carLicenceEXPF_013_other").addClass('hide');
        }
    });

    $("#carLicence_EXPF_014").on('change',function () {
        if($("#carLicence_EXPF_014").val() == "CL002"){
            $("#carLicenceEXPF_014_other").removeClass('hide');
        }else{
            $("#carLicenceOther_EXPF_014").val("");
            $("#carLicenceEXPF_014_other").addClass('hide');
        }
    });

    $("#liter_EXPF_001").on('blur',function () {
        if(parseFloat($("#liter_EXPF_001").val()) > dieselLiter){
            checkOverDiesel = true;
        }else{
            checkOverDiesel = false;
        }
    });

    $("#liter_EXPF_002").on('blur',function () {
        if(parseFloat($("#liter_EXPF_002").val()) > petrolLiter){
            checkOverPetrol = true;
        }else{
            checkOverPetrol = false;
        }
    });

    $("#addEmployeeInputEmployeeAll").on('keyup',function(){
        AutocompleteEmployeeAllForExpense.setId('addEmployeeInputEmployeeAll');
        AutocompleteEmployeeAllForExpense.search($("#addEmployeeInputEmployeeAll").val(),"addEmployeeInputEmployeeAll");
    });

    $("#place1_EXPF_007,#place2_EXPF_007").on('change',function () {
        var locationFrom = $("#place1_EXPF_007").val();
        var locationTo = $("#place2_EXPF_007").val();

        if(locationTo != "" && locationTo != "Other"){
            $(".EXPF_007_Distance").addClass('hide');
            $("#distance_EXPF_007").val("")
            var amount = $.ajax({
                url:session['context']+'/routeDistances/calculateRouteDistanceFromLocation?locationFrom='+locationFrom+"&locationTo="+locationTo+"&companyCode="+$DATA_EMPLOYEE.Position_PA_ID,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(amount){
                $("#amountModalAdd").autoNumeric('set',amount)
            }
        }else{
            if(locationTo == "Other"){
                $(".EXPF_007_Distance").removeClass('hide');
            }
            $("#amountModalAdd").autoNumeric('set',0);
        }
    });

    $("#distance_EXPF_007").on('blur',function () {
        var locationFrom = $("#place1_EXPF_007").val();
        var locationTo = $("#place2_EXPF_007").val();
        var distance = $("#distance_EXPF_007").val();

        if(locationFrom != "" && locationTo != "" && distance != ""){
            var amount = $.ajax({
                url:session['context']+'/routeDistances/calculateRouteDistanceFromDistance?distance='+distance+"&companyCode="+$DATA_EMPLOYEE.Position_PA_ID,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(amount){
                $("#amountModalAdd").autoNumeric('set',amount)
            }
        }

    });

    $("#amountModalAdd").on('change',function () {
        amountExpenseItem = $("#amountModalAdd").autoNumeric("get");
        startAmountTocalVat = parseFloat($("#amountModalAdd").autoNumeric("get"));
        calculateAcc();

        if(!$("#EXPF_015").hasClass('hide')){
            if(monthlyPhoneBill){
                if(parseFloat($("#amountModalAdd").autoNumeric('get')) > monthlyPhoneBill){
                    checkOverPhone = true;
                }else{
                    checkOverPhone = false;
                }
            }
        }
    });

    $("#taxModalAdd").on('blur',function () {
        calculateAcc();
    });

    $("#nameMemberInputEmployeeAll").on('keyup',function(){
        AutocompleteEmployeeAll.setId('nameMemberInputEmployeeAll');
        AutocompleteEmployeeAll.search($("#nameMemberInputEmployeeAll").val(),"nameMemberInputEmployeeAll");
    });

    $("#locationInputLocationForeign").on('keyup',function(){
        AutocompleteLocationForeignForExpense.setId('locationInputLocationForeign');
        AutocompleteLocationForeignForExpense.search($("#locationInputLocationForeign").val());
    });




    $('li').on('click',function () {
       $('li').on('click',function () {

           setTimeout(function(){
               var valueYear =  $('li[data-selected=\"true\"]').attr('data-value');
               $('#year_EXPF_015').val(valueYear);
           }, 500);

       })
    })

    $("#confirmDeleteExpenseType").on('click',function () {
        deleteDocumentExpenseGroup($("#idItemExpenseTypeGroupDelete").val());
    })

    $("#confirmSendToSAP").on('click',function () {

        if($("#postingDate").val()){
            SendSap();
        }else{
            $('#warningModal .modal-body').html(MSG_EXPENSE.MESSAGE_REQUIRE_FIELD +" "+LB_EXPENSE.LABEL_POSTING_DATE);
            $('#warningModal').modal('show');
        }
        
    })

    $("#confirmResendFlow").on('click',function () {
        resendFlow();
    });

    $("#previewBill").on('click',function () {
        preViewAttachmentFileExpenseItemAttachment($("#previewBill").attr('id-img'),$("#previewBill").attr('id-name'));
    });

    $("#month_EXPF_015").on('change',function () {
        if(monthlyPhoneBillPerMonth){
            if($("#month_EXPF_015").val()){
                if("01" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJan);
                }else if("02" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountFeb);
                }else if("03" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountMar);
                }else if("04" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountApr);
                }else if("05" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountMay);
                }else if("06" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJun);
                }else if("07" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJul);
                }else if("08" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountAug);
                }else if("09" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountSep);
                }else if("10" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountOct);
                }else if("11" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountNov);
                }else if("12" == $("#month_EXPF_015").val()){
                    $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountDec);
                }
            }else{
                $("#phonePerMonth").autoNumeric('set',0);
            }
        }else{
            $("#phonePerMonth").autoNumeric('set',0);
        }
    });

    $("#year_EXPF_015").on('change',function () {
       if($("#year_EXPF_015").val()) {
           var dataMonthlyPhoneBill = $.ajax({
               url:session['context']+'/monthlyPhoneBillPerYears/findMonthlyPhoneBillByEmpUserNameAndYear?userName='+$DATA_DOCUMENT.requester+"&year="+$("#year_EXPF_015").val(),
               headers: {
                   Accept : "application/json"
               },
               type: "GET",
               async: false
           }).responseJSON;

           if(dataMonthlyPhoneBill){
               monthlyPhoneBillPerMonth = dataMonthlyPhoneBill;
               if($("#month_EXPF_015").val()){
                   if("01" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJan);
                   }else if("02" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountFeb);
                   }else if("03" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountMar);
                   }else if("04" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountApr);
                   }else if("05" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountMay);
                   }else if("06" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJun);
                   }else if("07" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountJul);
                   }else if("08" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountAug);
                   }else if("09" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountSep);
                   }else if("10" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountOct);
                   }else if("11" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountNov);
                   }else if("12" == $("#month_EXPF_015").val()){
                       $("#phonePerMonth").autoNumeric('set',monthlyPhoneBillPerMonth.amountDec);
                   }
               }else{
                   $("#phonePerMonth").autoNumeric('set',0);
               }
           }else{
               $("#phonePerMonth").autoNumeric('set',0);
           }

       }else{
           $("#phonePerMonth").autoNumeric('set',0);
       }
    });

});




