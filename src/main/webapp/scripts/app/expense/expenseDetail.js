var $DATA_EMPLOYEE;
var $DATA_REQUEST;
var $DATA_LINE_APPROVE;
var $DATA_REQUEST_APPROVER;
var $DATA_EMPLOYEE_FOR_LINE_APPROVE;
var sumAll = 0;
var checkStatus=0;
var checkStatusReject = 0;
var listConditionalIO = [];

$(document).ready(function () {
    $('.dv-background').show();
    moveFixedActionButton();
    $("#amountModalAdd").autoNumeric('init',{vMin:0});
    $("#outstanding").autoNumeric('init',{vMin:0});
    $("#sumAllExpenseAmount").autoNumeric('init',{vMin:0});
    $("#advanceAmount").autoNumeric('init',{vMin:0});
    $("#vatModalAdd").autoNumeric('init',{vMin:0});
    $("#taxModalAdd").autoNumeric('init',{vMin:0});
    $("#netAmountModalAdd").autoNumeric('init',{vMin:0});
    $("#distance_EXPF_007").autoNumeric('init',{vMin:0,mDec:0});
    $("#payInAmount").autoNumeric('init',{vMin:0});
    $("#amountInternalMember").autoNumeric('init',{vMin:0});
    $("#amountExternalMember").autoNumeric('init',{vMin:0});
    $("#spanInternalOrderAmount").autoNumeric('init',{vMin:0});
    $("#phonePerMonth").autoNumeric('init',{vMin:0});

    readDataZip(function () {
        readMasterDataDocType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataAttachmentType();
        readMasterDataActionReason();
        readMasterDataRequestStatus();

        findConditionalIObyCompanyCode($DATA_DOCUMENT.companyCode);
        window.setTimeout(function () {
            findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
            findCreatorProfileByUserName($DATA_DOCUMENT.createdBy);
            initExpenseDetail();
            validateDocStatus();
            renderExpenseType();

            if($DATA_DOCUMENT.requests){
                findRequestByDocument($DATA_DOCUMENT.id);
            }else{
                getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
            }
            checkBtn();
            // findRequestByDocument($DATA_DOCUMENT.id);
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            $('.dv-background').hide();
        }, 1000);
    });

    $("#confirmCancel").on('click',function(){
        cancelRequest();
    });

    $("#confirmReject").on('click',function(){
        rejectRequest();
    });

});

function renderModalExpense(id,type,idExpenseItem,gl) {
    listIdTime = [];
    listIdDate = [];

    $("#documentDateModalAdd").val("");
    $("#docNumberModalAdd").val("");
    $("#sellerModalAdd").val("");
    $("#taxIDNumberModalAdd").val("");
    $("#branchModalAdd").val("");
    $("#addressModalAdd").val("");
    $("#amountModalAdd").val("");
    $("#vatModalAdd").val("");
    $("#taxModalAdd").val("");
    $("#netAmountModalAdd").val("");

    $("#dynamicField").empty();
    var data = $.ajax({
        url: session.context + "/expenseType/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false

    }).responseJSON;

    if(data != undefined){

        $("[name=fixCodeExpenseType]").addClass('hide');
        $("#rowInternalOrderAmount").addClass('hide');
        $(".requiredValue,.notRequiredValue").val("");

        $("#headExpense").text(data.description);
        $("#headExpense").attr('id-expense',id);
        $("#headExpense").attr('gl',gl);
        var dynamicField = '';
        var dataExpenseScreen = data.expenseTypeScreen;
        dataExpenseScreen.sort(compareExpenseScreen);

        /** check io **/
        var checkRequriedIo = false;
        if(listConditionalIO){
            if(listConditionalIO[0].gl){
                $.each(listConditionalIO,function (index,item) {
                    if(item.gl.length == 1 && item.gl == '*'){
                        checkRequriedIo = true;
                        return;
                    }else{
                        var checkGl = item.gl.split("*");
                        if(($('#headExpense').attr('gl')).startsWith(checkGl[0])){
                            checkRequriedIo = true;
                            return;

                        }
                    }
                })
            }
        }

        /** fix screen **/
        if(data.fixCode){
            console.log(data.fixCode)
            $("div."+data.fixCode).removeClass('hide');
            $("."+data.fixCode+"_VALUE").addClass('use');
            $("."+data.fixCode+"_VALUE_OTHER").addClass('use');
            hideInternalOrder(false);

            if(checkRequriedIo){
                $("#spanInternalOrder").text("*");
                $("#internalOrder").addClass("requiredValue");
            }else{
                $("#spanInternalOrder").text("");
                $("#internalOrder").removeClass("requiredValue");
            }

            if(data.fixCode == "EXPF_004"){
                hideInternalOrder(true);
            }
        }else {
            hideInternalOrder(false);

            if (checkRequriedIo) {
                $("#spanInternalOrder").text("*");
                $("#internalOrder").addClass("requiredValue");
            } else {
                $("#spanInternalOrder").text("");
                $("#internalOrder").removeClass("requiredValue");
            }

            /************** render dynamic field *****************/
            $.each(dataExpenseScreen, function (index, item) {
                if ("String".toUpperCase() == item.type.toUpperCase()) {
                    if (item.require) {
                        if (item.structureField.indexOf("time") !== -1) {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border" type="text" disabled="true" id="' + item.structureField + '" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            listIdTime.push(item.structureField);
                        } else {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border" type="text" disabled="true" id="' + item.structureField + '" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        }
                    } else {
                        if (item.structureField.indexOf("time") !== -1) {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border" type="text" disabled="true" id="' + item.structureField + '" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            listIdTime.push(item.structureField);
                        } else {
                            dynamicField +=
                                '<div class="row">' +
                                '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                                '<div class="form-group has-info" >' +
                                '<div class="col-md-8" style="margin-bottom: 8px">' +
                                '<input class="form-control border" type="text" disabled="true" id="' + item.structureField + '" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        }
                    }
                } else if ("Date".toUpperCase() == item.type.toUpperCase()) {
                    listIdDate.push(item.structureField);
                    if (item.require) {
                        dynamicField +=
                            '<div class="row">' +
                            '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '&#160;&#160;<span style="color:red">*</span></label>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-md-8" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border" id="' + item.structureField + '" disabled="true" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                            '<span class="input-group-addon"><i class="fa fa-calendar fa-2x" style="color: black"><jsp:text/></i></span></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    } else {
                        dynamicField +=
                            '<div class="row">' +
                            '<label class="col-sm-4" style="padding-top:5px;">' + item.nameENG + '</label>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-md-8" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border" id="' + item.structureField + '" disabled="true" require="' + item.require + '" type="text" style="color: #03a9f4"/>' +
                            '<span class="input-group-addon"><i class="fa fa-calendar fa-2x" style="color: black"><jsp:text/></i></span></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    }
                }
            });
        }

        $("#dynamicField").append(dynamicField)


        /********** render expense type file ************/
        $("#expenseTypeFileRequired").empty();
        var dataExpenseTypeFile = data.expenseTypeFile;
        var dynamicFieldExpenseTypeFile = '';
        $.each(dataExpenseTypeFile,function (index,item) {
            $.each(MASTER_DATA.ATTACHMENT_TYPE,function (index2,item2) {
                if(item.attachmentTypeCode == item2.code){
                    dynamicFieldExpenseTypeFile +=
                        '<div class="col-sm-12">'+
                        '<div class="col-sm-1">'+
                        '<jsp:text/>'+
                        '</div>'+
                        '<div class="form-group has-info" >'+
                        '<div class="col-sm-4">'+
                        '<label class="col-sm-12" style="padding-top:5px;text-align: right">'+item2.description+'</label>'+
                        '</div>'+
                        '<div class="col-md-7" style="margin-bottom: 8px">'+
                        '<div class="input-group">'+
                        '<input class="form-control border" type="text" disabled="true"/>'+
                        '<input class="form-control hide" type="file"/>'+
                        '<span class="input-group-btn input-group-sm">'+
                        // '<a ><img src="'+$IMAGE_PAPER_CLIP+'" width="30px"/></a></span>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                }
            })
        });
        $("#expenseTypeFileRequired").append(dynamicFieldExpenseTypeFile);

    }
    $("#modalAddDetail").attr('typeMethod',type);
    $("#modalAddDetail").attr('idExpenseItem',idExpenseItem);
    if("edit" == type){
        renderDataForEditDocumentExpenseItem(data.fixCode,idExpenseItem);
    }

    $("#modalAddDetail").modal('show')
}

function renderDataForEditDocumentExpenseItem(fixCode,id) {
    var data = $.ajax({
        type: "GET",
        url: session['context']+'/expenseItem/'+id,
        headers: {
            Accept : "application/json"
        },
        async: false
    }).responseJSON;

    console.log(data)

    if(data != undefined){

        if(session.roleName.indexOf("ROLE_ACCOUNT") != -1){
            if(!data.netAmount){
                $("#netAmountModalAdd").autoNumeric('set',data.amount);
            }else{
                $("#netAmountModalAdd").autoNumeric('set',data.netAmount);
            }

            if(!data.vatCode){
                $("#vatType").val("VX");
            }else{
                $("#vatType").val(data.vatCode);
            }


        }else{
            $("#netAmountModalAdd").autoNumeric('set',data.netAmount);
            $("#vatType").val(data.vatCode);
        }

        $("#remark").val(data.remark);
        $("#sellerModalAdd").val(data.seller);
        $("#internalOrder").val(data.internalOrder);
        $("#docNumberModalAdd").val(data.docNumber);
        $("#documentDateModalAdd").val(DateUtil.coverDateToString(data.documentDate));
        $("#amountModalAdd").autoNumeric('set',data.amount);
        $("#taxIDNumberModalAdd").val(data.taxIDNumber);
        $("#branchPurchaserModalAdd").val(data.businessPlace);
        $("#branchModalAdd").val(data.branch);
        $("#addressModalAdd").val(data.address);
        $("#vatModalAdd").autoNumeric('set',data.vat);
        $("#taxModalAdd").autoNumeric('set',data.whtAmount);
        $("#whtType").val(data.whtType);
        $("#whtCode").val(data.whtCode);

        var listDocExpItemDetail =  Object.keys(data.docExpItemDetail);

        if(fixCode == "EXPF_003"){
            if(data.docExpItemDetail['place1'] == "อื่นๆ"){
                $("#place1_EXPF_003").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_003_other").removeClass('hide');
                $("#placeOther1_EXPF_003").val(data.docExpItemDetail['placeOther1']);
            }else{
                $("#place1_EXPF_003").val(data.docExpItemDetail['place1']);
            }
        }else if(fixCode == "EXPF_008"){
            if(data.docExpItemDetail['place1'] == "อื่นๆ"){
                $("#place1_EXPF_008").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_008_other").removeClass('hide');
                $("#placeOther1_EXPF_008").val(data.docExpItemDetail['placeOther1']);
            }else{
                $("#place1_EXPF_008").val(data.docExpItemDetail['place1']);
            }
        }else if(fixCode == "EXPF_001"){
            $("#liter_EXPF_001").val(data.docExpItemDetail['variable20'])
        }else if(fixCode == "EXPF_002"){
            $("#liter_EXPF_002").val(data.docExpItemDetail['variable20'])
        }else if(fixCode == "EXPF_004"){
            $("#date1EXPF_004").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_004").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#time1EXPF_004").val(data.docExpItemDetail['time1']);
            $("#time2EXPF_004").val(data.docExpItemDetail['time2']);
            $("#percentRateEXPF_004").val(data.docExpItemDetail['variable1']);
            $("#currencyRateEXPF_004").val(data.exchangeRate);
            $("#location_EXPF_004").val(data.docExpItemDetail['country']);

        }
        else if(fixCode == "EXPF_006") {
            $("#place1_EXPF_006").val(data.docExpItemDetail['place1']);
            $("#date1EXPF_006").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_006").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#time1EXPF_006").val(data.docExpItemDetail['time1']);
            $("#time2EXPF_006").val(data.docExpItemDetail['time2']);
            $("#percentEXPF_006").val(data.docExpItemDetail['variable1']);
        }else if(fixCode == "EXPF_005") {
            if (data.docExpItemDetail['place1'] == "0022") {
                $("#place1_EXPF_005").val(data.docExpItemDetail['place1']);
                $("#place1EXPF_005_other").removeClass('hide');
                $("#placeOther1_EXPF_005").val(data.docExpItemDetail['placeOther1']);
            } else {
                $("#place1_EXPF_005").val(data.docExpItemDetail['place1']);
            }

            if (data.docExpItemDetail['place2'] == "0022") {
                $("#place2_EXPF_005").val(data.docExpItemDetail['place2']);
                $("#place2EXPF_005_other").removeClass('hide');
                $("#placeOther2_EXPF_005").val(data.docExpItemDetail['placeOther2']);
            } else {
                $("#place2_EXPF_005").val(data.docExpItemDetail['place2']);
            }
        }else if(fixCode == "EXPF_007") {
            $("#date1EXPF_007").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));

            if(data.docExpItemDetail['variable4'] != null && data.docExpItemDetail['variable4'] != 0){
                $("#distance_EXPF_007").val(data.docExpItemDetail['variable4']);
                $(".EXPF_007_Distance").removeClass('hide');
            }

            $("#place1_EXPF_007").val(data.docExpItemDetail['place1']);

            if(data.docExpItemDetail['place2'] == "0022") {
                $("#place2_EXPF_007").val(data.docExpItemDetail['place2']);
                $("#place2EXPF_007_other").removeClass('hide');
                $("#placeOther2_EXPF_007").val(data.docExpItemDetail['placeOther2']);
            } else {
                $("#place2_EXPF_007").val(data.docExpItemDetail['place2']);
            }
        }else if(fixCode == "EXPF_010") {
            $("#place1_EXPF_010").val(data.docExpItemDetail['place1']);
            $("#place2_EXPF_010").val(data.docExpItemDetail['place2']);
            $("#month_EXPF_010").val(data.docExpItemDetail['month']);
            $("#year_EXPF_010").val(data.docExpItemDetail['variable19']);
        }else if(fixCode == "EXPF_012") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_012").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_012_other").removeClass('hide');
                $("#carLicenceOther_EXPF_012").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_012").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_013") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_013").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_013_other").removeClass('hide');
                $("#carLicenceOther_EXPF_013").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_013").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_014") {
            if (data.docExpItemDetail['carLicence'] == "CL002") {
                $("#carLicence_EXPF_014").val(data.docExpItemDetail['carLicence']);
                $("#carLicenceEXPF_014_other").removeClass('hide');
                $("#carLicenceOther_EXPF_014").val(data.docExpItemDetail['carLicenceOther']);
            } else {
                $("#carLicence_EXPF_014").val(data.docExpItemDetail['carLicence']);
            }
        }else if(fixCode == "EXPF_016") {
            $("#addEmployeeInputEmployeeAll").val(data.docExpItemDetail['employee']);
            if (data.docExpItemDetail['variable5'] == "SV002") {
                $("#variable5_EXPF_016").val(data.docExpItemDetail['variable5']);
                $("#EXPF_016_other").removeClass('hide');
                $("#other_EXPF_016").val(data.docExpItemDetail['variable18']);
            } else {
                $("#variable5_EXPF_016").val(data.docExpItemDetail['variable5']);
            }
        }else if(fixCode == "EXPF_011") {
            $("#liter_EXPF_011").val(data.docExpItemDetail['liter']);
            $("#month_EXPF_011").val(data.docExpItemDetail['month']);
            $("#year_EXPF_011").val(data.docExpItemDetail['variable19']);
        }else if(fixCode == "EXPF_009"){
            $("#date1EXPF_009").val(DateUtil.coverDateToString(data.docExpItemDetail['date1']));
            $("#date2EXPF_009").val(DateUtil.coverDateToString(data.docExpItemDetail['date2']));
            $("#hotelEXPF_009").val(data.docExpItemDetail['variable6']);
            $("#externalMemberBody").empty();
            $("#internalMemberBody").empty();
            findInternalMember();
            findExternalMember();
        }else if(fixCode == "EXPF_015"){
            $("#phoneNumber_EXPF_015").val(data.docExpItemDetail['phoneNumber']);
            $("#month_EXPF_015").val(data.docExpItemDetail['month']);
            $("#year_EXPF_015").val(data.docExpItemDetail['variable19']);
        }else{
            $.each(listDocExpItemDetail,function (index,item) {
                if(item == "date1" || item == "date2"){
                    $("#"+item).val(DateUtil.coverDateToString(data.docExpItemDetail[item]));
                }else{
                    $("#"+item).val(data.docExpItemDetail[item]);
                }

            });
        }

        if(data.docExpItemAttachment.length != 0){
            $("#expenseTypeFileRequired").empty();
            $.each(data.docExpItemAttachment,function (index,item) {
                $.each(MASTER_DATA.ATTACHMENT_TYPE,function (index2,item2) {
                    if(item.attachmentCode == item2.code) {
                        var flagEmed = item.flagEmed?item.flagEmed:"";
                        var docEmed = item.docEmed?item.docEmed:"";
                        var sequence = item.sequence?item.sequence:"";

                        $("#expenseTypeFileRequired").append(
                            '<div class="col-sm-12">' +
                            '<div class="col-sm-1">' +
                            '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+index+'" idDocumentExpenseItemAttachmentDownload="'+item.id+'" index="' + index + '" onclick="preViewAttachmentFileExpenseItem(\''+item.id+'\',\''+item.fileName+'\',\''+flagEmed+'\',\''+docEmed+'\',\''+sequence+'\')"/></a>' +
                            '</div>' +
                            '<div class="col-sm-1" style="padding-left: 10px;">' +
                            '<a ><img style="margin-left: 0px;" src="' + $IMAGE_DOWNLOAD + '" width="30px" id="downloadDocumentExpenseItemAttachment'+index+'" idDocumentExpenseItemAttachmentDownload="'+item.id+'" index="' + index + '" onclick="downloadAttachmentFileExpenseItem(\''+item.id+'\',\''+item.fileName+'\',\''+flagEmed+'\',\''+docEmed+'\',\''+sequence+'\')"/></a>' +
                            '</div>' +
                            '<div class="form-group has-info" >' +
                            '<div class="col-sm-4">' +
                            '<label class="col-sm-12" style="padding-top:5px;text-align: right">' + item2.description + '</label>' +
                            '</div>' +
                            '<div class="col-md-6" style="margin-bottom: 8px">' +
                            '<div class="input-group">' +
                            '<input class="form-control border" type="text" id="dynamicFileTxt' + (index) + '" index="' + index + '" value="'+item.fileName+'" disabled="true">'+
                            '<input class="form-control hide" type="file" id="dynamicFile' + (index) + '" name="uploadFileExpenseItem" uploadType="' + item.attachmentTypeCode + '"/>' +
                            '<span class="input-group-btn input-group-sm">' +
                            // '<a ><img src="' + $IMAGE_PAPER_CLIP + '" width="30px" id="dynamicFileBtn' + (index) + '" index="' + index + '" onclick="uploadFileExpenseItemDetail(this)"/></a>' +
                            '</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    }
                })
            })
        }
    }
}

function findInternalMember() {
    $("#internalMemberBody").empty();
    var dataInternalMember = $.ajax({
        url:session['context']+'/internalMembers/findByDocNumberAndItemId?docNumber='+$DATA_DOCUMENT.docNumber+"&itemId="+$('#modalAddDetail').attr('idexpenseitem'),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataInternalMember.content[0].id){
        var append = "";
        $.each(dataInternalMember.content,function (index,item) {
            append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                '<div class="col-xs-12 col-sm-12" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold">' +
                '' + findInternalMemberProfileByUserName(item.empUser) + '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountInternalAndExternal">' + NumberUtil.formatCurrency(item.cost) + '</span>' +''+
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        })

        $("#internalMemberBody").append(append);
    }
}

function findInternalMemberProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data){
        return data.FOA+data.FNameTH+" "+data.LNameTH;
    }else{
        return "";
    }
}

function findExternalMember() {
    $("#externalMemberBody").empty();
    var dataInternalMember = $.ajax({
        url:session['context']+'/externalMembers/findByDocNumberAndItemId?docNumber='+$DATA_DOCUMENT.docNumber+"&itemId="+$('#modalAddDetail').attr('idexpenseitem'),
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(dataInternalMember.content[0].id){
        var append = "";
        $.each(dataInternalMember.content,function (index,item) {
            append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                '<div class="col-xs-12 col-sm-12" style="padding-right: 0;padding-left: 0;font-size: 14px;font-weight: bold">' +
                '' + item.name + '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountInternalAndExternal">' + NumberUtil.formatCurrency(item.cost) + '</span>' +''+
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        });

        $("#externalMemberBody").append(append);
    }

}

function compareExpenseScreen(a,b) {
    if (a.id < b.id)
        return -1;
    if (a.id > b.id)
        return 1;
    return 0;
}

function getAuthorizeForLineApprove(userName){

    var listJsonDetails = [];

    $.each($("[name=summaryAmount]"),function (index,item) {
        var flowType = $("#"+item.id).attr("flowtype");
        var jsonDetails = {};
        jsonDetails['amount'] = $("#sumAllExpenseAmount").autoNumeric('get')==0?1000:$("#sumAllExpenseAmount").autoNumeric('get');
        jsonDetails['flowType'] = flowType;
        listJsonDetails.push(jsonDetails);
        jsonDetails['costCenter']  = $DATA_DOCUMENT.costCenterCode;
    });

    var jsonData = {};
    jsonData['requester'] = userName;
    jsonData['approveType'] = null;
    jsonData['papsa'] = $("#comAgencyLabel").attr('pa')+"-"+$("#departmentLabel").attr('psa');
    jsonData['details'] = JSON.stringify(listJsonDetails);

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/getAuthorizeForLineApprove',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                renderLineApprove($DATA_LINE_APPROVE);
            }
        }
    });
}

function renderLineApproveAcc(requestApprover){
    $("#lineApproverDetail").empty();
    // $("#lineApproveMobile").empty();

    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;

    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle-complete" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterName+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requesterPosition+'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    for(var i=0;i<requestApprover.length;i++){
        findEmployeeProfileByUserNameLineApprove(requestApprover[i].userNameApprover);
        $("#lineApproverDetail").append(
            '<li class='+validateStatus(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
            '<span class="step"><a><span>'+ validateStatusReject(requestApprover[i].actionState,requestApprover[i].actionReasonDetail)+'</span></span>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionStateName+'</div>'+
            '<div style="color: blue;">'+nvl(requestApprover[i].approver)+'</div>'+
            '<div style="color: lightseagreen;">'+nvl($DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH)+'</div>'+
            '</span>'+
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
            '<div class="panel-heading '+validateStatusMobile(requestApprover[i].requestStatusCode)+'" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
            '<div class="container-fluid">'+
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
            '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
            '</div>'+
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+requestApprover[i].actionStateName+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl(requestApprover[i].approver)+'</b></label>'+
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+nvl($DATA_EMPLOYEE_FOR_LINE_APPROVE.PositionTH)+'</b></label>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    }

}

function renderLineApprove(requestApprover){
    console.log(requestApprover.length + 1);
    var requesterName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
    var requesterPosition = $DATA_EMPLOYEE.PositionTH;
    var dataStep = requestApprover.length + 1;
    $("#lineApproverDetail").empty();
    $("#lineApproverDetail").attr('data-steps',dataStep);

    $("#lineApproverDetail").append(

        '<li class="idle" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+requesterName+'</div>'+
        '<div style="color: lightseagreen;">'+requesterPosition+'</div>'+
        '</span>'+
        '</li>'
    );
    for(var i=0;i<requestApprover.length;i++){
        $("#lineApproverDetail").append(

            '<li class="idle" style="text-align: center;">'+
            '<span class="step"><span>'+ validateStatusReject(requestApprover[i].actionRoleCode,requestApprover[i].actionReasonDetail)+'</span></span>'+
            '<span class="name-idle">'+
            '<div>'+requestApprover[i].actionRoleName+'</div>'+
            '<div style="color: blue;">'+requestApprover[i].name+'</div>'+
            '<div style="color: lightseagreen;">'+requestApprover[i].position+'</div>'+
            '</span>'+
            '</li>'
        );
    }
}

function validateIMGACC(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}
}

function validateStatusMobile(status) {
    if(status != null){
        return "collapseLightGreen";
    }else{
        return "collapseLightBlue";
    }
}

function validateStatus(status){
    if(status != null && status != "REJ"){
        return "idle-complete";
    }else if(status != null && status == "REJ"){
        checkStatusReject++;
        return "idle";
    }else{
        checkStatus++;
        return "idle";
    }
}

function validateStatusReject(actionState,detail){
    if(checkStatus === 1 && checkStatusReject === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')" title="View Remark" class="animation-border two"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }else if(checkStatusReject === 1 && checkStatus === 0){
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"  title="View Remark" class="animation-border three"   src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }else{
        return '<img onclick="remarkRequestDetail(\''+nvl(detail)+'\')" title="View Remark" src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></a>';
    }
}

function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINACE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}
}

function validateDocStatus(){
    /* update by siriradC.  2017.08.04 */
    if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }
}

function findEmployeeProfileByUserName(userName){
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;

    $("#empNameLabel").text(data.FOA+data.FNameTH+" "+data.LNameTH);
    $("#empCodeLabel").text(data.Personal_ID);
    $("#positionLabel").text(data.PositionTH);
    $("#comapnyModalAddExpenseItem").text(data.Personal_PA_ID+" "+data.Personal_PA_Name);
    $("#costCenterLabel").text($DATA_DOCUMENT.costCenterCode);

    if($DATA_DOCUMENT.companyCode){
        findByPaCode($DATA_DOCUMENT.companyCode);
    }

    if($DATA_DOCUMENT.psa){
        findByPsaCode($DATA_DOCUMENT.psa);
    }

    setTimeout(function (){
        $('.dv-background').hide();
    },1000);
}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        $("#comAgencyLabel").text(paData.PaCode+" : "+paData.PaNameTh);
        $("#comAgencyLabel").attr('pa',paData.PaCode);
    }else{
        $("#comAgencyLabel").text("-");
        $("#comAgencyLabel").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        $("#departmentLabel").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        $("#departmentLabel").attr('psa',psaData.PsaCode);
    }else{
        $("#departmentLabel").text("-");
        $("#departmentLabel").attr('psa',"");
    }
}

function findCreatorProfileByUserName(userName){
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#creator").text(data.FOA+data.FNameTH+" "+data.LNameTH);
}

function findEmployeeProfileByUserNameLineApprove(userName){
    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE_FOR_LINE_APPROVE = data;
}

function initExpenseDetail() {
    $("#docNumberLabel").text($DATA_DOCUMENT.docNumber);
    $("#docDateLabel").text($DATA_DOCUMENT.createdDate);
    $("#documentApproveInputDocRefAppForExpense").attr('disabled',true);
    $("#documentAdvanceInputDocRefAdvForExpense").attr('disabled',true);
    $("#documentAdvanceBtn").attr('onclick','');
    $("#documentApproveBtn").attr('onclick','');

    if($DATA_DOCUMENT.documentReference.length != 0){
        $.each($DATA_DOCUMENT.documentReference,function (index,item) {
            if(item.documentTypeCode == "ADV"){
                $("#divBill").removeClass('hide');
                $("#documentAdvanceBtn").attr('onclick','viewDetailDocumentReferenceDocRefAdv(this)');
                $("#documentAdvanceBtn").attr('data-attn',item.docReferenceNumber);
                $("#documentAdvanceInputDocRefAdvForExpense").val(item.docReferenceNumber);
            }
            if(item.documentTypeCode == "APP"){
                $("#documentApproveBtn").attr('onclick','viewDetailDocumentReference(this)');
                $("#documentApproveBtn").attr('data-attn',item.docReferenceNumber);
                $("#documentApproveInputDocRefAppForExpense").val(item.docReferenceNumber);
            }
        })
    }
}

function checkBtn() {

    if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        console.log("======== DRAFT ========");
        $("[name=in]").addClass('hide');
        $("[name=out]").addClass('hide');
        $("[name=view]").removeClass('hide');
    }else if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        console.log("======== CANCEL ========");
        $("[name=in]").addClass('hide');
        $("[name=out]").addClass('hide');
        $("[name=view]").removeClass('hide');
    }else if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        console.log("======== ON PROCESS ========");
        var checkState = false;
        var checkRoleFinance = false;

        if($DATA_REQUEST_APPROVER){
            $.each($DATA_REQUEST_APPROVER,function (index,item) {
                if(item.requestStatusCode == null && session.userName == item.userNameApprover && item.actionState != 'ACC'){
                    checkState = true;
                }
                if(session.userName == item.userNameApprover && item.actionState == 'FNC'){
                    checkRoleFinance= true;
                }
            });
        }

        if(checkState){
            if(checkRoleFinance){
                $("[name=in]").addClass('hide');
                $("[name=out]").removeClass('hide');
                $("[name=view]").addClass('hide');
            }else{
                $("#actionReasonDiv").removeClass('hide');
                $("[name=in]").removeClass('hide');
                $("[name=out]").addClass('hide');
                $("[name=view]").addClass('hide');
            }
        }else{
            if (session.roleName.indexOf("ROLE_ACCOUNT") != -1) {
                $("[name=Acc]").removeClass('hide');
            }else{
                $("[name=Acc]").addClass('hide');
            }
            $("[name=in]").addClass('hide');
            $("[name=out]").addClass('hide');
            $("[name=view]").removeClass('hide');
        }
    }else if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        console.log("======== REJECT ========");
        $("[name=in]").addClass('hide');
        $("[name=out]").addClass('hide');
        $("[name=view]").removeClass('hide');
    }else if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        console.log("======== COMPLETE ========");
        $("[name=in]").addClass('hide');
        $("[name=out]").addClass('hide');
        $("[name=view]").removeClass('hide');
    }
    
}

function renderExpenseType() {
    var dataDocumentExpenseGroup = $DATA_DOCUMENT.documentExpense==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseGroup;
    $("#accordion1").empty();
    $("#docNumberLabel").text($DATA_DOCUMENT.docNumber);
    $("#docDateLabel").text($DATA_DOCUMENT.createdDate);

    var costCenter = $("#costCenterLabel").text();
    var charIndex4 = costCenter.charAt(3);

    if("" != dataDocumentExpenseGroup){
        $.each(dataDocumentExpenseGroup,function (index,item) {
            var gl = charIndex4 == "9" || costCenter == '1017920010'?item.expenseTypeByCompany.expenseTypes.headOfficeGL:item.expenseTypeByCompany.expenseTypes.factoryGL;
            $("#accordion1").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseGray" id="collapseHeaderDetail" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseExpense'+item.id+'" aria-expanded="false" aria-controls="collapseExpense'+item.id+'" style="">'+
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">'+
                '<div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0;padding-left: 0">'+
                '<img src="'+$IMAGE_EXPENSE+'" width="35px"/>'+
                '</div>'+
                '<div class="col-xs-10 col-sm-11" style="padding-right: 0;padding-left: 0">'+
                '<a class="collapsed" style="color:#ffffff;font-size: 13px" >'+item.expenseTypeByCompany.expenseTypes.description+'</a>'+'<sup><img width="20px;" height="20px" src="'+IMG.CLICK+'" /></sup>'+'<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="summaryAmount" id-expense-com="'+item.expenseTypeByCompany.id+'" id="summaryAmount'+item.expenseTypeByCompany.expenseTypes.code+'" flowType="'+item.expenseTypeByCompany.expenseTypes.flowType+'"><jsp:text/></span><br/>'+
                '<a class="collapsed" style="color:#ffffff;font-size: 13px" >'+gl+'</a>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div id="collapseExpense'+item.id+'" class="panel-collapse collapse in" role="tabpanel">'+
                '<div class="panel-body panel-white-perl">'+
                '<div class="row">'+
                '<div class="form-horizontal">'+
                '<div class="form-group">'+
                '<div class="container-fluid">'+
                '</div>'+
                '</div>'+
                '<div class="form-group" id="body'+item.expenseTypeByCompany.expenseTypes.code+'" name="bodyDocExpenseItem">'+
                '<jsp:text/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            )

            $("#summaryAmount"+item.expenseTypeByCompany.expenseTypes.code).text(0);
            $("#summaryAmount"+item.expenseTypeByCompany.expenseTypes.code).autoNumeric('init')
        });

        renderDocumentExpenseItem();
    }

}

function compare(a,b) {
    if(a){
        if (a.expenseTypeByCompanys.expenseTypes.code < b.expenseTypeByCompanys.expenseTypes.code)
            return -1;
        if (a.expenseTypeByCompanys.expenseTypes.code > b.expenseTypeByCompanys.expenseTypes.code)
            return 1;
        return 0;
    }
}

function renderDocumentExpenseItem() {
    var data = $.ajax({
        url: session.context + "/expenseItem/findDocumentExpenseItemByDocumentExp?id="+$DATA_DOCUMENT.documentExpense.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false

    }).responseJSON;

    var costCenter = $("#costCenterLabel").text();
    var charIndex4 = costCenter.charAt(3);

    var dataExpense = data.content;
    $('div[name=bodyDocExpenseItem]').empty();
    var sumAmount = 0;
    var append = '';
    var id;
    dataExpense.sort(compare);
    $.each(dataExpense,function (index,item) {
        var gl = charIndex4 == "9" || costCenter == '1017920010'?item.expenseTypeByCompanys.expenseTypes.headOfficeGL:item.expenseTypeByCompanys.expenseTypes.factoryGL;
        if(item.expenseTypeByCompanys) {
            var externalNumber = item.externalDocNumber?item.externalDocNumber:"";
            var externalPaymentNumber = item.externalPaymentDocNumber?item.externalPaymentDocNumber:"";
            var externalClearingNumber = item.externalClearingDocNumber?item.externalClearingDocNumber:"";
            var textFI = "";
            if(externalNumber){
                if(textFI){
                    textFI = textFI+" , "+externalNumber;
                }else{
                    textFI = externalNumber;
                }
            }
            if(externalPaymentNumber){
                if(textFI){
                    textFI = textFI+" , "+externalPaymentNumber;
                }else{
                    textFI = externalPaymentNumber;
                }
            }
            if(externalClearingNumber){
                if(textFI){
                    textFI = textFI+" , "+externalClearingNumber;
                }else{
                    textFI = externalClearingNumber;
                }
            }

            var fullTextFi = textFI?"("+textFI+")":"";
            if (id == undefined || id == item.expenseTypeByCompanys.expenseTypes.code) {
                var netAmount = item.netAmount;
                var whtAmount = 0;
                if(item.whtAmount){
                    whtAmount = item.whtAmount;
                }
                sumAmount += (netAmount+whtAmount);
                id = item.expenseTypeByCompanys.expenseTypes.code;
                append += '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                    '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                    '<div class="col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '</div>' +
                    '<div class="col-sm-11" style="padding-right: 0;padding-left: 0;color: black;font-size: 14px" onclick="renderModalExpense(\'' + item.expenseTypeByCompanys.expenseTypes.id + '\',\'' + "edit" + '\',\'' + item.id + '\',\''+gl+'\')">' +
                    '' + item.remark +" "+fullTextFi+ '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountExpenseItem">' + (netAmount+whtAmount) + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                if (index == dataExpense.length - 1) {
                    $("#body" + id).append(append);
                    $("#summaryAmount" + id).text(sumAmount);
                    $("#summaryAmount" + id).autoNumeric('init');
                    $("[name=amountExpenseItem]").autoNumeric('init');
                    $("#summaryAmount" + id).autoNumeric('set',sumAmount);
                }
            } else {
                $("#body" + id).append(append);
                $("#summaryAmount" + id).text(sumAmount);
                $("#summaryAmount" + id).autoNumeric('init');
                $("[name=amountExpenseItem]").autoNumeric('init');
                $("#summaryAmount" + id).autoNumeric('set',sumAmount);

                var netAmount = item.netAmount;
                var whtAmount = 0;
                sumAmount = 0;
                if(item.whtAmount){
                    whtAmount = item.whtAmount;
                }
                sumAmount = (netAmount+whtAmount);
                id = item.expenseTypeByCompanys.expenseTypes.code;
                append = '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                    '<div class="panel-heading collapseOrange" id="collapseHeaderDetail" role="button" data-toggle="collapse" aria-expanded="false">' +
                    '<div class="container-fluid" style="padding-right: 0;padding-left: 0">' +
                    '<div class="col-sm-1 text-center" style="padding-right: 0;padding-left: 0">' +
                    '</div>' +
                    '<div class="col-sm-11" style="padding-right: 0;padding-left: 0;color: black" onclick="renderModalExpense(\'' + item.expenseTypeByCompanys.expenseTypes.id + '\',\'' + "edit" + '\',\'' + item.id + '\',\''+gl+'\')">' +
                    '' + item.remark +" "+fullTextFi+ '<span class="pull-right"  style="color:#03a9f4;font-size: 20px" name="amountExpenseItem">' + (netAmount+whtAmount) + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                if (index == dataExpense.length - 1) {
                    $("#body" + id).append(append);
                    $("#summaryAmount" + id).text(sumAmount);
                    $("#summaryAmount" + id).autoNumeric('init');
                    $("[name=amountExpenseItem]").autoNumeric('init');
                    $("#summaryAmount" + id).autoNumeric('set',sumAmount);
                }
            }
        }else{
            $("#summaryAmount" + id).text(0);
            $("#summaryAmount" + id).autoNumeric('init');
            $("#summaryAmount" + id).autoNumeric('set',0);
        }

    });
    sumAllAmountExpense()

}

function sumAllAmountExpense() {
    sumAll = 0
    $("#sumAllExpenseAmount").empty();
    $.each($('[name=summaryAmount]'),function (index,item) {
        console.log(item.id)
        var sum = $("#"+item.id)==undefined?0:parseFloat($("#"+item.id).autoNumeric('get'));
        sumAll += sum;
    })
    console.log(sumAll);
    $("#sumAllExpenseAmount").text(sumAll)
    $("#sumAllExpenseAmount").autoNumeric('init');
    $("#sumAllExpenseAmount").autoNumeric('set',sumAll);
}

function genQRCode() {
    let user_account;

    $DATA_REQUEST_APPROVER.forEach(function (item) {
        if(item.actionState.indexOf("ACC") >= 0){
            user_account = item.userNameApprover;
        }
    })
    window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
}

function documentAttachment(){
    getDataAttachmentType();
    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}

function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/expense/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            var $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItemAttachment(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                // '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button></td>' +
                '</tr>'
            );

            if("M151" == dataDocumentAttachment[i].attachmentType){
                $("#billUploadTxt").text(dataDocumentAttachment[i].fileName);
            }
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
            return MASTER_DATA.ATTACHMENT_TYPE[i].description;
            break;
        }
    }
}

function cancelExpense(){
    var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelRequest(){

    console.info("cancel Request");
    $('.dv-background').show();
    var listJsonDetails = [];
    $.each($("[name=summaryAmount]"),function (index,item) {
        var flowType = $("#"+item.id).attr("flowtype");
        var jsonDetails = {};
        jsonDetails['amount'] = $("#sumAllExpenseAmount").autoNumeric('get')==0?1000:$("#sumAllExpenseAmount").autoNumeric('get');
        jsonDetails['flowType'] = flowType;
        listJsonDetails.push(jsonDetails);
    });

    var jsonData = {};
    jsonData['id'] = $DATA_DOCUMENT.id;
    jsonData['requester'] = $DATA_DOCUMENT.requester;
    jsonData['docNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['details'] = JSON.stringify(listJsonDetails);

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/cancelRequest',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_REQUEST = JSON.parse(xhr.responseText);
                    if($DATA_REQUEST != null){
                        cancelDocument($DATA_REQUEST.document.id);
                    }
                    $('.dv-background').hide();
                }
            }
        });
    },1000);

}

function cancelDocument(){
    var jsonData = {};
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']    			= $DATA_DOCUMENT.id;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

    var dataDocument = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/updateDocumentStatus',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                dataDocument = JSON.parse(xhr.responseText);
                window.location.href = session.context+'/expense/expenseDetail?doc='+$DATA_DOCUMENT.id;
            }
        }
    });
}

function findRequestByDocument(id){
    var dataRequest = $.ajax({
        url: session.context + "/approve/findRequestByDocument/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST = dataRequest;

    var dataRequestApprover = $.ajax({
        url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST_APPROVER = dataRequestApprover;
    renderLineApproveAcc($DATA_REQUEST_APPROVER)
}

function approveDocument(){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = $USERNAME;
    jsonData['actionStatus'] = validateActionState($USERNAME);
    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/approveRequest',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_APPROVE = JSON.parse(xhr.responseText);

                    if($DATA_APPROVE != null || $DATA_APPROVE != undefined){
                        window.location.href = session.context;
                    }
                    $('.dv-background').hide();
                }
            }
        });
    },1000);
    
}

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-55) + 'px';
    }
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function beforeRejectRequest() {

    var actionReasonCode = $("#actionReason")[0].selectedOptions[0].value;
    var actionReasonDetail = $("#actionReasonDetail").val();

    if(actionReasonCode == "" || actionReasonDetail == ""){
        $('#warningModal .modal-body').html(MSG.MESSAGE_INPUT_ACTION_REASON);
        $("#warningModal").modal('show');
    }else{
        var warningMessage = MSG.MESSAGE_CONFIRM_REJECT_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

        $('#confirmRejectModal .modal-body').html(warningMessage);
        $('#confirmRejectModal').modal('show');
    }

}

function rejectRequest(){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = $USERNAME;
    jsonData['actionStatus'] = validateActionState($USERNAME);
    jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
    jsonData['docType'] = $DATA_DOCUMENT.documentType;
    jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
    jsonData['processId'] = $DATA_DOCUMENT.processId;
    jsonData['documentId'] = $DATA_DOCUMENT.id;
    jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
    jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

    setTimeout(function () {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+$DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        } else {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/rejectRequest',
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },1000);
}

function backToMenu(){

    window.location.href = session.context+'/expense/expenseMainMenu';

}

function nvl(e){
    if(e == null || e == undefined){
        return "";
    }else{
        return e;
    }
}

function downloadAttachmentFileExpenseItem(id,fileName,flagEmed,docEmed,seq) {

    if(flagEmed != "Y") {
        window.location.href = session['context'] + '/expenseItem/downloadDocumentExpItemAttachment?id=' + id + '&fileName=' + fileName;
    }else{
        window.location.href = $URL_EMED+"?docNo="+docEmed+"&seq="+seq;
    }

}


function preViewAttachmentFileExpenseItem(id,fileName,flagEmed,docEmed,seq) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1];

    if(flagEmed != "Y") {
        if (fileType == 'pdf' || fileType == 'PDF' || fileType == 'txt' || fileType == 'TXT') {
            var url = session['context'] + '/expenseItem/preViewPDFDocumentExpItemAttachment?id=' + id + '&fileName=' + fileName

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }

        if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpg' || fileType == 'JPG' || fileType == 'jpeg' || fileType == 'JPEG') {

            var urlIMG = session['context']+'/expenseItem/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName;

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(urlIMG, '', properties);
        }
    }else{
        var url = $URL_EMED+"?docNo="+docEmed+"&seq="+seq;
        if (fileType == 'pdf' || fileType == 'PDF' || fileType == 'txt' || fileType == 'TXT') {

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }

        if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpg' || fileType == 'JPG' || fileType == 'jpeg' || fileType == 'JPEG') {

            var properties;
            var positionX = 0;
            var positionY = 0;
            var width = 0;
            var height = 0;
            properties = "width=" + width + ", height=" + height;
            properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
            properties += ", top=" + positionY + ", left=" + positionX;
            properties += ", fullscreen=1";
            window.open(url, '', properties);
        }
    }



}

function findConditionalIObyCompanyCode(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/conditionalIOs/findByCompanyCode?code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);
                    var item = obj.content
                    listConditionalIO = item
                }
            }
        }
    }).done(function (){
        //close loader
    });

}

function copyDocument(){
    var jsonDocument = {};
    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']   			= $DATA_DOCUMENT.id;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/copyDocument',
        data:jsonDocument,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                window.location.href = session.context+'/expense/clearExpenseDetail?doc='+xhr.responseJSON.id;
            }
        }
    });
}

function hideInternalOrder(check) {
    if(check){
        $("#rowInternalOrder").addClass('hide');
        $("#internalOrder").removeClass('requiredValue');
        $("#internalOrder").removeClass('use');
    }else{
        $("#rowInternalOrder").removeClass('hide');
        $("#internalOrder").addClass('requiredValue');
        $("#internalOrder").addClass('use');
    }
}

function downloadDocumentFile(btn){
    var id = btn.attr('id');
    var fileName = btn.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}


function remarkRequestDetail(txt){

    console.log('>>Message Reason>>>'+txt)

    if(txt != null && txt != '' && txt!= undefined && txt != 'null'){

        $("#alertModal").modal('show');
        $("label[id=detailAlert]").html('Remark : '+txt);
    }

}