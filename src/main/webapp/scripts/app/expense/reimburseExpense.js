
var listData = [];
var listId = [];
var $DATA_EMPLOYEE;
var checkCostCenterBoolean = true;

function addOtherExpense() {
    var data = {};
    var id = $("#clearExpenseInputExpense").attr("data-expense-company-id");
    var gl = $("#clearExpenseInputExpense").attr("data-gl");
    var desc = $("#clearExpenseInputExpense").attr("data-description");
    var text = $("#clearExpenseInputExpense").val();

    if(text != ""){
        data.id = id;
        data.gl = gl;
        data.desc = desc;
        listData.push(data);

        $("#clearExpenseInputExpense").val("");
        $("#clearExpenseInputExpense").attr("data-expense-id","");
        $("#clearExpenseInputExpense").attr("data-gl","");
        $("#clearExpenseInputExpense").attr("data-expense-company-id","");
        $("#clearExpenseInputExpense").attr("data-description","");
        renderItemExpense();
    }
}

function removeDuplicates(arr) {
    return arr.reduce(function (p, c) {

        // create an identifying id from the object values
        var id = [c.id, c.gl, c.desc].join('|');

        // if the id is not found in the temp array
        // add the object to the output array
        // and add the key to the temp array
        if (p.temp.indexOf(id) === -1) {
            p.out.push(c);
            p.temp.push(id);
        }
        return p;

        // return the deduped array
    }, { temp: [], out: [] }).out;
}

function renderItemExpense() {
    listData = removeDuplicates(listData);
    if(listData.length > 0){
        $("#renderItem").empty();
        $.each(listData,function (index,item) {
            $("#renderItem").append(''+
                // '<div class="col-md-12" >'+
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px">'+
                '<div class="container-fluid" style="padding-left: 0;padding-right: 0">'+
                '<div class="col-xs-2 col-md-2 text-right" style="padding-left: 0;padding-right: 0">'+
                '<a href="javascript:void(0)"><img src="'+$IMAGE_EXPENSE+'" width="45px"/></a>&#160;&#160;'+
                '</div>'+
                '<div class="col-xs-7 col-xs-offset-1" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 14px"><b>'+item.desc+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 14px"><b>'+item.gl+'</b></label>'+
                '</div>'+
                '<div class="col-xs-2" style="padding-left: 0;padding-right: 0">'+
                '<a onclick="deleteExpense('+index+')"><img src="'+$IMAGE_DELETE+'" width="45px"/></a>&#160;&#160;'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
                // '</div>'
            )
        })
    }else{
        $("#renderItem").empty();
    }
}

function addTop20(btn) {
    var data = {};
    var id = $("#"+btn.id).attr("data-expense-company-id");
    var gl = $("#"+btn.id).attr("data-gl");
    var desc = $("#"+btn.id).attr("desc");

    data.id = id;
    data.gl = gl;
    data.desc = desc;
    listData.push(data);
    renderItemExpense();
}

function deleteExpense(item) {
    listData.splice(item, 1);
    listId.splice(item,1);
    renderItemExpense();
}

function saveClearExpense() {
    $('.dv-background').show();
    var jsonDocument = {};
    var jsonDocumentExpense = {};
    var jsonDocumentExpenseItem = {};
    var costCenter = $("#costCenter").val();

    jsonDocumentExpense['createdDate'] = new Date().format('yyyy-mm-dd HH:mm:ss');

    jsonDocument['parentId']     	    = 1;
    jsonDocument[csrfParameter]  	    = csrfToken;
    jsonDocument['documentType']        = MASTER_DATA.DOC_TYPE;
    jsonDocument['documentStatus']      = MASTER_DATA.DOC_STATUS;
    jsonDocument['requester']           = $("#empNameInputEmployee").attr('data-userName');
    jsonDocument['companyCode']         = $("#company").attr('pa');
    jsonDocument['departmentCode']      = $("#empNameInputEmployee").attr('data-deptCode');
    jsonDocument['personalId']          = $("#empNameInputEmployee").attr('data-personalId');
    jsonDocument['psa']                 = $("#department").attr('psa');
    jsonDocument['costCenterCode']      = costCenter;
    jsonDocument['documentExpense']     = JSON.stringify(jsonDocumentExpense);
    listId = [];
    if(listData.length != 0){
        $.each(listData,function (index,item) {
            listId.push(item.id);
        })
    }
    jsonDocumentExpenseItem['expenseItem'] = listId.toString();

    setTimeout(function () {
        if(checkCostCenterBoolean) {
            if (listId.length != 0 && $("#empNameInputEmployee").val() != "" && costCenter != "") {
                $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context'] + '/expense/saveDocument',
                    data: jsonDocument,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            var dataDocument = JSON.parse(xhr.responseText);

                            jsonDocumentExpenseItem['document'] = dataDocument.id;

                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/expense/saveDocumentExpenseItem',
                                data: jsonDocumentExpenseItem,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + dataDocument.id;
                                        $('.dv-background').show();
                                    }
                                }
                            });
                        }
                    }
                });
            } else {

                var messageRequired = "";
                if ("" == $("#empNameInputEmployee").val()) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_CLEAR_EXPENSE.LABEL_EMPLOYEE_NAME + "<br/>";
                }
                if (listId.length == 0) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_CLEAR_EXPENSE.LABEL_EXPENSE_TYPE + "<br/>";
                }
                if ("" == costCenter) {
                    messageRequired += "&#8195;&#8195;&#8195;" + LB_CLEAR_EXPENSE.LABEL_COST_CENTER + "<br/>";
                }

                $('#warningModal .modal-body').html(MSG_CLEAR_EXPENSE.MESSAGE_REQUIRE_FIELD + "<br/>" + messageRequired);
                $('#warningModal').modal('show');
                $('.dv-background').hide();
            }
        }else{
            $('#warningCostCenterModal .modal-body').html(MSG_CLEAR_EXPENSE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
            $("#warningCostCenterModal").modal('show');
            $('.dv-background').hide();
        }
    },1000);

}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;
}

function initExpense(callback) {
    findEmployeeProfileByUserName(session.userName);
    $("#empNameInputEmployee").val($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
    setTimeout(function () {
        AutocompleteEmployee.setId('empNameInputEmployee');
        AutocompleteEmployee.search("empNameInputEmployee",$USERNAME);
        AutocompleteEmployee.renderValue($DATA_EMPLOYEE.User_name.toLocaleLowerCase());
        $("#empNameInputEmployee").blur();
        callback();
    },1000);

}

function renderTop20Favorite(callback) {

    var dataExpenseFavorite =  $.ajax({
        url: session.context + "/expenseByCompanies/findExpenseTypeByCompanyByPaAndPsaAndFavorite/" + $("#company").attr('pa') + '/' + $("#department").attr('psa')+'/'+session.userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    var costCenter = $("#costCenter").val();
    var charIndex4 = costCenter.charAt(3);

    $("#top20Body").empty();
    $.each(dataExpenseFavorite,function (index,item) {
        if(item.expenseTypes){
            if(item.expenseTypes.favorite){
                var gl = charIndex4 == "9" || costCenter == '1017920010'?item.expenseTypes.headOfficeGL:item.expenseTypes.factoryGL;
                $("#top20Body").append('' +
                    '<div class="col-md-12">'+
                    '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                    '<div class="panel-heading collapseGray" id="collapseHeaderDetail1" role="button" data-toggle="collapse" href="#collapseDetail1" aria-expanded="false" aria-controls="collapseDetail1" style="padding-top: 5px;padding-bottom: 5px">'+
                    '<div class="container-fluid" style="padding-left: 0;padding-right: 0">'+
                    '<div class="col-xs-2 col-md-2 text-right" style="padding-left: 0;padding-right: 0">'+
                    '<a href="javascript:void(0)"><img src="'+$IMAGE_EXPENSE+'" width="45px"/></a>&#160;&#160; </div>'+
                    '<div class="col-xs-7 col-xs-offset-1" style="padding-left: 0;padding-right: 0">'+
                    '<label class="col-xs-12" style="color: #ffffff;padding-left: 0;font-size: 14px">'+item.expenseTypes.description+'</label>'+
                    '<label class="col-xs-12" style="color: #ffffff;padding-left: 0;font-size: 14px">'+gl+'</label>'+
                    '</div>'+
                    '<div class="col-xs-2" style="padding-left: 0;padding-right: 0">'+
                    '<a onclick="addTop20(this)" id="top20_'+item.id+'" data-expense-company-id="'+item.id+'" data-expense-id="'+item.expenseTypes.id+'" data-gl="'+gl+'" desc="'+item.expenseTypes.description+'"><img src="'+$IMAGE_ARROW+'" width="45px"/></a>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        }
    });
    callback();
}

function checkCostCenter() {
    var errorMessage = '';
    var costCenter = $("#costCenter").val();
    if(costCenter != "" && costCenter != $("#empNameInputEmployee").attr('data-costCenter')){

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data) {
            var costCenterSplit = data.profile.split('#');
            if (costCenterSplit[2] != "0") {
                errorMessage += MSG_CLEAR_EXPENSE.MESSAGE_COST_CENTER_NOT_AFFILIATED + "<br/>";
                checkCostCenterBoolean = true;
                $('#warningCostCenterModal .modal-body').html(errorMessage);
                $("#warningCostCenterModal").modal('show');

                var paPsaSplit = costCenterSplit[2].split('-');
                var index1 = paPsaSplit.length-7!=0?paPsaSplit.length-7:0;
                var index2 = paPsaSplit.length-6!=0?paPsaSplit.length-6:0;
                var index3 = paPsaSplit.length-5!=0?paPsaSplit.length-5:0;
                var index4 = paPsaSplit.length-4!=0?paPsaSplit.length-4:0;
                var index5 = paPsaSplit.length-3!=0?paPsaSplit.length-3:0;

                var pa1 = index1 > 0?paPsaSplit[index1]:"";
                var pa2 = index2 > 0?paPsaSplit[index2]:"";
                var pa3 = index3 > 0?paPsaSplit[index3]:"";
                var pa4 = index4 > 0?paPsaSplit[index4]:"";
                var pa5 = index5 > 0?paPsaSplit[index5]:"";

                var text= pa1+pa2+pa3+pa4+pa5;

                $("#company").text(paPsaSplit[0] + " : " + text);
                $("#company").attr('pa', paPsaSplit[0]);
                // $("#department").text(paPsaSplit[paPsaSplit.length -2] + " : " + paPsaSplit[paPsaSplit.length -1]);
                // $("#department").attr('psa', paPsaSplit[paPsaSplit.length -2]);

            } else {
                $("#company").text("-");
                $("#company").attr('pa',"");
                // $("#department").text("-");
                // $("#department").attr('psa',"");
                errorMessage += MSG_CLEAR_EXPENSE.MESSAGE_COST_CENTER_DOES_NOT_EXIST + "<br/>";
                checkCostCenterBoolean = false;
                $('#warningCostCenterModal .modal-body').html(errorMessage);
                $("#warningCostCenterModal").modal('show');
            }
        }
    }else{
        if(costCenter == ""){
            $("#company").text("-");
            $("#company").attr('pa',"");
            // $("#department").text("-");
            // $("#department").attr('psa',"");
            checkCostCenterBoolean = false;
        }

        if(costCenter == $("#empNameInputEmployee").attr('data-costCenter')){
            checkCostCenterBoolean = true;
            var data = $.ajax({
                url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(data){

                var costCenterSplit = data.profile.split('#');
                if(costCenterSplit[2] != "0"){
                    var paPsaSplit = costCenterSplit[2].split('-');
                    var index1 = paPsaSplit.length-7!=0?paPsaSplit.length-7:0;
                    var index2 = paPsaSplit.length-6!=0?paPsaSplit.length-6:0;
                    var index3 = paPsaSplit.length-5!=0?paPsaSplit.length-5:0;
                    var index4 = paPsaSplit.length-4!=0?paPsaSplit.length-4:0;
                    var index5 = paPsaSplit.length-3!=0?paPsaSplit.length-3:0;

                    var pa1 = index1 > 0?paPsaSplit[index1]:"";
                    var pa2 = index2 > 0?paPsaSplit[index2]:"";
                    var pa3 = index3 > 0?paPsaSplit[index3]:"";
                    var pa4 = index4 > 0?paPsaSplit[index4]:"";
                    var pa5 = index5 > 0?paPsaSplit[index5]:"";

                    var text= pa1+pa2+pa3+pa4+pa5;

                    $("#company").text(paPsaSplit[0] + " : " + text);
                    $("#company").attr('pa', paPsaSplit[0]);
                    // $("#department").text(paPsaSplit[paPsaSplit.length -2] + " : " + paPsaSplit[paPsaSplit.length -1]);
                    // $("#department").attr('psa', paPsaSplit[paPsaSplit.length -2]);

                }else{
                    $("#company").text("-");
                    $("#company").attr('pa',"");
                    // $("#department").text("-");
                    // $("#department").attr('psa',"");

                    $('#warningCostCenterModal .modal-body').html(MSG_CLEAR_EXPENSE.MESSAGE_COST_CENTER_DOES_NOT_EXIST);
                    $("#warningCostCenterModal").modal('show');
                    checkCostCenterBoolean = false;
                    $('.dv-background').hide();
                }

            }
        }
    }
}

$(document).ready(function () {
    $('.dv-background').show();
    readDataZip(function () {
        readMasterDataDocType();
        readMasterDataDocStatus();
        readMasterDataAttachmentType();
        initExpense(function () {
            // AutocompleteExpense.init("clearExpenseInputExpense", $("#empNameInputEmployee").attr('data-compCode'), $("#empNameInputEmployee").attr('data-psa'), "","","", session.userName);
            setTimeout(function () {
                checkCostCenter();
                renderTop20Favorite(function () {
                    $('.dv-background').hide();
                });
            },1000);
        });
    });

    $("#empNameInputEmployee").on('keyup',function(){
        if($("#empNameInputEmployee").val() != ""){
            AutocompleteEmployee.setId('empNameInputEmployee');
            AutocompleteEmployee.search("empNameInputEmployee",$("#empNameInputEmployee").val());
        }
    });

    $("#empNameInputEmployee").on('blur',function (){
        $("#empCode").text($("#empNameInputEmployee").attr('data-personalid'));
        $("#position").text($("#empNameInputEmployee").attr('data-position'));
        // $("#company").text($("#empNameInputEmployee").attr('data-compCode')+" : "+$("#empNameInputEmployee").attr('data-compName'));
        $("#department").text($("#empNameInputEmployee").attr('data-psa')+" : "+$("#empNameInputEmployee").attr('data-psaname'));
        $("#costCenter").val($("#empNameInputEmployee").attr('data-costCenter'));
        $("#department").attr('psa',$("#empNameInputEmployee").attr('data-psa'));
        // $("#company").attr('pa',$("#empNameInputEmployee").attr('data-compcode'));

    });

    $("#clearExpenseInputExpense").on('keyup',function(){
        AutocompleteExpense.init("clearExpenseInputExpense",$("#company").attr('pa'),$("#department").attr('psa'),$("#clearExpenseInputExpense").val(),$("#clearExpenseInputExpense").val(),$("#clearExpenseInputExpense").val(),session.userName,$("#costCenter").val());
    });

    $("#costCenter").on('blur',function () {
        $('.dv-background').show();
        setTimeout(function () {
            checkCostCenter();
            // AutocompleteExpense.init("clearExpenseInputExpense",$("#company").attr('pa'),$("#department").attr('psa'),"","","",session.userName);
            renderTop20Favorite(function () {
                $('.dv-background').hide();
            })
        },1000)

    });
});