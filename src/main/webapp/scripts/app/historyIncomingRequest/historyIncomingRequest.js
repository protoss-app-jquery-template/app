let object = $.extend({},UtilPagination);
let DOCUMENT_TYPE;
let checkWidth;

$(document).ready(function () {

    readDataZip(function () {
        readMasterDataDocAppType();
        readMasterDataDocExpType();
        readMasterDataDocAdvType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataAprTypeSetDomestic();
        readMasterDataAprTypeSetForeign();
    });

    $('.dv-background').show();
    window.setTimeout(function () {

        $('#doc_status_row').empty();
        $('#doc_status_row').append(
            '<div class="form-horizontal">' +
            '<div class="col-sm-12" style="padding-right: 0px;">'+
            '<input type="hidden" id="search_doc_status_all" />'+
            '<input type="hidden" class="form-control" style="color: #ff0000;"  id="search_doc_status" value="${criteria_documentStatus}"/>'+
            '</div>'+
            '<div class="col-sm-offset-1 col-sm-2">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_DRAFT + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT  + '"/> '+ $LABEL_DRAFT_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_ON_PROCESS+ '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '"/> '+ $LABEL_ON_PROCESS_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_CANCEL + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL  + '"/> '+ $LABEL_CANCEL_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_REJECT + '" value="' + MASTER_DATA.DOC_STATUS_REJECT  + '"/> '+ $LABEL_REJECT_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_COMPLETE + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE  + '"/> ' + $LABEL_COMPLETE_STATUS +'</label>'+
            '</div>'+
            '</div>'+
            '<a title="' + $MESSAGE_SEARCH + '" onclick="searchHistory()"><img src="' + $SEARCH + '" width="70px"/>&#160;</a>'+
            '</div>'
        );

        $('#checkbox_modal').append(
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_DRAFT  + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT + '"/> ' + $LABEL_DRAFT_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS+ '"/> ' + $LABEL_ON_PROCESS_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_CANCEL  + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL + '"/> ' + $LABEL_CANCEL_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_REJECT  + '" value="' + MASTER_DATA.DOC_STATUS_REJECT + '"/> ' + $LABEL_REJECT_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_COMPLETE  + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE + '"/> ' + $LABEL_COMPLETE_STATUS + '</label>'+
            '</div>'+
            '</div>'
        )


        $.material.init();

        $(".checkbox_status").prop('checked', true);
        $(".checkbox_status1").prop('checked',true);

        if($HIS_CODE == 'APP')
            DOCUMENT_TYPE = MASTER_DATA.DOC_APP_TYPE;
        else
            DOCUMENT_TYPE = MASTER_DATA.DOC_ADV_TYPE;

        searchHistory();

        $('.dv-background').hide();
    },2000);





    if($HIS_CODE == 'APP'){
        $('#approve').removeClass('hide')
        $('#advance').addClass('hide');
        $('#expense').addClass('hide');

        $("#expense_mobile_button").hide();
        $("#advance_mobile_button").hide();

        $("#approve_mobile").removeClass('hide')
        $("#advance_mobile").addClass('hide');
        $("#expense_mobile").addClass('hide');

        // searchHistory();
    }else{

        $("#buttonBar").empty();

        $("#buttonBar").append(''+
            '<div title="' + $LABEL_DOCUMENT_ADVANCE + '" id="divADV" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 0px; padding:6px; position: fixed; border-radius:8px; top:160px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t1.png" id="btnADV" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>' +
            '<div title="' + $LABEL_DOCUMENT_EXPENSE + '" id="divEXP" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 80px; padding:6px; position: fixed; border-radius:8px; top:165px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t3.png" id="btnEXP" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>');
        moveFixedActionButton();

        if($HIS_CODE == 'ADV'){
            $('#approve').addClass('hide');
            $('#advance').removeClass('hide');
            $('#expense').addClass('hide');

            $("#divADV").attr('disabled',true);
            $('#btnADV').css('filter','grayscale(0%)');

            $("#divEXP").attr('disabled',false);
            $('#btnEXP').css('filter','grayscale(100%)');

            $("#approve_mobile").addClass('hide');
            $("#advance_mobile").removeClass('hide');
            $("#expense_mobile").addClass('hide');
            // searchHistory();
        }else {
            $("#divEXP").attr('disabled',true);
            $('#btnEXP').css('filter','grayscale(0%)');

            $("#divADV").attr('disabled',false);
            $('#btnADV').css('filter','grayscale(100%)');

            $('#approve').addClass('hide');
            $('#advance').addClass('hide');
            $('#expense').removeClass('hide');

            $("#approve_mobile").addClass('hide');
            $("#advance_mobile").addClass('hide');
            $("#expense_mobile").removeClass('hide');
            // searchHistory();
        }
    }

    $("#btnADV").on('click',function () {
        $("#divADV").attr('disabled',true);
        $("#divEXP").attr('disabled',false);

        $('#btnADV').css('filter','grayscale(0%)');
        $('#btnEXP').css('filter','grayscale(100%)');

        DOCUMENT_TYPE = MASTER_DATA.DOC_ADV_TYPE;

        $("#advance").removeClass("hide");
        $("#expense").addClass("hide");
        $("#approve").addClass("hide");

        window.setTimeout(function () {
            searchHistory();
        },500);

    });

    $("#btnEXP").on('click',function () {
        $("#divADV").attr('disabled',false);
        $("#divEXP").attr('disabled',true);

        $('#btnEXP').css('filter','grayscale(0%)');
        $('#btnADV').css('filter','grayscale(100%)');

        DOCUMENT_TYPE = MASTER_DATA.DOC_EXP_TYPE;

        $("#advance").addClass("hide");
        $("#expense").removeClass("hide");
        $("#approve").addClass("hide");


        window.setTimeout(function () {
            searchHistory();
        },500);
    })

    $('#expense_mobile_button').on('click',function () {
        DOCUMENT_TYPE = MASTER_DATA.DOC_EXP_TYPE;

        $("#approve_mobile").addClass('hide')
        $("#advance_mobile").addClass('hide');
        $("#expense_mobile").removeClass('hide');

        window.setTimeout(function () {
            searchHistory();
        },500);
    })
    $('#advance_mobile_button').on('click',function () {
        DOCUMENT_TYPE = MASTER_DATA.DOC_ADV_TYPE;

        $("#approve_mobile").addClass('hide')
        $("#advance_mobile").removeClass('hide');
        $("#expense_mobile").addClass('hide');

        window.setTimeout(function () {
            searchHistory();
        },500);
    })

    // $(window).resize(function () {
    //     moveFixedActionButton();
    //
    //     if(!checkWidth){
    //         if(!($(window).width >= 1300)){
    //             checkWidth = true;
    //             searchMyApproveRequestMobile();
    //         }
    //     }else {
    //         if(!($(window).width < 1300)){
    //             checkWidth = false;
    //             searchHistory();
    //         }
    //     }
    //
    // })

    // $('#paggingSearchMainPC').click(function () {
    //     // $('.dv-background').show();
    //     $(this).find("a").click(function(){
    //         // $('.dv-background').show();
    //     })
    // })
});


function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.left = (xPos-75) + 'px';
    }
}

function searchHistory() {
    $('.dv-background').show();

    window.setTimeout(function(){
        var documentStatus = "";
        $.each($(".checkbox_status:checked"), function (i, obj) {
            documentStatus += "," + obj.value;
        });


        if (documentStatus) {
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status').val(documentStatus);
        } else {
            $('#search_doc_status').val($('#search_doc_status_all').val());
        }

        if($('#search_doc_status').val() == ""){
            $(".checkbox_status").prop('checked',true);
        }

        let data = {
            approver: $USERNAME,
            docNumber: $(docNumber_input).val() ? $(docNumber_input).val() : '',
            titleDescription: $(titleDescription_input).val() ? $(titleDescription_input).val() : '',
            docStatus: $('#search_doc_status').val() == "" ?  "null" : $('#search_doc_status').val(),
            docType: DOCUMENT_TYPE
        }

        try {
            findByCriteria(data);
        }catch (e){
            console.error(e);
        }


    },500);
}

function searchMyApproveRequestMobile(){
    $('.dv-background').show();

    window.setTimeout(function(){
        let documentStatus = "";
        $.each( $( ".checkbox_status1:checked" ), function( i, obj ) {
            documentStatus += ","+obj.value;
        });

        if(documentStatus){
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status_mobile').val(documentStatus);
        }else{
            $('#search_doc_status_mobile').val($('#search_doc_status_all_mobile').val());
        }

        if($('#search_doc_status_mobile').val() == ""){
            $(".checkbox_status1").prop('checked',true);
        }

        let criteriaObject = {
            approver: $USERNAME,
            docNumber           : $("#input_docNumber_mobile").val() ? ("#input_docNumber_mobile").val() : '',
            titleDescription    :$("#input_titleDescription_mobile").val() ? $("#input_titleDescription_mobile").val() : '',
            docType        :  DOCUMENT_TYPE,
            docStatus        : $("#search_doc_status_mobile").val()==""?"null":$("#search_doc_status_mobile").val()
            // projection : "haveDoc"
        };

        findByCriteria(criteriaObject);

    },500);
}

function findByCriteria(criteriaObject) {

    if($(window).width() < 1300){
        // paggingSearchMainMobile
        checkWidth = true;
        object.setId("#paggingSearchMainMobile");
    }else{
        checkWidth = false;
        object.setId("#paggingSearchMainPC");
    }

    // let urlSearch;
    if($HIS_CODE  == MASTER_DATA.DOC_APP_TYPE){
        // urlSearch = '/historyApprove/approve';
        object.setUrlData("/historyApprove/approve");
        object.setUrlSize("/historyApprove/approveSize");
    }else{
        // urlSearch = '/historyExpense/expense';
        object.setUrlData("/historyExpense/expense");
        object.setUrlSize("/historyExpense/expenseSize");
    }


    // $.ajax({
    //     method: "GET",
    //     url: session.context + urlSearch,
    //     data: criteriaObject,
    //     async : false,
    //     complete: function (xhr) {
    //         if (xhr.responseText) {
    //
    //             let item = JSON.parse(xhr.responseText);
    object.loadTable = function(items) {
        let item = items;
        let record = '';
        let record_mobile = '';

        let to_day;
        let $TODAYS = new Date();
        let days = $TODAYS.getDate();
        let month = $TODAYS.getMonth() + 1; //January is 0!
        let year = $TODAYS.getFullYear();
        let hours = $TODAYS.getHours();
        let minutes = $TODAYS.getMinutes();
        let start_time;
        let end_time;

        if (days < 10)
            days = '0' + days;
        if (month < 10)
            month = '0' + month;
        if (hours < 10)
            hours = '0' + hours;
        if (minutes < 10)
            minutes = '0' + minutes;
        if (days < 10)
            days = days;

        to_day = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes;

        $('#body_record').empty();
        $('#mobile_content').empty();

        if (item) {
            let summaryDay;
            for (let i = 0; i < item.length; i++) {
                summaryDay = 0;
                record = '';
                record_mobile = '';
                if (item[i].id && item[i].request) {
                    let approvers = item[i].request.requestApprovers;

                    let sendDate = new Date(item[i].sendDate);
                    let sendDate_day = sendDate.getDate();
                    let sendDate_month = sendDate.getMonth() + 1;
                    let sendDate_Year = sendDate.getFullYear();
                    let sendDate_Hour = sendDate.getHours();
                    let sendDate_Minutes = sendDate.getMinutes();

                    sendDate_month < 10 ? sendDate_month = '0' + sendDate_month : sendDate_month;
                    sendDate_Hour < 10 ? sendDate_Hour = '0' + sendDate_Hour : sendDate_Hour;
                    sendDate_Minutes < 10 ? sendDate_Minutes = '0' + sendDate_Minutes : sendDate_Minutes;
                    sendDate_day < 10 ? sendDate_day = '0' + sendDate_day : sendDate_day;

                    let send_date = sendDate_Year + '-' + sendDate_month + '-' + sendDate_day + ' ' + sendDate_Hour + ':' + sendDate_Minutes;

                    record += '<div class="row">' +
                        '<div class=col-sm-12>' +
                        '<div class="col-sm-1 text-center">' +
                        '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + DOCUMENT_TYPE + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')"><img src="' + $MENU_BUTTON + '" width="40px"/></a>' +
                        '</div>' +
                        '<div class="col-sm-2 text-center">' +
                        '<div class="col-sm-12 text-center" style="font-weight: 900; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].docNumber + '">' +
                        item[i].docNumber +
                        '</div>' +
                        '<div class="col-sm-12 text-center" style="font-weight: 900; color: green; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;" title="' + item[i].titleDescription + '">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-3 text-center">' +
                        '<div class="col-sm-12 text-center" style="font-weight: 900;">' +
                        (item[i].requesterName ? item[i].requesterName : '-') +
                        '</div>' +
                        '<div class="col-sm-12 text-center" style="font-weight: 900; color: deepskyblue">' +
                        send_date +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-4 text-center" style="padding-left: 0px; padding-right: 0px;">' +
                        '<ol class="progress" data-steps="' + approvers.length + '" style="height: 100%; padding-right: 0px;padding-left: 0px;">';

                    record_mobile += '' +
                        '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-top: 10px;">' +
                        '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                        '<div class="col-xs-12" style="text-align: left; display: inline-block; vertical-align: middle; padding-left: 3px; padding-right: 3px; font-size: 17px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                        '<div style="padding-left: 12px; display: inline-block;">' + item[i].docNumber + '</div>' +
                        '</div>' +
                        '<div class="col-xs-8" style="font-size: 17px; color:#52F7FC; display: inline-block; text-align: left; padding-right: 3px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                        '<div style="display: inline-block">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '</div>' +
                        '<div id="sumProcessDay_mobile' + i + '" class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 3px; padding-left: 0px;">' +
                        '<jsp:text/>' +
                        '</div>' +
                        '</a>' +
                        '</div>' +
                        '<div id="detail' + i + '" class="panel-collapse collapse" role="detail' + i + '">' +
                        '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px;  background-color: white; text-align: center;">' +
                        '<div style="margin-top: 8px; height: 50px; width: 100%; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                        '<div class="col-xs-12" style="font-size: 17px; text-align: left">' +
                        $LABEL_REQUESTER +
                        '</div>' +
                        '<div class="col-xs-12" style="font-size: 17px; color:deepskyblue; text-align: right; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction:rtl;">' +
                        (item[i].requesterName ? item[i].requesterName : '-') +
                        '</div>' +
                        '</div>';


                    let differentDay = 0;
                    let checkActive = 0;

                    if (approvers) {
                        approvers.sort(function (a, b) {
                            return a.id > b.id;
                        });

                        let action_date;
                        let action_month;
                        let action_year;
                        let action_hour;
                        let action_minute;
                        let action_time;
                        let tempTime;

                        if (approvers[0].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT) {

                            tempTime = new Date(approvers[0].actionTime);
                            action_date = tempTime.getDate();
                            action_month = tempTime.getMonth() + 1;
                            action_year = tempTime.getFullYear();
                            action_hour = tempTime.getHours();
                            action_minute = tempTime.getMinutes();

                            action_hour < 10 ? action_hour = '0' + action_hour : action_hour;
                            action_minute < 10 ? action_minute = '0' + action_minute : action_minute;
                            action_month < 10 ? action_month = '0' + action_month : action_month;
                            action_date < 10 ? action_date = '0' + action_date : action_date;

                            action_time = action_year + '-' + action_month + '-' + action_date + ' ' + action_hour + ':' + action_minute;

                            start_time = moment(send_date, moment.ISO_8601);
                            end_time = moment(action_time, moment.ISO_8601);

                            differentDay = end_time.diff(start_time, 'days');

                            summaryDay += differentDay;

                            record += '' +
                                '<li class="done" style="text-align: left;">' +
                                '<div style="text-align: center">' +
                                '<span class="step">' +
                                '<span>' +
                                approvers[0].actionState +
                                '</span>' +
                                '</span>' +
                                '<span class="name">' + differentDay + '</span>' +
                                '</div>' +
                                '</li>';

                            record_mobile += '' +
                                '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ff734a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                '<div class="circle" style="background-color: #ff2d25">' +
                                '<a style="font-weight: 600">' +
                                approvers[0].actionState +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                approvers[0].actionStateName +
                                '</div>' +
                                '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                // action_time.substring(0,action_time.lastIndexOf(':')) +
                                action_time +
                                '</div>' +
                                '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color: transparent">' +
                                differentDay + ' Day' +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>';
                        } else if (approvers[0].actionTime) {

                            tempTime = new Date(approvers[0].actionTime);
                            action_date = tempTime.getDate();
                            action_month = tempTime.getMonth() + 1;
                            action_year = tempTime.getFullYear();
                            action_hour = tempTime.getHours();
                            action_minute = tempTime.getMinutes();

                            action_hour < 10 ? action_hour = '0' + action_hour : action_hour;
                            action_minute < 10 ? action_minute = '0' + action_minute : action_minute;
                            action_month < 10 ? action_month = '0' + action_month : action_month;
                            action_date < 10 ? action_date = '0' + action_date : action_date;

                            action_time = action_year + '-' + action_month + '-' + action_date + ' ' + action_hour + ':' + action_minute;

                            start_time = moment(send_date, moment.ISO_8601);
                            end_time = moment(action_time, moment.ISO_8601);

                            differentDay = end_time.diff(start_time, 'days');
                            summaryDay += differentDay;

                            record += '' +
                                '<li class="done" style=text-align:center;">' +
                                '<div style="text-align:center">' +
                                '<span class="step">' +
                                '<span>' +
                                approvers[0].actionState +
                                '</span>' +
                                '</span>' +
                                '<span class="name">' +
                                differentDay +
                                '</span>' +
                                '</div>' +
                                '</li>';

                            record_mobile += '' +
                                '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                '<div class="circle" style="background-color: #47e833">' +
                                '<a style="font-weight: 600">' +
                                approvers[0].actionState +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                approvers[0].actionStateName +
                                '</div>' +
                                '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                action_time +
                                '</div>' +
                                '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                end_time.diff(start_time, 'days') + ' Day' +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>';

                        } else {


                            end_time = moment(to_day, moment.ISO_8601);
                            start_time = moment(send_date, moment.ISO_8601);
                            differentDay = end_time.diff(start_time, 'days');
                            summaryDay += differentDay;

                            record += '' +
                                '<li class="active" style=text-align:left;">' +
                                '<div style="text-align:center">' +
                                '<span class="step">' +
                                '<span>' +
                                approvers[0].actionState +
                                '</span>' +
                                '</span>' +
                                '<span class="name">' + differentDay + '</span>' +
                                '</div>' +
                                '</li>'

                            record_mobile += '' +
                                '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                '<div class="circle" style="background-color: #ff9800">' +
                                '<a style="font-weight: 600">' +
                                approvers[0].actionState +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                approvers[0].actionStateName +
                                '</div>' +
                                '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                approvers[0].actionTime +
                                '</div>' +
                                '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                end_time.diff(start_time, 'days') + ' Day' +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>';
                        }

                        for (let j = 1; j < approvers.length; j++) {

                            if (approvers[j].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT) {
                                tempTime = new Date(approvers[j - 1].actionTime);
                                action_date = tempTime.getDate();
                                action_month = tempTime.getMonth() + 1;
                                action_year = tempTime.getFullYear();
                                action_hour = tempTime.getHours();
                                action_minute = tempTime.getMinutes();

                                action_hour < 10 ? action_hour = '0' + action_hour : action_hour;
                                action_minute < 10 ? action_minute = '0' + action_minute : action_minute;
                                action_month < 10 ? action_month = '0' + action_month : action_month;
                                action_date < 10 ? action_date = '0' + action_date : action_date;

                                action_time = action_year + '-' + action_month + '-' + action_date + ' ' + action_hour + ':' + action_minute;

                                let actionTimeNow = new Date(approvers[j].actionTime);
                                let actionTimeNow_date = actionTimeNow.getDate() < 10 ? '0' + actionTimeNow.getDate() : actionTimeNow.getDate();
                                let actionTimeNow_month = actionTimeNow.getMonth() + 1 < 10 ? '0' + (actionTimeNow.getMonth() + 1) : actionTimeNow.getMonth() + 1;
                                let actionTimeNow_year = actionTimeNow.getFullYear();
                                let actionTimeNow_hour = actionTimeNow.getHours() < 10 ? '0' + actionTimeNow.getHours() : actionTimeNow.getHours();
                                let actionTimeNow_minute = actionTimeNow.getMinutes() < 10 ? '0' + actionTimeNow.getMinutes() : actionTimeNow.getMinutes();

                                actionTimeNow = actionTimeNow_year + '-' + actionTimeNow_month + '-' + actionTimeNow_date + ' ' + actionTimeNow_hour + ':' + actionTimeNow_minute;

                                start_time = moment(action_time, moment.ISO_8601);
                                end_time = moment(actionTimeNow, moment.ISO_8601);

                                differentDay = end_time.diff(start_time, 'days');
                                summaryDay += differentDay;

                                record +=
                                    '<li class="reject" style="text-align: left;">' +
                                    '<div style="text-align: center">' +
                                    '<span class="step">' +
                                    '<span>' +
                                    approvers[j].actionState +
                                    '</span>' +
                                    '</span>' +
                                    '<span class="name">' + differentDay + '</span>' +
                                    '</div>' +
                                    '</li>';


                                record_mobile += '' +
                                    '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ff734a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                    '<div class="circle" style="background-color: #ff2d25">' +
                                    '<a style="font-weight: 600">' +
                                    approvers[j].actionState +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                    approvers[j].actionStateName +
                                    '</div>' +
                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                    // action_time.substring(0,action_time.lastIndexOf(':')) +
                                    action_time +
                                    '</div>' +
                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; ">' +
                                    differentDay + ' Day' +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>' +
                                    '</div>';


                            } else if (approvers[j].actionTime) {
                                tempTime = new Date(approvers[j - 1].actionTime);
                                action_date = tempTime.getDate();
                                action_month = tempTime.getMonth() + 1;
                                action_year = tempTime.getFullYear();
                                action_hour = tempTime.getHours();
                                action_minute = tempTime.getMinutes();

                                action_hour < 10 ? action_hour = '0' + action_hour : action_hour;
                                action_minute < 10 ? action_minute = '0' + action_minute : action_minute;
                                action_month < 10 ? action_month = '0' + action_month : action_month;
                                action_date < 10 ? action_date = '0' + action_date : action_date;

                                action_time = action_year + '-' + action_month + '-' + action_date + ' ' + action_hour + ':' + action_minute;

                                let actionTimeNow = new Date(approvers[j].actionTime);
                                let actionTimeNow_date = actionTimeNow.getDate() < 10 ? '0' + actionTimeNow.getDate() : actionTimeNow.getDate();
                                let actionTimeNow_month = actionTimeNow.getMonth() + 1 < 10 ? '0' + (actionTimeNow.getMonth() + 1) : actionTimeNow.getMonth() + 1;
                                let actionTimeNow_year = actionTimeNow.getFullYear();
                                let actionTimeNow_hour = actionTimeNow.getHours() < 10 ? '0' + actionTimeNow.getHours() : actionTimeNow.getHours();
                                let actionTimeNow_minute = actionTimeNow.getMinutes() < 10 ? '0' + actionTimeNow.getMinutes() : actionTimeNow.getMinutes();

                                actionTimeNow = actionTimeNow_year + '-' + actionTimeNow_month + '-' + actionTimeNow_date + ' ' + actionTimeNow_hour + ':' + actionTimeNow_minute;

                                start_time = moment(action_time, moment.ISO_8601);
                                end_time = moment(actionTimeNow, moment.ISO_8601);

                                differentDay = end_time.diff(start_time, 'days');
                                summaryDay += differentDay;

                                record += '' +
                                    '<li class="done" style="text-align: center;">' +
                                    ' <div style="text-align: center">' +
                                    ' <span class="step">' +
                                    ' <span>' +
                                    approvers[j].actionState +
                                    '</span>' +
                                    ' </span>' +
                                    ' <span class="name">' +
                                    differentDay +
                                    '</span>' +
                                    '</div>' +
                                    '</li>';

                                record_mobile += '' +
                                    '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                    '<div class="circle" style="background-color: #47e833">' +
                                    '<a style="font-weight: 600">' +
                                    approvers[j].actionState +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left">' +
                                    approvers[j].actionStateName +
                                    '</div>' +
                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                    // action_time.substring(0,action_time.lastIndexOf(':')) +
                                    action_time +
                                    '</div>' +
                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                    differentDay + ' Day' +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>' +
                                    '</div>';


                            } else if ((approvers[j - 1].actionTime && approvers[j - 1].requestStatusCode != MASTER_DATA.DOC_STATUS_REJECT) && !approvers[j].actionTime) {

                                let previousActionTime = new Date(approvers[j - 1].actionTime);
                                let previousDay = previousActionTime.getDate() < 10 ? '0' + previousActionTime.getDate() : previousActionTime.getDate();
                                let previousMonth = (previousActionTime.getMonth() + 1) < 10 ? '0' + (previousActionTime.getMonth() + 1) : previousActionTime.getMonth() + 1;
                                let previousYear = previousActionTime.getFullYear();
                                let previousHour = previousActionTime.getHours() < 10 ? '0' + previousActionTime.getHours() : previousActionTime.getHours();
                                let previousMinute = previousActionTime.getMinutes() < 10 ? '0' + previousActionTime.getMinutes() : previousActionTime.getMinutes();

                                previousActionTime = previousYear + '-' + previousMonth + '-' + previousDay + ' ' + previousHour + ':' + previousMinute;

                                // let actionTimeNow = new Date(approvers[j].actionTime);
                                // let actionTimeNow_date = actionTimeNow.getDate();
                                // let actionTimeNow_month = actionTimeNow.getMonth() + 1 < 10 ? '0' + (actionTimeNow.getMonth() + 1) : actionTimeNow.getMonth() + 1 ;
                                // let actionTimeNow_year = actionTimeNow.getFullYear();
                                // let actionTimeNow_hour = actionTimeNow.getHours() < 10 ? '0' + actionTimeNow.getHours() : actionTimeNow.getHours();
                                // let actionTimeNow_minute = actionTimeNow.getMinutes()  < 10 ? '0' + actionTimeNow.getMinutes() : actionTimeNow.getMinutes();
                                //
                                // actionTimeNow = actionTimeNow_year + '-' + actionTimeNow_month + '-' + actionTimeNow_date + ' ' + actionTimeNow_hour + ':' + actionTimeNow_minute;

                                end_time = moment(to_day, moment.ISO_8601);

                                start_time = moment(previousActionTime, moment.ISO_8601);
                                differentDay = end_time.diff(start_time, 'days');
                                summaryDay += differentDay;

                                record +=
                                    '<li class="active" style="text-align: left;">' +
                                    '<div style="text-align: center">' +
                                    '<span class="step">' +
                                    '<span>' +
                                    approvers[j].actionState +
                                    '</span>' +
                                    '</span>' +
                                    '<span class="name">' + differentDay + '</span>' +
                                    '</div>' +
                                    '</li>';


                                record_mobile += '' +
                                    '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                    '<div class="circle" style="background-color: #ff9800">' +
                                    '<a style="font-weight: 600">' +
                                    approvers[j].actionState +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                    approvers[j].actionStateName +
                                    '</div>' +
                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                    // to_day.substring(0,to_day.lastIndexOf(':')) +
                                    to_day +
                                    '</div>' +
                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                    differentDay + ' Day' +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>' +
                                    '</div>';

                            } else {
                                record += '' +
                                    '<li>' +
                                    '<span class="step">' +
                                    '<span>'
                                    + approvers[j].actionState +
                                    '</span>' +
                                    '</span>' +
                                    '<span class="name" style="color: transparent">' +
                                    'Waiting' +
                                    '</span>' +
                                    '</li>';


                                record_mobile += '' +
                                    '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                    '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">' +
                                    '<div class="circle" style="background-color: #c0c0c0">' +
                                    '<a style="font-weight: 600">' +
                                    approvers[j].actionState +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '<a data-toggle="collapse" data-target="#detail' + i + '">' +
                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                    approvers[j].actionStateName +
                                    '</div>' +
                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                    // DateUtil.coverDateToString(objectRequestApprover_Mobile[j].createdDate) +
                                    '</div>' +
                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                    '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color:transparent">' +
                                    // diffDays + ' Day' +
                                    '</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>' +
                                    '</div>';
                            }

                        }
                    }

                    record += '' +
                        '</ol>' +
                        '</div>' +
                        '<div class="col-sm-2 text-center" style="font-weight: 900; font-size: 50px;">' +
                        summaryDay +
                        '</div>' +
                        '</div>' +
                        '</div><hr style="margin-top: 10px; margin-bottom: 10px;"/>';

                    record_mobile += '' +
                        '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')"/>';


                    record_mobile += '</div>' +
                        '</div>';

                    $('#body_record').append(record);
                    $('#mobile_content').append(record_mobile);
                    $('#sumProcessDay_mobile' + i).append(summaryDay + ' Day');

                }
            }
        }
        $('.dv-background').hide();
    }


            // }
    //     }
    //     // }
    // });


    object.setDataSearch(criteriaObject);
    object.search(object);
    // object.loadPage(($CRITERIA_PAGE*1)+1,object);


}


function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    return data.FOA+data.FNameTH+" "+data.LNameTH;
}


function ViewDoc(id,documentType,approveType,createdBy) {
    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/approve/viewCreateDocDetail?doc=' + id;
        }
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id
    }
}
