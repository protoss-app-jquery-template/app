
var object = $.extend({},UtilPagination);
var code;
var pageType;
var $JSON;
var sumDateProcess = 0;
var startCount_Request=111
let $APPROVERS;
var objectRequestApprover_Mobile;
let checkWidth;

$(document).ready(function () {

    readDataZip(function () {
        readMasterDataDocAppType();
        readMasterDataDocExpType();
        readMasterDataDocAdvType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataAprTypeSetDomestic();
        readMasterDataAprTypeSetForeign();
    });


    $('.dv-background').show();
    setTimeout(function () {

        $('#doc_status_row').empty();
        $('#doc_status_row').append(
            '<div class="form-horizontal">' +
                '<div class="col-sm-12" style="padding-right: 0px;">'+
                    '<input type="hidden" id="search_doc_status_all" />'+
                    '<input type="hidden" class="form-control" style="color: #ff0000;"  id="search_doc_status" value="${criteria_documentStatus}"/>'+
                '</div>'+
                // '<div class="col-sm-offset-1 col-sm-2">'+
                //     '<div class="checkbox checkbox-info">'+
                //         '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_DRAFT + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT  + '"/> '+ $LABEL_DRAFT_STATUS + '</label>'+
                //     '</div>'+
                // '</div>'+
                // '<div class="col-sm-2">'+
                //     '<div class="checkbox checkbox-info">'+
                //         '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_ON_PROCESS+ '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '"/> '+ $LABEL_ON_PROCESS_STATUS + '</label>'+
                //     '</div>'+
                // '</div>'+
                '<div class="col-sm-3">'+
                    '<div class="checkbox checkbox-info">'+
                        '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_CANCEL + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL  + '"/> '+ $LABEL_CANCEL_STATUS + '</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                    '<div class="checkbox checkbox-info">'+
                        '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_REJECT + '" value="' + MASTER_DATA.DOC_STATUS_REJECT  + '"/> '+ $LABEL_REJECT_STATUS + '</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                '<div class="checkbox checkbox-info">'+
                    '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_COMPLETE + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE  + '"/> ' + $LABEL_COMPLETE_STATUS +'</label>'+
                '</div>'+
            '</div>'+
            '<a title="' + $MESSAGE_SEARCH + '" onclick="searchMyApproveRequest()"><img src="' + $SEARCH + '" width="70px"/>&#160;</a>'+
            '</div>'
        );

        $('#checkbox_modal').append(
            // '<div class="col-xs-12">' +
            //     '<div class="checkbox checkbox-info">' +
            //         '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_DRAFT  + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT + '"/> ' + $LABEL_DRAFT_STATUS + '</label>'+
            //     '</div>'+
            // '</div>'+
            // '<div class="col-xs-12">'+
            //     '<div class="checkbox checkbox-info">'+
            //         '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS+ '"/> ' + $LABEL_ON_PROCESS_STATUS + '</label>'+
            //     '</div>'+
            // '</div>'+
            '<div class="col-xs-12">'+
                '<div class="checkbox checkbox-info">'+
                    '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_CANCEL  + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL + '"/> ' + $LABEL_CANCEL_STATUS + '</label>'+
                '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
                '<div class="checkbox checkbox-info">'+
                    '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_REJECT  + '" value="' + MASTER_DATA.DOC_STATUS_REJECT + '"/> ' + $LABEL_REJECT_STATUS + '</label>'+
                '</div>'+
            '</div>'+
            '<div class="col-xs-12">'+
                '<div class="checkbox checkbox-info">'+
                    '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_COMPLETE  + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE + '"/> ' + $LABEL_COMPLETE_STATUS + '</label>'+
                '</div>'+
            '</div>'
        )

        $.material.init();



        $(".checkbox_status").prop('checked', true);
        $(".checkbox_status1").prop('checked', true);
        // }

        if($TYPE_REQUEST == 'APP')
            code = MASTER_DATA.DOC_APP_TYPE;
        else
            code = MASTER_DATA.DOC_ADV_TYPE;

        searchMyApproveRequest();
        // searchMyApproveRequestMobile()
        $('.dv-background').hide();

    },2000);


    if($TYPE_REQUEST == 'APP'){
        $("#buttonBar").empty();
        $("#advance").addClass("hide");
        $("#expense").addClass("hide");
        $("#approve").removeClass("hide");

        $("#advance_mobile").addClass("hide");
        $("#expense_mobile").addClass("hide");
        $("#approve_mobile").removeClass("hide");

        $('#expense_li').addClass('hide');
        $('#advance_li').addClass('hide');


    }else {

        // $('.fab').removeClass('hide');

        $("#buttonBar").empty();

        $("#buttonBar").append(''+
            '<div title="' + $LABEL_DOCUMENT_ADVANCE + '" id="divADV" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 0px; padding:6px; position: fixed; border-radius:8px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t1.png" id="btnADV" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>' +
            '<div title="' + $LABEL_DOCUMENT_EXPENSE + '" id="divEXP" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 80px; padding:6px; position: fixed; border-radius:8px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t3.png" id="btnEXP" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>');
        moveFixedActionButton();

        if($TYPE_REQUEST == 'ADV'){
            $("#divADV").attr('disabled',true);
            $('#btnADV').css('filter','grayscale(0%)');

            $("#divEXP").attr('disabled',false);
            $('#btnEXP').css('filter','grayscale(100%)');

            $("#advance").removeClass("hide");
            $("#expense").addClass("hide");
            $("#approve").addClass("hide");

            $("#advance_mobile").removeClass("hide");
            $("#expense_mobile").addClass("hide");
            $("#approve_mobile").addClass("hide");
            searchMyApproveRequest()
        }else{
            $("#divEXP").attr('disabled',true);
            $('#btnEXP').css('filter','grayscale(0%)');

            $("#divADV").attr('disabled',false);
            $('#btnADV').css('filter','grayscale(100%)');

            $("#advance").addClass("hide");
            $("#expense").removeClass("hide");
            $("#approve").addClass("hide");

            $("#advance_mobile").addClass("hide");
            $("#expense_mobile").removeClass("hide");
            $("#approve_mobile").addClass("hide");
            searchMyApproveRequest()
        }


    }

    $("#btnADV").on('click',function () {
        $("#divADV").attr('disabled',true);
        $("#divEXP").attr('disabled',false);

        $('#btnADV').css('filter','grayscale(0%)');
        $('#btnEXP').css('filter','grayscale(100%)');

        code = MASTER_DATA.DOC_ADV_TYPE;
        $("#advance").removeClass("hide");
        $("#expense").addClass("hide");
        $("#approve").addClass("hide");

        searchMyApproveRequest();
    });

    $('#advance_mobile_button').on('click',function () {
        code = MASTER_DATA.DOC_ADV_TYPE;
        pageType = 'ADV';

        $("#advance_mobile").removeClass("hide");
        $("#expense_mobile").addClass("hide");
        $("#approve_mobile").addClass("hide");

        searchMyApproveRequestMobile();
    })

    $("#btnEXP").on('click',function () {


        $("#divADV").attr('disabled',false);
        $("#divEXP").attr('disabled',true);

        $('#btnEXP').css('filter','grayscale(0%)');
        $('#btnADV').css('filter','grayscale(100%)');


        code = MASTER_DATA.DOC_EXP_TYPE;

        $("#advance").addClass("hide");
        $("#expense").removeClass("hide");
        $("#approve").addClass("hide");

        searchMyApproveRequest();
    })

    $('#expense_mobile_button').on('click',function () {
        pageType = 'EXP';

        $("#advance_mobile").addClass("hide");
        $("#expense_mobile").removeClass("hide");
        $("#approve_mobile").addClass("hide");

        code = MASTER_DATA.DOC_EXP_TYPE;
        searchMyApproveRequestMobile();
    })

    $("#btnAPP").on('click',function () {
        moveFixedActionButton();
        // showListView();
    });


    // $(window).resize(function () {
    //     moveFixedActionButton();
    //
    //     if(code == MASTER_DATA.DOC_APP_TYPE){
    //
    //     }else if(code == MASTER_DATA.DOC_APP_TYPE){
    //
    //     }else{
    //
    //     }
    //
    // });

    // $(window).resize(function () {
    //     moveFixedActionButton();
    //
    //     if(!checkWidth){
    //         if(!($(window).width >= 1300)){
    //             checkWidth = true;
    //             searchMyApproveRequestMobile();
    //         }
    //     }else {
    //         if(!($(window).width < 1300)){
    //             checkWidth = false;
    //             searchMyApproveRequest()
    //         }
    //     }
    //
    // })



});

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.left = (xPos-75) + 'px';
    }
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    return data.FOA+data.FNameTH+" "+data.LNameTH;
}

function searchMyApproveRequest(){
    $('.dv-background').show();
    window.setTimeout(function () {
        let documentStatus = "";
        $.each( $( ".checkbox_status:checked" ), function( i, obj ) {
            documentStatus += ","+obj.value;
        });

        if(documentStatus){
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status').val(documentStatus);
        }else{
            $('#search_doc_status').val($('#search_doc_status_all').val());
        }

        let criteriaObject = {
            requester : $USERNAME,
            docNumber           : $("#input_docNumber").val()==""?"":$("#input_docNumber").val(),
            titleDescription    :$("#input_titleDescription").val()==""?"":$("#input_titleDescription").val(),
            documentType        :  code,
            documentStatus        : $("#search_doc_status").val()==""?"":$("#search_doc_status").val()
        };

        $JSON = criteriaObject;

        queryApproveByCriteria(criteriaObject);
    },500);

}


function searchMyApproveRequestMobile(){
    $('.dv-background').show();

    window.setTimeout(function () {
        let documentStatus = "";
        $.each( $( ".checkbox_status1:checked" ), function( i, obj ) {
            documentStatus += ","+obj.value;
        });

        if(documentStatus){
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status_mobile').val(documentStatus);
        }else{
            $('#search_doc_status_mobile').val($('#search_doc_status_all_mobile').val());
        }

        let criteriaObject = {
            requester : $USERNAME,
            docNumber           : $("#input_docNumber_mobile").val()==""?"":$("#input_docNumber_mobile").val(),
            titleDescription    :$("#input_titleDescription_mobile").val()==""?"":$("#input_titleDescription_mobile").val(),
            documentType        :  code,
            documentStatus        : $("#search_doc_status_mobile").val()==""?"":$("#search_doc_status_mobile").val()
            // projection : "haveDoc"
        };

        $JSON = criteriaObject;

        queryApproveByCriteria(criteriaObject);
    },500);

}

function queryApproveByCriteria(criteriaObject) {

    let urlSearch;

    if($(window).width() < 1300){
        // paggingSearchMainMobile
        checkWidth = false;
        object.setId("#paggingSearchMainMobile");
    }else{
        checkWidth = true;
        object.setId("#paggingSearchMainPC");
    }

    if(code == MASTER_DATA.DOC_APP_TYPE){
        // urlSearch = "/myApprove/findByCriteria";
        object.setUrlData("/myApprove/findByCriteria");
        object.setUrlSize("/myApprove/findSize");
    }else{
        // urlSearch = "/myExpense/findByCriteria";
        object.setUrlData("/myExpense/findByCriteria");
        object.setUrlSize("/myExpense/findSize");
    }

    // $.ajax({
    //     url: session.context + urlSearch,
    //     data: criteriaObject,
    //     async : false,
    //     complete: function (xhr) {
    //         if (xhr.status == 200) {
    //             if (xhr.responseText) {
    //                 let item = JSON.parse(xhr.responseText);
    object.loadTable = function(items) {
        let item = items.content;

        let nextApprover;

        if (item.length > 0) {
            $("#bodyGrid").empty();
            $('#mobile_content').empty();
            for (let i = 0; i < item.length; i++) {
                let record = '';
                let mobile_record = '';
                sumDateProcess = 0;
                if (item[i].id) {

                    record += '' +
                        '<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<div class="col-sm-1" style="padding-top: 10px; padding-left: 0; padding-right: 0; text-align: right">';

                    mobile_record += '' +
                        '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-top: 10px;">' +
                        '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">';

                    if (item[i].documentType == MASTER_DATA.DOC_APP_TYPE) {
                        if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
                            if (item[i].request) {

                                record += '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '" onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                                    '<img src="' + $img_cancel + '" width="40px"/>&#160;' +
                                    '</a>';

                                mobile_record += '' +
                                    '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#numberDocument_mobile\').val(\'' + item[i].docNumber + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\')"  data-target="#cancleRequest_mobile" data-toggle="modal">' +
                                    '<img src="' + $IMAGE_CANCEL + '" width="50px;"/>' +
                                    '</a>';

                                record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                    '<img src="' + $menu + '" width="40px"/>' +
                                    '</a>';
                            }
                            else {
                                record += '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '" onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                                    '<img src="' + $img_cancel + '" width="40px"/>&#160;' +
                                    '</a>';

                                mobile_record += '' +
                                    '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\'),$(\'#docnumber_noRequest_mobile\').append(\'' + item[i].docNumber + '\')"  data-target="#cancleDocMobile" data-toggle="modal"><img src="' + $IMAGE_CANCEL + '" width="50px;"/></a>';


                                record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                    '<img src="' + $menu + '" width="40px"/>' +
                                    '</a>';
                            }

                        } else if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) {
                            record += '<a title="' + $MESSAGE_CANCEL_DOC_TOOLTIP + '" onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal" data-toggle="modal"><img src="' + $img_cancel + '" width="40px"/>&#160;</a>';
                            mobile_record += '<a onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal_mobile" data-toggle="modal">' +
                                '<img src="' + $img_cancel + '"width="50px"/>' +
                                '</a>';


                            record += '<a title="' + $MESSAGE_VIEW_DRAFT_DOC_TOOLTIP + '" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $pencil + '" width="40px"/>' +
                                '</a>';

                        } else {

                            record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $menu + '" width="40px"/>' +
                                '</a>';
                        }
                    } else if (item[i].documentType == MASTER_DATA.DOC_EXP_TYPE) {
                        if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {

                            record += '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '" onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                                '<img src="' + $img_cancel + '" width="40px"/>&#160;' +
                                '</a>';

                            record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $menu + '" width="40px"/>' +
                                '</a>';

                            mobile_record += '' +
                                '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#numberDocument_mobile\').val(\'' + item[i].docNumber + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\')"  data-target="#cancleRequest_mobile" data-toggle="modal"><img src="' + $IMAGE_CANCEL + '" width="50px;"/></a>';

                        } else if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) {

                            record += '<a title="' + $MESSAGE_CANCEL_DOC_TOOLTIP + '" onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal" data-toggle="modal"><img src="' + $img_cancel + '" width="40px"/></a>&#160;';

                            record += '<a title="' + $MESSAGE_VIEW_DRAFT_DOC_TOOLTIP + '" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $pencil + '" width="40px"/>' +
                                '</a>';

                            mobile_record += '<a  onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal_mobile" data-toggle="modal">' +
                                '<img src="' + $img_cancel + '" width="50px"/>' +
                                '</a>';

                        } else {

                            record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $menu + '" width="40px"/>' +
                                '</a>';
                        }
                    } else {
                        if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
                            record += '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '" onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                                '<img src="' + $img_cancel + '" width="40px"/>&#160;' +
                                '</a>';
                            // '<a h="' + session.context + '//advance/advanceDetail?doc=' + item[i].id + '&type=' + item[i].approveType + '"><img src="' + $menu + '" width="40px"/>&#160;</a>';
                            record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $menu + '" width="40px"/>' +
                                '</a>';

                            mobile_record += '' +
                                '<a onclick="$(\'#idDocument_mobile\').val(\'' + item[i].id + '\'),$(\'#numberDocument_mobile\').val(\'' + item[i].docNumber + '\'),$(\'#typeDocument_mobile\').val(\'' + item[i].documentType + '\'),$(\'#approveTypeDocument_mobile\').val(\'' + item[i].approveType + '\')"  data-target="#cancleRequest_mobile" data-toggle="modal"><img src="' + $IMAGE_CANCEL + '" width="50px;"/></a>';
                        } else if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) {

                            record += '<a title="' + $MESSAGE_CANCEL_DOC_TOOLTIP + '" onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal" data-toggle="modal"><img src="' + $img_cancel + '" width="40px"/>&#160;</a>';
                            // '<a h="' + session.context + '//advance/createDoc?doc=' + item[i].id + '"><img src="' + $pencil + '" width="40px"/></a>';
                            record += '<a title="' + $MESSAGE_VIEW_DRAFT_DOC_TOOLTIP + '" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $pencil + '" width="40px"/>' +
                                '</a>';


                            mobile_record += '<a onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal_mobile" data-toggle="modal">' +
                                '<img src="' + $img_cancel + '" width="50px"/>' +
                                '</a>';
                        } else {
                            // record += '' +
                            //     '<a h="' + session.context + '//advance/advanceDetail?doc=' + item[i].id + '&type=' + item[i].approveType + '"><img src="' + $menu + '" width="40px"/>&#160;</a>';
                            record += '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src="' + $menu + '" width="40px"/>' +
                                '</a>';
                        }
                    }


                    record += '' +
                        '</div>' +
                        '<div class="col-sm-2" style="text-align: left; padding-right: 0px;">' +
                        '<div class="col-sm-12" style="padding-right: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].docNumber + '">' +
                        item[i].docNumber +
                        '</div>' +
                        '<div class="col-sm-12" style="color: orange; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;" title="' + (item[i].titleDescription ? item[i].titleDescription : '-') + '">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '<div class="col-sm-12" style="color: deepskyblue; font-size: 12px;"> ' +
                        ((item[i].createdDate) ? item[i].createdDate.substring(0, item[i].createdDate.lastIndexOf(':')) : '-') +
                        '</div>' +
                        '<div class="col-sm-12">' +
                        ((item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) ? '<span style="background-color: red; color: white; font-weight: 900;">DRAFT</span>' : '') +
                        '</div>' +
                        '</div>';

                    mobile_record += '' +
                        '</div>' +
                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                        '<div class="col-xs-12" style="text-align: left; font-size: 17px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl">' +
                        '<div style="display: inline-block;">' +
                        item[i].docNumber +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 3px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                        '<div style="display: inline-block; ">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '</div>' +
                        '<div id="sumProcessDay' + i + '" class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                        '0 Days' +
                        '</div>' +
                        '</div>' +
                        '</a>' +
                        '</div>';


                    if (item[i].requests) {

                        nextApprover = findEmployeeProfileByUserName(item[i].requests.nextApprover);

                        $.ajax({
                            url: session.context + '/myApprove/getApprover/' + item[i].requests.id,
                            type: "GET",
                            async: false,
                            complete: function (xhr) {

                                $APPROVERS = JSON.parse(xhr.responseText);
                                $APPROVERS = objectRequestApprover_Mobile = $APPROVERS.requestApprovers;
                                $APPROVERS.sort(function (a, b) {
                                    return a.sequence > b.sequence;
                                });

                                record += '' +
                                    '<div class="col-sm-4">' +
                                    '<ol class="progress" data-steps="' + $APPROVERS.length + '" style="height: 100%;">';

                                mobile_record += '' +
                                    '<div id="detail' + (startCount_Request) + '" class="panel-collapse collapse" role="detail' + (startCount_Request) + '">' +
                                    '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px;  background-color: white; text-align: center;">';

                                let checkActive = 0;
                                let diffDays;
                                let nameLast;
                                let dateLast;

                                let to_day = new Date();
                                let days = to_day.getDate();
                                let month = to_day.getMonth() + 1; //January is 0!
                                let year = to_day.getFullYear();
                                let hours = to_day.getHours();
                                let minutes = to_day.getMinutes();

                                if (days < 10)
                                    days = '0' + days;
                                if (month < 10)
                                    month = '0' + month;
                                if (hours < 10)
                                    hours = '0' + hours;
                                if (minutes < 10)
                                    minutes = '0' + minutes;

                                to_day = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes;

                                let start_time;
                                let end_time;

                                if ($APPROVERS[0].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT) {

                                    start_time = moment(item[i].sendDate, moment.ISO_8601);
                                    end_time = moment($APPROVERS[0].actionTime, moment.ISO_8601);

                                    diffDays = end_time.diff(start_time, 'days');

                                    sumDateProcess += diffDays;

                                    record += '' +
                                        '<li class="reject" style="text-align: left;">' +
                                        '<div style="text-align: center">' +
                                        '<span class="step">' +
                                        '<span>' +
                                        $APPROVERS[0].actionState +
                                        '</span>' +
                                        '</span>' +
                                        '<span class="name">' + diffDays + '</span>' +
                                        '</div>' +
                                        '</li>';

                                    mobile_record += '' +
                                        '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ff734a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                        '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                        '<div class="circle" style="background-color: #ff2d25">' +
                                        '<a style="font-weight: 600">' +
                                        $APPROVERS[0].actionState +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                        '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                        ($APPROVERS[0].approver ? $APPROVERS[0].approver : '-') +
                                        '</div>' +
                                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                        $APPROVERS[0].createdDate.substring(0, $APPROVERS[0].createdDate.lastIndexOf(':')) +
                                        '</div>' +
                                        '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                        '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color: transparent">' +
                                        diffDays + ' Day' +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>' +
                                        '</div>';

                                } else if ($APPROVERS[0].actionTime) {

                                    start_time = moment(item[i].sendDate, moment.ISO_8601);
                                    end_time = moment($APPROVERS[0].actionTime, moment.ISO_8601);

                                    diffDays = end_time.diff(start_time, 'days');

                                    sumDateProcess = sumDateProcess + diffDays;
                                    record += '' +
                                        '<li class="done" style="text-align: center;">' +
                                        '<div style="text-align: center">' +
                                        '<span class="step">' +
                                        '<span>' +
                                        $APPROVERS[0].actionState +
                                        '</span>' +
                                        '</span>' +
                                        '<span class="name">' +
                                        diffDays +
                                        '</span>' +
                                        '</div>' +
                                        '</li>';


                                    mobile_record += '' +
                                        '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                        '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                        '<div class="circle" style="background-color: #47e833">' +
                                        '<a style="font-weight: 600">' +
                                        objectRequestApprover_Mobile[0].actionState +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                        '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                        (objectRequestApprover_Mobile[0].approver ? objectRequestApprover_Mobile[0].approver : '-') +
                                        '</div>' +
                                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                        $APPROVERS[0].actionTime.substring(0, $APPROVERS[0].actionTime.lastIndexOf(':')) +
                                        '</div>' +
                                        '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                        '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                        end_time.diff(start_time, 'days') + ' Day' +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>' +
                                        '</div>';
                                    // checkFirst = 1;
                                }
                                else if ($APPROVERS[0].userNameApprover === item[i].requests.nextApprover) {


                                    end_time = moment(to_day, moment.ISO_8601);
                                    start_time = moment(item[i].sendDate, moment.ISO_8601);
                                    diffDays = end_time.diff(start_time, 'days');

                                    sumDateProcess = sumDateProcess + diffDays;

                                    nameLast = $APPROVERS[0].approver;
                                    dateLast = $APPROVERS[0].receiveTime;
                                    record += '' +
                                        '<li class="active" style="text-align: left;">' +
                                        '<div style="text-align: center">' +
                                        '<span class="step">' +
                                        '<span>'
                                        + $APPROVERS[0].actionState +
                                        '</span>' +
                                        '</span>' +
                                        '<span class="name">' + diffDays + '</span>' +
                                        '</div>' +
                                        '</li>';


                                    mobile_record += '' +
                                        '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                        '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                        '<div class="circle" style="background-color: #ff9800">' +
                                        '<a style="font-weight: 600">' +
                                        objectRequestApprover_Mobile[0].actionState +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                        '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                        (objectRequestApprover_Mobile[0].approver ? objectRequestApprover_Mobile[0].approver : '-') +
                                        '</div>' +
                                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                        item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':')) +
                                        '</div>' +
                                        '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                        '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                        end_time.diff(start_time, 'days') + ' Day' +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>' +
                                        '</div>';
                                } else {
                                    record += '' +
                                        '<li>' +
                                        '<span class="step">' +
                                        '<span>'
                                        + $APPROVERS[0].actionState +
                                        '</span>' +
                                        '</span>' +
                                        '<span class="name" style="color: transparent">' +
                                        'Waiting' +
                                        '</span>' +
                                        '</li>';


                                    mobile_record += '' +
                                        '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                        '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                        '<div class="circle" style="background-color: #c0c0c0">' +
                                        '<a style="font-weight: 600">' +
                                        objectRequestApprover_Mobile[0].actionState +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                        '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                        '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                        (objectRequestApprover_Mobile[0].approver ? objectRequestApprover_Mobile[0].approver : '-') +
                                        '</div>' +
                                        '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                        // DateUtil.coverDateToString(objectRequestApprover_Mobile[j].createdDate) +
                                        '</div>' +
                                        '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                        '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color:transparent">' +
                                        // diffDays + ' Day' +
                                        '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>' +
                                        '</div>';
                                }

                                for (let j = 1; j < $APPROVERS.length; j++) {


                                    let sendDate_day = new Date($APPROVERS[j - 1].actionTime);
                                    let sendDate_days = sendDate_day.getDate();
                                    let sendDate_month = sendDate_day.getMonth() + 1;
                                    let sendDate_year = sendDate_day.getFullYear();
                                    let sendDate_hours = sendDate_day.getHours();
                                    let sendDate_minutes = sendDate_day.getMinutes();

                                    if (sendDate_days < 10)
                                        sendDate_days = '0' + sendDate_days;
                                    if (sendDate_month < 10)
                                        sendDate_month = '0' + sendDate_month;
                                    if (sendDate_hours < 10)
                                        sendDate_hours = '0' + sendDate_hours;
                                    if (sendDate_minutes < 10)
                                        sendDate_minutes = '0' + sendDate_minutes;

                                    sendDate_day = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes;

                                    if ($APPROVERS[j].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT) {
                                        start_time = moment($APPROVERS[j - 1].actionTime, moment.ISO_8601);
                                        end_time = moment($APPROVERS[j].actionTime, moment.ISO_8601);

                                        diffDays = end_time.diff(start_time, 'days');

                                        sumDateProcess = sumDateProcess + diffDays;

                                        record +=
                                            '<li class="reject" style="text-align: left;">' +
                                            '<div style="text-align: center">' +
                                            '<span class="step">' +
                                            '<span>' +
                                            $APPROVERS[j].actionState +
                                            '</span>' +
                                            '</span>' +
                                            '<span class="name">' + diffDays + '</span>' +
                                            '</div>' +
                                            '</li>';


                                        mobile_record += '' +
                                            '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ff734a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #ff2d25">' +
                                            '<a style="font-weight: 600">' +
                                            $APPROVERS[j].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                            ($APPROVERS[j].approver ? $APPROVERS[j].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // $APPROVERS[j].createdDate.substring(0,$APPROVERS[j].createdDate.lastIndexOf(':')) +
                                            sendDate_day +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color: transparent">' +
                                            diffDays + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';


                                    } else if ($APPROVERS[j].actionTime) {
                                        start_time = moment($APPROVERS[j - 1].actionTime, moment.ISO_8601);
                                        end_time = moment($APPROVERS[j].actionTime, moment.ISO_8601);
                                        diffDays = end_time.diff(start_time, 'days');

                                        sumDateProcess = sumDateProcess + diffDays;
                                        record += '' +
                                            '<li class="done" style="text-align: center;">' +
                                            ' <div style="text-align: center">' +
                                            ' <span class="step">' +
                                            ' <span>' +
                                            $APPROVERS[j].actionState +
                                            '</span>' +
                                            ' </span>' +
                                            ' <span class="name">' +
                                            diffDays +
                                            '</span>' +
                                            '</div>' +
                                            '</li>';

                                        mobile_record += '' +
                                            '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #58c34a; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #47e833">' +
                                            '<a style="font-weight: 600">' +
                                            objectRequestApprover_Mobile[j].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left">' +
                                            (objectRequestApprover_Mobile[j].approver ? objectRequestApprover_Mobile[j].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // objectRequestApprover_Mobile[j].createdDate.substring(0,objectRequestApprover_Mobile[j].createdDate.lastIndexOf(':')) +
                                            sendDate_day +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                            diffDays + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';


                                    } else if (($APPROVERS[j - 1].actionTime && $APPROVERS[j - 1].requestStatusCode != MASTER_DATA.DOC_STATUS_REJECT) && !$APPROVERS[j].actionTime) {

                                        start_time = moment($APPROVERS[j - 1].actionTime, moment.ISO_8601);
                                        end_time = moment(to_day, moment.ISO_8601);
                                        diffDays = end_time.diff(start_time, 'days');

                                        sumDateProcess = sumDateProcess + diffDays;

                                        record +=
                                            '<li class="active" style="text-align: left;">' +
                                            '<div style="text-align: center">' +
                                            '<span class="step">' +
                                            '<span>' +
                                            $APPROVERS[j].actionState +
                                            '</span>' +
                                            '</span>' +
                                            '<span class="name">' + diffDays + '</span>' +
                                            '</div>' +
                                            '</li>';


                                        mobile_record += '' +
                                            '<div style=" margin-top: 8px; height: 50px; width: 100%; background-color: #ffc107; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #ff9800">' +
                                            '<a style="font-weight: 600">' +
                                            objectRequestApprover_Mobile[j].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + (startCount_Request) + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                            (objectRequestApprover_Mobile[j].approver ? objectRequestApprover_Mobile[j].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // objectRequestApprover_Mobile[j].createdDate.substring(0,objectRequestApprover_Mobile[j].createdDate.lastIndexOf(':')) +
                                            sendDate_day +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px;">' +
                                            diffDays + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';

                                    } else {
                                        record += '' +
                                            '<li>' +
                                            '<span class="step">' +
                                            '<span>'
                                            + $APPROVERS[j].actionState +
                                            '</span>' +
                                            '</span>' +
                                            '<span class="name" style="color: transparent">' +
                                            'Waiting' +
                                            '</span>' +
                                            '</li>';


                                        mobile_record += '' +
                                            '<div style="margin-top: 8px; height: 50px; width: 100%; background-color: #9e9e9e; padding-right: 0px; padding-left: 0px; padding-bottom: 4px; ">' +
                                            '<div class="col-xs-2 col-xs-2" style="padding-top: 5px; padding-left: 5px; width: 40px; display: inline-block">' +
                                            '<div class="circle" style="background-color: #c0c0c0">' +
                                            '<a style="font-weight: 600">' +
                                            objectRequestApprover_Mobile[j].actionState +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '<a data-toggle="collapse" data-target="#detail' + j + '">' +
                                            '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                            '<div class="col-xs-12" style="font-size: 17px; color: white; display: inline-block; text-align: left;">' +
                                            (objectRequestApprover_Mobile[j].approver ? objectRequestApprover_Mobile[j].approver : '-') +
                                            '</div>' +
                                            '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px;">' +
                                            // DateUtil.coverDateToString(objectRequestApprover_Mobile[j].createdDate) +
                                            '</div>' +
                                            '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">' +
                                            '<a style="font-weight: 800; margin-left: 2px; margin-right: 2px; color:transparent">' +
                                            // diffDays + ' Day' +
                                            '</a>' +
                                            '</div>' +
                                            '</div>' +
                                            '</a>' +
                                            '</div>';
                                    }
                                }
                                record +=
                                    '</ol></div>';

                                mobile_record += '' +
                                    '<a style="text-align: center;" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                    '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '"/>' +
                                    '</a>';

                            }
                        });

                    } else {
                        record += '' +
                            '<div class="col-sm-4">' +
                            '<ol class="progress" data-steps="3" style="height: 100%;">' +
                            '<li>' +
                            '<span class="step"><span>VRF</span></span>' +
                            '<span class="name" style="color: transparent">1 Day</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span>APR</span></span>' +
                            '<span  class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span >ADM</span></span>' +
                            '<span class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '</ol>' +
                            '</div>';


                        mobile_record += '' +
                            '<div id="detail' + (startCount_Request) + '" class="panel-collapse collapse" role="detail' + (startCount_Request) + '">' +
                            '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; padding-bottom: 4px; background-color: white; text-align: center;">' +
                            '<div style="  height: 50px; width: 100%; background-color: #e7e124; padding-right: 0px; padding-left: 0px; padding-bottom: 4px;">' +
                            '<a data-toggle="collapse" data-target="detail' + (startCount_Request) + '">' +
                            '<div class="col-xs-12" style="font-size: 30px; font-weight: bold; color: white; display: inline-block; text-align: center; vertical-align: middle;">' +
                            $LABEL_DRAFT_STATUS +
                            '</div>' +
                            '</a>' +
                            '</div>';

                        mobile_record += '' +
                            '<a style="text-align: center;" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                            '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                            '</a>';


                    }

                    record += '<div class="col-sm-1" style="text-align: center; padding-top: 0px; font-size: 40px;">' + sumDateProcess + '</div>' +
                        '<div class="col-sm-2" style="text-align: center; padding-top: 0px; padding-left: 0px; padding-right: 0px; text-align: left;">' +
                        '<div class="col-sm-12">' +
                        (((nextApprover != 'NaN undefined' && nextApprover)) ? nextApprover : '-') +
                        '</div>' +
                        '<div class="col-sm-12" style="font-size: 12px; color: deepskyblue;">' +
                        (item[i].requests ? (item[i].requests.nextApprover ? (item[i].requests.lastActionTime ? item[i].requests.lastActionTime.substring(0, item[i].requests.lastActionTime.lastIndexOf(':')) : item[i].sendDate.substring(0, item[i].sendDate.lastIndexOf(':'))) : '-') : '-') +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-2" style="padding-top: 0px; padding-right: 0px; color: limegreen; text-align: right;">' +
                        '<b style="font-size: x-large;">' + ((item[i].totalAmount) ? item[i].totalAmount.toLocaleString(undefined, {minimumFractionDigits: 2}) : 0) + ' ฿</b>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';


                    mobile_record += '</div>' +
                        '</div>';

                    startCount_Request++;
                    $('#sumProcessDay' + i).empty();
                    if (item[i].documentStatus != MASTER_DATA.DOC_STATUS_DRAFT) {
                        $('#sumProcessDay' + i).append(sumDateProcess + ' Days');
                    }
                }
                $("#bodyGrid").append(record);
                $('#mobile_content').append(mobile_record);
            }
        }
    }
        //             }
        //         }
        //     }
        // });


    object.setDataSearch(criteriaObject);
    object.search(object);
    // object.loadPage(($CRITERIA_PAGE*1)+1,object);

    $('.dv-background').hide();
}

function cancelRequest(id,docNumber,type,approveType){
    $('.dv-background').show();
    window.setTimeout(function () {
        var listJsonDetails = [];

        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = validateFlowTypeByApproveType(approveType);
        listJsonDetails.push(jsonDetails);

        var jsonData = {};
        jsonData['id'] = id;
        jsonData['requester'] = $USERNAME;
        jsonData['docNumber'] = docNumber;
        jsonData['details'] = JSON.stringify(listJsonDetails);
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/cancelRequest',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    let $DATA_REQUEST = JSON.parse(xhr.responseText);
                    if($DATA_REQUEST != null){
                        cancelDocument(id,type,approveType);
                    }
                }

                $('#cancleRequest').modal('hide')
                $('#cancleRequest_mobile').modal('hide')
                $('.dv-background').hide();
            }
        });
    },500);
}

function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_FOREIGN){
        return MASTER_DATA.FLOW_TYPE_FOREIGN;
    }else if(approveType == MASTER_DATA.APR_TYPE_CAR){
        return MASTER_DATA.FLOW_TYPE_CAR;
    }else if(approveType == MASTER_DATA.APR_TYPE_HOTEL){
        return MASTER_DATA.FLOW_TYPE_HOTEL;
    }else if(approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        return MASTER_DATA.FLOW_TYPE_FIGHT_BOOKING;
    }
}

function cancelDocument(id,type,approveType){

    $('.dv-background').show();
    window.setTimeout(function () {

        if(type == MASTER_DATA.DOC_APP_TYPE) {
            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id;
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        dataDocument = JSON.parse(xhr.responseText);
                        searchMyApproveRequest();
                    }
                    $('#noRequestModal').modal('hide');
                    $('#haveRequestModal').modal('hide');
                    $('.dv-background').hide();
                }
            });
        }

        if(type == MASTER_DATA.DOC_EXP_TYPE) {

            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL


            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/expense/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        // dataDocument = JSON.parse(xhr.responseText);

                        searchMyApproveRequest();
                    }
                    $('#noRequestModal').modal('hide');
                    $('#haveRequestModal').modal('hide');
                    $('.dv-background').hide();
                }
            });
        }


        if(type == MASTER_DATA.DOC_ADV_TYPE) {
            var jsonData = {}
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id;
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/advance/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        searchMyApproveRequest();
                    }
                    $('#noRequestModal').modal('hide');
                    $('#haveRequestModal').modal('hide');
                    $('.dv-background').hide();
                }
            });
        }
    },500);
}

function ViewDoc(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/approve/viewCreateDocDetail?doc=' + id;
        }
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}

function viewDraft(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            window.location.href = session.context + '/approve/createDocSetDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/approve/createDocDetail?doc=' + id;
        }
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE && $USERNAME == createdBy){
        window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/createDoc?doc=' + id;
    }
}