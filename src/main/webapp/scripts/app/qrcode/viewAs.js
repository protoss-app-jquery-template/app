// let $DATA_EMPLOYEE;


$(document).ready(function () {

    $('.dv-background').show();

    let userHead;
    let item;

    $.ajax({
        type : "GET",
        url : window.location.origin + '/GWF/masterDatas/findUserMember?userHead=' + $actionUser,
        headers : {
            Accept: "application/json"
        },
        async : false,
        complete : function (xhr) {
            if(xhr.responseText){
                item = JSON.parse(xhr.responseText);
                item = item.content;

                $('#content').empty();

                // if(item[0].variable1 == userHead){
                $('#content').append(''+
                    '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-bottom: 10px; vertical-align: middle;">' +
                        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                            '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 17px; padding-top: 5px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                findEmployeeProfileByUserName($actionUser) +
                            '</div>' +
                            '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 17px; padding-top: 5px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                $actionUser +
                            '</div>' +
                            '<div style="padding-top: 7px; text-align: center;">' +
                                '<img src="' + $ARROW_RIGHT + '" width="45px" style="cursor: pointer;" onclick="viewDocument(\'' + $actionUser + '\',\'' + $qrCode + '\')">' +
                            '</div>'+
                        '</div>' +
                    '</div>'
                    );
                // }

                for(let i = 0 ; i < item.length ; i++){
                    $('#content').append(''+
                        '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-bottom: 10px; vertical-align: middle;">' +
                            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">' +
                                '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 17px; padding-top: 5px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                    findEmployeeProfileByUserName(item[i].code) +
                                '</div>' +
                                '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 17px; padding-top: 5px; color: white; display: inline-block; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                    item[i].code +
                                '</div>' +
                                '<div style="padding-top: 7px; text-align: center;">' +
                                    '<img src="' + $ARROW_RIGHT + '" width="45px" style="cursor: pointer;" onclick="viewDocument(\'' + item[i].code + '\',\'' + $qrCode + '\')">' +
                                '</div>'+
                            '</div>' +
                        '</div>'
                    );
                }

            }


            $('.dv-background').hide();

        }
    })

})

function findEmployeeProfileByUserName(userName){

    let data = $.ajax({
        url: window.location.origin + "/GWF/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    // $DATA_EMPLOYEE = data;
    // console.log(data.FOA+data.FNameTH+" "+data.LNameTH);

    return data.FOA+data.FNameTH+" "+data.LNameTH;
}

function viewDocument(actionUser,qrcode){
    // window.location.href = window.location.origin + '/GWF/qrcodeActionData/' + actionUser + '/' + qrcode;
    console.log(actionUser,qrcode);
    let a = $.ajax({
        type : 'GET',
        url : window.location.origin + '/GWF/qrcodeActionData/' + actionUser + '/' + qrcode,
        headers: {
            Accept : "application/json"
        },
    }).responseJSON;
}

// function findUserHead() {
//     userHead = $.ajax({
//         url: session.context + '/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCode?masterdata=M020&code='+$USERNAME,
//         headers: {
//             Accept : "application/json"
//         },
//         type: "GET",
//         async: false
//     }).responseJSON;

    // userHead.variable1 ? userHead : userHead = $USER_NAME;
// }

