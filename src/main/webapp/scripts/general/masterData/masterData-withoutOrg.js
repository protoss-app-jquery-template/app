/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvlNum(e){
	if(e == null || e == undefined || isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvl(e){
	if(e == null){
		return "";
	}else{
		return e;
	}
}

function numberWithCommas(x) {
    var data =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if(data == null || data == undefined || isNaN(data) || !isFinite(data)  || data == "" ){
		return "0";
	}else{
		return data;
	}
}

function checkDupplicate(orgCode,code,id){
	if(!orgCode){
		orgCode = 'ALL';
	}
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterDataByCodeByOrgCode?masterdata='+$DATA_MASTER_DATA.id+'&code='+code+'&orgCode='+orgCode,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		if(userObj.content[0].id != undefined && id != userObj.content[0].id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE);
                			$('#warningModal').modal('show');
                        }else{
                        	save();
                        }
                	}else{
                		save();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function save(){
	var jsonParams = {};
	
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();

	jsonParams['flagActive']    = $('#modal_edit_flag_active').prop('checked');
	jsonParams['code']     	    = $('#modal_edit_code').val();
	jsonParams['name']     	    = $('#modal_edit_name').val();
	jsonParams['description']   = $('#modal_edit_description').val();
	jsonParams['orgCode']       = 'ALL';
	
	
	//Dynamic Field
	for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
		var index = (j+1)+"";
		if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
			jsonParams['variable'+index]   = $('#modal_edit_variable'+index).prop('checked');
		}else{
			jsonParams['variable'+index]   = $('#modal_edit_variable'+index).val();
		}
	}
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/masterDatas',
        data: jsonParams,
        complete: function (xhr) {
        	var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
        		 if(userObj.id){ 
        			 var jsonParentParams = {};
        			 jsonParentParams['masterdata']  	= $DATA_MASTER_DATA.id;
            		 $.ajax({
            		        type: "POST",
            		        headers: {
            		            Accept: 'application/json'
            		        },
            		        url: session['context']+'/masterDatas/'+userObj.id+'/masterdata',
            		        data: jsonParentParams,
            		        complete: function (xhr) {
            		        	search();
            	        		$('#editItemModal').modal('hide');
            		        }
            		    }).done(function (){
            		        //close loader
            		    });
        		 }
        		 
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });
	
}

function edit(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		
            		$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_code').val(obj.code);
            		$('#modal_edit_name').val(obj.name);
            		$('#modal_edit_description').val(obj.description);
            		
            		
            		//Dynamic Field
            		for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
            			var index = (j+1)+"";
            			if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
            				if(obj['variable'+index] == "true"){
                    			$('#modal_edit_variable'+index).prop('checked', true);
                    		}else{
                    			$('#modal_edit_variable'+index).removeAttr( "checked" );
                    		}
            			}else{
            				$('#modal_edit_variable'+index).val(obj['variable'+index]);
            			}
            		}
            		
            		
            		
            		
            		
            		$('#editItemModal').modal('show');
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function deleteFunction(id){
	//delete
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/masterDatas/'+id,
        data: jsonParams,
        complete: function (xhr) {
        	search();
        	$('#deleteItemModal').modal('hide');
        }
    }).done(function (){
        //close loader
    });
}



function search(){
	
	var criteriaObject = {
			masterdata      : $DATA_MASTER_DATA.id,
			masterdataCode  : $DATA_MASTER_DATA.code,
			code            : $("#search_code").val()==""?"":$("#search_code").val(),
            name            : $("#search_name").val()==""?"":$("#search_name").val(),
            orgCode         : ""
	    };
	queryUserByCriteria(criteriaObject);
}

function queryUserByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/masterDatas/findByCriteria"); 
    object.setUrlSize("/masterDatas/findSize"); 
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
            	
                var code       = item[j].code==null?"":item[j].code;
                var name       = item[j].name==null?"":item[j].name;
                var description       = item[j].description==null?"":item[j].description;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;
                
                itemIdTmp = item[j].id;
                if(itemIdTmp){

                	var recordsData = ''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ><td class="text-center headcol tdFix"  style="width: 98px;" > ' : ' class="trlightgray" ><td class="text-center headcol tdFixGrey" style="width: 98px;" >')+
                            '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="edit('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
            				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\''+name+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
            				'</td>'+
            				((j%2 == 0) ? '<td class="text-center  headcol tdFix" style="width: 93px;left: 113px;" > ' : '<td class="text-center  headcol tdFixGrey"  style="width: 93px;left: 113px;" >')+
                            ((flagActive) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
                            '</td>'+
                            ((j%2 == 0) ? '<td class="text-center  headcol tdFix"  style="width: 100px;left: 206px;"> ' : '<td class="text-center  headcol tdFixGrey"  style="width: 100px;left: 206px;">')+
                            code+'</td>'+
                            ((j%2 == 0) ? '<td class="text-center  headcol tdFix"  style="width: 240px;left: 306px;"> ' : '<td class="text-center  headcol tdFixGrey"  style="width: 240px;left: 306px;">')+
                            name+'</td>'+
                            
                            
                            '<td class="text-center">'+description+'</td>';
                	
                	for(var i=0;i<$DATA_MASTER_DATA.numberOfColumn;i++){
                		if($DATA_MASTER_DATA['variableType'+(i+1)] == "BOOLEAN" ){
                			if(item[j]['variable'+(i+1)] == "true" ){
                				
                				
                				
                				recordsData += '<td class="text-center" style="padding-top: 5px;padding-bottom: 5px;" ><img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" /></td>';
                			}else{
                				recordsData += '<td class="text-center" style="padding-top: 5px;padding-bottom: 5px;" ><img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" /></td>';
                			}  
                		}else if($DATA_MASTER_DATA['variableType'+(i+1)] == "INTEGER" ){
                			recordsData += '<td class="text-center">'+ numberWithCommas(nvlNum(item[j]['variable'+(i+1)])) +'</td>';
                		}else if($DATA_MASTER_DATA['variableType'+(i+1)] == "FLOAT" ){
                			recordsData += '<td class="text-center">'+ numberWithCommas(parseFloat(nvlNum(item[j]['variable'+(i+1)])).toFixed(2))  +'</td>';
                		}else{
                			recordsData += '<td class="text-center">'+nvl(item[j]['variable'+(i+1)])+'</td>';
                		}
                		
                	}
                	recordsData += '</tr>';
                	
                	$('#gridMainBody').append(recordsData);
                }
                
            }
            
            
        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object); 
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}




$(document).ready(function () {
	
	$('#search_button').on('click', function () {
		search();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		
		$('#modal_edit_flag_active').prop('checked', true);
		$('#modal_edit_code').val('');
		$('#modal_edit_name').val('');
		$('#modal_edit_description').val('');
		
		//Set Default Value
		for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
			if($DATA_MASTER_DATA['variableDefaultValue'+(j+1)] != null){
				
				if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
					if($DATA_MASTER_DATA['variableDefaultValue'+(j+1)] == "true"){
						$('#modal_edit_variable'+(j+1)).prop('checked', true);
					}
				}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "INTEGER"){
					$('#modal_edit_variable'+(j+1)).val($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]);
				}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "FLOAT"){
					$('#modal_edit_variable'+(j+1)).val(parseFloat($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]).toFixed(2)    );
				}else{
					$('#modal_edit_variable'+(j+1)).val($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]);
				}
			}else{
				$('#modal_edit_variable'+(j+1)).val('');
			}
		}
	});
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		if(!$('#modal_edit_code').val()){
			errorMessage += $MESSAGE_REQUIRE +" " + $LABEL_CODE+"<br/>";
		}
		if(!$('#modal_edit_name').val()){
			errorMessage += $MESSAGE_REQUIRE +" " +$LABEL_NAME+"<br/>";
		}
		
		//fetch dynamic require field
		$.each( $( ".requiredField" ), function( i, obj ) {
			if(!obj.value){
				errorMessage += $MESSAGE_REQUIRE +" " +obj.name+"<br/>";
			}
		});
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkDupplicate($('#modal_edit_orgCode').val(),$('#modal_edit_code').val(),$('#modal_edit_id').val());
		}
		
	});
	
	
	
	
	//Load Dynamic Column
	for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
		$('#gridHeader').append(''+
                '<th class="text-center" style="width: 138px;">'+$DATA_MASTER_DATA['variableName'+(j+1)]+'</th>'
        );
		
		if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
			$('#contentEdit').append(''+
	                '<div class="form-group">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_MASTER_DATA['variableName'+(j+1)]+' </label> '+
	                '	</div> '+
	                ' 	<div class="col-sm-3 togglebutton" style="padding-right: 0px;top: 10px;left: 40px;">'+
	                ' 		<label><input type="checkbox" id="modal_edit_variable'+(j+1)+'"    /><span class="toggle" /></label> '+
	                ' 	</div>'+
	                ' 	<div class="col-sm-6" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "INTEGER"){
			$('#contentEdit').append(''+
	                '<div class="form-group">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_MASTER_DATA['variableName'+(j+1)]+' </label> '+(($DATA_MASTER_DATA['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="text" class="form-control numbersOnly" name="'+$DATA_MASTER_DATA['variableName'+(j+1)]+'" id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0"/>'+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "FLOAT"){
			$('#contentEdit').append(''+
	                '<div class="form-group">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_MASTER_DATA['variableName'+(j+1)]+' </label> '+(($DATA_MASTER_DATA['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="text" class="form-control amount2digit"  name="'+$DATA_MASTER_DATA['variableName'+(j+1)]+'"  id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0.00"/> '+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "DATE"){
			$('#contentEdit').append(''+
	                '<div class="form-group">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;padding-right: 0px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_MASTER_DATA['variableName'+(j+1)]+' </label> '+(($DATA_MASTER_DATA['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8 input-group" style="padding-top: 10px;left: 10px;" >'+
	                ' 		<input autocomplete="off"     type="text" class="form-control"  name="'+$DATA_MASTER_DATA['variableName'+(j+1)]+'" id="modal_edit_variable'+(j+1)+'" data-dtp="dtp_CPXwH" style="color: #2196f3;font-size: 16px;" /> '+
	                ' 		<span id="modal_edit_btnDate_variable'+(j+1)+'" type="button" class="input-group-addon btn-material-blue-500 btn-raised"><span class="fa fa-calendar"></span></span> '+
	                ' 	</div>'+
	                '</div>'
	        );
		}else{
			$('#contentEdit').append(''+
	                '<div class="form-group">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_MASTER_DATA['variableName'+(j+1)]+' </label> '+(($DATA_MASTER_DATA['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="text" class="form-control "  name="'+$DATA_MASTER_DATA['variableName'+(j+1)]+'"  id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;"  />'+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
			
		}
		
	}
	
	//Active Date Picker 
	for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
		if($DATA_MASTER_DATA['variableType'+(j+1)] == "DATE"){
			var index = (j+1)+"";
			$('#modal_edit_variable'+index).bootstrapMaterialDatePicker({ weekStart : 0,time: false,format : 'DD/MM/YYYY',clearButton:true });
			$('#modal_edit_btnDate_variable'+index).on('click',function(){
				$(this).parent().children("input:first").focus();
			});
		}
	}
	
	//Set Default Value
	for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
		if($DATA_MASTER_DATA['variableDefaultValue'+(j+1)] != null){
			
			if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
				if($DATA_MASTER_DATA['variableDefaultValue'+(j+1)] == "true"){
					$('#modal_edit_variable'+(j+1)).prop('checked', true);
				}
			}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "INTEGER"){
				$('#modal_edit_variable'+(j+1)).val($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]);
			}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "FLOAT"){
				$('#modal_edit_variable'+(j+1)).val(parseFloat($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]).toFixed(2)    );
			}else{
				$('#modal_edit_variable'+(j+1)).val($DATA_MASTER_DATA['variableDefaultValue'+(j+1)]);
			}
		}
	}
	
	//Set Require Field
	for(var j=0;j<$DATA_MASTER_DATA.numberOfColumn;j++){
		var index = (j+1)+"";
		if($DATA_MASTER_DATA['variableFlagRequire'+index] == true){
			if($DATA_MASTER_DATA['variableType'+(j+1)] == "BOOLEAN" ){
				//CASE : Require Boolean
			}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "INTEGER"){
				//CASE : Require Integer
			}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "FLOAT"){
				//CASE : Require Float
			}else if($DATA_MASTER_DATA['variableType'+(j+1)] == "DATE"){
				//CASE : Require Date
				$('#modal_edit_variable'+index).addClass('requiredField');
			}else{
				//CASE : Require String
				$('#modal_edit_variable'+index).addClass('requiredField');
			}
		}
		
		
	}
	
	search();
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
});

