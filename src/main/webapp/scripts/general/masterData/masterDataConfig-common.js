/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvlNum(e){
	if(e == null || e == undefined || isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvl(e){
	if(e == null){
		return "";
	}else{
		return e;
	}
}

function numberWithCommas(x) {
    var data =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if(data == null || data == undefined || isNaN(data) || !isFinite(data)  || data == "" ){
		return "0";
	}else{
		return data;
	}
}



function save(){
	var jsonParams = {};
	
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#edit_id').val();
	jsonParams['createdBy'] 	= $('#edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#edit_updateDate').val();
	jsonParams['nameMessageCode'] 	= $('#edit_nameMessageCode').val();
	
	jsonParams['code']     	    = $('#edit_code').html();
	jsonParams['name']     	    = $('#edit_name').val();
	jsonParams['description']   = $('#edit_description').html();
	jsonParams['flagActive']    = $('#modal_edit_flag_active').prop('checked');
	jsonParams['flagOrgCode']   = $('#modal_edit_flag_org_code').prop('checked');
	
	
	
	var numberOfColumn = 0;
	$.each( $( ".checkbox_use:checked" ), function( i, obj ) {
		numberOfColumn++;
	});
	jsonParams['numberOfColumn']   = numberOfColumn;
	
	//Dynamic Field
	for(var j=0;j<50;j++){
		var index = (j+1)+"";
		jsonParams['variableName'+index]           = ($('#edit_variableName'+index).val())?$('#edit_variableName'+index).val():null;
		jsonParams['variableDefaultValue'+index]   = ($('#edit_variableDefaultValue'+index).val())?$('#edit_variableDefaultValue'+index).val():null;
		
		jsonParams['variableFlagRequire'+index]    = ($('#edit_variableFlagRequire'+index).prop('checked'))?true:false;
		jsonParams['variableType'+index]           = ($('#edit_variableType'+index+' option:selected').val())?$('#edit_variableType'+index+' option:selected').val():null;
	}
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/masterDataConfig',
        data: jsonParams,
        complete: function (xhr) {
        	var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
        		 //
        		 
        		 $('#successModal').modal('show');
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });
}

function checkDupplicate(code,id){
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDataConfig/findByMasterDataByCode?code='+code,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		if(userObj.id != undefined && id != userObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE);
                			$('#warningModal').modal('show');
                        }else{
                        	saveMasterData();
                        }
                	}else{
                		saveMasterData();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function saveMasterData(){
	var jsonParams = {};
	
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();
	jsonParams['nameMessageCode'] 	= $('#modal_edit_nameMessageCode').val();
	jsonParams['flagActive']    = $('#modal_edit_flag_active').prop('checked');
	jsonParams['flagOrgCode']   = $('#modal_edit_flag_org_code').prop('checked');
	
	jsonParams['code']     	    = $('#modal_edit_code').val();
	jsonParams['name']     	    = $('#modal_edit_name').val();
	jsonParams['description']   = $('#modal_edit_description').val();
	jsonParams['numberOfColumn']= 0;
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/masterDataConfig',
        data: jsonParams,
        complete: function (xhr) {
        	var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
             	 $('#editItemModal').modal('hide');
        		 $('#successModal').modal('show');
        		 loadDataPageAfterAdd(userObj.id);
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });
}

function edit(id){
	
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDataConfig/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                	
                	$('#gridMainBody').empty();
                	
                	var code = obj.code==null?"":obj.code;
                	var description = obj.description==null?"":obj.description;
                	
                	$('#edit_id').val(obj.id);
                	$('#edit_createdBy').val(obj.createdBy);
                	$('#edit_createdDate').val(obj.createdDate);
                	$('#edit_updateBy').val(obj.updateBy);
                	$('#edit_updateDate').val(obj.updateDate);
                	$('#edit_name').val(obj.name);

                	$('#edit_nameMessageCode').val(obj.nameMessageCode);
                	$('#edit_code').html(code);
                	$('#edit_description').html(description);
                	

                	//genComponent
                	for(var i=0;i<50;i++){
                		
                		var recordsData = '';
                		
                			recordsData = recordsData+
                            '<tr> '+
                        	'<td class="text-center " > '+
                        	'    <div class="checkbox col-sm-6" style="color: #2196f3;"> '+
                        	'		<label><input type="checkbox" class="checkbox_use"   '+(((i+1) <= obj.numberOfColumn)?"checked":"")+' /><span class="checkbox-material" style=""><span class="check" style="right: 5px;bottom: 10px;" ></span></span></label> '+
                        	'    </div> '+
                        	'</td> '+
                        	'<td class="text-center" >'+(i+1)+'</td> '+
                        	'<td class="text-center " ><input type="text"  style="width: 200px;"   id="edit_variableName'+(i+1)+'"   value= "'+((obj['variableName'+(i+1)]!==null && obj['variableName'+(i+1)] != undefined)?obj['variableName'+(i+1)]:"")+'" /></td> '+
                        	'<td class="text-center " style="padding-top: 4px;" ><select class="form-control"   id="edit_variableType'+(i+1)+'"  style="color: #2196f3;" > '+
                        	'			            	<option value="STRING"  '+((obj['variableType'+(i+1)]=="STRING" || obj['variableType'+(i+1)]==null)?"selected":"")+' >Text</option> '+
                        	'			            	<option value="INTEGER" '+((obj['variableType'+(i+1)]=="INTEGER")?"selected":"")+' >Integer</option> '+
                        	'			            	<option value="FLOAT"   '+((obj['variableType'+(i+1)]=="FLOAT")?"selected":"")+' >Float</option> '+
                        	'			            	<option value="DATE"    '+((obj['variableType'+(i+1)]=="DATE")?"selected":"")+' >Date</option> '+
                        	'			            	<option value="BOOLEAN" '+((obj['variableType'+(i+1)]=="BOOLEAN")?"selected":"")+' >Boolean</option> '+
                        	'			            </select> '+
                        	'</td> '+
                        	'<td class="text-center " > '+
                        	'     <div class="checkbox col-sm-6" style="color: #2196f3;margin-top: 10px;"> '+
                        	'			<label><input type="checkbox" class="checkbox_roles" id="edit_variableFlagRequire'+(i+1)+'" '+((obj['variableFlagRequire'+(i+1)]==true)?"checked":"")+'  /><span class="checkbox-material" style="left: 30px;margin-right: 30px;"><span class="check" style="right: 5px;bottom: 10px;" ></span></span></label> '+
                        	'     </div> '+
                        	'</td> '+
                        	'<td class="text-center" ><input type="text" style="width: 280px;"  id="edit_variableDefaultValue'+(i+1)+'"   value="'+((obj['variableDefaultValue'+(i+1)]==null)?"":obj['variableDefaultValue'+(i+1)])+'" /></td> '+
                        	'</tr> ';
                		
                		

                        $('#gridMainBody').append(recordsData);
                        
                	}
                	
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function loadDataPageAfterAdd(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDataConfig',
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		
                		if(userObj.content.length){
                			$('#masterDataSelected').empty();
                			//load select type
                			for(var j=0;j<userObj.content.length;j++){
                    			
                    			var recordsData = "";
                    			if(id == userObj.content[j].id){
                    				recordsData ='<option  value="'+userObj.content[j].id+'"   selected  >'+userObj.content[j].name+'</option>';
                    			}else{
                    				recordsData ='<option  value="'+userObj.content[j].id+'" >'+userObj.content[j].name+'</option>';
                    			}
                    			
                    			
                    	        $('#masterDataSelected').append(recordsData);
                    		}
                			
                			edit(id);
                		}
                		
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function loadDataPage(){
	$.ajax({
        type: "GET",
        url: session['context']+'/masterDataConfig',
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		
                		if(userObj.content.length){
                			
                			//load select type
                			for(var j=0;j<userObj.content.length;j++){
                    			
                    			var recordsData = '<option  value="'+userObj.content[j].id+'"   '+((j==0)?"selected":"")+'  >'+userObj.content[j].name+'</option>';
                    	        $('#masterDataSelected').append(recordsData);
                    		}
                			
                			edit(userObj.content[0].id);
                		}
                		
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}




$(document).ready(function () {
	
	$('#nav_save').on('click', function () {
		save();
	});
	
	$('#add_button').on('click', function () {
		$('#editItemModal').modal('show');
	});
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_code').val()){
			errorMessage += $MESSAGE_REQUIRE +" "+$LABEL_CODE+"<br/>";
		}
		if(!$('#modal_edit_name').val()){
			errorMessage += $MESSAGE_REQUIRE +" "+$LABEL_NAME+"<br/>";
		}
		if(!$('#modal_edit_nameMessageCode').val()){
			errorMessage += $MESSAGE_REQUIRE +" "+$LABEL_NAME_CODE+"<br/>";
		}
		if(!$('#modal_edit_description').val()){
			errorMessage += $MESSAGE_REQUIRE +" "+$LABEL_DESCRIPTION+"<br/>";
		}
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkDupplicate($('#modal_edit_code').val(),$('#modal_edit_id').val());
		}
		
	});
	
	
	
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#masterDataSelected').on('change', function() {
		edit( this.value );
	});
	
	
	
	$('#successModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#successModal').modal('hide');
		},1000);
	});
	

	 loadDataPage();
	 
});

