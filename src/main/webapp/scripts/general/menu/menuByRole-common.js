/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function editMenu(){
	var id =  $('#menuIdPopup').val();
	
	$.ajax({
        type: "GET",
        url: session['context']+'/menus/'+id,
        complete: function (xhr) {
        	$('#editMenuModal').modal('hide');
        	
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		
            		$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		$('#modal_edit_code').val(obj.code);
            		$('#modal_edit_icon').val(obj.icon);
            		$('#modal_edit_url_link').val(obj.urlLink);
            		$('#modal_edit_program_path').val(obj.programPath);
            		$('#modal_edit_parent').val(obj.parent);
            		$('#modal_edit_sequence').val(obj.sequence);
            		$('#modal_edit_flagHasChild').val(obj.flagHasChild);
            		if(obj.flagHasChild){
            			$('#modal_edit_flag_has_child').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_has_child').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_name_lang1').val(obj.nameLang1);
            		$('#modal_edit_name_lang2').val(obj.nameLang2);
            		$('#modal_edit_name_lang3').val(obj.nameLang3);
            		$('#modal_edit_name_lang4').val(obj.nameLang4);
            		$('#modal_edit_name_lang5').val(obj.nameLang5);
            		$('#modal_edit_name_lang6').val(obj.nameLang6);
            		$('#modal_edit_name_lang7').val(obj.nameLang7);
            		$('#modal_edit_name_lang8').val(obj.nameLang8);
            		
            		
            		$('#editItemModal').modal('show');
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
	
}

function shiftUpMenu(){
var id =  $('#menuIdPopup').val();
	
	$.ajax({
        type: "GET",
        url: session['context']+'/menus/'+id,
        complete: function (xhr) {
        	$('#editMenuModal').modal('hide');
        	
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		
            		$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		$('#modal_edit_code').val(obj.code);
            		$('#modal_edit_icon').val(obj.icon);
            		$('#modal_edit_url_link').val(obj.urlLink);
            		$('#modal_edit_program_path').val(obj.programPath);
            		$('#modal_edit_parent').val(obj.parent);
            		$('#modal_edit_sequence').val(((obj.sequence*1)-1));
            		$('#modal_edit_flagHasChild').val(obj.flagHasChild);
            		if(obj.flagHasChild){
            			$('#modal_edit_flag_has_child').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_has_child').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_name_lang1').val(obj.nameLang1);
            		$('#modal_edit_name_lang2').val(obj.nameLang2);
            		$('#modal_edit_name_lang3').val(obj.nameLang3);
            		$('#modal_edit_name_lang4').val(obj.nameLang4);
            		$('#modal_edit_name_lang5').val(obj.nameLang5);
            		$('#modal_edit_name_lang6').val(obj.nameLang6);
            		$('#modal_edit_name_lang7').val(obj.nameLang7);
            		$('#modal_edit_name_lang8').val(obj.nameLang8);
            		
            		
            		save();
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function addSubMenu(){
	var id =  $('#menuIdPopup').val();
	$('#editMenuModal').modal('hide');
	$('#modal_edit_parent').val(id);
	$.ajax({
        type: "GET",
        url: session['context']+'/menus/getMaxSequence/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	$('#modal_edit_sequence').val(xhr.responseText);
                    $('#editItemModal').modal('show');
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
	
	
	
    
    
    
}

function shiftDownMenu(){
var id =  $('#menuIdPopup').val();
	
	$.ajax({
        type: "GET",
        url: session['context']+'/menus/'+id,
        complete: function (xhr) {
        	$('#editMenuModal').modal('hide');
        	
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		
            		$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		$('#modal_edit_code').val(obj.code);
            		$('#modal_edit_icon').val(obj.icon);
            		$('#modal_edit_url_link').val(obj.urlLink);
            		$('#modal_edit_program_path').val(obj.programPath);
            		$('#modal_edit_parent').val(obj.parent);
            		$('#modal_edit_sequence').val(((obj.sequence*1)+1));
            		$('#modal_edit_flagHasChild').val(obj.flagHasChild);
            		if(obj.flagHasChild){
            			$('#modal_edit_flag_has_child').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_has_child').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_name_lang1').val(obj.nameLang1);
            		$('#modal_edit_name_lang2').val(obj.nameLang2);
            		$('#modal_edit_name_lang3').val(obj.nameLang3);
            		$('#modal_edit_name_lang4').val(obj.nameLang4);
            		$('#modal_edit_name_lang5').val(obj.nameLang5);
            		$('#modal_edit_name_lang6').val(obj.nameLang6);
            		$('#modal_edit_name_lang7').val(obj.nameLang7);
            		$('#modal_edit_name_lang8').val(obj.nameLang8);
            		
            		
            		save();
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
}


function savePrivilege(){
	
	var jsonMenuRole = {};
	var jsonMenuRoleBefore = {};
	
	for(var i=0;i<$menusAll.length;i++){
		
		var rolesString = "";
		$.each( $( ".checkbox_roles_"+$menusAll[i].id+":checked" ), function( i, obj ) {
			rolesString += ","+obj.value;
		});
		
		if(rolesString){
			rolesString = rolesString.substring(1);
		}
		jsonMenuRole[$menusAll[i].id] = rolesString;
		
		var rolesBeforeString = "";
		$.each( $( ".checkbox_roles_"+$menusAll[i].id ), function( i, obj ) {
			rolesBeforeString += ","+obj.value;
		});
		if(rolesBeforeString){
			rolesBeforeString = rolesBeforeString.substring(1);
		}

		jsonMenuRoleBefore[$menusAll[i].id] = rolesBeforeString;
	}
	
	
	var  jsonRoleParams = {};
		 jsonRoleParams['roles']  	= JSON.stringify(jsonMenuRole);
		 jsonRoleParams['rolesBefore']  	= JSON.stringify(jsonMenuRoleBefore);
		
		 $.ajax({
		        type: "POST",
		        headers: {
		            Accept: 'application/json'
		        },
		        url: session['context']+'/menus/0/roles',
		        data: jsonRoleParams,
		        complete: function (xhr) {
		        	$.ajax({
	     		        type: "GET",
	     		        url: session['context']+'/menus/reloadMenu',
	     		        complete: function (xhr3) {
	        	        	$('#successModal').modal('show');
	     		        }
	     		    }).done(function (){
	     		        //close loader
	     		    });
		        }
		    }).done(function (){
		        //close loader
		    });
	
	
	
}

function deleteMenu(){
	$('#editMenuModal').modal('hide');
	$('#deleteItemModal').modal('show');
	
}

function checkDupplicate(code,id){
	$.ajax({
        type: "GET",
        url: session['context']+'/menus/findByCode?code='+code,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                        if(id != userObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE);
                			$('#warningModal').modal('show');
                        }else{
                        	save();
                        }
                	}else{
                		save();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function save(){
	var jsonParams = {};
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();
	jsonParams['flagActive']    = $('#modal_edit_flag_active').prop('checked');


	jsonParams['code'] 			= $('#modal_edit_code').val();
	jsonParams['icon'] 			= $('#modal_edit_icon').val();
	jsonParams['urlLink'] 		= $('#modal_edit_url_link').val();
	jsonParams['programPath'] 	= $('#modal_edit_program_path').val();
	jsonParams['parent'] 		= $('#modal_edit_parent').val();
	jsonParams['sequence'] 		= $('#modal_edit_sequence').val();
	jsonParams['flagHasChild']  = $('#modal_edit_flag_has_child').prop('checked');
	jsonParams['nameLang1'] 	= $('#modal_edit_name_lang1').val();
	jsonParams['nameLang2'] 	= $('#modal_edit_name_lang2').val();
	jsonParams['nameLang3'] 	= $('#modal_edit_name_lang3').val();
	jsonParams['nameLang4'] 	= $('#modal_edit_name_lang4').val();
	jsonParams['nameLang5'] 	= $('#modal_edit_name_lang5').val();
	jsonParams['nameLang6'] 	= $('#modal_edit_name_lang6').val();
	jsonParams['nameLang7'] 	= $('#modal_edit_name_lang7').val();
	jsonParams['nameLang8'] 	= $('#modal_edit_name_lang8').val();
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/menus',
        data: jsonParams,
        complete: function (xhr) {
        	 var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
        		 
        		 if($('#modal_edit_id').val()){
        			 $.ajax({
          		        type: "GET",
          		        url: session['context']+'/menus/reloadMenu',
          		        complete: function (xhr2) {
          		        	$('#successModal').modal('show');
          		        }
          		    }).done(function (){
          		        //close loader
          		    });
        		 }else{
        			 $.ajax({
           		        type: "GET",
           		        url: session['context']+'/menus/addPrivilegeAndReloadMenu/'+userObj.id,
           		        complete: function (xhr2) {
           		        	$('#successModal').modal('show');
           		        }
           		    }).done(function (){
           		        //close loader
           		    });
        		 }
        		 
        		 
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });
	
}



function deleteFunction(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "POST",
        url: session['context']+'/menus/'+id+"/roles",
        data: jsonParams,
        complete: function (xhr) {
        	var jsonParams2 = {};
    		jsonParams2['parentId']     = 1;
    		jsonParams2[csrfParameter]  = csrfToken;
    		
    		$.ajax({
    	        type: "DELETE",
    	        url: session['context']+'/menus/'+id,
    	        data: jsonParams2,
    	        complete: function (xhr2) {
    	        	 $.ajax({
    	     		        type: "GET",
    	     		        url: session['context']+'/menus/reloadMenu',
    	     		        complete: function (xhr3) {
    	     		        	$('#deleteItemModal').modal('hide');
    	        	        	$('#successModal').modal('show');
    	     		        }
    	     		    }).done(function (){
    	     		        //close loader
    	     		    });
    	        	
    	        }
    	    }).done(function (){
    	        //close loader
    	    });
        }
    }).done(function (){
        //close loader
    });
	
	
}

var $rolesAll;
var $menusAll;

function loadAllRole(){
	var roleData = $.ajax({
        type: "GET",
        url: session['context']+'/roles/',
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var roles = JSON.parse(xhr.responseText).content;
                    $rolesAll  = JSON.parse(xhr.responseText).content;;
                    var rolesAll = "";
                    for(var i=0;i<roles.length;i++){
                    	rolesAll += ","+roles[i].id;
                    	if(roles.length < 3){
                    		$('#gridHeader').append('<th class="text-center" style="width: 100%;">'+roles[i].roleName+'</th>');
                        }else{
                    		$('#gridHeader').append('<th class="text-center" style="width: 200px;">'+roles[i].roleName+'</th>');
                        }
                    	
                    	var recordsData = '<option  value="'+roles[i].id+'"   '+((i==0)?"selected":"")+'  >'+roles[i].roleName+'</option>';
            	        $('#roleSelected').append(recordsData);
                    }
                    
                    
                    
                    
                	
                    search();
                }
            }
        }
    }).done(function (){
        //close loader
    	
    });
}



function search(){
	
	var criteriaObject = {
			 
	    };
	queryMenuByCriteria(criteriaObject);
}

function queryMenuByCriteria(criteriaObject){
	object.setLimitData(30);
    object.setId("#paggingSearchMain");                
    object.setUrlData("/menus/findByCriteria"); 
    object.setUrlSize("/menus/findSize"); 
    
    object.loadTable = function(items){
     
    	
    	var item = items;  
    	
    	
    	sort(item)
    
    	for(var i=0; i<item.length; i++){
    		var name = item[i].name
    		var space1 = (space(item[i].layer))
    		item[i].name = space1+name
    	}
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            $menusAll = item;
            var itemIdTmp ;
            for(var j=0; j<item.length; j++){
                
                var name    = item[j].name==null?"":item[j].name	;
                var code    = item[j].code==null?"":item[j].code;
                var layer   = item[j].layer==null?"":item[j].layer;
                
                itemIdTmp = item[j].id;
                if(itemIdTmp){ 
                	var recordsData = ''+
                	'<tr id="'+item[j].id+'" '+
                	((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>';
                	
                	
                	/*Gen Column*/
                	for(var i=0;i<$rolesAll.length;i++){
                		if($rolesAll[i].id == $('#roleSelected').val()){
                			recordsData += '<td class="text-center" style="padding-top: 0px;padding-bottom: 0px;border-top-width: 0px;height: 43px;" ><div class="checkbox" style="margin-top: 5px;margin-bottom: 5px;" ><label><input type="checkbox" class="checkbox_roles_'+item[j].id+'" id="checkbox_roles_'+item[j].id+'_'+$rolesAll[i].id+'"  value="'+$rolesAll[i].id+'" /><span class="checkbox-material" style="left: 30px;margin-right: 30px;"><span class="check" style="right: 15px;top: 4px;" ></span></span></label></div></td>';
                        }else{
                        	recordsData += '<td class="text-center hide" style="padding-top: 0px;padding-bottom: 0px;border-top-width: 0px;height: 43px;" ><div class="checkbox" style="margin-top: 5px;margin-bottom: 5px;" ><label><input type="checkbox" class="checkbox_roles_'+item[j].id+'" id="checkbox_roles_'+item[j].id+'_'+$rolesAll[i].id+'"  value="'+$rolesAll[i].id+'" /><span class="checkbox-material" style="left: 30px;margin-right: 30px;"><span class="check" style="right: 15px;top: 4px;" ></span></span></label></div></td>';
                        }
                	}
                	
                	recordsData += '<td class="text-left">'+name+'('+code+')</td>'+
                	((j%2 == 0) ? '<td class="text-center"  style="padding-left: 0px;padding-right: 0px;" > ' : '<td class="text-center" style="padding-left: 0px;padding-right: 0px;" >')+
    				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="loadEditMenu(\''+name+'\','+item[j].id+');deleteMenu();"><span class="fa fa-trash"><jsp:text/></span></button>'+
                    '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="loadEditMenu(\''+name+'\','+item[j].id+');editMenu();" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                    '	<button type="button" class="btn btn-material-green-500  btn-style-small"  title="Add Sub Menu" onclick="loadEditMenu(\''+name+'\','+item[j].id+');addSubMenu();" ><span class="fa fa-sitemap"><jsp:text/></span></button>'+
                    '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="loadEditMenu(\''+name+'\','+item[j].id+');shiftUpMenu();" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                    '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick="loadEditMenu(\''+name+'\','+item[j].id+');shiftDownMenu();"  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+
    				'</td>';
                	
                	
                    recordsData += '</tr>';
                    $('#gridMainBody').append(recordsData);
                    
                    /* load privilege */
                    for(var i=0;i<item[j].privilege.length;i++){
                    	$('#checkbox_roles_'+item[j].id+'_'+item[j].privilege[i].id).prop('checked', true);
                    }
                    
                }
                
            }
            
            
        }else{
            //Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object); 
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}




$(document).ready(function () {
	$('#successModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#successModal').modal('hide');
			//location.reload();
		},1000);
	});
	

	$('#nav_save').on('click', function () {
		savePrivilege();
	});
	
	$('#roleSelected').on('change', function() {
		search();
	});
	
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		search();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_roleName').val('');
		$('#modal_edit_flag_active').removeAttr( "checked" );
		$('#modal_edit_flag_has_child').removeAttr( "checked" );
		$('#modal_edit_code').val('');
		$('#modal_edit_icon').val('');
		$('#modal_edit_url_link').val('');
		$('#modal_edit_program_path').val('');
		$('#modal_edit_parent').val('');
		$('#modal_edit_sequence').val('');
		$('#modal_edit_name_lang1').val('');
		$('#modal_edit_name_lang2').val('');
		$('#modal_edit_name_lang3').val('');
		$('#modal_edit_name_lang4').val('');
		$('#modal_edit_name_lang5').val('');
		$('#modal_edit_name_lang6').val('');
		$('#modal_edit_name_lang7').val('');
		$('#modal_edit_name_lang8').val('');
    	
	});
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_code').val()){
			errorMessage += $MESSAGE_REQUIRE+" "+$LABEL_CODE+"<br/>";
		}
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkDupplicate($('#modal_edit_code').val(),$('#modal_edit_id').val());
		}
		
	});
	loadAllRole();
	
	
	
});

function sort(a)
{
    var swapped;
    do {
        swapped = false;
        for (var i=0; i < a.length-1; i++) {
            if (a[i].value > a[i+1].value) {
                var temp = a[i];
                a[i] = a[i+1];
                a[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}

function space(str){
	var len= str*5-5
	console.log('len'+len)
	var ret = ''
		for(var t=0; t<len; t++){
			ret = ret+' &nbsp;';
		}
	return ret;
}


