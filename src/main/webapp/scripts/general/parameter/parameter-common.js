/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvlNum(e){
	if(e == null || e == undefined || isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvl(e){
	if(e == null){
		return "";
	}else{
		return e;
	}
}

function numberWithCommas(x) {
    var data =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if(data == null || data == undefined || isNaN(data) || !isFinite(data)  || data == "" ){
		return "0";
	}else{
		return data;
	}
}


function save(){
	var jsonParams = {};
	if($DATA_PARAMETER.screenType == "1"){
		jsonParams['parentId']     	= 1;
		jsonParams[csrfParameter]  	= csrfToken;
		jsonParams['id']     		= $('#edit_id').val();
		jsonParams['createdBy'] 	= $('#edit_createdBy').val();
		jsonParams['createdDate'] 	= $('#edit_createdDate').val();
		jsonParams['updateBy'] 		= $('#edit_updateBy').val();
		jsonParams['updateDate'] 	= $('#edit_updateDate').val();

		
		
		//Dynamic Field
		for(var j=0;j<$DATA_PARAMETER.numberOfColumn;j++){
			var index = (j+1)+"";
			if($DATA_PARAMETER['variableType'+(j+1)] == "BOOLEAN" ){
				jsonParams['variable'+index]   = $('#edit_variable'+index).prop('checked');
			}else{
				jsonParams['variable'+index]   = $('#edit_variable'+index).val();
			}
		}
		
		
		
		var itemData = $.ajax({
	        type: "POST",
	        headers: {
	            Accept: 'application/json'
	        },
	        url: session['context']+'/parameters',
	        data: jsonParams,
	        complete: function (xhr) {
	        	var userObj;
	        	 if(userObj = JSON.parse(xhr.responseText)){
	        		 if(userObj.id){ 
	        			 var jsonParentParams = {};
	        			 jsonParentParams['parameter']  	= $DATA_PARAMETER.id;
	            		 $.ajax({
	            		        type: "POST",
	            		        headers: {
	            		            Accept: 'application/json'
	            		        },
	            		        url: session['context']+'/parameters/'+userObj.id+'/parameter',
	            		        data: jsonParentParams,
	            		        complete: function (xhr) {
	            		        	$('#successModal').modal('show');
	            		        }
	            		    }).done(function (){
	            		        //close loader
	            		    });
	        		 }
	        		 
	        	 }
	        	 
	        }
	    }).done(function (){
	        //close loader
	    });
	}else{
		jsonParams['parentId']     	= 1;
		jsonParams[csrfParameter]  	= csrfToken;
		jsonParams['id']     		= $('#modal_edit_id').val();
		jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
		jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
		jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
		jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();

		
		
		//Dynamic Field
		for(var j=0;j<$DATA_PARAMETER.numberOfColumn;j++){
			var index = (j+1)+"";
			if($DATA_PARAMETER['variableType'+(j+1)] == "BOOLEAN" ){
				jsonParams['variable'+index]   = $('#modal_edit_variable'+index).prop('checked');
			}else{
				jsonParams['variable'+index]   = $('#modal_edit_variable'+index).val();
			}
		}
		
		
		
		var itemData = $.ajax({
	        type: "POST",
	        headers: {
	            Accept: 'application/json'
	        },
	        url: session['context']+'/parameters',
	        data: jsonParams,
	        complete: function (xhr) {
	        	var userObj;
	        	 if(userObj = JSON.parse(xhr.responseText)){
	        		 if(userObj.id){ 
	        			 var jsonParentParams = {};
	        			 jsonParentParams['parameter']  	= $DATA_PARAMETER.id;
	            		 $.ajax({
	            		        type: "POST",
	            		        headers: {
	            		            Accept: 'application/json'
	            		        },
	            		        url: session['context']+'/parameters/'+userObj.id+'/parameter',
	            		        data: jsonParentParams,
	            		        complete: function (xhr) {
	            		        	$('#editItemModal').modal('hide');
	            		        	$('#successModal').modal('show');
	            		        	edit($DATA_PARAMETER.id);
	            		        }
	            		    }).done(function (){
	            		        //close loader
	            		    	
	            		    });
	        		 }
	        		 
	        	 }
	        	 
	        }
	    }).done(function (){
	        //close loader
	    });
	}
	
	
	
}

$DATA_PARAMETER = {};

function edit(id){
	
	$.ajax({
        type: "GET",
        url: session['context']+'/parameterConfig/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                	
                	$('#gridMainBody').empty();
                	
                	var code = obj.code==null?"":obj.code;
                	var description = obj.description==null?"":obj.description;
                	
                	$('#edit_id').val(obj.id);
                	$('#edit_createdBy').val(obj.createdBy);
                	$('#edit_createdDate').val(obj.createdDate);
                	$('#edit_updateBy').val(obj.updateBy);
                	$('#edit_updateDate').val(obj.updateDate);
                	$('#edit_name').val(obj.name);

                	$('#edit_nameMessageCode').val(obj.nameMessageCode);
                	$('#edit_code').html(code);
                	$('#edit_description').html(description);
                	
                	$DATA_PARAMETER = obj;
                	
                	if(obj.screenType == "1"){
                		//Single Type
                		$('#single-screen').removeClass('hide');
                		$('#gridMainHead').addClass('hide');
                		
                		$('#add_button').addClass('hide');
                		
                		//Setup field
                		$('#gridHeader').empty();
                		$('#contentEdit').empty();
                		$('#single-screen').empty();
                		
                		$('#single-screen').append(''+
                		'<div class="row"  >'+
		                '		<div class="col-sm-12" style="top: 13px;">'+
						'		<button id="add_button" onClick="valisdateSave()" type="button" class="btn btn-lg" style="float: right;box-shadow: 5px 5px 5px #060606;border-radius: 60px;width: 60px;height: 60px;padding-left: 18px;padding-right: 20px;margin-top: -20;margin-top: 0px;margin-bottom: 0px;margin-right: 0px;margin-left: 0px;position: relative;left: 0px;background-color: #4caf50;top: -10;bottom: 0px;top: 0px;font-size: 20px;" ><span class="fa fa-floppy-o fa-2x" style="font-size: 30px;color: rgba(14, 12, 12, 0.84);"></span></button>'+
						'	</div>'+
		                '</div>');
                 
                		
					
					
                		for(var j=0;j<obj.numberOfColumn;j++){
                			
                			if(obj['variableType'+(j+1)] == "BOOLEAN" ){
                				$('#single-screen').append(''+
                		                '<div class="row"  >'+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-2" style="top: 13px;padding-right: 0px;text-align: right;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3 togglebutton" style="padding-right: 0px;top: 10px;left: 40px;">'+
                		                ' 		<label><input type="checkbox" id="edit_variable'+(j+1)+'"    /><span class="toggle" /></label> '+
                		                ' 	</div>'+
                		                ' 	<div class="col-sm-7" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                		                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                		                ' 	</div>'+
                		                '</div>'+
                		                '</div>'
                		        );
                			}else if(obj['variableType'+(j+1)] == "INTEGER"){
                				$('#single-screen').append(''+
                		                '<div class="row"  >'+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-2" style="top: 13px;padding-right: 0px;text-align: right;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3" style="padding-right: 3px;">'+
                		                ' 		<input type="text" class="form-control numbersOnly" name="'+obj['variableName'+(j+1)]+'" id="edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0"/>'+
                		                ' 	</div>'+
                		                ' 	<div class="col-sm-7" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                		                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                		                ' 	</div>'+
                		                '</div>'+
                		                '</div>'
                		        );
                			}else if(obj['variableType'+(j+1)] == "FLOAT"){
                				$('#single-screen').append(''+
                		                '<div class="row"  >'+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-2" style="top: 13px;padding-right: 0px;text-align: right;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3" style="padding-right: 3px;">'+
                		                ' 		<input type="text" class="form-control amount2digit"  name="'+obj['variableName'+(j+1)]+'"  id="edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0.00"/> '+
                		                ' 	</div>'+
                		                ' 	<div class="col-sm-7" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                		                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                		                ' 	</div>'+
                		                '</div>'+
                		                '</div>'
                		        );
                			} else if(obj['variableType'+(j+1)] == "DATE"){
                				$('#single-screen').append(''+
                		                '<div class="row"  >'+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-2" style="top: 13px;padding-right: 0px;text-align: right;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3 input-group" style="padding-top: 10px;left: 10px;" >'+
                		                ' 		<input autocomplete="off"     type="text" class="form-control "  name="'+obj['variableName'+(j+1)]+'" id="edit_variable'+(j+1)+'" data-dtp="dtp_CPXwH" style="color: #ffffff;font-size: 16px;" /> '+
                		                ' 		<span id="edit_btnDate_variable'+(j+1)+'" type="button" class="input-group-addon btn-material-blue-500 btn-raised"><span class="fa fa-calendar"></span></span> '+
                		                ' 	</div>'+
                		                '</div>'+
                		                '</div>'
                		        );
                			}else{
                				$('#single-screen').append(''+
                		                '<div class="row"  >'+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-2" style="top: 13px;padding-right: 0px;text-align: right;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3" style="padding-right: 3px;">'+
                		                ' 		<input type="text" class="form-control "  name="'+obj['variableName'+(j+1)]+'"  id="edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;"  />'+
                		                ' 	</div>'+
                		                ' 	<div class="col-sm-7" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                		                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                		                ' 	</div>'+
                		                '</div>'+
                		                '</div>'
                		        );
                				
                			}
                			
                		}
                		
            			//Active Date Picker 
            			for(var j=0;j<obj.numberOfColumn;j++){
            				if(obj['variableType'+(j+1)] == "DATE"){
            					var index = (j+1)+"";
            					$('#edit_variable'+index).bootstrapMaterialDatePicker({ weekStart : 0,time: false,format : 'DD/MM/YYYY',clearButton:true });
            					$('#edit_btnDate_variable'+index).on('click',function(){
            						$(this).parent().children("input:first").focus();
            					});
            				}
            			}
            			
            			//Set Require Field
            			for(var j=0;j<obj.numberOfColumn;j++){
            				var index = (j+1)+"";
            				if(obj['variableFlagRequire'+index] == true){
            					if(obj['variableType'+(j+1)] == "BOOLEAN" ){
            						//CASE : Require Boolean
            					}else if(obj['variableType'+(j+1)] == "INTEGER"){
            						//CASE : Require Integer
            					}else if(obj['variableType'+(j+1)] == "FLOAT"){
            						//CASE : Require Float
            					}else if(obj['variableType'+(j+1)] == "DATE"){
            						//CASE : Require Date
            						$('#edit_variable'+index).addClass('requiredField');
            					}else{
            						//CASE : Require String
            						$('#edit_variable'+index).addClass('requiredField');
            					}
            				}
            			}
            			
            			//Set Data Call Ajax
                		$.ajax({
            		        type: "GET",
            		        headers: {
            		            Accept: 'application/json'
            		        },
            		        url: session['context']+'/parameters/'+obj.id+'/details',
            		        complete: function (xhr) {
            		        	var objDetail = JSON.parse(xhr.responseText);
            		        	$('#edit_id').val(objDetail.content[0]['id']);
            		        	for(var j=0;j<obj.numberOfColumn;j++){
            		        		var index = (j+1)+"";
            		        		$('#edit_variable'+index).val(objDetail.content[0]['variable'+index]);
            		        	}
            		        	
            		        }
            		    }).done(function (){
            		        //close loader
            		    });
            			//Case Array 0
            			
                	}else{
                		//Multiple Type
                		$('#single-screen').addClass('hide');
                		$('#gridMainHead').removeClass('hide');
                		
                		$('#add_button').removeClass('hide');
                		
                		//Setup Header
                		$('#gridHeader').empty();
                		$('#contentEdit').empty();
                		$('#single-screen').empty();
                		
                		
                		$('#gridHeader').append(''+
            	                '<th class="text-center colHeadFix" style="width: 138px;">'+$LABEL_ACTION+'</th>'
            	        );
                		
                		for(var j=0;j<obj.numberOfColumn;j++){
                			if(obj.numberOfColumn < 5){
                				$('#gridHeader').append(''+
                    	                '<th class="text-center" style="width: 50%;">'+obj['variableName'+(j+1)]+'</th>'
                    	        );
                				
                				
                			}else{
                				$('#gridHeader').append(''+
                    	                '<th class="text-center" style="width: 170px;">'+obj['variableName'+(j+1)]+'</th>'
                    	        );
                			}
                			
                			//Generate Modal Editor
                			if(obj['variableType'+(j+1)] == "BOOLEAN" ){
                				$('#contentEdit').append(''+
                		                '<div class="form-group">'+
                		                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
                		                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+
                		                '	</div> '+
                		                ' 	<div class="col-sm-3 togglebutton" style="padding-right: 0px;top: 10px;left: 40px;">'+
                		                ' 		<label><input type="checkbox" id="modal_edit_variable'+(j+1)+'"    /><span class="toggle" /></label> '+
                		                ' 	</div>'+
                		                ' 	<div class="col-sm-6" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                		                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                		                ' 	</div>'+
                		                '</div>'
                		        );
        					}else if(obj['variableType'+(j+1)] == "INTEGER"){
        						$('#contentEdit').append(''+
        				                '<div class="form-group">'+
        				                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
        				                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
        				                '	</div> '+
        				                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
        				                ' 		<input type="text" class="form-control numbersOnly" name="'+obj['variableName'+(j+1)]+'" id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0"/>'+
        				                ' 	</div>'+
        				                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
        				                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
        				                ' 	</div>'+
        				                '</div>'
        				        );
        					}else if(obj['variableType'+(j+1)] == "FLOAT"){
        						$('#contentEdit').append(''+
        				                '<div class="form-group">'+
        				                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
        				                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
        				                '	</div> '+
        				                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
        				                ' 		<input type="text" class="form-control amount2digit"  name="'+obj['variableName'+(j+1)]+'"  id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0.00"/> '+
        				                ' 	</div>'+
        				                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
        				                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
        				                ' 	</div>'+
        				                '</div>'
        				        );
        					}else if(obj['variableType'+(j+1)] == "DATE"){
        						$('#contentEdit').append(''+
        				                '<div class="form-group">'+
        				                '	<div class="control-label col-sm-3" style="top: 13px;padding-right: 0px;"> '+
        				                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
        				                '	</div> '+
        				                ' 	<div class="col-sm-8 input-group" style="padding-top: 10px;left: 10px;" >'+
        				                ' 		<input autocomplete="off"     type="text" class="form-control"  name="'+obj['variableName'+(j+1)]+'" id="modal_edit_variable'+(j+1)+'" data-dtp="dtp_CPXwH" style="color: #ffffff;font-size: 16px;" /> '+
        				                ' 		<span id="modal_edit_btnDate_variable'+(j+1)+'" type="button" class="input-group-addon btn-material-blue-500 btn-raised"><span class="fa fa-calendar"></span></span> '+
        				                ' 	</div>'+
        				                '</div>'
        				        );
        					}else{
        						$('#contentEdit').append(''+
        				                '<div class="form-group">'+
        				                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
        				                '		<label style="color: #009688;" >'+obj['variableName'+(j+1)]+' </label> '+((obj['variableFlagRequire'+(j+1)] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
        				                '	</div> '+
        				                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
        				                ' 		<input type="text" class="form-control "  name="'+obj['variableName'+(j+1)]+'"  id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;"  />'+
        				                ' 	</div>'+
        				                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
        				                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
        				                ' 	</div>'+
        				                '</div>'
        				        );
        					}
                			
                		}
                		
                		//Active Date Picker 
            			for(var j=0;j<obj.numberOfColumn;j++){
            				if(obj['variableType'+(j+1)] == "DATE"){
            					var index = (j+1)+"";
            					$('#modal_edit_variable'+index).bootstrapMaterialDatePicker({ weekStart : 0,time: false,format : 'DD/MM/YYYY',clearButton:true });
            					$('#modal_edit_btnDate_variable'+index).on('click',function(){
            						$(this).parent().children("input:first").focus();
            					});
            				}
            			}
            			
            			//Set Default Value
            			for(var j=0;j<obj.numberOfColumn;j++){
            				if(obj['variableDefaultValue'+(j+1)] != null){
            					
            					if(obj['variableType'+(j+1)] == "BOOLEAN" ){
            						if(obj['variableDefaultValue'+(j+1)] == "true"){
            							$('#modal_edit_variable'+(j+1)).prop('checked', true);
            						}
            					}else if(obj['variableType'+(j+1)] == "INTEGER"){
            						$('#modal_edit_variable'+(j+1)).val(obj['variableDefaultValue'+(j+1)]);
            					}else if(obj['variableType'+(j+1)] == "FLOAT"){
            						$('#modal_edit_variable'+(j+1)).val(parseFloat(obj['variableDefaultValue'+(j+1)]).toFixed(2)    );
            					}else{
            						$('#modal_edit_variable'+(j+1)).val(obj['variableDefaultValue'+(j+1)]);
            					}
            				}
            			}
            			
            			//Set Require Field
            			for(var j=0;j<obj.numberOfColumn;j++){
            				var index = (j+1)+"";
            				if(obj['variableFlagRequire'+index] == true){
            					if(obj['variableType'+(j+1)] == "BOOLEAN" ){
            						//CASE : Require Boolean
            					}else if(obj['variableType'+(j+1)] == "INTEGER"){
            						//CASE : Require Integer
            					}else if(obj['variableType'+(j+1)] == "FLOAT"){
            						//CASE : Require Float
            					}else if(obj['variableType'+(j+1)] == "DATE"){
            						//CASE : Require Date
            						$('#modal_edit_variable'+index).addClass('requiredField');
            					}else{
            						//CASE : Require String
            						$('#modal_edit_variable'+index).addClass('requiredField');
            					}
            				}
            			}
                		
                		//Set Data( Call Ajax)
                		$.ajax({
            		        type: "GET",
            		        headers: {
            		            Accept: 'application/json'
            		        },
            		        url: session['context']+'/parameters/'+obj.id+'/details',
            		        complete: function (xhr) {
            		        	var objDetail = JSON.parse(xhr.responseText);
            		        	
            		        	var itemIdTmp ;
            		        	for(var i=0;i<objDetail.content.length;i++){
            		        		var item = objDetail.content[i];
            		        		itemIdTmp = item.id;
            		        		if(itemIdTmp){
            		        			var recordsData = ''+
                                        '<tr id="'+item.id+'" '+
                                        ((i%2 == 0) ? ' class="trwhite" ><td class="text-center headcol tdFix"  style="width: 138px;" > ' : ' class="trlightgray" ><td class="text-center headcol tdFixGrey" style="width: 138px;" >')+
                                        '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editModal('+item.id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item.id+'\');$(\'#codeItemDelete\').html(\''+item.variable1+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
                        				'</td>';
            		        			for(var j=0;j<obj.numberOfColumn;j++){
                    		        		if(obj['variableType'+(j+1)] == "BOOLEAN" ){
                                    			if(item['variable'+(j+1)] == "true" ){
                                    				recordsData += '<td class="text-center"><img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" /></td>';
                                    			}else{
                                    				recordsData += '<td class="text-center"><img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" /></td>';
                                    			}  
                                    		}else if(obj['variableType'+(j+1)] == "INTEGER" ){
                                    			recordsData += '<td class="text-center">'+ numberWithCommas(nvlNum(item['variable'+(j+1)])) +'</td>';
                                    		}else if(obj['variableType'+(j+1)] == "FLOAT" ){
                                    			recordsData += '<td class="text-center">'+ numberWithCommas(parseFloat(nvlNum(item['variable'+(j+1)])).toFixed(2))  +'</td>';
                                    		}else{
                                    			recordsData += '<td class="text-center">'+nvl(item['variable'+(j+1)])+'</td>';
                                    		}
                    		        	}
            		        			recordsData += '</tr>';
            		                	
            		                	$('#gridMainBody').append(recordsData);
            		        		}
            		        		
            		        		
            		        	}
            		        }
            		    }).done(function (){
            		        //close loader
            		    });
                		//Grid Case
                		
                	}

                	
                	
                	$('.numbersOnly').on("keydown",function(e){
            	        NumberUtil.numberOnly(e);
            	    });
            		
            		$('.amount2digit').on("keydown",function(e){
            	        NumberUtil.numberOnly(e);
            	        NumberUtil.numberFixDigit($(this),2);
            	    });
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function deleteFunction(id){
	//delete
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/parameters/'+id,
        data: jsonParams,
        complete: function (xhr) {
        	$('#deleteItemModal').modal('hide');
        	edit($DATA_PARAMETER.id);
        	$('#successModal').modal('show');
        	
        }
    }).done(function (){
        //close loader
    });
}

function editModal(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/parameters/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		
            		/*$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_code').val(obj.code);
            		$('#modal_edit_name').val(obj.name);
            		$('#modal_edit_orgCode').val(obj.orgCode);
            		$('#modal_edit_description').val(obj.description);*/
            		
            		
            		//Dynamic Field
            		for(var j=0;j<$DATA_PARAMETER.numberOfColumn;j++){
            			var index = (j+1)+"";
            			if($DATA_PARAMETER['variableType'+(j+1)] == "BOOLEAN" ){
            				if(obj['variable'+index] == "true"){
                    			$('#modal_edit_variable'+index).prop('checked', true);
                    		}else{
                    			$('#modal_edit_variable'+index).removeAttr( "checked" );
                    		}
            			}else{
            				$('#modal_edit_variable'+index).val(obj['variable'+index]);
            			}
            		}
            		
            		
            		
            		
            		
            		$('#editItemModal').modal('show');
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}


function loadDataPage(){
	$.ajax({
        type: "GET",
        url: session['context']+'/parameterConfig',
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		
                		if(userObj.page.totalElements > 0 && userObj.content.length){
                			
                			//load select type
                			for(var j=0;j<userObj.content.length;j++){
                    			
                    			var recordsData = '<option  value="'+userObj.content[j].id+'"   '+((j==0)?"selected":"")+'  >'+userObj.content[j].name+'</option>';
                    	        $('#parameterSelected').append(recordsData);
                    		}
                			
                			edit(userObj.content[0].id);
                		}
                		
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function valisdateSave(){
	var errorMessage = "";
	
	//fetch dynamic require field
	$.each( $( ".requiredField" ), function( i, obj ) {
		if(!obj.value){
			errorMessage += $MESSAGE_REQUIRE +" " +obj.name+"<br/>";
		}
	});
	
	if(errorMessage){
		$('#warningModal .modal-body').html(errorMessage);
		$('#warningModal').modal('show');
	}else{
		save();
	}
}




$(document).ready(function () {
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		
		
		//Set Default Value
		for(var j=0;j<$DATA_PARAMETER.numberOfColumn;j++){
			if($DATA_PARAMETER['variableDefaultValue'+(j+1)] != null){
				
				if($DATA_PARAMETER['variableType'+(j+1)] == "BOOLEAN" ){
					if($DATA_PARAMETER['variableDefaultValue'+(j+1)] == "true"){
						$('#modal_edit_variable'+(j+1)).prop('checked', true);
					}
				}else if($DATA_PARAMETER['variableType'+(j+1)] == "INTEGER"){
					$('#modal_edit_variable'+(j+1)).val($DATA_PARAMETER['variableDefaultValue'+(j+1)]);
				}else if($DATA_PARAMETER['variableType'+(j+1)] == "FLOAT"){
					$('#modal_edit_variable'+(j+1)).val(parseFloat($DATA_PARAMETER['variableDefaultValue'+(j+1)]).toFixed(2)    );
				}else{
					$('#modal_edit_variable'+(j+1)).val($DATA_PARAMETER['variableDefaultValue'+(j+1)]);
				}
			}else{
				$('#modal_edit_variable'+(j+1)).val('');
			}
		}
	});
	
	$('#parameterSelected').on('change', function() {
		edit( this.value );
	});
	
	
	
	$('#successModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#successModal').modal('hide');
		},1000);
	});
	

	 loadDataPage();
	 
		$('.numbersOnly').on("keydown",function(e){
	        NumberUtil.numberOnly(e);
	    });
		
		$('.amount2digit').on("keydown",function(e){
	        NumberUtil.numberOnly(e);
	        NumberUtil.numberFixDigit($(this),2);
	    });
		
		$('#editItemModalSave').on('click', function () {
			//validate Mandatory
			var errorMessage = "";
			
			
			//fetch dynamic require field
			$.each( $( ".requiredField" ), function( i, obj ) {
				if(!obj.value){
					errorMessage += $MESSAGE_REQUIRE +" " +obj.name+"<br/>";
				}
			});
			
			if(errorMessage){
				$('#warningModal .modal-body').html(errorMessage);
				$('#warningModal').modal('show');
			}else{
				//Check Mandatory Field
				save();
			}
			
		});
	 
});

