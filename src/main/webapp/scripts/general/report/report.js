/**
 * 
 */
function genReportFunction(){
	//validate Mandatory
	var errorMessage = "";
	//fetch dynamic require field
	$.each( $( ".requiredField" ), function( i, obj ) {
		if(!obj.value){
			errorMessage += $MESSAGE_REQUIRE +" " +obj.name+"<br/>";
		}
	});
	
	
	if(errorMessage){
		$('#warningModal .modal-body').html(errorMessage);
		$('#warningModal').modal('show');
	}else{
		var jsonParams = {};
		$.each( $( ".criteria_report" ), function( i, obj ) {
			if('checkbox' == obj.type ){
				jsonParams[obj.name]     = obj.checked;
			}else if('number' == obj.type ){
				if(obj.value){
					jsonParams[obj.name]     = obj.value;
				}else{
					jsonParams[obj.name]     = 0;
				}
			}else{
				if($(this).hasClass('date_field')){
					var dateStr = obj.value.split("/").reverse().join("-");
					jsonParams[obj.name]     = dateStr+' 00:00:00';
				}else{
					jsonParams[obj.name]     = obj.value;
				}
				
			}
		});
		
		var paramString = jQuery.param( jsonParams );
		window.location.href = session['context']+'/report/gen/'+$DATA_REPORT_CODE+'?'+paramString;
	}
	
	
	
	
}




$(document).ready(function () {
	
	for (var i = 0; i < $DATA_CRITERIA_OBJECT.length; i++) {
		
		if($DATA_CRITERIA_OBJECT[i]['variableType'] == "BOOLEAN" ){
			$('#criteriaField').append(''+
	                '<div class="form-group col-sm-'+$DATA_CRITERIA_OBJECT[i]['columnDisplay']+'  col-sm-offset-'+$DATA_CRITERIA_OBJECT[i]['offsetColumnDisplay']+'">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_CRITERIA_OBJECT[i]['variableName']+' </label> '+
	                '	</div> '+
	                ' 	<div class="col-sm-3 togglebutton" style="padding-right: 0px;top: 10px;left: 40px;">'+
	                ' 		<label><input type="checkbox"   class= "criteria_report" name="'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'"   /><span class="toggle" /></label> '+
	                ' 	</div>'+
	                ' 	<div class="col-sm-6" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else if($DATA_CRITERIA_OBJECT[i]['variableType'] == "INTEGER" ){
			$('#criteriaField').append(''+
	                '<div class="form-group col-sm-'+$DATA_CRITERIA_OBJECT[i]['columnDisplay']+'  col-sm-offset-'+$DATA_CRITERIA_OBJECT[i]['offsetColumnDisplay']+'">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_CRITERIA_OBJECT[i]['variableName']+' </label> '+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="number" class="form-control numbersOnly criteria_report" name="'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0"/>'+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else  if($DATA_CRITERIA_OBJECT[i]['variableType'] == "FLOAT" ){
			$('#criteriaField').append(''+
	                '<div class="form-group  col-sm-'+$DATA_CRITERIA_OBJECT[i]['columnDisplay']+'  col-sm-offset-'+$DATA_CRITERIA_OBJECT[i]['offsetColumnDisplay']+'">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_CRITERIA_OBJECT[i]['variableName']+' </label> '+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="number" class="form-control amount2digit criteria_report" name="'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'" style="color: #2196f3;font-size: 16px;text-align: right;" placeholder="0"/>'+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
		}else  if($DATA_CRITERIA_OBJECT[i]['variableType'] == "DATE"){
			$('#criteriaField').append(''+
	                '<div class="form-group  col-sm-'+$DATA_CRITERIA_OBJECT[i]['columnDisplay']+'  col-sm-offset-'+$DATA_CRITERIA_OBJECT[i]['offsetColumnDisplay']+'">'+
	                '	<div class="control-label col-sm-3" style="top: 13px;padding-right: 0px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_CRITERIA_OBJECT[i]['variableName']+' </label> '+(($DATA_CRITERIA_OBJECT[i]['variableFlagRequire'] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8 input-group" style="padding-top: 10px;left: 10px;" >'+
	                ' 		<input autocomplete="off"     type="text" class="form-control criteria_report date_field    '+(($DATA_CRITERIA_OBJECT[i]['variableFlagRequire'] == true) ?'requiredField' : '')+'" id="criteria_report_'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'"  name="'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'"  data-dtp="dtp_CPXwH" style="color: #2196f3;font-size: 16px;" /> '+
	                ' 		<span id="criteria_report_btnDate_'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'" type="button" class="input-group-addon btn-material-blue-500 btn-raised"><span class="fa fa-calendar"></span></span> '+
	                ' 	</div>'+
	                '</div>'
	        );
		}else {
			$('#criteriaField').append(''+
	                '<div class="form-group  col-sm-'+$DATA_CRITERIA_OBJECT[i]['columnDisplay']+'  col-sm-offset-'+$DATA_CRITERIA_OBJECT[i]['offsetColumnDisplay']+'" >'+
	                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
	                '		<label style="color: #009688;" >'+$DATA_CRITERIA_OBJECT[i]['variableName']+' </label> '+(($DATA_CRITERIA_OBJECT[i]['variableFlagRequire'] == true) ?'<label style="color: #ea4f83;font-size: 15px;margin-left: 3px;margin-top: 0px;padding-top: 0px;">*</label>' : '')+
	                '	</div> '+
	                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
	                ' 		<input type="text" class="form-control criteria_report  '+(($DATA_CRITERIA_OBJECT[i]['variableFlagRequire'] == true) ?'requiredField' : '')+'"  name="'+$DATA_CRITERIA_OBJECT[i]['variableParamName']+'"  style="color: #2196f3;font-size: 16px;"  />'+
	                ' 	</div>'+
	                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
	                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
	                ' 	</div>'+
	                '</div>'
	        );
			
		}

   	 
   	 
    }
	
	//Active Date Picker 
	$.each( $( ".date_field" ), function( i, obj ) {
		$('#criteria_report_'+obj.name).bootstrapMaterialDatePicker({ weekStart : 0,time: false,format : 'DD/MM/YYYY',clearButton:true });
		$('#criteria_report_btnDate_'+obj.name).on('click',function(){
			$(this).parent().children("input:first").focus();
		});
	});
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
});

