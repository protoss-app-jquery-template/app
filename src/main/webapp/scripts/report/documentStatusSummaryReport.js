$(document).ready(function () {


    $("#startDate").flatpickr({
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#imageCalendar1").on('click',function () {
        $("#startDate").focus()
    });

    $("#endDate").flatpickr({
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $("#imageCalendar2").on('click',function () {
        $("#endDate").focus()
    });

});

function downloadReportCUS001() {
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var newStartDate = '';
    var newEndDate = '';

    if(startDate){
        var startDateArray = startDate.split("/");
        newStartDate = startDateArray[2]+"-"+startDateArray[1]+"-"+startDateArray[0];
    }

    if(endDate){
        var endDateArray = endDate.split("/");
        newEndDate   = endDateArray[2]+"-"+endDateArray[1]+"-"+endDateArray[0];
    }

    window.location.href = session.context+"/documentStatusSummary/CUS001?startDate="+newStartDate+"&endDate="+newEndDate;



}
