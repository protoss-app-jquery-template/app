package com.spt.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.constant.ApplicationConstant;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorizeFlowServiceTest {
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	
	static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeFlowServiceTest.class);
	
	
	@Autowired
	FlowManageService flowManageService;
	@Autowired
	JBPMFlowService jbpmFlowService;
	
	
	@Test
	public void findEmployeeProfileByEmployeeCode_F001Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_F002Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F002;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));
	}


	@Test
	public void findEmployeeProfileByEmployeeCode_F002_1Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F002-1;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(3,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(2).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_F003Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F003;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_F004Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F004;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_F005Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F005;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H001Test() {
		String employeeStr = "karoons";
		String approveTypeStr = "0005";
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H001;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForApproveByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(2,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H003Test() {
		String employeeStr = "karoons";
		String approveTypeStr = "0002";
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H003;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForApproveByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(2,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H004Test() {
		String employeeStr = "karoons";
		String approveTypeStr = "0004";
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H004;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForApproveByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(2,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}



	@Test
	public void findEmployeeProfileByEmployeeCode_H006Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H006;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H007Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H007;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H008Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H008;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H009Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H009;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H010Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H010;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H011Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H011;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("HR",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H012Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H012;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_H013Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H013;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A001Test() {
		String employeeStr = "karoons";
		String approveTypeStr = "0003";
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A001;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForApproveByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(3,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Office Admin",lineApproveData.get(2).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A002Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A002;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(6,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("Office Admin",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("HR",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(4).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(5).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A003Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A003;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));
	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A004Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A004;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(5,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName"));  
		assertEquals("Office Admin",lineApproveData.get(2).get("actionRoleName"));
		assertEquals("Account",lineApproveData.get(3).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(4).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A005Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A005;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A006Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A006;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_A007Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:AdminAuth:0.99-AdminAuth.A007;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_M001Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:OtherAuth:0.99-OtherAuth.M001;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_P001Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:OtherAuth:0.99-OtherAuth.P001;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_PC01Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:OtherAuth:0.99-OtherAuth.PC01;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}


	@Test
	public void findEmployeeProfileByEmployeeCode_RM01Test() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:OtherAuth:0.99-OtherAuth.RM01;1.0";
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals(4,lineApproveData.size());
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); 
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); 
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); 
		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

	}
	
	
	


}
