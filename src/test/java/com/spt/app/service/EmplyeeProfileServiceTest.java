package com.spt.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EmplyeeProfileServiceTest {
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	
	static final Logger LOGGER = LoggerFactory.getLogger(EmplyeeProfileServiceTest.class);
	
	@Autowired
	EmplyeeProfileService emplyeeProfileService;
	
	@Test
	public void findEmployeeProfileByEmployeeCodeTest() {
		ResponseEntity<String> result = emplyeeProfileService.findEmployeeProfileByEmployeeCode("karoons");
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertEquals(userMap.get("NameEN"),"Mr. Karoon  Sillapapan");
	}
	

	@Test
	public void findEmployeeProfileByKeySearchUserInTest() {
		ResponseEntity<String> result = emplyeeProfileService.findEmployeeProfileByKeySearchUserIn("การุ","jaruneet,nattapornt");
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map<String,String>> restBodyList = (List<Map<String, String>>) userMap.get("restBodyList");
		assertEquals(restBodyList.size(),2);
		assertEquals(restBodyList.get(0).get("User_name"),"JARUNEET");
		assertEquals(restBodyList.get(1).get("User_name"),"NATTAPORNT");
	}

	@Test
	public void findEmployeeProfileByKeySearchTest() {
		ResponseEntity<String> result = emplyeeProfileService.findEmployeeProfileByKeySearch("การุ");
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map<String,String>> restBodyList = (List<Map<String, String>>) userMap.get("restBodyList");
		assertNotNull(restBodyList);
	}
	

	@Test
	public void findAutoCompleteEmployeeByKeySearchUserInTest() {
		String keySearch = "การุ";
		String userName = "karoons";
		ResponseEntity<String> result = emplyeeProfileService.findAutoCompleteEmployeeByKeySearchUserIn(keySearch, userName);
		List<Map<String,String>> userData = gson.fromJson(result.getBody(), List.class);
		
		 LOGGER.info("==============================json={}",userData);
		 assertNotNull(userData);
		 
		
		
	}
	


	@Test
	public void findAutoCompleteEmployeeByKeySearchUserIn_EmptyTest() {
		String keySearch = "";
		String userName = "karoons";
		ResponseEntity<String> result = emplyeeProfileService.findAutoCompleteEmployeeByKeySearchUserIn(keySearch, userName);
		List<Map<String,String>> userData = gson.fromJson(result.getBody(), List.class);
		
		 LOGGER.info("==============================json={}",userData);
		 assertNotNull(userData);
		 
		
		
	}
	

	@Test
	public void findAutoCompleteEmployeeByKeySearchTest() {
		String keySearch = "การุ";
		ResponseEntity<String> result = emplyeeProfileService.findAutoCompleteEmployeeByKeySearch(keySearch);
		List<Map<String,String>> userData = gson.fromJson(result.getBody(), List.class);
		
		 LOGGER.info("==============================json={}",userData);
		 assertNotNull(userData);
		 
		
		
	}
}

//Ex.
//"EmployeeId":5617230,
//	"Approver_Name":"นาง สริดาภ์ ช่ออัญชัญ"
//		"NameEN":"Mrs.Jarunee Thepwacharakarun"
//			"PositionTH":"เจ้าหน้าที่การเงิน",
//			"PositionEN":"Financial Officer",
//			
//			"Position_PA_ID":"1010",
//			"Position_PA_Name":"บริษัท น้ำตาลมิตรผล",
//			"Org_Name_TH_100":"บริษัท น้ำตาลมิตรผล จำกัด",
//			"Org_Name_EN_100":"Mitr Phol Sugar Corp., Ltd.",
//			
//			Org_ID_800
//			"Org_Name_TH_800":"แผนกบัญชีโรงงาน",
//			"Org_Name_EN_800":"Accounting Section (Sugar Manufacturing)",
//			
//			"Position_Cost_Center":"1013401000",
//			
